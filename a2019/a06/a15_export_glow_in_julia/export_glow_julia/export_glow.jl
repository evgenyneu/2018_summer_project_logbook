using FITSIO

# Creates a .fits image by keeping pixels between fibres and erasing the fibres.
# Parameters
# ----------
# arguments : list of str
#     A list containing command line arguments
function export_glow(arguments)
  fibre_thickness = 15  # Width of fibres in pixels

  # Check the command line arguments were provided
  if size(arguments)[1] != 3
      println("ERROR: three command line arguments required.")
      println("Example:")
      println("\$ julia export_glow.jl data.fits tram.fits output.fits")
      return
  end


  # Read the arguments
  filename_data = arguments[1]
  filename_tram = arguments[2]
  filename_output = arguments[3]

  # Check if the output file already exists
  if isfile(filename_output)
    println("ERROR: output file already exists: '$filename_output'.")
    return
  end

  # Open the tram FITS file
  # -----------------

  hdul_tram = FITS(filename_tram)


  # Open the data FITS file
  # -----------------

  hdul_data = FITS(filename_data)


  # Determine the size of the tram image.
  # ----------------

  header_tram = read_header(hdul_tram[1])
  naxes_tram = [header_tram["NAXIS1"], header_tram["NAXIS2"]]

  # Check that it found both NAXIS1 and NAXIS2 keywords.
  if naxes_tram[1]==nothing || naxes_tram[2]==nothing
    print("Failed to read the NAXIS keywords for tram.")
    return
  end

  total_fibres = naxes_tram[2]


  # Determine the size of the data image.
  # ----------------

  header_data = read_header(hdul_data[1])
  naxes_data = [header_data["NAXIS1"], header_data["NAXIS2"]]

  # Check that it found both NAXIS1 and NAXIS2 keywords.
  if header_data[1]==nothing || naxes_data[2]==nothing
    print("Failed to read the NAXIS keywords for data.")
    return
  end

  # Find the width of the image
  xmax = min(naxes_tram[1], naxes_data[1])

  # Load the image data
  image_data = read(hdul_data[1]);

  # Find the table containing information about fibres
  # ----------------

  # record = header_data["EXTNAME"]
  record = read_key(hdul_data[2], "EXTNAME")[1]

  if record != "STRUCT.MORE.FIBRES" && record != "FIBRES"
    println("Can not find the fibres table.")
  end

  # Find the non-parked fibres
  # ---------------------------
  max_x = 5000  # Used for defining array dimension
  fibre_names = read(hdul_data[2], "NAME")
  nonparked = zeros(Int, max_x) 

  # Collect the indexes of nonparked fibres
  n_nonparked = 1

  for (irow, name) in enumerate(fibre_names)
    if name != "PARKED"
        nonparked[n_nonparked] = irow
        n_nonparked += 1
    end
  end

  # Load the y coordinates of fibres
  # -------------

  fibre_y = read(hdul_tram[1])

  tram_y_start = nothing

  # Loop over the x values
  for x in 1:xmax
    # Loop over fibres
    for i_fibre in 1:n_nonparked-1
      fibre_index = nonparked[i_fibre]

      # Calculate the start and end y-coordinate of the fibre
      # --------------------

      tram_y = fibre_y[x, fibre_index] + 1

      if tram_y_start==nothing
        tram_y_start = floor(Int, tram_y - fibre_thickness / 2.0)
      end

      tram_y_end = ceil(Int, tram_y + fibre_thickness / 2.0)

      # Check if the current tram fibre overlaps with the next
      if i_fibre < n_nonparked-1
        next_fibre_index = nonparked[i_fibre + 1]
        next_tram_y_start = floor(Int, fibre_y[x, next_fibre_index] + 1 - fibre_thickness / 2.0)

        if (tram_y_end >= next_tram_y_start)
            # The fibre is very close to the next one.
            # Skip until the next fibre is further away, since we want to erase fibres
            # with brightness values from regions far away from fibres.
            continue
        end
      end

      # Erase the fibre pixels
      image_data[x, tram_y_start: tram_y_end - 2] .= image_data[x, tram_y_start - 1]

      # Use next fibre as starting point for the region we want to erase
      tram_y_start = nothing
    end
  end

  # Save the data array into the output fits file
  data_out = FITS(filename_output, "w");

  for hdu in hdul_data
    if typeof(hdu)==FITSIO.ImageHDU
      # Write the image to output file
      write(data_out, image_data; header=header_data)
    elseif typeof(hdu)==FITSIO.TableHDU
      # Copy the table into the output file
      table_data = Dict{String,Array}()

      for colname in FITSIO.colnames(hdu)
        col_data = read(hdu, colname)
        table_data[colname] = col_data
      end

      header = read_header(hdu)
      ext_name = header["EXTNAME"]
      write(data_out, table_data; hdutype=FITSIO.TableHDU, header=header, name=ext_name)
    end
  end

  # Close the fits files
  close(hdul_tram)
  close(hdul_data)
  close(data_out)

  println("Glow extracted successfully to $filename_output")
end

arguments = ARGS
# dir = "/Users/evgenii/Downloads"
# arguments = ["$dir/27oct10020.fits", "$dir/27oct10022tlm.fits", "$dir/out_julia.fits"]
export_glow(arguments)
