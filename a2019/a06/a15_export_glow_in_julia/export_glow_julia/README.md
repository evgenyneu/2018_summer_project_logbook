# Export glow utility

Julia's version of export glow, the simple method without interpolation.

## Usage


```
$ julia export_glow.jl data.fits tram.fits output.fits
```

## Installing prerequisites

From julia command prompt:

```
using Pkg
Pkg.update()
Pkg.add("FITSIO")
```
