# Writing export glow utility in Julia

Implementing glow removal (median interpolation method) in Julia

Code: [julia_medium_interpolate](julia_medium_interpolate)

Benchmark:

* Fortran: 2.3 s.

* Julia: 3.9 s.

* Python: 64 s.

