# Export glow utility

Julia's version of export glow, the median interpolation method

## Usage


```
$ julia export_glow_median.jl data.fits tram.fits output.fits
```

## Installing prerequisites

From julia command prompt:

```
using Pkg
Pkg.update()
Pkg.add("FITSIO")
```