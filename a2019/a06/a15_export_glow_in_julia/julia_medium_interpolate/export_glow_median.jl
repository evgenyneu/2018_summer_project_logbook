using FITSIO
using Statistics

#=
Linear intepolations

Parameters
----------
x : Array of Float
    The x-coordinates at which to evaluate the interpolated values.
x1: Float
x2: Float
    The x-coordinates of the data points.
y1: Float
y2: Float
    The y-coordinates of the data points
=#
function interpolate(;x, x1, x2, y1, y2)
  delta_x = x2 - x1

  if delta_x < 1e-20
    return (x .* 0) .+ y1
  end

  (x .- x1) * (y2 - y1) / delta_x .+ y1
end

#=
Calculates the brightness value that will be used to erase a fibre at the given x-coordinate.

Parameters
----------
x : int
    The x-coordinate in the image.
tram_y_start: int
    The y-coordinate of the start of the fibre.
tram_y_end: int
    The y-coordinate of the start of the fibre.
interpolate: Boolean
    If True, the fibres are filled with brightness values interpolated from pixels outside
    the fibres, which results in smoother glow image. If False, the each fibre is filled
    with single brightness value form a pixellocated just below the fibre.

Returns
-------
int or numpy.ndarray
    The brightness value to be used to erase the fibre. If interpolate is True, then we
    return an array of brightness values.
=#
function calculate_bightness(;data, x, tram_y_start, tram_y_end)

  ymax = size(data)[2]
  # println(ymax)

  # Calculate the median brightness of pixels in a square region
  box_size = 3

  if box_size > 1 && tram_y_start - box_size >= 1 && x - box_size >= 1
    pixels = data[x - box_size: x - 1, tram_y_start - box_size: tram_y_start - 1]
    brightness_start = median(pixels)
  else
    brightness_start = data[x, tram_y_start]
  end

  if box_size > 1 && tram_y_end + box_size <= ymax && x - box_size >= 1
    pixels = data[x - box_size: x - 1, tram_y_end: tram_y_end + box_size - 1]
    brightness_end =  median(pixels)
  else
    brightness_end = data[x, tram_y_end]
  end

  interpolted = interpolate(x=Array(tram_y_start : tram_y_end - 1),
    x1=tram_y_start, x2=tram_y_end, y1=brightness_start, y2=brightness_end)

  interpolted = round.(UInt16, interpolted) # Convert to integers

  return interpolted
end

# Creates a .fits image by keeping pixels between fibres and erasing the fibres.
# Parameters
# ----------
# arguments : list of str
#     A list containing command line arguments
function export_glow(arguments)
  fibre_thickness = 15  # Width of fibres in pixels

  # Check the command line arguments were provided
  if size(arguments)[1] != 3
      println("ERROR: three command line arguments required.")
      println("Example:")
      println("\$ julia export_glow.jl data.fits tram.fits output.fits")
      return
  end


  # Read the arguments
  filename_data = arguments[1]
  filename_tram = arguments[2]
  filename_output = arguments[3]

  # Check if the output file already exists
  if isfile(filename_output)
    rm(filename_output)
    # println("ERROR: output file already exists: '$filename_output'.")
    # return
  end

  # Open the tram FITS file
  # -----------------

  hdul_tram = FITS(filename_tram)


  # Open the data FITS file
  # -----------------

  hdul_data = FITS(filename_data)


  # Determine the size of the tram image.
  # ----------------

  header_tram = read_header(hdul_tram[1])
  naxes_tram = [header_tram["NAXIS1"], header_tram["NAXIS2"]]

  # Check that it found both NAXIS1 and NAXIS2 keywords.
  if naxes_tram[1]==nothing || naxes_tram[2]==nothing
    print("Failed to read the NAXIS keywords for tram.")
    return
  end

  total_fibres = naxes_tram[2]


  # Determine the size of the data image.
  # ----------------

  header_data = read_header(hdul_data[1])
  naxes_data = [header_data["NAXIS1"], header_data["NAXIS2"]]

  # Check that it found both NAXIS1 and NAXIS2 keywords.
  if header_data[1]==nothing || naxes_data[2]==nothing
    print("Failed to read the NAXIS keywords for data.")
    return
  end

  # Find the width of the image
  xmax = min(naxes_tram[1], naxes_data[1])

  # Load the image data
  image_data = read(hdul_data[1]);

  # Find the table containing information about fibres
  # ----------------

  # record = header_data["EXTNAME"]
  record = read_key(hdul_data[2], "EXTNAME")[1]

  if record != "STRUCT.MORE.FIBRES" && record != "FIBRES"
    println("Can not find the fibres table.")
  end

  # Find the non-parked fibres
  # ---------------------------
  max_x = 5000  # Used for defining array dimension
  fibre_names = read(hdul_data[2], "NAME")
  nonparked = zeros(Int, max_x) 

  # Collect the indexes of nonparked fibres
  n_nonparked = 1

  for (irow, name) in enumerate(fibre_names)
    if name != "PARKED"
        nonparked[n_nonparked] = irow
        n_nonparked += 1
    end
  end

  # Load the y coordinates of fibres
  # -------------

  fibre_y = read(hdul_tram[1])

  tram_y_start = nothing

  # Loop over the x values
  for x in 1:xmax
    # Loop over fibres
    for i_fibre in 1:n_nonparked-1
      fibre_index = nonparked[i_fibre]

      # Calculate the start and end y-coordinate of the fibre
      # --------------------

      tram_y = fibre_y[x, fibre_index] + 1

      if tram_y_start==nothing
        tram_y_start = floor(Int, tram_y - fibre_thickness / 2.0)
      end

      tram_y_end = ceil(Int, tram_y + fibre_thickness / 2.0)

      # Check if the current tram fibre overlaps with the next
      if i_fibre < n_nonparked-1
        next_fibre_index = nonparked[i_fibre + 1]
        next_tram_y_start = floor(Int, fibre_y[x, next_fibre_index] + 1 - fibre_thickness / 2.0)

        if (tram_y_end >= next_tram_y_start)
            # The fibre is very close to the next one.
            # Skip until the next fibre is further away, since we want to erase fibres
            # with brightness values from regions far away from fibres.
            continue
        end
      end

      # Erase the fibre pixels
      brightness = calculate_bightness(data=image_data, x=x, tram_y_start=tram_y_start,
                                             tram_y_end=tram_y_end)

      image_data[x, tram_y_start: tram_y_end - 1] .= brightness

      # Use next fibre as starting point for the region we want to erase
      tram_y_start = nothing
    end
  end

  # Save the data array into the output fits file
  data_out = FITS(filename_output, "w");

  for hdu in hdul_data
    if typeof(hdu)==FITSIO.ImageHDU
      # Write the image to output file
      write(data_out, image_data; header=header_data)
    elseif typeof(hdu)==FITSIO.TableHDU
      # Copy the table into the output file
      table_data = Dict{String,Array}()

      for colname in FITSIO.colnames(hdu)
        col_data = read(hdu, colname)
        table_data[colname] = col_data
      end

      header = read_header(hdu)
      ext_name = header["EXTNAME"]
      write(data_out, table_data; hdutype=FITSIO.TableHDU, header=header, name=ext_name)
    end
  end

  # Close the fits files
  close(hdul_tram)
  close(hdul_data)
  close(data_out)

  println("Glow extracted successfully to $filename_output")
end

arguments = ARGS
# dir = "/Users/evgenii/Downloads"
# arguments = ["$dir/27oct10020.fits", "$dir/27oct10022tlm.fits", "$dir/out_julia.fits"]
export_glow(arguments)

