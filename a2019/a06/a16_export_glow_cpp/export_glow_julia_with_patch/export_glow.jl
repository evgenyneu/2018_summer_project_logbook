using FITSIO
using Statistics

#=
Linear intepolations

Parameters
----------
x : Array of Float
    The x-coordinates at which to evaluate the interpolated values.
x1: Float
x2: Float
    The x-coordinates of the data points.
y1: Float
y2: Float
    The y-coordinates of the data points
=#
function interpolate(;x, x1, x2, y1, y2)
  delta_x = x2 - x1

  if delta_x < 1e-20
    return (x .* 0) .+ y1
  end

  (x .- x1) * (y2 - y1) / delta_x .+ y1
end

# Returns an array of the source pixels located below the position x and y
function source_box_above(;image_data, x, y, xmax)
    source_box_width = 11
    source_box_height = 3

    ymax = size(image_data, 2)

    source_box_x_start = x - floor(Int, source_box_width / 2)
    if source_box_x_start < 1; source_box_x_start = 1; end
    source_box_x_end = x + floor(Int, source_box_width / 2)
    if (source_box_x_end > xmax); source_box_x_end = xmax; end

    source_box_y_start = floor(Int, y + 1)

    if source_box_y_start < 1; source_box_y_start = 1; end

    if source_box_y_start > ymax
      box = [] # Empty array
    end

    source_box_y_end = source_box_y_start + source_box_height - 1
    if source_box_y_end > ymax; source_box_y_end = ymax; end
    box_size = (source_box_x_end - source_box_x_start + 1) * (source_box_y_end - source_box_y_start + 1)
    box = image_data[source_box_x_start:source_box_x_end, source_box_y_start:source_box_y_end]
    box = reshape(box, 1, length(box))
    return box
end

# Returns an array of the source pixels located below the position x and y
function source_box_below(;image_data, x, y, xmax)
    source_box_width = 11
    source_box_height = 3

    ymax = size(image_data, 2)

    source_box_x_start = x - floor(Int, source_box_width / 2)
    if source_box_x_start < 1; source_box_x_start = 1; end
    source_box_x_end = x + floor(Int, source_box_width / 2)
    if (source_box_x_end > xmax); source_box_x_end = xmax; end

    source_box_y_end = y - 1

    if source_box_y_end < 1
      box = [] # Empty array
    end

    if source_box_y_end > ymax; source_box_y_end = ymax; end

    source_box_y_start = source_box_y_end - source_box_height + 1
    if source_box_y_start < 1; source_box_y_start = 1; end

    box_size = (source_box_x_end - source_box_x_start + 1) * (source_box_y_end - source_box_y_start + 1)

    box = image_data[source_box_x_start:source_box_x_end, source_box_y_start:source_box_y_end]
    box = reshape(box, 1, length(box))
    return box
end


#=
Calculates the brightness value that will be used to erase a fibre at the given x-coordinate.

Parameters
----------
x : int
    The x-coordinate in the image.
tram_y_start: int
    The y-coordinate of the start of the fibre.
tram_y_end: int
    The y-coordinate of the start of the fibre.
interpolate: Boolean
    If True, the fibres are filled with brightness values interpolated from pixels outside
    the fibres, which results in smoother glow image. If False, the each fibre is filled
    with single brightness value form a pixellocated just below the fibre.

Returns
-------
int or numpy.ndarray
    The brightness value to be used to erase the fibre. If interpolate is True, then we
    return an array of brightness values.
=#
function calculate_bightness(;data, x, tram_y_start, tram_y_end)

  ymax = size(data)[2]
  # println(ymax)

  # Calculate the median brightness of pixels in a square region
  box_size = 3

  if box_size > 1 && tram_y_start - box_size >= 1 && x - box_size >= 1
    pixels = data[x - box_size: x - 1, tram_y_start - box_size: tram_y_start - 1]
    brightness_start = median(pixels)
  else
    brightness_start = data[x, tram_y_start]
  end

  if box_size > 1 && tram_y_end + box_size <= ymax && x - box_size >= 1
    pixels = data[x - box_size: x - 1, tram_y_end: tram_y_end + box_size - 1]
    brightness_end =  median(pixels)
  else
    brightness_end = data[x, tram_y_end]
  end

  interpolted = interpolate(x=Array(tram_y_start : tram_y_end - 1),
    x1=tram_y_start, x2=tram_y_end, y1=brightness_start, y2=brightness_end)

  interpolted = round.(UInt16, interpolted) # Convert to integers

  return interpolted
end

# Creates a .fits image by keeping pixels between fibres and erasing the fibres.
# Parameters
# ----------
# arguments : list of str
#     A list containing command line arguments
function export_glow(arguments)
  fibre_thickness = 15  # Width of fibres in pixels

  # Check the command line arguments were provided
  if size(arguments)[1] != 3
      println("ERROR: three command line arguments required.")
      println("Example:")
      println("\$ julia export_glow.jl data.fits tram.fits output.fits")
      return
  end


  # Read the arguments
  filename_data = arguments[1]
  filename_tram = arguments[2]
  filename_output = arguments[3]

  # Check if the output file already exists
  if isfile(filename_output)
    rm(filename_output)
    # println("ERROR: output file already exists: '$filename_output'.")
    # return
  end

  # Copy data file to destination
  cp(filename_data, filename_output)

  # Open the tram FITS file
  # -----------------

  hdul_tram = FITS(filename_tram)


  # Open the data FITS file
  # -----------------

  hdul_data = FITS(filename_data)


  # Determine the size of the tram image.
  # ----------------

  header_tram = read_header(hdul_tram[1])
  naxes_tram = [header_tram["NAXIS1"], header_tram["NAXIS2"]]

  # Check that it found both NAXIS1 and NAXIS2 keywords.
  if naxes_tram[1]==nothing || naxes_tram[2]==nothing
    print("Failed to read the NAXIS keywords for tram.")
    return
  end

  total_fibres = naxes_tram[2]


  # Determine the size of the data image.
  # ----------------

  header_data = read_header(hdul_data[1])
  naxes_data = [header_data["NAXIS1"], header_data["NAXIS2"]]

  # Check that it found both NAXIS1 and NAXIS2 keywords.
  if header_data[1]==nothing || naxes_data[2]==nothing
    print("Failed to read the NAXIS keywords for data.")
    return
  end

  # Find the width of the image
  xmax = min(naxes_tram[1], naxes_data[1])

  # Load the image data
  image_data = read(hdul_data[1]);

  # Find the table containing information about fibres
  # ----------------

  # record = header_data["EXTNAME"]
  record = read_key(hdul_data[2], "EXTNAME")[1]

  if record != "STRUCT.MORE.FIBRES" && record != "FIBRES"
    println("Can not find the fibres table.")
  end

  # Find the non-parked fibres
  # ---------------------------
  # max_x = 5000  # Used for defining array dimension
  fibre_names = read(hdul_data[2], "NAME")
  # nonparked = zeros(Int, max_x) 

  # # Collect the indexes of nonparked fibres
  # n_nonparked = 1

  # for (irow, name) in enumerate(fibre_names)
  #   if name != "PARKED"
  #       nonparked[n_nonparked] = irow
  #       n_nonparked += 1
  #   end
  # end

  # Load the y coordinates of fibres
  # -------------

  fibre_y = read(hdul_tram[1])

  # tram_y_start = nothing

  y_erase_start = -1
  previous_parked_y = -1
  parked_fibre_margin = 2
  tries = 1

  # Loop over the x values
  for x in 1:xmax
    # Loop over fibres
    for fibre_index in 1:total_fibres
      if y_erase_start == -1 # we have not found the start of the region to erase yet
        if fibre_names[fibre_index] == "PARKED"
          previous_parked_y = round(Int, fibre_y[x, fibre_index])
        else
          # Use the center of the parked fibed as the start of the region that will be erased
          y_erase_start = previous_parked_y + parked_fibre_margin

          if previous_parked_y == -1
            print("ERROR: the bottom fibre is not PARKED for fibre: " + fibre_index)
            return
          end
        end

        continue
      else
        if fibre_names[fibre_index] != "PARKED"
          # The next fibre is not parked
          # Skip until the next fibre is parked, since we want to erase fibres
          # with brightness values from parked fibres.
          continue
        end
      end

      # Store this parked fibre y to be used as the start of the erased region for the NEXT
      # non-parked fibre
      previous_parked_y = round(Int, fibre_y[x, fibre_index])

      # Use the center of the parked fibed as the end of the region that will be erased
      y_erase_end = round(Int, fibre_y[x, fibre_index]) - parked_fibre_margin


      # Get the median brightness values from the source boxes below and above the fibre
      # ----------------

      source_box_start = source_box_below(image_data=image_data, x=x, y=y_erase_start, xmax=xmax)

      start_median = median(source_box_start)


      source_box_end = source_box_above(image_data=image_data, x=x, y=y_erase_end, xmax=xmax)

      end_median = median(source_box_end)

      interpolation_size = y_erase_end - y_erase_start + 1

      interpolted = interpolate(x=Array(1 : interpolation_size),
        x1=1, x2=interpolation_size, y1=start_median, y2=end_median)

      interpolted = round.(UInt16, interpolted) # Convert to integers

      image_data[x, y_erase_start: y_erase_end] = interpolted[1: interpolation_size]

      # Use next fibre as starting point for the region we want to erase
      y_erase_start = -1
    end
  end

  # Save the data array into the output fits file
  data_out = FITS(filename_output, "r+")  # Open in read-write mode
  out_image_hdu = data_out[1]
  write(out_image_hdu, image_data)

  # Close the fits files
  close(hdul_tram)
  close(hdul_data)
  close(data_out)

  println("Glow extracted successfully to $filename_output")
end

arguments = ARGS
in_dir = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_normal/ccd1"
out_dir = "/Users/evgenii/Downloads"
arguments = ["$in_dir/27oct10020.fits", "$in_dir/27oct10022tlm.fits", "$out_dir/out_julia.fits"]
export_glow(arguments)
