## Testing EW measurement methods

We want to test two methods of measurement of equivalent widths and see if the produce same results.

* Asked a question if we can use t-test on [stackexchange](https://stats.stackexchange.com/questions/389354/can-i-use-a-paired-sample-t-test-to-compare-two-methods-of-measuring-absorption).

### Asked Andy, he suggested

* Plot a distribution of:


```math
z_{n} = abs(EW_{a,n} - EW_{b,n})/\sqrt{\sigma_{a,n}^2 + \sigma_{b,n}^2}
```

> ...a histogram of that quantity (for all `n` lines) will tell you how many “joint sigma” the measurements are different from each other. T hat distribution should peak at 0 and have some tails that follow a `\chi^2` distribution (i am pretty sure)

> so say if you had 50% of the measurements were more than 5\sigma apart from each other (in joint uncertainty) — that would be many more than you would expect from a \chi^2 distribution

* Another way of comparing two methods of measuring EWs:

> so normally when i am comparing EWs, the very first thing I do is plot them relative to each other: A (x-axis) to B (y-axis), and then A (x-axis) to A - B (y-axis)

> and then i calculate things like mean difference, standard deviation of difference, etc

> for a solar-like star, 0.5 mA mean difference in EWs corresponds to something like 40 K difference in


## Discussed corner glow issue with Andy

Andy suggested

* Plot the frames from Blue and Red **at the same scale** (0 to 300) to see if there is a corner glow in red CCD. Because on red CCD the fibres are much brighter, so the corner glow might not be visible at typical scale.

* Plot the flat image at the same scale (0 to 300).

## Plotting Blue and Red images in same scale

Set `export IMTDEV=inet:5137` variable to make IRAF communicate with DS9 ([source](http://spacepioneers.in/iraf-and-ds9-on-mac-os-x-el-capitan/)).

### With `display` in DS9

```
display ccd1/27oct10020.fits fill=yes z1=1 z2=424 zrange=no zscale=no
display ccd3/27oct30020.fits fill=yes z1=1 z2=424 zrange=no zscale=no
```

### Export to GIF file with `export`


![Raw frame from 27 Oct p1 observation for CCD1 and CCD3](27oct_p1_raw_cd1_ccd3_scale_1_424.jpg)

Figure 1: Raw frames from 27 Oct p1 observation for CCD1 and CCD3 with the same scale 1 to 424.

Code

```
export ccd1/27oct10020.fits 27oct10020 format="gif" outbands="zscale(i1,1,424)"
export ccd3/27oct30020.fits 27oct30020 format="gif" outbands="zscale(i1,1,424)"
```


### Plotting the flat image at the same scale (0 to 300)


![ Raw flat frames from 27 Oct p1 observation for CCD1 and CCD3](27oct_p1_raw_flats_cd1_ccd3_scale_1_424.jpg)

Figure 2: Raw flat frames from 27 Oct p1 observation for CCD1 and CCD3 with the same scale 1 to 424.


Code:

```
export ccd1/27oct10022.fits raw_flat_27oct10022 format="gif" outbands="zscale(i1,1,424)"
export ccd3/27oct30022.fits raw_flat_27oct30022 format="gif" outbands="zscale(i1,1,424)"

convert +append *.gif -resize 1200x out2.gif
```


## Plot equivalent widths relative to each other

![Equivalent widths relative to each other](ew_ew1_ew2.png)

Figure 3: Equivalent width measurements with and without sky subtraction.

Plotting code: [plot_difference.py](plot_difference.py).


## Plot equivalent width A (x-axis) to A - B (y-axis)


![Equivalent widths difference vs equivalent width](ew_difference_vs_ew.png)

Figure 4: Difference of equivalent width with and without sky subtraction (a-b) vs equivalent width with sky subtraction (a).

Plotting code: [plot_a_minus_b_vs_a.py](plot_a_minus_b_vs_a.py).


## Descriptive statistics

* Mean difference (All CCDs): 9±10 mA
* Mean difference (Blue): 18±11 mA
* Mean difference (Green): 3±2 mA
* Mean difference (Red): 14±9 mA

Code: [ew_descriptive_stats.py](ew_descriptive_stats.py)


## Z-scores of difference of equivalent widths

We plot the z scores of the difference of equivalent widths on Fig. 5.


```math
z_{n} = abs(EW_{a,n} - EW_{b,n})/\sqrt{\sigma_{a,n}^2 + \sigma_{b,n}^2},
```

where $`EW_{a,n}`$ and $`EW_{b,n}`$ are mean equivalent widths of the n-th absorption line for data with and without sky subtraction respectively, and $`\sigma_{a,n}`$, $`\sigma_{b,n}`$ are standard errors of those means.

![Z-scores of difference of equivalent widths](z_scores_equivalent_widths.png)

Figure 5: Difference of equivalent width for data with and without sky subtraction.

Code: [z_scores_difference.py](z_scores_difference.py).


