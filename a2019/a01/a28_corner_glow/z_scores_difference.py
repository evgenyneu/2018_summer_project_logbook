import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt


df = pd.read_csv('ew_sky_subtracted_comparison.csv')

ew_a = 'Equivalent width mean WITH sky subtraction, a [mA]'
ew_b = 'Equivalent width mean with NO sky subtraction, b [mA]'

sigma_a = 'u(a) [mA]'
sigma_b = 'u(b) [mA]'

df['z'] = np.abs(df[ew_a] - df[ew_b]) / np.sqrt(df[sigma_a]**2 + df[sigma_b]**2 )

df.hist(column='z', bins=int(len(df['z'])/2))
plt.title('Z-scores of difference of equivalent widths\nfor measurements with and without sky subtraction')
plt.show()