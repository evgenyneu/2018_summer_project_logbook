import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('ew_sky_subtracted_comparison.csv')
ax = None

xcolumn = 'Equivalent width mean WITH sky subtraction, a [mA]'
ycolumn = 'a-b [mA]'
xerr_column = 'u(a) [mA]'
yerr_column = 'u(b) [mA]'

CCDs = [1,2,3]
colors = ['Blue', 'Green', 'Red']

# Plot the scatter markers for each CCD
for index, ccd in enumerate(CCDs):
    df_ccd = df.loc[df.CCD == CCDs[index]]
    color = colors[index]
    ax = df_ccd.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, color=color)

    ax.errorbar(df_ccd[xcolumn], df_ccd[ycolumn], xerr=df_ccd[xerr_column], yerr=df_ccd[yerr_column],
                fmt='none', ecolor=color, capsize=3, elinewidth=0.5, capthick=0.5)

ax.set_ylabel("Difference of equivalent widths, a - b [mA]")

plt.show()