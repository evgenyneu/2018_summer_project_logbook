## Measuring lines

* Put cursor for continuum and press "k".

* Put at the center and press "c".

* Clear: r

Another method: k - k

## Ploting histogram

:hist yes




## Normalizing

Blue:

```
continuum 29Oct18/reduce/288/p0/stars/113/ccd1.fits 29Oct18/reduce/288/p0/stars/normalized/113/ccd1.fits order=3 mark=no grow=0 high_reject=0 low_reject=1.2 function=spline3 niterat=10
```


Green:

```
continuum 29Oct18/reduce/288/p0/stars/113/ccd2.fits 29Oct18/reduce/288/p0/stars/normalized/113/ccd2.fits order=6 mark=no grow=1 high_reject=0 low_reject=2 function=spline3 niterat=10
```

Red

```
continuum 29Oct18/reduce/288/p0/stars/113/ccd3.fits 29Oct18/reduce/288/p0/stars/normalized/113/ccd3.fits order=3 mark=no grow=0 high_reject=0 low_reject=1.5 function=spline3 niterat=10
```


## Determine radial velocities

```
fxcor 29Oct18/reduce/288/p0/stars/normalized/113/ccd1.fits sun.fits autowrite+ osample=4810-4845 rsample=4810-4845 rebin="largest" continuum="both" filter="no" output="fxcor113" interactive="yes"
```

-31.506(0.060)

```
fxcor 29Oct18/reduce/288/p0/stars/normalized/113/ccd3.fits sun.fits autowrite+ osample=6600-6635 rsample=6600-6635 rebin="largest" continuum="both" filter="no" output="fxcor113" interactive="yes"
```

-31.438(0.062)

Final velocity: -31.2(0.3) km/s


## Doppler correction

Do each channel separately.

```
dopcor 20aug2_combined.fits[0] 20aug2_combined-dopcor-plus100.8kms.fits 100.8 isvelocity=yes
```


Blue:

```
dopcor 29Oct18/reduce/288/p0/stars/normalized/113/ccd1.fits 29Oct18/reduce/288/p0/stars/doppler_corrected/113/ccd1.fits -31.2 isvelocity=yes
```

Green:

```
dopcor 29Oct18/reduce/288/p0/stars/normalized/113/ccd2.fits 29Oct18/reduce/288/p0/stars/doppler_corrected/113/ccd2.fits -31.2 isvelocity=yes
```


Red:

```
dopcor 29Oct18/reduce/288/p0/stars/normalized/113/ccd3.fits 29Oct18/reduce/288/p0/stars/doppler_corrected/113/ccd3.fits -31.2 isvelocity=yes
```

## Lines

* Check sodium lines: in air: 5682.63 5688.20

* H-Alpha: vacuume: 6564.61, in air: 6562.80

* H-Beta: vacuume: 4862.68, in air: 4861.363
