from logbook.code.python.observations.radial_velocities import star_velocities
import pandas as pd

def check():
    star = 'AGBSYAZ017762'

    star_velocities(ccd=3, wavelength_start=6580, range_width=35, max_fibres=1,
        include_stars=[star], interactive=True)

if __name__ == '__main__':
    check()
