# Looking at sky spectrum

We wanted to look at spectrum of sky fibres in order to see if there are some unnusual ones.

* cd to CCD3 directory of 27 Oct p1 observations.

* In 2DfDr: un-reduce previous reductions.

* Disable Sky > Subtract Sky

* Disable Sky > Throughput Callibrate

* Enable Subtract Bias frames

* Stars Auto Reduction.

## Typical sky

![a_typical_sky.png](a_typical_sky.png)

Figure 1: A typical sky spectrum, it is mostly flat with increase of about 100 counts near the right edge. The H-alpha line is visible.


## Typical star

![a_typical_rgb_star_RGBSYAZ015658.png](a_typical_rgb_star_RGBSYAZ015658.png)

Figure 2: A spectrum of RGBSYAZ015658 star, a typical spectrum of the star of this type. The counts increase from left to right, flatten at about 6650 Å and then rise near the right edge.


## Sky anomaly at high fiber number


![sky_high_left.png](sky_high_left.png)

Figure 3: A sky spectrum at high fiber number. Unlike typical sky, which was mostly flat on the left, this one has negative slope on the left, with 250 higher counts on the left relative to the middle portion.


## A star at high fiber number


![star_high_on_the_left_RHBSYAZ031888.png](star_high_on_the_left_RHBSYAZ031888.png)

Figure 4: A spectrum of RHBSYAZ031888 star at hight fiber number. The left part is higher than the middle by about 400 counts.

## Discussion

The left parts of the spectra is increasing significantly with increasing fiber number, it seems (Figures 3 and 4). It would be useful to find the source of this increase and check other channels and observations.
