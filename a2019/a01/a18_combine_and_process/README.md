## Reduce with 2DfDr without throughput calibration

The throughput callibration, which is only present in CCD3, seems to add unnecessary electron counts during sky subtraction. We want to run 2DfDr for CCD3 with Sky > Throughput Callibrate option off.

This is done for following directories:

* 27Oct18/reduce/288/p1/ccd3

* 29Oct18/reduce/288/p0/ccd3

* 29Oct18/reduce/288/p1_A/ccd3


### The method

* cd to directory.

* Open 2DfDr `drcontrol &`.

* File > Remove all reductions.

* Select the *combined.fits and click Unreduce.

* Select General > Subtract Bias Frame.

* Unselect Sky > Throughput Callibration.

* Select Combined Sky and Telluric correction in Plots.

* Click Start Auto Reduction.


## Copying .fits files for combination


Run `pyraf` from the parent directory of logbook.


```
from logbook.code.python.observations.combine_observations import *
copy_all_ccds(day=27, take="p1")
copy_all_ccds(day=29, take="p0")
copy_all_ccds(day=29, take="p1_A")
```


## Combining observations with 2DfDr

For each ccd in the combined directory `30Oct18/reduce/288/combined`:

* Run 2DfDr `drcontrol &`.

* Select Commands > Combine Reduced Runs.

* Select the .fits in the left pannel, click 'Add' and 'OK'.

* This will create the `combined_frames.fits` file.



### Copy the combined observations file

The following script will copy the `combined_frames.fits` files created by 2DfDr to files named according to the 2DfFr conventions (i.e. `30oct3_combined.fits`. This way we can reuse existing scripts we wrote for IRAF processing.

```
from logbook.code.python.observations.combine_observations import *
copy_to_canonical_path_all_ccds()
```

### Extract 'AGBSYAZ017762' star

```
from logbook.code.python.observations.extract_stars import *
from logbook.code.python.observations.paths import COMBINED_OBSERVATIONS_DAY, COMBINED_OBSERVATIONS_TAKE
extract_star_all_ccds(name='AGBSYAZ017762', day=COMBINED_OBSERVATIONS_DAY, take=COMBINED_OBSERVATIONS_TAKE)
```


Extraction fails with error:


> Warning: MWCS: too many coordinate transformation (ctran) descriptors
> Warning: No spectra selected
> Traceback (innermost last):
>   File "<CL script CL1>", line 1, in <module>
> stsci.tools.irafglobals.IrafError: Undefined variable `uparm' in string `uparm$/oncscopy.par'


```
scopy data/observe/30Oct18/reduce/288/combined_observations/ccd1/30oct1_combined.fits[0] data/observe/30Oct18/reduce/288/combined_observations/stars/unprocessed/AGBSYAZ017762/ccd1.fits aperture="116"
```



### Measuring lines

* [lines.csv](lines.csv)

* [fe.lines.txt](fe.lines.txt)

* [naomgal.lines.txt](naomgal.lines.txt)
