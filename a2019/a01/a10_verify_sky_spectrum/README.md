## Running the IRAF

* CD to ccd image

* `splot`

* `List of images to plot:  29oct1_combined.fits[0]`

* `Image line/aperture to plot (0:) (1): 198`

* View the image in irafterm

* iraf alias `xgterm -e cl`.

## Splot command

Lookup splot for more info.

* a-a: Zoom-in.

* C: reset zoom.

* Q: go back to terminal.

* m-m: signal to noise.

* k-k: line fitting.

* e-e: equivalent width.


## Mapping between star names an fibers

List of stars: [starlist.txt](starlist.txt)

* `2dfinfo 29oct1_combined.fits fibres > starlist.txt`.

Show RGB stars

`ccd1 $ grep " AGB" starlist.txt`



##  Check sky lines  (exercise)

* Rerun 2DfDr with Sky > Subtract sky off

* Compare with one with sky subtracted.



## Sky absorption (tulluric lines)

Need to be careful for wavelengths: 5800 - 5870 A. There is a dip in CCD2.



## Installing code

Since splot command did not work, I'm reinstalling IRAF.

View current environments:
```
conda info --envs
```

Remove environment:

```
conda remove --name iraf --all
```

Install environment:

```
conda create -n iraf python iraf-all pyraf-all stsci
```


## Run iraf

```
 xgterm -e cl
 ```


## Get single line out of fits

* Start iraf

* ccd1

* scopy 29oct1_combined.fits[0] star113_ccd1.fits aperture="113"


## Combine multiple CCDs for star 113

* `scombine star113_ccd1.fits,star113_ccd2.fits,star113_ccd3.fits combined_113.fits aperture="113"`

![star_113_combined.png](star_113_combined.png)


## What's next

* Radial velocity correction. (Look at continuum function at IRAF: fxcor).

* Plot velocities of all stars (histogram).

* Look at Simbad for stars by positions.

* Normalize the spectrum.






