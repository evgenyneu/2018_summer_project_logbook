## CCD corner glow problem

## 27 Oct p1 observation

On image from 27 Oct p1, the top-left corner of the CCD 1 gets gradually brighter with time and peaks at the last frame #20 (Fig. 1). 

* The images are made after bias frame subtraction, but before flat field and sky correction.

* This bright region is not visible on CCDs 2 and 3 (Figures 2 and 3). However, [we observed](a2019/a01/a18_combine_and_process/sky_spectrum_analysis.md) higher counts for high fiber numbers at lower wavelengths in CCD3. In CCD2 such anomaly was not observed. 

* This corner glow problem is most visible in CCD 1, less visible (but still present) in CCD 3 and not present in CCD 2.

* Fig. 1 also has more bright dots than Figures 2 and 3.


### CCD 1

![CCD corner glow problem](27OCt_p1_corner_glow_ccd1.gif)

Figure 1: Images from CCD 1 of frames 16 to 20 from Oct 27 p1 observation. The top-left corner becomes brighter with time.


### CCD 2

![CCD corner glow problem](27OCt_p1_corner_glow_ccd2.png)

Figure 2: Image from CCD 2 of frame 20 from Oct 27 p1 observation, after bias frame subtraction, but before flat field and sky correction.


### CCD 3

![CCD corner glow problem](27OCt_p1_corner_glow_ccd3.png)

Figure 3: Image from CCD 3 of frame 20 from Oct 27 p1 observation, after bias frame subtraction, but before flat field and sky correction.


## 29 Oct p0 observation

* The corner glow features are present **in all frames** 24-28 for both CCD 1 and CCD 3, but not in CCD 2.

* This is different from 27 Oct p1 observation, which did not have visible corner glow in CCD 3.

* Another difference is that the first frames of CCD 1 from 27 Oct p1 did not have the corner glow, while in 29 Oct p0 observation all frames have this feature.

* Moreover, the corner glow in 27 Oct p1 observation was increasing with time rapidly, while in 29 Oct p0 observation the glow appears to change intermittently with time.


### CCD 1

![CCD corner glow problem](29Oct_p0_corner_glow_ccd1.gif)

Figure 4: Images from CCD 1 of frames 24 to 28 from Oct 29 p0 observation. The top-left corner is brighter than the rest of the image and the brightness varies by between frames.


### CCD 2

![CCD corner glow problem](29Oct_p0_corner_glow_ccd2.png)

Figure 5: Image from CCD 2 of frame 28 from Oct 29 p0 observation. The image appears to be uniformly lit and not have the corner glow feature.


### CCD 3

![CCD corner glow problem](29Oct_p0_corner_glow_ccd3.png)

Figure 6: Image from CCD 3 of frame 28 from Oct 29 p0 observation. The top-left corner is brighter than the rest of the image.


## 29 Oct p1_A observation


### CCD 1

![CCD corner glow problem](29Oct_p1_A_corner_glow_ccd1_animated.gif)

Figure 7: Images from CCD 1 of frames 19-23 from Oct 29 p1_A observation. The top-left corner is brighter than the rest of the image and the amount of glow changes from frame to frame.


### CCD 2

![CCD corner glow problem](29Oct_p1_A_corner_glow_ccd2.png)

Figure 8: Image from CCD 2 of frame 23 from Oct 29 p1_A observation. The image appears to be uniformly lit (unlike CCD 1).


### CCD 3

![CCD corner glow problem](29Oct_p1_A_corner_glow_ccd3_animated.gif)

Figure 9: Images from CCD 3 of frames 19-23 from Oct 29 p1_A observation. The top-left corner is brighter than the rest of the image and the amount of glow changes from image to image.





## Useful ImageMagic commands


### Annotating an image


```
convert frame19.gif -pointsize 72 -gravity south -stroke '#000C' -strokewidth 2 -annotate +0+50 'My label' -stroke none -fill white -annotate +0+50 'My label' frame19_annotated.gif
```


### Creating an animated gif from multiple images

```
convert -delay 100 -loop 0 *.gif animated.gif
```

