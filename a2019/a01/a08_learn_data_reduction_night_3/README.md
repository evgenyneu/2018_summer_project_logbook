
# Learning how to reduce data

Today we learned how to use 2DfDr program to reduce the data from the telescope. We used data from the 3rd night (Oct 29), plate #0, the run numbers 24-28, shown in Fig 3.

## Observing log for Oct 27 2018

![Observing Log Night 3/3](observing_log_1_3.jpg)

Figure 1: observing log for Night 1.

## Observing log for Oct 28 2018

![Observing Log Night 3/3](observing_log_2_3.jpg)

Figure 2: observing log for Night 2.

## Observing log for Oct 29 2018

![Observing Log Night 3/3](observing_log_3_3.jpg)

Figure 3: observing log for Night 3.


## Reducing biases

* `cd reduce/biases_29Oct18/ccd1`

* Run 2DfDr: `drcontrol &`.

* File > remove all reductions

* Unreduce the combined file.

* Start autoreductions > got BIAScombined.fits file.

* Repeat for ccd2 and ccd3.



## Reduce the science frames

* `cd reduce/288/p0/ccd1`.

* Run 2DfDr: `drcontrol &`.

* File > remove all reductions. Unreduce the combined file.

* `cp ../../../biases_29Oct18/ccd1/BIAScombined.fits ./`.


### Change settings

* General tab > Subtract bias frame.

* Sky > Subtract Sky.

* Plots > Generate plots for:

  * Combined sky
  * Telluric correction

### Reduce

* Start Auto-reduction.

* View spectrum of single frame: Expand .fits, click on red.fits, click 2D Plot.

* View combined spectrum: in combined.gits, click 2D Plot.