from logbook.code.python.observations.paths import tramline_single_path
from astropy.io import fits


def find_tramlines_pixel(ccd, day, take):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    numpy.ndarray
        A 2D array containing y-pixel numbers for at (fibre, x-pixel) coordinates on the CCD.
    """

    tramline_path = tramline_single_path(ccd=ccd, day=day, take=take)

    with fits.open(tramline_path) as hdul:
        return hdul['PRIMARY'].data


def find_tramlines_wavelenth(ccd, day, take):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    numpy.ndarray
        A 2D array containing wavelength (in nm) at (fibre, x-pixel) coordinates on the CCD.
    """

    tramline_path = tramline_single_path(ccd=ccd, day=day, take=take)

    with fits.open(tramline_path) as hdul:
        return hdul['WAVELA'].data
