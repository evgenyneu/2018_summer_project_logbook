from .tramline import find_tramlines_pixel, find_tramlines_wavelenth
from logbook.code.python.observations.paths import ccd_day_take_dir
from logbook.code.python.shared import files, fixture_paths, cleanup
from pytest import approx


def test_find_tramlines_pixels():
    tramline_dir = ccd_day_take_dir(ccd=1, day=27, take="p1")
    output_path = f"{tramline_dir}/27oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, output_path)

    result = find_tramlines_pixel(ccd=1, day=27, take="p1")

    assert result[300, 2000] == approx(3120.8308)

    cleanup.remove_test_fits()


def test_find_tramlines_wavelenth():
    tramline_dir = ccd_day_take_dir(ccd=1, day=27, take="p1")
    output_path = f"{tramline_dir}/27oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, output_path)

    result = find_tramlines_wavelenth(ccd=1, day=27, take="p1")

    assert result[300, 2000] == approx(480.99493)

    cleanup.remove_test_fits()
