## Extract glow

Here we want to extract the glow from a CCD out pixel between fibres. The regions of CCD with light from fibres are erased by interpolating the pixel outside the fibres.

Script: [export_glow.py](code/python/observations/export_glow.py)

### Usage

```
python export_glow.py -data_path='27oct10020.fits' -tram_path='27oct10022tlm.fits' -output_path='27oct10020_glow.fits'
```

The script needs the following Python libraries to be installed:
```
pip install numpy
pip install astropy
pip install tqdm
```

### Comparing raw image with extracted glow

![The raw image and exctracted glow](raw_glow.gif)

Figure 1: The raw image and extracted glow for CCD1, 27 Oct p1 observation, frame 20.


### Extracting glow with interpolation

One can use `-interpolate=yes` option that will make a smother glow image, as shown on Figure 2.

![Normal and interpolated glow image](glow_interpolated.gif)

Figure 2: The normal and interpolated glow image.


