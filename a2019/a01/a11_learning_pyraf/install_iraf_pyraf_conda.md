# Installing Conda, Iraf and Pyraf on Linux

* Download a relevant Miniconda shell script from https://conda.io/miniconda.html. For example, [Miniconda3-latest-Linux-x86_64.sh](https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh).

* Run the script:

```
bash Miniconda3-latest-Linux-x86_64.sh
```

* Add astroconda channel:

```
conda config --add channels http://ssb.stsci.edu/astroconda
```

* Create conda environment called `iraf` and install python3, iraf, pyraf and other astronomical tools


```
conda create -n iraf python iraf-all pyraf-all stsci
```

## Installing libraries on Ubuntu


```
sudo apt-get install libc6:i386 libz1:i386 libncurses5:i386 libbz2-1.0:i386 libuuid1:i386 libxcb1:i386 libxmu-dev:i386
```


## Using IRAF

* Activate environment

```
source activate iraf
```

* Open pyraf terminal with `pyraf` to run any IRAF command.

* Run IRAF: 

  * Run `mkiraf` in your project directory (the first time only).

  * `xgterm -e cl`.

* At the end of the day, deactivate: `source deactivate`.



## Running a Python script from Pyraf

* Create a `hello_world.py` file containing:

```Python
print("Hello World!")
```

* Open pyraf terminal: `pyraf`

* Run the Python script from terminal:

```Python
pyexecute("hello_world.py")
```

## Running IRAF commands in a Python script

Running the `scopy` IRAF command from Python:

```Python
from pyraf import iraf
iraf.scopy(input = "input.fits", output = "output.fits", aperture = "113")
```

* Other commands are run the same way: `iraf.COMMAND(arguments...)`.

* Use `help COMMAND` in pyraf terminal to get the names of the arguments.

* The first line of the help page also shows the submodule that you need to import. For example, the first line of `help lineclean` is

> LINECLEAN (May85)             images.imfit             LINECLEAN (May85)

Thus in order to use `lineclean` we need to add an additional `imfit` import:

```
from pyraf import iraf
from iraf import imfit

imfit.lineclean(input=input_path, output=output_path,
                sample="*", interactive="no",
                low_reject=5.0, high_reject=5.0, function="spline1",
                order=100, grow=1, naverage=-1)
```