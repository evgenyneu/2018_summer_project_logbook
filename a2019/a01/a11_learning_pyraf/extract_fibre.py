from pyraf import iraf
import os
import shutil

def extract_ccd_fibre(plate_path, fibre_number, ccd_number, output_dir):
    """
    Extracts a single fibre from a fits file for a single CCD.
    :return: path to the output fits file.
    """

    star_single_fibre_path = f"{output_dir}/{fibre_number}"
    if not os.path.exists(star_single_fibre_path): os.makedirs(star_single_fibre_path)
    ccd_dir = f"ccd{ccd_number}"
    fibre_file = f"{star_single_fibre_path}/{ccd_dir}.fits"

    # Extract a single fibre
    iraf.scopy(input = f"{plate_path}/{ccd_dir}/29oct{ccd_number}_combined.fits[0]",
        output = fibre_file,
        aperture = f"{fibre_number}")

    return fibre_file

def extract_fibre_from_all_ccds(plate_path, fibre_number, output_dir, number_of_ccds = 3):
    """
    Extracts a single fibre from a fits file for all CCDs.
    :return: paths to the output files.
    """

    ccd_files = []

    for ccd_number in range(1, number_of_ccds + 1):
        outputPath = extract_ccd_fibre(plate_path = plate_path, fibre_number = fibre_number,
            ccd_number = ccd_number, output_dir = output_dir)

        ccd_files.append(outputPath)

    return ccd_files

def combine_ccds(plate_path, fibre_number, ccd_files, output_dir):
    """
    Combines the fits files from all CCDs
    """
    ccs_files_joined = ",".join(ccd_files)
    output_file = f"{output_dir}/combined_{fibre_number}.fits"

    # Remove combined file if exists (otherwise it will add data to existing file)
    try:
        os.remove(output_file)
    except OSError:
        pass

    iraf.scombine(input = ccs_files_joined,
        output = output_file,
        aperture = f"{fibre_number}")

def remove_ccd_files(plate_path, fibre_number, output_dir):
    """
    Removes the individual CCD files created with `extract_ccd_fibre`.
    """
    star_single_fibre_path = f"{output_dir}/{fibre_number}"
    shutil.rmtree(star_single_fibre_path)

def extract_fibre_from_ccds(plate_path, output_dir, fibre_number):
    """
    Extract spectra for a single fibre from all ccds and combine them in a single fits file.
    """
    ccd_files = extract_fibre_from_all_ccds(plate_path = plate_path, fibre_number = fibre_number,
        output_dir = output_dir)

    combine_ccds(plate_path = plate_path, fibre_number = fibre_number, ccd_files = ccd_files,
        output_dir = output_dir)

    # remove_ccd_files(plate_path = plate_path, fibre_number = fibre_number, output_dir = output_dir)


plate_path = "29Oct18/reduce/288/p0";
extract_fibre_from_ccds(plate_path = plate_path, output_dir = f"{plate_path}/stars", fibre_number = 4)