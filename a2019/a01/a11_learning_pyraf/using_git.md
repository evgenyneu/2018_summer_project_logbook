# Using Git (source code management)

* Check if you have git by typing `git`. If not, install with an ubuntu package named `git`.

* Cd to a root directory of your project.

* Initialize git repository: `git init`

* Create a text file and save it:

  * Add the new file to repository: `git add .`

* Change a file.

* View list of changed files: `git status`

* View the changes: `git diff`

* Commit (means 'save') changes to repository: `git commit -am 'Describe your changes here'`



## Uploading repository to Github (or Gitlab, Bitbucket etc.)

* Create a Github account.

* Add a new SSH key on Github (Settings/[SSH and GPG keys](https://github.com/settings/keys)). Check the help links there for instructions.

* Create new public repository on github.

* Follow instructions under "or push an existing repository from the command line":

  * Associate your new Github repository with your local repository (needs to be done only once):

    `git remote add origin git@github.com:YOUNAME/REPOSITORY_NAME.git`

  * Upload your local repository to Github:

    `git push -u origin master`



## Markdown

Instead of txt files, you can use Markdown format. It will show formatted text with images and links on Github.

* Create a file with .md extension. The file named `README.md` will be displayed when selecting a directory on Github by default.

Example of a markdown text:


````
# Main title

## Subtitle

### Sub-subtitle

Some text. Here is some `inline code`.

[Link](http://microsoft.com)

Image:

![Image](Maine-Coon_02.jpg)

List:

* Item 1.

* Item 2.


Some code Python block:

```Python
def my_function()
  print("hello there")
```
````

This is how it looks:


# Main title

## Subtitle

### Sub-subtitle

Some text. Here is some `inline code`.

[Link](http://microsoft.com)

Image:

![Image](Maine-Coon_02.jpg)

List:

* Item 1.

* Item 2.


Some code Python block:

```Python
def my_function()
  print("hello there")
```


