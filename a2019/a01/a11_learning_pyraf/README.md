## Enabling paste in xgterm

Added the following to ~/.Xdefaults and restarted Z Quartz:

```
*VT100.translations: #override  Meta <KeyPress> V:  insert-selection(PRIMARY, CUT_BUFFER0) \n
```



## Fixing delete key in xgterm

Asked questions:

* https://astronomy.stackexchange.com/questions/29113/delete-key-does-not-always-work-in-irafs-xgterm-on-mac

* https://stackoverflow.com/questions/54138621/delete-key-does-not-always-work-in-irafs-xgterm-on-mac



##  Checking sky lines

We want to rerun 2DfDr without accounting for Sky images and compare the spectra with the one we done previously, that subtracted the sky to see the effects.

* `cp p0 p0_without_sky`

* Unreduce everything in 2DfDr.

* Check "Subtract Bias frame" in General tab.

* Uncheck "Subtract Sky" in the Sky tab.

* Run Start Autoreduction.

Shows error:

```
DREXEC1:TDFIO_OPEN: Error opening
DREXEC1:FITS IO Error
DREXEC1:Cannot get keyword number 165.  It does not exist.
DREXEC1:Error in TDFIO API
DREXEC1:Failed opening master bias frame
DREXEC1:Reduce flat error
Action "REDUCE_FFLAT" to task "DREXEC1", completed with status = %NONAME-F-NOMSG, Message Number 0F29800C.
```

## Learning pyraf

* [Pyraf guide](https://lancesimms.com/programs/Python/pyraf/Docs/pyraf_guide.pdf).

* Run pyraf console: `pyraf`.


## Python sript for extracting a single fibre from CCDs

Script: [extract_fibre.py](extract_fibre.py)

Cd to data dir containing 29Oct18 folder, and run from pyraf console:

```
pyexecute("../../logbook/a2019/a01/a11_learning_pyraf/extract_fibre.py")
```

## Estimating radial velocity

Checking the lines on [http://bass2000.obspm.fr/solar_spect.php](http://bass2000.obspm.fr/solar_spect.php)

Obsject catalog: [http://physwww.mcmaster.ca/~harris/mwgc.dat](http://physwww.mcmaster.ca/~harris/mwgc.dat)

For fibre 113: -34 km/s


Mathematica code:

```
lambdaRest = 4859.75*^-3;
lambda = 4859.2*^-3;
v = (lambda - lambdaRest)/lambdaRest * c
-33952.4
```

347: -30 km/s


### Estimating 113 with fxcore

```
rv
fxcor 29Oct18/reduce/288/p0/stars/113/ccd2.fits sun.fits autowrite+ osample=5700-5800 rsample=5700-5800 continuum="yes" filter="no" rebin="largest" output="results-fxcor.txt"
```

* s: interactive mode.
* U: remove spectrum
* s: set spectrum.
* x: see the velocity
* e: convert to velocity


* For 113: -29.8 (0.1) km/s
* For 198: 12.5 (0.3) km/s
* For 347: -25.6 (0.1) km/s
* For 4: -28.35 (0.12) km/s

## Velocities for star #4 (different regions of spectrum)

* -31.28(0.4)
* -28.2(0.5)
* -31.74(0.2)
* -27.8(0.1)
* -28.97(0.11)
* -28.09(0.16)

## Heliocentric velocity

Heliocentric velocity: -45.55 km/s.

```
rvcorrect ra=00:52:45.2 dec=-26:34:57.4 ut=14:00 year=2019 month=10 
day=29 vobs=-30.0 observatory=aao
# RVCORRECT: Observatory parameters for Anglo-Australian Observatory
#       latitude = -31:16:37.34
#       longitude = 210:56:2.09
#       altitude = 1164
##   HJD          VOBS   VHELIO     VLSR   VDIURNAL   VLUNAR  VANNUAL 
VSOLAR
2458786.08741   -30.00   -45.55   -53.25     -0.142   -0.009  -15.395 
-7.706
```


## What's next

* Measure velocities of multiple stars:

```

fxcor 20aug2_combined-dopcor-plus100.8kms.fits sun.fits apertures="3 17" autowrite+ osample=5700-5720 rsample=5700-5720 rebin="largest" continuum="both" filter="no" output="results-fxcor-dopcor-plus100.8kms-wrtSun-all"
```

Then dopcor all stars.

* Shift: dopcor:

  doppler correction: `dopcor 20aug2_combined.fits[0] 20aug2_combined-dopcor-plus100.8kms.fits 100.8 isvelocity=yes`

* Normalize.

* Measure the lines.

[Summer project page on sites.google.com](https://sites.google.com/s/1RiRpPxHWrUw7GJ-y1-V-0gIdKyAhL3HP/p/1PvlCJ12Qpx1AmllE6RSaZDs0w3vmhswt/edit)


## Tutorials

* [Install Conda, Iraf, Pyraf, Python](install_iraf_pyraf_conda.md).
* [Using git and Github](using_git.md).

