## Comparing spectra with sky

We compare two reductions, with and without sky subtraction, and see how they differ, shown on Figures 1-3.

#### Analysis

* The electrons counts are higher for spectra with no sky subtraction.

* Near the right sides of the plots on CCD1 and CCD3, continuum of sky subtracted spectra is decreasing, while it is increasing for spectra without sky subtraction.


#### CCD 1

![Comparing combined spectra with and without sky subtraction](compare_combined/ccd1_compare_spectra.png)

Figure 1: Combined spectra spectra from CCD1 of AGBSYAZ017762 star with (left) and without (right) sky subtraction.


#### CCD 2

![Comparing combined spectra with and without sky subtraction](compare_combined/ccd2_compare_spectra.png)

Figure 2: Combined spectra spectra from CCD2 of AGBSYAZ017762 star with (left) and without (right) sky subtraction.


#### CCD 3

![Comparing combined spectra with and without sky subtraction](compare_combined/ccd3_compare_spectra.png)

Figure 3: Combined spectra spectra from CCD3 of AGBSYAZ017762 star with (left) and without (right) sky subtraction.


### Compare signal-to-noise ratios (SNR)

Next we measure SNR at same wavelength ranges for spectra with and without sky subtraction, shown in Tables 1-3. We measure SNR by pressing `m` keys twice at the end of the range in `splot` window.

Data: [snr_ccd1.csv](snr_ccd1.csv), [snr_ccd2.csv](snr_ccd2.csv), [snr_ccd3.csv](snr_ccd3.csv).


#### Analysis

* The reported SNR is higher for spectra without sky subtraction for all measured wavelength ranges except the first range from CCD3.

* We need to perform a test to see if this difference is statistically significant.


#### CCD 1

Table 1: Signal-to-noise ratios (SNR) from CCD1 of AGBSYAZ017762 star with and without sky subtraction.

<img src="sompare_snr/ccd1_snr_sky_subtraction.png" width="500" alt="Comparing signal-to-noise ratio for spectra with and without sky subtraction">

#### CCD 2

Table 2: Signal-to-noise ratios (SNR) from CCD2 of AGBSYAZ017762 star with and without sky subtraction.

<img src="sompare_snr/ccd2_snr_sky_subtraction.png" width="500" alt="Comparing signal-to-noise ratio for spectra with and without sky subtraction">


#### CCD 3

Table 3: Signal-to-noise ratios (SNR) from CCD3 of AGBSYAZ017762 star with and without sky subtraction.

<img src="sompare_snr/ccd3_snr_sky_subtraction.png" width="500" alt="Comparing signal-to-noise ratio for spectra with and without sky subtraction">

## Measuring equivalent width of lines in spectra without sky subtraction

Measurements: [AGBSYAZ017762.csv](data/equivalent_widths/AGBSYAZ017762/2019-01-24_no_sky_subtraction/AGBSYAZ017762.csv)


## Findings

* Sky subtraction makes absorption lines deeper for ccd1 and ccd3.

* Found the left side of Blue spectra gets higher with increasing fiber numbers for Oct 27 p1.


## TODO

* Plot fiber numbers for Blue 27 Oct p1

* Measure EQ before normalization