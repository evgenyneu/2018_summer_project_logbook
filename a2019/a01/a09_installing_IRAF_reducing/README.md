## Installing IRAF on Mac


### Install Conda

Used the blog post: [The easy way to install IRAF](https://glauffer.github.io/2018-08-03-the-easy-way-to-install-iraf/)

Download bash installer form [here](https://conda.io/miniconda.html).

Install:

```
bash FILENAME.sh
```


### Install IRAF

```
conda config --add channels http://ssb.stsci.edu/astroconda
conda create -n iraf iraf
```


### Install DS9 Viewer


```
conda install -n iraf ds9
```

Run DS9

```
\ds9 &
```

## Activate

```
source activate iraf
```


## Start IRAF


`cd` to working directory and cleate `login.cl` by choosing xterm.

```
mkiraf
```

Start IRAF

```
cl
```

Quit IRAF

```
logout
```


## Reduce the science frames from ccd 2 and 3

* `cd reduce/288/p0/ccd1`

* Follow the 'Reduce science frame' method from [yesterday](/a2019/a01/a08_learn_data_reduction_night_3#reduce-the-science-frames).


