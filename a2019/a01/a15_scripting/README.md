## Writing scripts for data reduction

## Open pyraf and import functions

Run `pyraf` from the parent directory of logbook.


### Extract a star

```
from logbook.code.python.observations.extract_stars import *
extract_star(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
```

All CCDs:

```
extract_star_all_ccds(name='AGBSYAZ017762', day=29, take="p0")
```

### Get star's radial velocity


```
from logbook.code.python.observations.radial_velocities import *
radial_velocity(name='AGBSYAZ017762', day=29, take="p0")
```

### Correct for doppler shift

```
from logbook.code.python.observations.radial_velocities import *
doppler_correct(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
```

All CCDs:

```
doppler_correct_all_ccds(name='AGBSYAZ017762', day=29, take="p0")
```

### Get fibre by star name

```
from logbook.code.python.observations.fibres import *
fibre_by_star_name(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
```



## Measuring equivalent widths of lines



Wavelength   Atomic_Num  Atomic_Species  Hermes_Band

* 7771.94 8.0     OI [IR]

* 7774.16 8.0     OI [IR]

* 7775.39 8.0     OI [IR]

* 5682.63 11.0    NaI [G]

* 5688.20 11.0    NaI [G]

* 4730.03 12.0    MgI [B]

* 5711.09 12.0    MgI [G]

* 7691.53 12.0    MgI [IR]

* 6696.02 13.0    AlI [R]

* 6698.67 13.0    AlI [R]

* 7835.31 13.0    AlI [IR]

* 7836.13 13.0    AlI [IR]


* Plot green band for AGBSYAZ017762

```
splot 29Oct18/reduce/288/p0/stars/doppler_corrected/113/ccd2.fits
```

* press ':', type 'hist yes'.

* Put cursor for continuum and press "h".

* Put at the center and press "c".

* Clear: "r"

## Measuring NaI_5682.63

eqw gfwhm

0.032 0.23

0.034 0.24

0.033 0.24




* NaI_5688.20 = 



## Plotting two gaussians

* Place on continnum left, press d.

* On right, press d.

* Follow the prompts.



## Another way

* Press h.

* Place cursor below and press r.


## What's next

* Stack spectra.

* Measure all the lines.

* Next week: starst phobos.