# Our method of measuring equivalent widths of absorption lines

This is the method we use to measured absorption lines from a specturm of a star. Let's say we want to measure the line at 4730.03 Å.

### 1. Locate the line

* Open the .fits file containing the star's spectrum using `splot` IRAF's command.

* Zoom-in by pressing `a` at the left and `a` again at the right edge of a region around the line (Fig. 1).


![Line we want to measure](line_we_want_to_measure.png)

Figure 1: Locating an absorption line at 4730.03 Å in IRAF's `splot` window.


### 2. Compare with Sun's spectrum

* In a separate window, open Sun's high resolution spectrum, locate the same line and compare with our spectrum (Fig. 2).


![Compare the line with the Sun's](compare_with_sun.png)

Figure 2: Comparing the same line between the Sun's spectrum (bottom) and the spectrum of our star (top).


* It can be seen from Sun's spectrum in Fig. 2 that there is another line to the left from our line of interest. This could make the line in our spectrum broader then it is and make us overestimate its equivalent width. We will keep this in mind when measuring the line.


### 3. Find similar lines

* In the spectrum of our star (not the Sun), locate another line of similar depth of about 0.8, as shown in Fig. 3.

* Move cursor vertically at continuum level and horizontally at the middle of the line.

* Press `h` and then `c`.

* Record the Gaussian full width at half maximum value, which is 0.2136 Å in our case.

* Find a couple of other lines and measure their Gaussian full width at half maximum values.

* Use these values to estimate a *characteristic Gaussian full width at half maximum*. In our spectrum, for the lines of 0.8 the characteristic value is about 0.20 Å.

* We will use this characteristic value later as a target when measuring our line. This will prevent us from making a mistake by treating a merged double line as a single line, for example.


![Finding similar lines](gfwhm_other_line.png)

Figure 3: Measuring Gaussian full width at half maximum of a different line of similar depth to our line.


### 4. Making three measurements

* Next, we locate our line again and make three measurements of the equivalent width (Fig. 4)

  1. First, we make a "Fat" Guassian fit, where the red dotted profile is wider than the observed line, but is still reasonable (this is subjective).

  2. Next, we make a "Skinny" fit, with red profile being slightly narrower than the observed line.

  3. Finally, we make a "good fit", which is a fit that is between the "fat" and "skinny", and which we think is the closest one to the observed data.

* The above measurements are done in `splot` by moving the cursor over the line, pressing `h` key and then pressing one of `l`, `r` or `k` keys to measure from the left, right and middle of the line respectively.

* When doing the measurement we keep the Gaussian full width at half maximum not too far (within 20%) from 0.20 Å, which was the characteristic value we estimated earlier from other similar lines.

![Three measurements](three_measurements.gif)

Figure 4: Making three measurements of the equivalent width of the absorption line.


### 5. Recording raw data

* For each of the three measurements, we save the equivalent withs and Gaussian full width at half maximum value in a spreadsheet shown in Table 1.

* The values are saved with all available significant figures that were reported by the `splot` utility.


Table 1: Raw data from the measurements of the absorption lines in the spectrum of a star ([equivalent_width_data.xlsx](equivalent_width_data.xlsx)).

![Raw data](raw_data.png)


### 6. Calculating the average equivalent width and its uncertainty

* Next, we calculate the average of the three measurements (Fat, Skinny and Good). For example, for our line at 4730.03 Å, the average equivalent width is 37.1 mÅ (milliangstrom).

* The uncertainty of the measurement is estimated by taking the half of the range of three measurements. For our three measurements of 0.0436, 0.0304 and 0.0373 Å, the uncertainty is


```
u(EW) = (0.0436 - 0.0304) / 2 = 6.6 mÅ.

```

### 7. Result

We have calculated the equivalent width for our 4730.03 Å absorption line to be:

```
37 ± 7 mÅ.
```


### The sources of systematic errors

#### 1. Choice of continuum level

Placing continuum of the spectrum at different level will result in different measurements of equivalent widths. For example, if we choose the continuum to be above our value of 1 shown in Fig. 3, then the area inside the absorption line will be greater, which will result in higher equivalent width value.


#### 2. Subtraction of background radiation


In addition to the light form our star, the telescope also collects background light coming from other sources (sky, instrument etc.). If this background light was not subtracted properly, then all the absorption lines will be taller or shallower than they should be.

For example, if we don't subtract enough light form the sky, then the hight of our line relative to the continuum will be smaller, resulting in smaller measurement of the equivalent width.



### The sources of uncertainty

One source of uncertainty in our measurement comes from the noise. Low signal-to-noise ratio makes it harder for us to see weak lines and detect multiple merged lines.

Another source of uncertainty is the subjective judgment of a person measuring the lines. When fitting the light with a Gaussian curve, one person may decide that the fit is reasonable, while another might not.