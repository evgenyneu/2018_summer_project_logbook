from logbook.code.python.jobs.reduce_data import start_reduction


def reduce_all_observations(name):
    """
    Reduce the data.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    """

    # Observations that will be combined
    observations = [
            {"day": 27, "take": "p1_sky"},
            {"day": 29, "take": "p0_sky"},
            {"day": 29, "take": "p1_A_sky"}
        ]

    description = "No throughput Calibration. No Sky Subtracted. Doppler shifted. Normalized."

    start_reduction(observations=observations, name=name, day=30, take="sky", description=description)
