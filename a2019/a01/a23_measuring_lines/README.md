## Measuring lines of `AGBSYAZ017762` star

Measuring the equivalent widths of the absorption lines in the spectra of the star using the [method](measuring_lines_method.md).

Measurements: [AGBSYAZ017762.csv](data/equivalent_widths/AGBSYAZ017762/2019-01-23/AGBSYAZ017762.csv)



## Useful `splot` commands

* `.`: Move window right

* `,`: Move window left

* `j c`: Center window at cursor.


## Reducing data without sky subtraction

We want to reduce the data without sky subtraction in order to see how it affect measurements of equivalent widths.

* Copied the reduced directories for the three observations

  * 27 Oct p1: `cp -r p1 p1_sky'

  * 29 Oct p0: `cp -r p0 p0_sky'

  * 29 Oct p1_A: `cp -r p1_A p1_A_sky'


For all CDD directory and all observation:

* Run `drcontrol &`

* File > Remove all reductions.

* Click on *combined.fits file and click "Unreduce".

* Select General > Subtract Bias Frame.

* Deselect Sky > Throughput Calibrate.

* Deselect Sky > Subtract Sky.

* Click "Star Auto Reduction".


## Perform data reduction

Run the code:

```
from logbook.a2019.a01.a23_measuring_lines.reduce_with_sky import reduce_all_observations

reduce_all_observations(name="AGBSYAZ017762")
```



