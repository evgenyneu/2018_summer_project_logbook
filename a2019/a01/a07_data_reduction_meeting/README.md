# Data reduction meeting with Simon and Chris


## Sources of data

1. **Signal from stars**: 100 stars * 3 CCD.

1. **Bias**: noise from CCD, t=0 exposure. 10x biases * 3 CCD. Subtracted from data.

1. **Sky exposures**: 25 * 3 CCD. Combine to get avg sky.

1. **Flat fields**. 3 exposures of uniform light * 3 CCD. Account for different sensitivity of pixels.


## Software Documentation

* [2dF AAOmega Manual](manuals/2dF-AAOmega-obs-manual.pdf)

* [2dfdr website](https://www.aao.gov.au/science/software/2dfdr)


### Installing 2DfDr

* [Download](https://www.aao.gov.au/science/software/2dfdr) and extract the distributive.

* Add the bin directory to PATH (.bashrc on Linux):

```
export PATH=/path/to/software/2dfdr_install/bin:$PATH
```

* Run 2DfDr


```
drcontrol
```


## Data reduction

### Using 2dfdr

1. Raw data: electron counts (e) vs pixels.

1. Compensate for systematic offset (using biases): e vs pixels.

1. Change coordinates: pixels -> wavelength using **arcs** 3 CCD files (photos of a gas with known emissions).

### Using IRAF

1. Normalize continuum: Norm. flux vs λ_1. Fit curve to spectrum and normalize

1. Measure radial velocities + offset λ to rest λ_0.

1. Final spectrum: norm. flux vs λ_0.

## Stellar parameters determination

Key parameters:

1. Teff. Get from empirical photometric estimates.

1. Log(g) - determines the depth of the lines.

1. V_mic - microturbulent velocity, influences the width of the lines.


## Installing IRAF on Ubuntu

Download and install miniconda from https://conda.io/miniconda.html:

```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Install IRAF

```
conda config --add channels http://ssb.stsci.edu/astroconda
conda create -n iraf iraf-all
```

Install DS9 image viewer:

```
sudo apt install saods9
```



## Use IRAF

Start ds9 before iraf

```
sudo ds9 &
```

Activate iraf environment:

```
source activate iraf
```


`cd` to working directory and cleate `login.cl` by choosing xterm.

```
mkiraf
```

Start IRAF

```
cl
```

To exit IRAF:

```
logout
```


## IRAF commands


* `display` view data in ds9.









