import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

df = pd.read_csv('ew_normal_vs_no_glow.csv')

ew_a = 'Equivalent width mean for normal data, a [mA]'
ew_b = 'Equivalent width mean for "no glow" data, b [mA]'

sigma_a = 'u(a) [mA]'
sigma_b = 'u(b) [mA]'

df['z'] = (df[ew_a] - df[ew_b]) / np.sqrt(df[sigma_a]**2 + df[sigma_b]**2)

ax = plt.figure().gca()
df.hist(ax=ax, column='z')
plt.xlabel("Z-score of a-b")
plt.ylabel("Number of observations")

plt.title('Z-scores of difference of equivalent widths\n'
          'between measurements with "normal" (a) and "no glow" (b) data')

ax.yaxis.set_major_locator(MaxNLocator(integer=True))  # Force integer tick labels for y axis
plt.show()
