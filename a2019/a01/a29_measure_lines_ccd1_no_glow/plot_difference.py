import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('ew_normal_vs_no_glow.csv')
upper_limit = 200
df_line = pd.DataFrame({'x': [0, upper_limit], 'y': [0, upper_limit]})
ax = None
ax = df_line.plot(ax=ax, x='x', y='y', color='g', legend=False)

xcolumn = 'Equivalent width mean for normal data, a [mA]'
ycolumn = 'Equivalent width mean for "no glow" data, b [mA]'
xerr_column = 'u(a) [mA]'
yerr_column = 'u(b) [mA]'

CCDs = [1, 2, 3]
colors = ['Blue', 'Green', 'Red']

# Plot the scatter markers for each CCD
for index, ccd in enumerate(CCDs):
    df_ccd = df.loc[df.CCD == CCDs[index]]

    if len(df_ccd) == 0:
        continue

    color = colors[index]
    ax = df_ccd.plot.scatter(ax=ax, x=xcolumn, y=ycolumn,
                             xlim=(0, upper_limit), ylim=(0, upper_limit), color=color)

    ax.errorbar(df_ccd[xcolumn], df_ccd[ycolumn], xerr=df_ccd[xerr_column],
                yerr=df_ccd[yerr_column], fmt='none', ecolor=color, capsize=3,
                elinewidth=0.5, capthick=0.5)

plt.show()
