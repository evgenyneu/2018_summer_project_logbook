## Measuring equivalent widths without extreme corner glow frames

Our goal here is to repeat data reduction without the last three frames (18, 19 and 20) from Oct 27 p1 observation. We want to exclude these frames because they have a particularly bright glow. Next, we will measure equivalent widths in CCD1 and compare them with previous measurements. This will allow us to see if the three frames containing bright glow affected the measurements of equivalent widths.

### Method

* `cp 27Oct18/reduce/288/p1 27Oct18/reduce/288/p1_noglow`.

* `cd 27Oct18/reduce/288/p1 27Oct18/reduce/288/p1_noglow/ccd1`.

* Delete all files for frames 18, 19 and 20.

* Launch 2DfDr: `drcontrol &`.

* File > Remove all reductions.

* Select *combined.fits file and click 'Unreduce' button.

* Select General > Subtract Bias Frame

* Unselect General > Throughput Calibration.

* Select Plots > Generate plots of combined sky.

* Click 'Start Auto Reduction' button.


### Perform data reduction

Run the code:

```
from logbook.a2019.a01.a29_measure_lines_ccd1_no_glow.reduce_no_glow import reduce_all_observations

reduce_all_observations(name="AGBSYAZ017762")
```

### Comparing spectra between glow and no glow data


The spectra is shown in Figures 1-3.

* Spectra on Figures 2 and 3 look identical. This is expected, since the data for those CCDs was identical. We only removed the extreme glow frames from CCD1 for one observation.

* Spectra on Fig. 1 look different. The absorption lines for the no glow (white) spectra look appear slightly deeper.


#### CCD 1

![Comparing spectra with and without glow CCD1](glow_vs_no_glow/ccd1_comparing_with_no_glow.png)

Figure 1: Comparing spectra from normal (red dashed) and "no glow" (white) data for AGBSYAZ017762 star for CCD1.


#### CCD 2

![Comparing spectra with and without glow CCD2](glow_vs_no_glow/ccd2_comparing_with_no_glow.png)

Figure 2: Comparing spectra from normal (red dashed) and "no glow" (white) data for AGBSYAZ017762 star for CCD2.


#### CCD 3

![Comparing spectra with and without glow CCD3](glow_vs_no_glow/ccd3_comparing_with_no_glow.png)

Figure 3: Comparing spectra from normal (red dashed) and "no glow" (white) data for AGBSYAZ017762 star for CCD3.





### Measuring equivalent widths for CCD1

We measure equivalent widths for CCD1 using 'no glow' data.

Measurements: [AGBSYAZ017762.xlsx](data/equivalent_widths/AGBSYAZ017762/2019-01-29_noglow/AGBSYAZ017762.xlsx)



### Measuring equivalent widths for normal and 'no glow' data

We compare equivalent widths for normal and 'no glow' data for CCD 1. We don't compare CCD 2 and 3, since the spectra are identical.


#### Plot equivalent widths relative to each other

We can see from Fig. 4 that equivalent widths for "no glow" data are all higher than those for "normal data". This is consistent with the spectral on Fig. 1, which showed that "no glow" lines were deeper.

![Equivalent widths relative to each other](ew_no_glow_vs_normal.png)

Figure 4: Equivalent width measurements for normal and "no glow" data.

Plotting code: [plot_difference.py](plot_difference.py).


#### Plot equivalent width A (x-axis) to A - B (y-axis)

![Equivalent widths difference vs equivalent width](ew_difference_vs_ew.png)

Figure 5: Difference of equivalent width for normal and "no glow" data  (a-b) vs equivalent width with "normal" data (a).

Plotting code: [plot_a_minus_b_vs_a.py](plot_a_minus_b_vs_a.py).


#### Mean difference

Mean difference: -7±2 mA.


Code: [ew_descriptive_stats.py](ew_descriptive_stats.py)



#### Z-scores of difference of equivalent widths

We plot the z scores of the difference of equivalent widths on Fig. 6.


```math
z_{n} = abs(EW_{a,n} - EW_{b,n})/\sqrt{\sigma_{a,n}^2 + \sigma_{b,n}^2},
```

where $`EW_{a,n}`$ and $`EW_{b,n}`$ are the measured n-th absorption lines for "normal" and "no glow" data respectively, and $`\sigma_{a,n}`$, $`\sigma_{b,n}`$ are the uncertainties of those measurements.

![Z-scores of difference of equivalent widths](z_scores_equivalent_widths.png)

Figure 5: Difference of equivalent width for "normal" and "no glow" data.

Code: [z_score_difference.py](z_score_difference.py).


It can be seen that the two sets of measurements lie within one uncertainty from each other.


#### Hypothesis testing

*Test*: [Wilcoxon signed-rank test](https://en.wikipedia.org/wiki/Wilcoxon_signed-rank_test).

*Null hypothesis*: the median difference is zero: Median($`EW_{a,n}`$) = Median($`EW_{b,n}`$)

*Alternative hypothesis*:  the median difference is NOT zero Median($`EW_{a,n}`$) ≠ Median($`EW_{b,n}`$)

Calculation: [wilcoxon.xlsx](wilcoxon.xlsx).

We found W- = 0. Using the [Wilcoxon test table](wilcoxon_signed_rank_table.pdf) for n=9 (number of observations), **we reject the null hypothesis with confidence level of at least 99.5%**.



## Todo

* Check star with high fiber (normal vs no glow), also around fiber # 350.

