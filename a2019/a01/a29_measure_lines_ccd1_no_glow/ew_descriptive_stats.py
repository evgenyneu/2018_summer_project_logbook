import pandas as pd

df = pd.read_csv('ew_normal_vs_no_glow.csv')

# Mean difference
mean = df['a-b [mA]'].mean()
stdev = df['a-b [mA]'].std()
print(f"Mean difference (All CCDs): {mean:0.0f} +/- {stdev:0.0f} mA")

CCDs = [1,2,3]
colors = ['Blue', 'Green', 'Red']

# Plot the scatter markers for each CCS
for index, ccd in enumerate(CCDs):
    df_ccd = df.loc[df.CCD == CCDs[index]] # Select data for one star
    color = colors[index]
    mean = df_ccd['a-b [mA]'].mean()
    stdev = df_ccd['a-b [mA]'].std()
    print(f"Mean difference ({color}): {mean:0.0f} +/- {stdev:0.0f} mA")
