import pandas as pd
import os


def calculate():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    csv_path = os.path.join(dir_path,"velocities_AGBSYAZ017762_29oct_p1_A.csv")
    df = pd.read_csv(csv_path)
    df = df[df.Bad != 1] # Exclude bad columns

    # For each star keep three values with smallest errors
    df2 = df.reset_index()
    df = df2.loc[df2.groupby('Name')['VERR'].nsmallest(3).index.levels[1]]

    # Calculate velociti means
    df_means = df.groupby('Name')['VREL'].mean().rename("VREL_mean")
    df_means = pd.DataFrame(df_means)

    # Calculate standard errors of the mean for velocity of each star
    df_uncertainties = df.groupby('Name')['VREL'].sem().rename("VREL_err")
    df_uncertainties = pd.DataFrame(df_uncertainties)

    # Keep a single star in the table with lowest VERR
    df2 = df.reset_index()
    df = df2.loc[df2.groupby('Name')['VERR'].idxmin()]
    df = df.set_index('Name')

    # Add the mean velocities with uncertainties
    df = pd.concat([df, df_means, df_uncertainties], axis=1, join="inner")

    # Remove unused columns
    df = df.drop(['level_0','index','Unnamed: 0','Bad'], axis=1)

    csv_path_result = os.path.join(dir_path,"velocity_AGBSYAZ017762_29oct_p1_A.csv")
    df.to_csv(csv_path_result)

    return df

if __name__ == '__main__':
   calculate()