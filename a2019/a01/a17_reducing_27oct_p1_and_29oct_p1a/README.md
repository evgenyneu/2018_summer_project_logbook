# Data Reduction

## Reducing data from 27 Oct p1

### Changing file structure

Here are the changes to file structure I made to make it match the one from 29 Oct.

* Create dir `288` in `27Oct18/reduce` and move `288_p0` and `288_p1` into `288/`.

* Inside `288/` rename `288_p0` to `p0` and `288_p1` to `p1`.

* Rename `reduce/biases_27oct18` to `reduce/biases_27Oct18` (capicalization of 'O').

* Inside `biases_27Oct18` directory, rename `ccd_1` to `ccd1`, same for `ccd2` and `ccd3`.



### Reducing biases

1. `cd biases_27oct18`

1. Run 2DfDr: `drcontrol &`

1. File > Remove all reductions.

1. Click the `BIASCombined.fits` file and press "Unreduce".

1. Start Auto Reductions. Creates `BIAScombined.fits` file.

Repeat the steps for ccd2 and ccd3.



### Reducing `p1` frames 16-22

* `cd reduce/288/p1/ccd1`

* Run 2DfDr: `drcontrol &`

* File > Remove all reductions. Unreduce the combined file and combined bias file.

* Copy the BIAS: `cp ../../../biases_27Oct18/ccd1/BIAScombined.fits ./`.  **Warning**: make sure to copy from correct ccd directory.

* Restart 2DfDr.

#### Change settings

* General tab > Subtract bias frame.

* Plots > Generate plots for:

  * Combined sky

  * Telluric correction


#### Reduce

* Click `Start Auto Reduction`.

* Repeat for ccd2 and ccd3.


#### A problem with sky frames?

The `27Oct18/reduce/288/p1/ccd1/27oct10016_outdir/redMessages.log` includes text

> No sky lines were found therefore not correcting for throughput

> {           0  of          128  sky lines used for throughput calibration} {GET_THPUT_MEDIAN: no sky lines found} {No sky lines were found therefore not correcting for throughput} Sky-subtracting { Sky subtracting using           25  fibres} {          25  Sky Fibres Found} {Using Sky Combination Method MEDIAN} {Plot of combined sky spectrum 27oct10016_outdir/combined_sky_spectrum.gif}



From the manual


> OFFSET SKIES. These are used for fibre throughput and normalisation of sky fibres. These calibration frames are crucial for high resolution blue data that does not cover the λ5577A sky line. For most fields, these can be taken 20–30 arcsec from the field, though for crowded fields (e.g. LMC or a globular cluster), larger offsets should be done. It is recommended to take 3 of these, offsetting 20–30 arcsec each time, since often stars will land in one or more fibres. With AAOmega, if a setting has been chosen which includes strong night sky lines (and λ5577A is the only one in the blue) then one may dispense with these observations as calibration will be performed from the sky lines. However, for strong continuum objects, the sky lines become unmeasurable and so attention must be paid to ones data in this situation and a judgement call made on the need for offset sky frames.



#### Assessing the spectra quality

* Compare the spectra for out pet star `AGBSYAZ017762` from Oct 27 p1 with Oct 29 p0.

* CCD1:

  * 27 Oct: avg 800, snr 34.

  * 29 Oct: avg 700, snr 30.

* CCD2:

  * 27 Oct: avg 1300, snr 60.

  * 29 Oct: avg 1200, snr 50.

* CCD3:

  * 27 Oct: avg 3900, snr 90

  * 29 Oct: avg 3200, snr70.

We conclude that 27 Oct p1 spectra is better than thafrom 29 Oct p0.


#### Errors in ccd3

2DfDr return the following errors when processing ccd3:

> Bad throughput in fibre 226 (BGBSYAZ009280 star, see Figures 1 and 2) and 82 (BGBSYAZ012027 star, see Figures 3 and 4).


#### Star BGBSYAZ009280 ccd3 from Oct 27 p1 and Oct 29 p0

![27Oct_p1_ccd3_BGBSYAZ009280](27Oct_p1_ccd3_BGBSYAZ009280.png)

Figure 1: Combined reduced spectrum of BGBSYAZ009280 star for 27 Oct, p1, ccd3.

![29Oct_p0_ccd3_BGBSYAZ009280](29Oct_p0_ccd3_BGBSYAZ009280.png)

Figure 2: Combined reduced spectrum of BGBSYAZ009280 star for 29 Oct, p0, ccd3.



#### Star BGBSYAZ012027 ccd3 from Oct 27 p1 and Oct 29 p0

![27Oct_p1_ccd3_BGBSYAZ009280](27Oct_p1_ccd3_BGBSYAZ012027.png)

Figure 3: Combined reduced spectrum of BGBSYAZ012027 star for 27 Oct, p1, ccd3.

![29Oct_p0_ccd3_BGBSYAZ009280](29Oct_p0_ccd3_BGBSYAZ012027.png)

Figure 4: Combined reduced spectrum of BGBSYAZ012027 star for 29 Oct, p0, ccd3.



## Reducing data from 29 Oct p1_A


### Changing file structure

Here are the changes to file structure I made to make it match the naming conventions from [Exposures to Stack/Combine](https://sites.google.com/monash.edu/ngc288/home) manual.

* In `29Oct18/reduce/288` rename:

  * `p0set1of1` to `p0`.

  * `p1set1of2` to `p1_A`.

  * `p1set2of2` to `p1_B`.


### Reducing `p1_A` frames 17-23

* Delete existing reduces files in `29Oct18/reduce/288/p0_A/` for all ccds.

* Copy the frames from 17 to 23 (inclusive) from `29Oct18/orig_data/181029` to corresponding ccd directories `29Oct18/reduce/288/p0_A/ccd1', '29Oct18/reduce/288/p0_A/ccd2' and '29Oct18/reduce/288/p0_A/ccd3'.


#### Reducing

* `cd ccd1`

* Copy the BIAS: `cp ../../../biases_29Oct18/ccd1/BIAScombined.fits ./`. **Warning**: make sure to copy from correct ccd directory.

* Run 2DfDr: `drcontrol &`


#### Change settings

* General tab > Subtract bias frame.

* Plots > Generate plots for:

  * Combined sky

  * Telluric correction


#### Reduce

* Click `Start Auto Reduction`.

* Repeat for ccd2 and ccd3.



#### Assessing the spectra quality

* Compare the spectra for out pet star `AGBSYAZ017762` from Oct 29 p1_A with Oct 29 p0.

* CCD1:

  * 29 Oct p1_A: avg 800, snr 30.

  * 29 Oct p0: avg 700, snr 24.


* CCD2:

  * 29 Oct p1_A: avg 1450, snr 50.

  * 29 Oct p0: avg 1200, snr 50.


* CCD3:

  * 29 Oct p1_A: avg 3500, snr 80.

  * 29 Oct p0: avg 3200, snr 70.


Concolusion: p1_A has about the same quality of spectra as p0 (both 29 Oct).


#### Errors in ccd3

2DfDr return the following errors when processing ccd3:

> Bad throughput in fibre 226 (BGBSYAZ009280 star).


## Process `AGBSYAZ017762` with IRAF

### Extract star

```
from logbook.code.python.observations.extract_stars import *
extract_star_all_ccds(name='AGBSYAZ017762', day=27, take="p1")
extract_star_all_ccds(name='AGBSYAZ017762', day=29, take="p0")
extract_star_all_ccds(name='AGBSYAZ017762', day=29, take="p1_A")
```

### Calcualate velocity of the star for 27 Oct p1

```
python -m logbook.a2019.a01.a17_reducing_27oct_p1_and_29oct_p1a.check_velocity_AGBSYAZ017762_27oct_p1
```

* The script creates [velocities_AGBSYAZ017762_27oct_p1.csv](velocities_AGBSYAZ017762_27oct_p1.csv).

* Add column 'Bad' and mark bad fits with value 1 (a number).

* Save the `velocities_AGBSYAZ017762_27oct_p1.csv` file and run:

```
python -m logbook.a2019.a01.a17_reducing_27oct_p1_and_29oct_p1a.calculate_velocity_AGBSYAZ017762_27oct_p1
```

Output file [velocity_AGBSYAZ017762_27oct_p1.csv](velocity_AGBSYAZ017762_27oct_p1.csv)

Calculated velocity: -31.89473333 (0.397742053) km/s

Calculated previously for 29 Oct p0: -31.119833333333332 (0.2868650398900345) km/s

Two values agree within 3 uncertainties. The change in velocity can be due to difference in Earth rotation and earth movement around the Sun between two observations.


### Calcualate velocity of the star for 29 Oct p1_A

```
python -m logbook.a2019.a01.a17_reducing_27oct_p1_and_29oct_p1a.check_velocity_AGBSYAZ017762_29oct_p1_A
```

* Script created file [velocities_AGBSYAZ017762_29oct_p1_A](velocities_AGBSYAZ017762_29oct_p1_A). 

* Add column 'Bad' and mark bad fits with value 1 (a number).

* Save the file file and run:

```
python -m logbook.a2019.a01.a17_reducing_27oct_p1_and_29oct_p1a.calculate_velocity_AGBSYAZ017762_29oct_p1_A
```

Output file: [velocity_AGBSYAZ017762_29oct_p1_A](velocity_AGBSYAZ017762_29oct_p1_A)

Calculated velocity: -31.1327(0.540780633) km/s

Agrees with previous two values within one uncertainty.



### Correct for doppler shift

```
from logbook.code.python.observations.radial_velocities import *
doppler_correct_all_ccds(name='AGBSYAZ017762', day=27, take="p1")
doppler_correct_all_ccds(name='AGBSYAZ017762', day=29, take="p0")
doppler_correct_all_ccds(name='AGBSYAZ017762', day=29, take="p1_A")
```


### Normalize 29 p0

#### Blue

```
continuum 29Oct18/reduce/288/p0/stars/doppler_corrected/AGBSYAZ017762/ccd1.fits 29Oct18/reduce/288/p0/stars/normalized/AGBSYAZ017762/ccd1.fits order=3 markrej=no grow=0 high_reject=0 low_reject=1.2 function=spline3 niterat=10 override=yes
```

#### Green

```
continuum 29Oct18/reduce/288/p0/stars/doppler_corrected/AGBSYAZ017762/ccd2.fits 29Oct18/reduce/288/p0/stars/normalized/AGBSYAZ017762/ccd2.fits order=6 markrej=no grow=1 high_reject=0 low_reject=2 function=spline3 niterat=10 override=yes
```

#### Red

```
continuum 29Oct18/reduce/288/p0/stars/doppler_corrected/AGBSYAZ017762/ccd3.fits 29Oct18/reduce/288/p0/stars/normalized/AGBSYAZ017762/ccd3.fits order=3 markrej=no grow=0 high_reject=0 low_reject=1.5 function=spline3 niterat=10 override=yes
```


### Normalize 27 p1

#### Blue

```
continuum 27Oct18/reduce/288/p1/stars/doppler_corrected/AGBSYAZ017762/ccd1.fits 27Oct18/reduce/288/p1/stars/normalized/AGBSYAZ017762/ccd1.fits order=3 markrej=no grow=0 high_reject=0 low_reject=1.2 function=spline3 niterat=10 override=yes
```

#### Green

```
continuum 27Oct18/reduce/288/p1/stars/doppler_corrected/AGBSYAZ017762/ccd2.fits 27Oct18/reduce/288/p1/stars/normalized/AGBSYAZ017762/ccd2.fits order=6 markrej=no grow=1 high_reject=0 low_reject=2 function=spline3 niterat=10 override=yes
```

#### Red

```
continuum 27Oct18/reduce/288/p1/stars/doppler_corrected/AGBSYAZ017762/ccd3.fits 27Oct18/reduce/288/p1/stars/normalized/AGBSYAZ017762/ccd3.fits order=3 markrej=no grow=0 high_reject=0 low_reject=1.5 function=spline3 niterat=10 override=yes
```


### Normalize 29 Oct p1_A


#### Blue

```
continuum 29Oct18/reduce/288/p1_A/stars/doppler_corrected/AGBSYAZ017762/ccd1.fits 29Oct18/reduce/288/p1_A/stars/normalized/AGBSYAZ017762/ccd1.fits order=3 markrej=no grow=0 high_reject=0 low_reject=1.2 function=spline3 niterat=10 override=yes
```

#### Green

```
continuum 29Oct18/reduce/288/p1_A/stars/doppler_corrected/AGBSYAZ017762/ccd2.fits 29Oct18/reduce/288/p1_A/stars/normalized/AGBSYAZ017762/ccd2.fits order=6 markrej=no grow=1 high_reject=0 low_reject=2 function=spline3 niterat=10 override=yes
```

#### Red

```
continuum 29Oct18/reduce/288/p1_A/stars/doppler_corrected/AGBSYAZ017762/ccd3.fits 29Oct18/reduce/288/p1_A/stars/normalized/AGBSYAZ017762/ccd3.fits order=3 markrej=no grow=0 high_reject=0 low_reject=1.5 function=spline3 niterat=10 override=yes
```

## To do

* Redo everything without throughput calibration (just ccd3).

* Combine the combined in 2dfdr.

* Extract, dopppler correct and normalized the combined frames.