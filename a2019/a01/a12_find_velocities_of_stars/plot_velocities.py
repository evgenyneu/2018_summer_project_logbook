from logbook.code.python.observations.radial_velocities import star_velocities
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

def errors_table(wavelength_start, ccd):
    df = pd.DataFrame()

    widths = [1, 2, 5, 8, 10, 15, 20, 28, 35, 43, 50, 60, 70, 85, 100]

    for this_range in widths:
        new_df = star_velocities(ccd=ccd, wavelength_start=wavelength_start, range_width=this_range, max_fibres=5)
        new_df['Width'] = this_range
        new_df['VERR_inflated'] = new_df['VERR'] * 50
        df = pd.concat([df, new_df])

    # Replace empty values with a low number, so we can see them as large errors in the plot
    df.replace(to_replace=np.nan, value=1, inplace=True)

    # Use absolute value of velocity
    df['VREL'] = df['VREL'].abs()

    return df

def plot(show_error_bars):
    xcolumn = 'Width'
    ycolumn = 'VREL'
    wavelength = 5700

    df = errors_table(wavelength_start=5700, ccd=2)

    colors = ['Blue', 'Green', 'Red', 'Black', 'Brown']
    markers = ['.', '+', '3', '*', '2']
    stars = ['RGBSYAZ021904', 'RGBSYAZ025174', 'AGBSYAZ024409', 'AGBSYAZ030871', 'RGBSYAZ031345']
    ax = None

    for idx, star in enumerate(stars):
        color = colors[idx]
        marker = markers[idx]
        df_star = df.loc[df.Name == star] # Select data for one star

        ax = df_star.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, c=color, label=star, marker=marker,
            s=80)

        if show_error_bars == "yes":
            ax.errorbar(df_star[xcolumn], df_star[ycolumn], yerr=df_star['VERR_inflated'],
                fmt='none', ecolor=color, capsize=3, elinewidth=0.5, capthick=0.5)

    ax.set_title(f"Fxcor radial velocities of stars for wavelength {wavelength} Å (green)")
    ax.set_xlabel("Wavelength range [Å]")
    ax.set_ylabel("Radial velocity (km/s)")

    plt.show();

if __name__ == '__main__':
    arguments = dict(map(lambda x: x.lstrip('-').split('='), sys.argv[1:]))
    plot(show_error_bars = arguments.get('errorbars'))
