from logbook.code.python.observations.radial_velocities import star_velocities
import pandas as pd
import numpy as np

def check_visually(df):
    df = df.sort_values(by=['Name','VERR'])
    df.to_csv('candidates.csv')

    for index, row in df.iterrows():
        print(f"{index} {row['Name']}")
        star_velocities(ccd=row['ccd'], wavelength_start=row['wavelength_start'],
            range_width=35, max_fibres=1, interactive=True, include_stars=[row['Name']])

def load_data():
    df = pd.DataFrame()

    wavelengths = [4750, 5700, 6550]

    for idx, wavelength in enumerate(wavelengths):
        ccd = idx + 1

        for i in range(0, 9):
            wavelength_start = wavelength + i*10
            if wavelength_start == 5780: continue # causes segmentation violation
            new_df = star_velocities(ccd=ccd, wavelength_start=wavelength_start, range_width=35,
                interactive=False)
            new_df['ccd'] = ccd
            new_df['wavelength_start'] = wavelength_start
            df = pd.concat([df, new_df])

    # Remove rows with empty velocities
    df = df[df['VREL'].notnull()]

    # For each star keep five values with smallest errors
    df2 = df.reset_index()
    df = df2.loc[df2.groupby('Name')['VERR'].nsmallest(5).index.levels[1]]
    check_visually(df=df)

    return df

def check():
    df = load_data()
    return df

if __name__ == '__main__':
   check()
