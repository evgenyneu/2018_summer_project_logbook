from logbook.code.python.observations.radial_velocities import star_velocities
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def errors_table(wavelength_start, ccd):
    df = pd.DataFrame()

    widths = [1, 2, 5, 8, 10, 15, 20, 28, 35, 43, 50, 60, 70, 85, 100]

    for this_range in widths:
        new_df = star_velocities(ccd=ccd, wavelength_start=wavelength_start, range_width=this_range, max_fibres=15)
        new_df['Width'] = this_range
        df = pd.concat([df, new_df])

    # Replace empty values with a large number, so we can see them as large errors in the plot
    df.replace(to_replace=np.nan, value=5, inplace=True)

    return df

def plot():
    xcolumn = 'Width'
    ycolumn = 'VERR'

    df = errors_table(wavelength_start=4800, ccd=1)
    ax = df.plot.scatter(x=xcolumn, y=ycolumn, c='Blue')

    df2 = errors_table(wavelength_start=5700, ccd=2)
    df2.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, c='Green')

    df3 = errors_table(wavelength_start=6600, ccd=3)
    df3.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, c='Red')

    ax.set_title("Fxcor velocity errors for wavelengths 4800, 5700 and 6600 Å")
    ax.set_xlabel("Wavelength range [Å]")
    ax.set_ylabel("Velocity Error (km/s)")

    plt.show();

if __name__ == '__main__':
   plot()
