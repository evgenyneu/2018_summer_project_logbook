from logbook.code.python.observations.radial_velocities import star_velocities
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

def load_data():
    df = pd.DataFrame()
    wavelengths = [4750, 5700, 6550]

    for idx, wavelength in enumerate(wavelengths):
        ccd = idx + 1

        for i in range(0, 9):
            wavelength_start = wavelength + i*10
            new_df = star_velocities(ccd=ccd, wavelength_start=wavelength_start, range_width=35, max_fibres=5)
            new_df['wavelength_start'] = wavelength_start
            new_df['VERR_inflated'] = new_df['VERR'] * 50
            df = pd.concat([df, new_df])

    # Replace empty values with a large number, so we can see them as large numbers in the plot
    df.replace(to_replace=np.nan, value=5, inplace=True)

    # Use absolute value of velocity
    df['VREL'] = df['VREL'].abs()

    return df

def plot(show_error_bars):
    xcolumn = 'wavelength_start'
    ycolumn = 'VREL'

    df = load_data()

    colors = ['Blue', 'Green', 'Red', 'Black', 'Brown']
    markers = ['.', '+', '3', '*', '2']
    stars = ['RGBSYAZ021904', 'RGBSYAZ025174', 'AGBSYAZ024409', 'AGBSYAZ030871', 'RGBSYAZ031345']
    ax = None

    for idx, star in enumerate(stars):
        color = colors[idx]
        marker = markers[idx]
        df_star = df.loc[df.Name == star] # Select data for one star
        ax = df_star.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, c=color, label=star, marker=marker, s=80)

        if show_error_bars != "yes": continue

        ax.errorbar(df_star[xcolumn], df_star[ycolumn], yerr=df_star['VERR_inflated'],
            fmt='none', ecolor=color, capsize=3, elinewidth=0.5, capthick=0.5)

    ax.set_title("Dependence of fxcor radial velocity on starting wavelength")
    ax.set_xlabel("Starting wavelength [Å]")
    ax.set_ylabel("Radial velocity (km/s)")

    plt.show();

if __name__ == '__main__':
    arguments = dict(map(lambda x: x.lstrip('-').split('='), sys.argv[1:]))
    plot(show_error_bars = arguments.get('errorbars'))
