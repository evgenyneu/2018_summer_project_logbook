from logbook.code.python.observations.radial_velocities import star_velocities
import pandas as pd
import numpy as np
from pyraf import iraf
from iraf import rv

def load_data():
    df = pd.DataFrame()
    wavelengths = [4800, 5700, 6550]

    for idx, wavelength in enumerate(wavelengths):
        ccd = idx + 1
        if wavelength != 5700: continue

        for i in range(0, 9):
            wavelength_start = wavelength + i*10
            new_df = star_velocities(ccd=ccd, wavelength_start=wavelength_start, range_width=35,
                max_fibres=1, interactive=True, include_stars=['AGBSYAZ024409'])
            new_df['wavelength_start'] = wavelength_start
            df = pd.concat([df, new_df])

    # Replace empty values with a large number, so we can see them as large numbers in the plot
    df.replace(to_replace=np.nan, value=5, inplace=True)

    # Use absolute value of velocity
    df['VREL'] = df['VREL'].abs()

    return df

def check():
    df = load_data()

if __name__ == '__main__':
   check()
