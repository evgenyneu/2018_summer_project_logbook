# Finding velocities of the stars

Here we use `fxcor` IRAF function to find radial velocities of the stars. We want to find the optimal parameters for `fxcor` that give consistent measurements of radial velocities with low errors.

## Radial velocity error dependence on wavelength range

Here we want to find the optimal range of wavelength for fxcorr that gives the lowest error. We run `fxcor` for 15 stars and different wavelength ranges and plot errors on Fig. 1 for each of the three channels.

![Velocity errors vs range](plot_error_range.png)

Figure 1: `Fxcor` velocity errors for wavelengths 4800, 5700 and 6600 Å. The colors correspond to the blue, green and red channels.

We can see from Fig. 1 that **ranges between 20 and 50 Å give lowest error ranges** for all three channels.

The zoomed in plot of this region is shown in Fig 2. We can see that **any of the three channels (blue, green and red) are good candidates** for measuring velocities, since there seems to be no particular channel which is significantly better or worse than the others.


![Velocity errors vs range zoomed](plot_error_range_zoomed.png)

Figure 2: Zoomed in version of Fig. 1 at region between 20 and 50 Å.

Plotting code: [plot_error_range.py](plot_error_range.py).

Run: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.plot_error_range`


## Radial velocity dependence on wavelength range

We want to see how measured radial velocities depend on wavelength range (Fig. 3). It can be seen that velocity measurements **become consistent for wavelength ranges above 20 Å**.


![Velocities vs range](plot_velocities.png)

Figure 3: `Fxcor` radial velocities of stars for wavelength 5700 Å (green).



![Velocities vs range with error bars](plot_velocities_with_error_bars.png)

Figure 3b: Same as Fig.3 but with error bars reported by fxcor and inflated 50 times for visibility.


Plotting code: [plot_velocities.py](plot_velocities.py).

Plot: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocities`

With error bars: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocities --errorbars=yes`



## Radial velocity dependence on wavelength

Next, we want to see how radial velocity measurement depends on the choice of the starting wavelength. We plot velocities and starting wavelength for five stars on Fig. 4. We used the wavelength range of `35 Å`. It can be seen that velocities vary both across the three channels (blue, green and red) and within each channel. There are also few outliers. The amount of variation for an individual star, excluding outliers, seem to be about 3-5 km/s.

![Velocities vs wavelength](plot_velocity_vs_wavelength.png)

Figure 4: Dependence of `fxcor` radial velocity on starting wavelength.


### Variation of velocities in individual channels

Let's zoom into individual channels (Figures 5 - 7). We can see the outliers at wavelength 5720 Å, where velocity of all stars was not calculated (the value 5 km/s was chosen in the code to indicate missing velocity calculation). In red channel there is a single outlier for a single star.

#### Blue channel

![Velocities vs wavelength blue channel](plot_velocity_vs_wavelength_blue.png)

Figure 5: Dependence of `fxcor` radial velocity on starting wavelength for the blue channel.


![Velocities vs wavelength blue channel](plot_velocity_vs_wavelength_blue_zoomed.png)

Figure 5b: Same as Fig. 5 but with error bars reported by fxcor and inflated 50 times for visibility. 



#### Green channel

![Velocities vs wavelength blue channel](plot_velocity_vs_wavelength_green.png)

Figure 6: Dependence of `fxcor` radial velocity on starting wavelength for the green channel.



![Velocities vs wavelength blue channel](plot_velocity_vs_wavelength_green_error_bars.png)

Figure 6b: Same as Fig. 6 but with error bars reported by fxcor and inflated 50 times for visibility.



#### Red channel

![Velocities vs wavelength red channel](plot_velocity_vs_wavelength_red.png)

Figure 7: Dependence of `fxcor` radial velocity on starting wavelength for the red channel.



![Velocities vs wavelength red channel](plot_velocity_vs_wavelength_red_error_bars.png)

Figure 7: Same as Fig. 7 but with error bars reported by fxcor and inflated 50 times for visibility.


#### Code

Plotting code: [plot_velocity_vs_wavelength.py](plot_velocity_vs_wavelength.py).

Plot: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocity_vs_wavelength`

With error bars: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocity_vs_wavelength --errorbars=yes`


## Dependency of error on visual fit

We run `fxcor` in interactive mode in order to see how radial velocity and its error depends on the quality of fit shown in the visual inspector. We run `fxcor` nine times for star AGBSYAZ024409 starting from wavelength of 5700 Å and incrementing it by 10 Å. As before, we use the wavelength range of `35 Å`. The fits are shown in Fig. 8.

We can see from Fig. 8 that better quality of fit seem to produce smaller error. For example, the best fits at wavelengths 5700 and 5750 Å give smallest errors 0.1 and 0.139 km/s respectively. Those fits correspond to the velocity estimates of -28.5 and -29.7 km/s respectively. While the worst fits at wavelength 5730, 5760, 5770 and 5780 Å give errors larger than 0.252 km/s and velocity estimates of -26.6, -31.7, -32.1 and -32.2 km/s respectively.


![Visually checking velocity fit](velocities_visual_check.png)

Figure 8: Running `fxcor` at different starting wavelength and observing the quality of fit.


Plotting code: [velocities_visual_check.py](velocities_visual_check.py).

Run: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.velocities_visual_check`


## Estimating velocities method

Based on our investigation of `fxcor` function, we propose the following method of finding velocities:

* For each star, run `fxcor` for nine different starting wavelengths (5700, 5710, 5720 etc.) with fixed range of 35 Å.

* Keep five results with lowest reported velocity errors.

* Run `fxcor` for those results again in interactive mode and check the fit visually.

  * If the fit is reasonably good (similar to 5700 and 5750 Å plots in Fig. 8), then keep the result.

  * If the fit is bad (similar to 5730, 5760, 5770 and 5780 Å plots in Fig. 8), mark the result as 'bad'.

* Exclude the 'bad' results from the dataset.

* For each star, take three result with lowest reported error.

* For each star, calculate the mean radial velocity.

* Calculate uncertainty of the mean radial velocity using *standard error of the mean* *SE*:

```math
SE = \frac{s}{\sqrt{n}},

```
where *n* is the example size and *s* is sample standard deviation.


## Estimating velocities: step 1/2

1. We ran the script for all stars. It creates [candidates_original.csv](candidates_original.csv) file and then goes over each row interactively.

1. Opened the CSV and created a column called 'Bad'.

1. Checked each fit and marked bad wits with value 1 in 'Bad' column. The script prints ID and star name in the console for easier matching of the results.

Code: [find_lowest_errors.py](find_lowest_errors.py).

Run: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.find_lowest_errors`


## Estimating velocities: step 2/2

* Saved the file as [candidates.csv](candidates.csv).

* Ran `calculate_velocities`. It reads the CSV, removes 'bad' columns, picks three results with lowest errors and calculates mean velocities (VREL_mean) with uncertainties (VREL_err).

Velocities file: [star_velocities.csv](star_velocities.csv).

Code: [calculate_velocities.py](calculate_velocities.py).

Run: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.calculate_velocities`


## Problematic stars

I could not find velocities for the following stars because of bad fits in fxcor and significantly varying reported velocities:

* 63  BHBSYAZ014452
* 130 BHBSYAZ015315
* 168 BHBSYAZ017595
* 152 BHBSYAZ017699
* 173 BHBSYAZ019581
* 76  BHBSYAZ022830
* 208 BHBSYAZ023018
* 330 BHBSYAZ026863
* 263 RGBSYAZ005719
* 89  RHBSYAZ011595
* 87  RHBSYAZ020140

Following stars were better but still had poor fits, those stars could be worth taking another look at:

* 311 RGBSYAZ032094
* 182 RHBSYAZ002330
* 271 RHBSYAZ003851
* 274 RHBSYAZ003944
* 398 RHBSYAZ031888
* 87  RHBSYAZ020140


## Plotting histogram of velocities

The histogram of radial velocities is shown in Fig. 10. It can be see nthan most star have velocities between -40 and -10 km/s.

![Velocity distribution](plot_velocity_histogram.png)

Figure 10: Distribution of radial velocities of stars.

Plotting code: [plot_velocity_histogram.py](plot_velocity_histogram.py).

Run: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocity_histogram`


### Zoomed histogram of velocities

Next, we plot a histogram for starts with velocities smaller than -15 km/s, shown in Fig. 11. We can see a single prominent peak at about 30 km/s.

![Velocity distribution zoom](plot_velocity_histogram_zoomed.png)

Figure 11: Distribution of radial velocities of stars, for velocities smaller than -15 km/s.

Plotting code: [plot_velocity_histogram_zoomed.py](plot_velocity_histogram_zoomed.py).

Run: `python -m logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocity_histogram_zoomed`

