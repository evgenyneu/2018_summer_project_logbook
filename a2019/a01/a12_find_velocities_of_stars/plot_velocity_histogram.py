import pandas as pd
import matplotlib.pyplot as plt
import os

def plot():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    csv_path = os.path.join(dir_path,"star_velocities.csv")
    df = pd.read_csv(csv_path)
    df.hist(column='VREL_mean', bins=20)
    plt.title('Radial velocities [km/s]')
    plt.show()

if __name__ == '__main__':
   plot()