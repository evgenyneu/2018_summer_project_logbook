## Reducing data for star "AGBSYAZ017762"

Open pyraf in logbook's parent directory and run:

```
from logbook.code.python.jobs.reduce_data import reduce_all_observations

reduce_all_observations(name="AGBSYAZ017762", description="No throughput Calibration. Sky Subtracted. Doppler shifted. Normalized.")
```

## Comparison reduced spectra from Chris and Evgenii

We compared fully reduced spectra of AGBSYAZ017762 star made by Chris and Evgenii, shown on Figures 1-3.

* **X-axis**. We measured the wavelengths of the same lines (shown as red lines on Figures 1-3). The measurements for Evgenii's and Chris' spectra **agree within uncertainty** of ±0.1 A. This uncertainty was chosen to be half of the smallest wavelength difference that could be measured with `splot`.

* **Y-axis**. **There is ±0.01 difference in the choice of continuum** between Chris' and Evgenii's spectra. **The line depths  differ by ±0.01**.



#### CCD 1

![Comparing spectra from Chris and Evgenii CCD1](comparison/ccd1_comparison_evgenii_chris.png)

Figure 1: Reduced spectra from CCD1 of AGBSYAZ017762 star from Chris and Evgenii.


#### CCD 2

![Comparing spectra from Chris and Evgenii CCD2](comparison/ccd2_comparison_evgenii_chris.png)

Figure 2: Reduced spectra from CCD2 of AGBSYAZ017762 star from Chris and Evgenii.


#### CCD 3

![Comparing spectra from Chris and Evgenii CCD3](comparison/ccd3_comparison_evgenii_chris.png)

Figure 3: Reduced spectra from CCD3 of AGBSYAZ017762 star from Chris and Evgenii.






