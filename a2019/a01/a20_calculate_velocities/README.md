## Calculating velocity

Here we calculate the radial velocity of one star from its combined observations.

```
from logbook.code.python.observations.paths import COMBINED_OBSERVATIONS_DAY, COMBINED_OBSERVATIONS_TAKE
from logbook.code.python.observations.radial_velocities import *
calculate_mean_velocities(names=['AGBSYAZ017762'], day=COMBINED_OBSERVATIONS_DAY, take=COMBINED_OBSERVATIONS_TAKE)
```