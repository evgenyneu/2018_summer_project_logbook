## Compare "normal" and "no glow" data for RGBSYAZ017382 star (fibre 396 in Oct 27 p1)

### Perform data reduction (normal)

Run the code:

```
from logbook.a2019.a01.a30_high_fibre_stars.reduce_normal import reduce_all_observations

reduce_all_observations(name="RGBSYAZ017382")
```


### Perform data reduction (no glow)

Run the code:

```
from logbook.a2019.a01.a30_high_fibre_stars.reduce_no_glow import reduce_all_observations

reduce_all_observations(name="RGBSYAZ017382")
```


### Comparing spectra between glow and no glow data


The spectra is shown in Figures 1-4.

* The absorption lines for CCD 1 for "no glow" data appear to be deeper than in "normal" data. This effect is more prominent for shorter wavelengths.



#### CCD 1

![Comparing spectra with and without glow CCD1](glow_vs_no_glow/ccd1_comparing_with_no_glow.png)

Figure 1: Comparing spectra from normal (red dashed) and "no glow" (white) data for RGBSYAZ017382 star for CCD1.

![Comparing spectra with and without glow CCD1](glow_vs_no_glow/ccd1_comparing_with_no_glow_2.png)

Figure 2: Comparing spectra from normal (red dashed) and "no glow" (white) data for RGBSYAZ017382 star for CCD1.


#### CCD 2

![Comparing spectra with and without glow CCD2](glow_vs_no_glow/ccd2_comparing_with_no_glow.png)

Figure 3: Comparing spectra from normal (red dashed) and "no glow" (white) data for RGBSYAZ017382 star for CCD2.


#### CCD 3

![Comparing spectra with and without glow CCD3](glow_vs_no_glow/ccd3_comparing_with_no_glow.png)

Figure 4: Comparing spectra from normal (red dashed) and "no glow" (white) data for RGBSYAZ017382 star for CCD3.



### Measuring equivalent widths for CCD1

We measure equivalent widths for CCD1.

Measurements for 'normal' data: [RGBSYAZ017382.xlsx](data/equivalent_widths/RGBSYAZ017382/2019-01-30/RGBSYAZ017382.xlsx)

Measurements for 'no glow' data: [RGBSYAZ017382.xlsx](data/equivalent_widths/RGBSYAZ017382/2019-01-30_no_glow/RGBSYAZ017382.xlsx)


#### Plot equivalent widths relative to each other

We can see from Fig. 4 that equivalent widths for "no glow" data are all higher than those for "normal data". This is consistent with the spectral on Fig. 1, which showed that "no glow" lines were deeper.

![Equivalent widths relative to each other](ew_no_glow_vs_normal.png)

Figure 5: Equivalent width measurements for normal and "no glow" data for RGBSYAZ017382 star.

Plotting code: [plot_difference.py](plot_difference.py).


#### Plot equivalent width A (x-axis) to A - B (y-axis)

![Equivalent widths difference vs equivalent width](ew_difference_vs_ew.png)

Figure 6: Difference of equivalent width for normal and "no glow" data  (a-b) vs equivalent width with "normal" data (a).

Plotting code: [plot_a_minus_b_vs_a.py](plot_a_minus_b_vs_a.py).

#### Plot A - B (y-axis) vs wavelength (x-axis)

![Equivalent widths difference vs equivalent width](wavelength_vs_a_minus_b.png)

Figure 7: Wavelength vs difference of equivalent width for normal and "no glow" data  (a-b).

Plotting code: [difference_vs_wavelength.py](difference_vs_wavelength.py).


#### Mean difference

Mean difference: -5±3 mA.


Code: [ew_descriptive_stats.py](ew_descriptive_stats.py)



#### Z-scores of difference of equivalent widths

We plot the z scores of the difference of equivalent widths on Fig. 8.


```math
z_{n} = abs(EW_{a,n} - EW_{b,n})/\sqrt{\sigma_{a,n}^2 + \sigma_{b,n}^2},
```

where $`EW_{a,n}`$ and $`EW_{b,n}`$ are the measured n-th absorption lines for "normal" and "no glow" data respectively, and $`\sigma_{a,n}`$, $`\sigma_{b,n}`$ are the uncertainties of those measurements.

![Z-scores of difference of equivalent widths](z_scores_equivalent_widths.png)

Figure 8: Difference of equivalent width for "normal" and "no glow" data.

Code: [z_score_difference.py](z_score_difference.py).







## Compare "normal" and "no glow" data for RGBSYAZ021600 star (fibre 347 in Oct 27 p1)



### Perform data reduction (normal)

Run the code:

```
from logbook.a2019.a01.a30_high_fibre_stars.reduce_normal import reduce_all_observations

reduce_all_observations(name="RGBSYAZ021600")
```

### Perform data reduction (no glow)

Run the code:

```
from logbook.a2019.a01.a30_high_fibre_stars.reduce_no_glow import reduce_all_observations

reduce_all_observations(name="RGBSYAZ021600")
```



### Comparing spectra between glow and no glow data


The spectra is shown in Figures 9-12.

* The absorption lines for CCD 1 for "no glow" data appear to be deeper than in "normal" data in Fig 9. However, on Fig 10, the opposite appears to be true.



#### CCD 1

![Comparing spectra with and without glow CCD1](glow_vs_no_glow/ccd1_comparing_with_no_glow_RGBSYAZ021600.png)

Figure 9: Comparing spectra from normal (red dashed) and "no glow" (white) data for RGBSYAZ021600 star for CCD1.

![Comparing spectra with and without glow CCD1](glow_vs_no_glow/ccd1_comparing_with_no_glow_2_RGBSYAZ021600.png)

Figure 10: Comparing spectra from normal (red dashed) and "no glow" (white) data for RGBSYAZ021600 star for CCD1.


#### CCD 2

![Comparing spectra with and without glow CCD2](glow_vs_no_glow/ccd2_comparing_with_no_glow_RGBSYAZ021600.png)

Figure 11: Comparing spectra from normal (red dashed) and "no glow" (white) data for RGBSYAZ021600 star for CCD2.


#### CCD 3

![Comparing spectra with and without glow CCD3](glow_vs_no_glow/ccd3_comparing_with_no_glow_RGBSYAZ021600.png)

Figure 12: Comparing spectra from normal (red dashed) and "no glow" (white) data for RGBSYAZ021600 star for CCD3.



## ToDo

* Make a glow fits file from places between fibres.

* Get locations from tram lines file: tml.fits.

* Read article from header of tml.fits.
