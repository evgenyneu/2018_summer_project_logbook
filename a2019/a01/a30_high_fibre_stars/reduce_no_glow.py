from logbook.code.python.jobs.reduce_data import start_reduction


def reduce_all_observations(name):
    """
    Reduce the data.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    """

    # Observations that will be combined
    observations = [
            {"day": 27, "take": "p1_noglow"},
            {"day": 29, "take": "p0"},
            {"day": 29, "take": "p1_A"}
        ]

    description = "This reduction excludes frames 18, 19 and 20 from Oct 27 p1 obserctions, since they contain bright and changing glow. No throughput Calibration. Sky Subtracted. Doppler shifted. Normalized."

    start_reduction(observations=observations, name=name, day=30, take="noglow", description=description)
