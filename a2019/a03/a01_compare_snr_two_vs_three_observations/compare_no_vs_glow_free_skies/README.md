# Compare CCD 2 normal vs glow-free reduction

We want to compare the normal reduction with glow-free reduction for all CCDs to see the effect of the glow-free reduction. 


### CCD 1

![Mean skies CCD 1](normal_glow_free_ccd1.png)

Figure 1: Normal vs glow free mean sky levels for CCD 1.


### CCD 2

![Mean skies CCD 2](normal_glow_free_ccd2.png)

Figure 2: Normal vs glow free mean sky levels for CCD 2.


### CCD 3

![Mean skies CCD 3](normal_glow_free_ccd3.png)

Figure 3: Normal vs glow free mean sky levels for CCD 3.


### Code

```
from logbook.a2019.a03.a01_compare_snr_two_vs_three_observations.compare_no_vs_glow_free_skies.plot_skies import plot_skies

plot_skies()
```

