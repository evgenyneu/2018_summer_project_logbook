from logbook.code.python.plot.compare_sky_levels import compare_sky_levels


def plot_skies():
    ccds = [1, 2, 3]
    colors = ['dodgerblue', 'lightgreen', 'orangered']

    for ccd in ccds:
        path27 = "data/observe/27Oct18/reduce/288"
        path29 = "data/observe/29Oct18/reduce/288"

        obs1 = f"{path27}/p1_noglow_sub_sky/ccd{ccd}/27oct{ccd}_combined.fits"
        obs2 = f"{path29}/p0_noglow_sub_sky/ccd{ccd}/29oct{ccd}_combined.fits"
        obs3 = f"{path29}/p1_A_noglow_sub_sky/ccd{ccd}/29oct{ccd}_combined.fits"
        observation1 = [obs1, obs2, obs3]

        obs1 = f"{path27}/p1/ccd{ccd}/27oct{ccd}_combined.fits"
        obs2 = f"{path29}/p0/ccd{ccd}/29oct{ccd}_combined.fits"
        obs2 = f"{path29}/p1_A/ccd{ccd}/29oct{ccd}_combined.fits"
        observation2 = [obs1, obs2, obs3]

        subtitles = ["Glow-free", "Normal"]

        labels = ['27 Oct p1', '29 Oct p0', '29 Oct p1 A']

        color = [colors[ccd - 1]] * 3

        compare_sky_levels(paths=[observation1, observation2], subtitles=subtitles,
                           title=f"Mean sky levels for CCD {ccd}", labels=labels, colors=color)