import pandas as pd
import matplotlib.pyplot as plt

xy_min = 35
xy_max = 105
df = pd.read_csv('snr_2_vs_3_observations.csv')
df_line = pd.DataFrame({'x': [xy_min, xy_max], 'y': [xy_min, xy_max]})
ax = None
ax = df_line.plot(ax=ax, x='x', y='y', color='black', legend=False, linewidth=0.5)

xcolumn = 'Three observations signal-to-noise n1'
ycolumn = 'Two obsevations signal-to-noise n2'

df_aggr = df.groupby('CCD').agg(['mean', 'sem'])[[xcolumn, ycolumn]]
df_aggr.columns = df_aggr.columns.map(' '.join)
df_aggr.to_csv('mean_snr.csv')

xcolumn_mean = 'Three observations signal-to-noise n1 mean'
ycolumn_mean = 'Two obsevations signal-to-noise n2 mean'
xerr_column = 'Three observations signal-to-noise n1 sem'
yerr_column = 'Two obsevations signal-to-noise n2 sem'

CCDs = [1, 2, 3]
colors = ['Blue', 'Green', 'Red']

# Plot the scatter markers for each CCD
for index, ccd in enumerate(CCDs):
    df_ccd = df_aggr[df_aggr.index == CCDs[index]]
    color = colors[index]
    ax = df_ccd.plot.scatter(ax=ax, x=xcolumn_mean, y=ycolumn_mean, color=color, s=30)

    ax.errorbar(df_ccd[xcolumn_mean], df_ccd[ycolumn_mean], xerr=df_ccd[xerr_column], yerr=df_ccd[yerr_column],
                fmt='none', ecolor=color, capsize=3, elinewidth=0.5, capthick=0.5)

plt.title("Mean signal-to-noise of two vs three stacked observations")
plt.show()