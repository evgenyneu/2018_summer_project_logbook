# Reducing star AGBSYAZ017762 in normal way

We repeat the reduction in original, 'normal' way, i.e. without any special changes.

```
from logbook.code.python.jobs.reduce_data import reduce_single_star

description = "This is the original normal reduction without any special tweaks. No glow removal. Bias subtracted. No throughput Calibration. Sky Subtracted. Doppler shifted. Normalized."

take_prefixes = ["p1", "p0", "p1_A"]

reduce_single_star(name="AGBSYAZ017762", days=[27, 29, 29], take_prefixes=take_prefixes, take_input="normal", take_combined="normal", description=description)
```

![Mean sky levels](mean_skies_observations.png)

Figure 1: Mean sky levels.


![Mean sky levels](mean_skies.png)

Figure 2: Mean sky levels.