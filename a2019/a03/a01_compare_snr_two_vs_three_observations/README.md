# Comparing signal-to-noise between two and three observations

Here we compare the signal-to-noise of the spectrum of the AGBSYAZ017762 (14.1 mag) star between three (27 Oct p1, 29 Oct p0 and p1_A) and two (29 Oct p0 and p1_A) stacked observations. The data were reduced with glow and sky removal from all CCDs.

Raw data: [snr_2_vs_3_observations.xlsx](snr_2_vs_3_observations.xlsx)


## Plot signal-to-noise relative to each other

![Signal-to-noise of two methods relative to each other](one_vs_other.png)

Figure 2: Signal-to-noise of two vs three stacked observations. The error bars are two small to see at this scale. The colors indicate measurements from blue, green and red CCDs.

Plotting code: [plot_one_vs_other.py](plot_one_vs_other.py).


## Plot mean signal-to-noise relative to each other

![Mean signal-to-noise of two methods relative to each other](one_vs_other_mean.png)

Figure 2: Mean Signal-to-noise of two vs three stacked observations. The colors indicate measurements from blue, green and red CCDs.

Plotting code: [one_vs_other_mean.py](one_vs_other_mean.py).


## Log

* Plotted the [stacked skies](a2019/a02/a25_analyzing_glow_removal_form_parked/no_glow_all_ccds_subtract_sky) (Fig. 3) for three observations.

* [Reduced](a2019/a02/a27_reducing_two_observations/combined_two_observations) two observaionts.

* [Compare](compare_no_vs_glow_free_skies) sky levels between normal and glow-free reductions for each CCD.

* [Compare](compare_snr_normal_vs_glow_free) signal-to-noise between normal and glow-free reductions.

* [Normal reduction](reducing_normal).