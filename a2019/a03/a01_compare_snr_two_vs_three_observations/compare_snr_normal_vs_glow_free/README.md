# Comparing signal-to-noise between normal and glow-free reductions

Here we compare the signal-to-noise of the spectrum of the AGBSYAZ017762 star (14.1 mag) between normal and glow-free reductions. The data were fully reduced and stacked with sky subtraction.

Raw data: [snr_normal_vs_flow_free.xlsx](snr_normal_vs_flow_free.xlsx)


## Plot signal-to-noise relative to each other

![Signal-to-noise of normal vs glow-free reduction](one_vs_other_box_plots.png)

Figure 1: Signal-to-noise of normal vs glow-free reduction of AGBSYAZ017762 star (14.1 mag).


Code:

```
from logbook.a2019.a03.a01_compare_snr_two_vs_three_observations.compare_snr_normal_vs_glow_free.box_plots import show_box_plots

show_box_plots()
```

## Plot signal-to-noise relative to each other (scatte plot)

![Signal-to-noise of normal vs glow-free reduction](one_vs_other.png)

Figure 2: Signal-to-noise of normal vs glow-free reduction of AGBSYAZ017762 star (14.1 mag). The error bars are two small to see at this scale. The colors indicate measurements from blue, green and red CCDs.

Plotting code: [plot_one_vs_other.py](plot_one_vs_other.py).




## Plot mean signal-to-noise relative to each other

![Mean signal-to-noise of normal vs glow-free reduction](one_vs_other_mean.png)

Figure 3: Mean Signal-to-noise of normal vs glow-free reduction of AGBSYAZ017762 star (14.1 mag). The colors indicate measurements from blue, green and red CCDs.

Plotting code: [one_vs_other_mean.py](one_vs_other_mean.py).


## Why signla-to-noise is RED is so high in normal redution (vs glow-free)


![Signal-to-noise of normal vs glow-free reduction](ccd3_normal_vs_glow_free.png)

Figure 1: Signal-to-noise of normal vs glow-free reduction of AGBSYAZ017762 star (14.1 mag).


