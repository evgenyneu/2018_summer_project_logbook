import pandas as pd
import matplotlib.pyplot as plt
from logbook.code.python.plot.compare_boxplots import compare_boxplots


def show_box_plots():
    df = pd.read_csv('logbook/a2019/a03/a01_compare_snr_two_vs_three_observations/compare_snr_normal_vs_glow_free/snr_normal_vs_flow_free.csv')

    name1 = 'Glow-free signal-to-noise n1'
    name2 = 'Normal signal-to-noise n2'

    CCDs = [1, 2, 3]

    ccds1 = []
    ccds2 = []

    for index, ccd in enumerate(CCDs):
        df_ccd = df.loc[df.CCD == CCDs[index]]

        ccds1.append(df_ccd[name1])
        ccds2.append(df_ccd[name2])

    grous = [ccds1, ccds2]

    compare_boxplots(groups=grous, subtitles=["Glow-free", "Normal"], title="Signal-to-noise of AGBSYAZ017762 star spectrum")