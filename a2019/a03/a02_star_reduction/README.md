# How to extract a single star

Here is how to extract a single star from combined the FITS files. 

The script requires the following files:

* `data/observe/30Oct18/reduce/288/noglow_sub_sky`. These FITS files were created previously by calling `reduce_single_star` function with `take_combined=noglow_sub_sky` parameter.

* `data/observe/good_sprectra/sun.fits`. The spetrum from the Sun.


## Change folder

Change current directory to the **parent** the `logbook` directory.

### Initialize IRAF

From parent directory of logbook, run

```
mkiraf
```

and select `xgterm`.

## Run pyraf

Initialize your conda environment (`iraf` in this example) and run pyraf:

```
conda activate iraf
pyraf
```

## Extract star

To extract a star AGBSYAZ029428, run the following code. Replace the star name to exacts a different star.

```
from logbook.code.python.jobs.reduce_data import extract_star

description = "Glow subtracted. No bias subtraction. No throughput calibration. Sky subtracted. Doppler shifted. Normalized."

extract_star(name="AGBSYAZ029428", take="noglow_sub_sky", description=description)
```

## Rate velocities

The script will open an fxcor window for you to evaluate the quality of fit.

* Look at the fit and press `q`.

* In terminal, enter a value from 0 (worst fit) to 5 (best fit) and press `<ENTER>`.

## Review normalization

The script will open an IRAF's `continuum` window. All you need to do is to look if continuum is reasonable and press `q`. 


### Continuum is not good

If continuum fit is not good, you can normalize it yourself from files located in `data/observe/30Oct18/reduce/288/noglow_sub_sky/stars/doppler_corrected/YOUR_STAR` directory. After the FITS files are normalized, copy them to `logbook/data/reduced_spectra/YOUR_STAR` directory, and create a text description file.