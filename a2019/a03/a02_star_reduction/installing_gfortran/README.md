## Installing gfortran 5.4.0

```
cd
mkdir gfortran
cd gfortran/
wget http://gfortran.meteodat.ch/download/x86_64/sources/releases/gcc-5.4.0.tar.xz
tar xf gcc-5.4.0.tar.xz
```


```
cd tmp/gcc-5.4.0-source/gcc-5.4.0
mkdir gcc-build

./contrib/download_prerequisites
cd gcc-build
../configure --prefix=$HOME/gcc-trunk --enable-languages=c,fortran
make
```

Did not work:


```
config.status: creating Makefile
config.status: error: cannot find input file: `testsuite/Makefile.in'
Makefile:12080: recipe for target 'configure-stage1-libiberty' failed
make[2]: *** [configure-stage1-libiberty] Error 1
make[2]: Leaving directory '/home/osboxes/gfortran/tmp/gcc-5.4.0-source/gcc-5.4.0/gcc-build'
Makefile:24857: recipe for target 'stage1-bubble' failed
make[1]: *** [stage1-bubble] Error 2
make[1]: Leaving directory '/home/osboxes/gfortran/tmp/gcc-5.4.0-source/gcc-5.4.0/gcc-build'
Makefile:910: recipe for target 'all' failed
make: *** [all] Error 2
```


## Try again without c

```
rm -rf gcc-build/
mkdir gcc-build
./contrib/download_prerequisites
cd gcc-build
../configure --prefix=$HOME/gcc-trunk --enable-languages=fortran
make
```

Get the [error](gfortran_make_error.txt)


