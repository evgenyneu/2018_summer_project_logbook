## Cleaning the glow

```
lineclean 27oct10020_glow.fits 27oct10020_glow_linecleaned.fits sample = "*" interactive=no low_reject = 5.0 high_reject=5.0 function=spline1 order=100 grow=1 naverage = -1
```

## Todo

1. Extract glow from raw frame: get the "raw glow".

1. Clean the raw glow with lineclean.

1. Subtract raw glow from raw files: "glow-free raw".

1. Replaces original raw files with "glow-free raw" files.

1. Reduce without subtracting bias.


## Reducing one frame


1. Extract raw glow

```
python export_glow.py -data_path='27oct10020.fits' -tram_path='27oct10022tlm.fits' -output_path='27oct10020_raw_glow.fits'
```


2. Clean with line-clean

```
lineclean 27oct10020_raw_glow.fits 27oct10020_raw_glow_linecleaned.fits sample = "*" interactive=no low_reject = 5.0 high_reject=5.0 function=spline1 order=100 grow=1 naverage = -1
```

3. Subtract raw glow from raw files: "glow-free raw"

