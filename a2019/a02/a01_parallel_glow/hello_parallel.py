import multiprocessing
from multiprocessing import Pool

# Example of doing parallel computation by spawning multiple processes using available CPU cores
# Based on https://stackoverflow.com/a/28463266/297131

def my_function(a):
    print(f"Processing element {a}...")

    # Do some computationally intensive work
    for i in range(50000000):
        pass

    return a


my_array = [1, 2, 3, 4]

cpus = multiprocessing.cpu_count()

with Pool(cpus) as pool:
    results = pool.map(my_function, my_array)

print(results)
