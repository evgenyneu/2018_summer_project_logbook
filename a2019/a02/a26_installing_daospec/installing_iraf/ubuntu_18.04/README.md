# Installing IRAF on Ubuntu 18.04 and newer

Ubuntu 18.04 includes changes that prevent it from using old library versions from [iraf.noao.edu](http://iraf.noao.edu) web site. Fortunately, iraf is now available as an apt-get package:

```
sudo apt-get install iraf-lib
sudo apt-get install iraf
```
