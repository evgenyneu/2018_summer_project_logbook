# Installing IRAF libraries

The following instruction shows how to install IRAF astronomical data manipulation software on Ubuntu Linux. Installation depends on Ubuntu version, which can be shown with `lsb_release -a` command.

* [Ubuntu version before 18.04](before_ubuntu_18.04)

* [Ubuntu version 18.04 and newer](ubuntu_18.04)