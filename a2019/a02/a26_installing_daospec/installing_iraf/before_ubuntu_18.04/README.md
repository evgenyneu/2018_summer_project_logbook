# Installing IRAF on Ubuntu before 18.04

The following instruction shows how to install IRAF astronomical data manipulation software on Ubuntu Linux version older than 18.04.


### Install C-shell

Install the C-shell prerequisite:

```
sudo apt-get install csh
```

### Create an IRAF directory

Create a directory for IRAF installation at a location of your choice. I've used a directory called `iraf` in my home directory:

```
cd
mkdir iraf
cd iraf
```


### Download

Download the Combo Linux 32/64-bit archive from IRAF's web site:

```
wget ftp://iraf.noao.edu/iraf/v216/PCIX/iraf-linux.tar.gz
```


### Extract the archive

```
tar xvzf iraf-linux.tar.gz
```

### Delete the archive

```
rm iraf-linux.tar.gz
```

### Install

```
./install
```

#### Installation choices

I've pressed `<ENTER>` for all the prompts:

* New iraf root directory (/home/evgenii/iraf): `<ENTER>`

* Default root image storage directory (/home/evgenii/.iraf/imdir/): `<ENTER>`

* Default root cache directory (/home/evgenii/.iraf/cache/): `<ENTER>`

* Local unix commands directory (/home/evgenii/.iraf/bin/): `<ENTER>`

* Enter default terminal type (xgterm): `<ENTER>`



```
========================================================================
=====================  Verifying System Settings  ======================
========================================================================

Hostname      = evgenii-VirtualBox    OS version    = Linux 4.15.0-39-generic
Architecture  = linux64               HSI arch      = linux64             
Old iraf root = /iraf/iraf            New iraf root = /home/evgenii/iraf  
Old imdir     = /iraf/imdir           New imdir     = /home/evgenii/.iraf/imdir/
Old cache     = /iraf/cache           New cache     = /home/evgenii/.iraf/cache/

Local bin dir = /home/evgenii/.iraf/bin
```

* Proceed (yes): `<ENTER>`


My [install output](install_output.txt).

### Test

To verify the IRAF installation type

```
cl
```


If everything went well, you will see a console shown in Fig. 1.

![An iraf console](iraf_console.png)

Figure 1: An IRAF console.

To quit the IRAF console, type

```
logout
```


Congrats, mission accomplished!



## Useful links

* [IRAF website](http://iraf.noao.edu).