# Installing DAOSPEC

The following instruction shows how to install DAOSPEC astronomical spectroscopy program on Ubuntu Linux after all other prerequisites [have been installed](a2019/a02/a26_installing_daospec).



### Get the source code

Obtain DAOSPEC source code file daospec.tar.gz from the authors: [stetson](http://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/en/community/STETSON/daospec/) or [pancino](http://www.arcetri.inaf.it/~pancino/daospec.html).

### Create a DAOSPEC directory

Create a directory for DAOSPEC installation at a location of your choice. I've used a directory called `daospec` in my home directory:

```
cd
mkdir daospec
cd daospec
```

### Copy the source code

Copy the `daospec.tar.gz` file to your daospec directory.


### Extract the archive

```
tar xvzf daospec.tar.gz
```


### Create a Make file

Create a `Makefile` with the following contents, depending on Ubuntu version, which can be shown with `lsb_release -a` command.

#### Makefile for Ubuntu older than 18.04

```
FCOMP = gfortran
FFLAGS = -Wall -Wextra -fPIC -fmax-errors=1 -O3 -march=native -ffast-math -funroll-loops

.SUFFIXES: .o .f
.f.o:
	$(FCOMP) -c $(FFLAGS) $<

default : daospec

daospec: daospec.o lnxsubs.o iosubs.o mathsubs.o bothsubs.o
	$(FCOMP) -o daospec daospec.o lnxsubs.o iosubs.o mathsubs.o bothsubs.o -L/usr/local/lib/ -lcfitsio -lplotsub -ldevices -lutils -L/usr/lib/x86_64-linux-gnu/ -lX11 -L/home/YOUR_USERNAME/iraf/bin.linux64/ -limfort -lsys -lvops -L/home/YOUR_USERNAME/iraf/unix/bin.linux64/ -los -lf2c -lcurl 

clean:
	rm -rf daospec *.o
```

where `YOUR_USERNAME` is replaced with your user name, which can be shown by running the command `whoami`.


#### Makefile for Ubuntu 18.04 and newer

```
FCOMP = gfortran
FFLAGS = -Wall -Wextra -fPIC -fmax-errors=1 -O3 -march=native -ffast-math -funroll-loops

.SUFFIXES: .o .f
.f.o:
  $(FCOMP) -c $(FFLAGS) $<

default : daospec

daospec: daospec.o lnxsubs.o iosubs.o mathsubs.o bothsubs.o
  $(FCOMP) -o daospec daospec.o lnxsubs.o iosubs.o mathsubs.o bothsubs.o -L/usr/local/lib/ -lcfitsio -lplotsub -ldevices -lutils -L/usr/lib/x86_64-linux-gnu/ -lX11 -L/usr/lib/iraf/bin/ -limfort -lsys -lvops -L/usr/lib/iraf/unix/bin/ -los -lf2c -lcurl

clean:
  rm -rf daospec *.o
```


### Install prerequisites

```
sudo apt-get install gfortran
sudo apt-get install libcurl3-dev
```

### Replace function names

Add underscores "_" to the calls of supermongo functions:


```
find . -name \*.f -exec sed -i "s/SM_GRAPHICS/SM_GRAPHICS_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_ERASE/SM_ERASE_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_LOCATION/SM_LOCATION_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_LIMITS/SM_LIMITS_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_BOX/SM_BOX_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_RELOCATE/SM_RELOCATE_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_DRAW/SM_DRAW_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_GFLUSH/SM_GFLUSH_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_ALPHA/SM_ALPHA_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_PUTLABEL/SM_PUTLABEL_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_XLABEL/SM_XLABEL_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_YLABEL/SM_YLABEL_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_DOT/SM_DOT_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_LWEIGHT/SM_LWEIGHT_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_EXPAND/SM_EXPAND_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_PTYPE/SM_PTYPE_/gI" {} \;
find . -name \*.f -exec sed -i "s/SM_DEVICE/SM_DEVICE_/gI" {} \;
```

### Change `MXSPEC` parameter

Open `daospec.f` file and change MXSPEC to 500 000:

```
			PARAMETER (MXSPEC=500 000, VELLIM=1000, MXPOLY=90, NOPT=16)
```

### Compile

```
make
```

My [make output](make_output.txt).

### Run DAOSPEC

Open xterm terminal

```
xterm
```

From xterm, run DAOSPEC

```
./daospec
```


### Test

To test installation, hit `<ENTER>` in DAOSPEC console, a new window will open with a picture of a maple leaf shown in Fig. 1.


![An initial DAOSPEC screen](dao_spec_initial_window.png)

Figure 1: A window presented by DAOSPEC on launch.


Next, in the `Input spectrum` prompt, type `test.fits` and press `<ENTER>` twice. During the test, you will see pictures similar to the one Fig. 2.

![A window in DAOSPEC during test](daospec_test.png)

Figure 2: A window in DAOSPEC during the test.



Congrats, jobs done!



## Useful links


* DAOSPEC authors: [stetson](http://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/en/community/STETSON/daospec/), [pancino](http://www.arcetri.inaf.it/~pancino/daospec.html).

* Original [documentation](http://www.arcetri.inaf.it/~pancino/Docs/daospec.pdf).