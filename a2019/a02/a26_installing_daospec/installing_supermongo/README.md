# Installing supermongo

The following instruction shows how to install [supermongo](https://www.astro.princeton.edu/~rhl/sm/) plotting program and its libraries on Ubuntu Linux.

### Get the source code

[Obtain](https://www.astro.princeton.edu/~rhl/sm/) supermongo source code: `sm2_4_26.tar.gz`.

### Extract the source code

```
sudo tar xvzf sm2_4_26.tar.gz -C /usr/local/src/
```

### Take ownership of the files

```
sudo chown -R YOUR_USERNAME /usr/local/src/sm2_4_26
```

where `YOUR_USERNAME` needs to be replaced by your user name, which can be found by typing `whoami`.


### Navigate to supermongo directory


```
cd /usr/local/src/sm2_4_26
```

### Prepare for setup:

```
./set_opts
```

### Select choises

* Choose a machine `linux`

* You are on a Linux using the cc -Wall -Dlinux -DNEED_SWAP compiler; is this OK?: `y`

* What debugging/optimisation flags do you want? `-g`

* What devices do you want to compile in? `X11`

* You have selected devices: X11; ok? `y`

* We can help you configure things for which the defaults are usually OK `y`

* Choose data type for vectors, "float" or "double"? `float`

* Choose length of longest macro `10000`

* Choose length for string-valued-vector's elements `40`

* Do you want to treat numbers starting 0 as octal? `n`

* Do you want to build and install the callable parser, libparser.a? `n`

* Top level path to install things? `/usr/local`

* The commands make install (and make installinfo) will install files as follows ... is this ok? `y`

* Do you want to install SM-mode for (gnu)emacs? `n`

* I am about to start modifying files, proceed? `y`


You will be presented with the message:

> You will be ready to make SM as soon as you have edited src/options.h to remove our copyright notice.

### Remove copyright notice

Open `src/options.h` in any text editor, remove the following copyright notice form the top and save the file.

```
#error You must edit options.h before SM will compile.
SM will not compile until you have removed these seven lines
By removing them you acknowledge that you have read our
copyright notices, and you confirm that you have a legal
copy of SM. If you are not sure of your status please send
email to either Robert Lupton (rhl@astro.princeton.edu) or
Patricia Monger (monger@mcmaster.ca).
```

### Compile

Build the programs:

```
make
```

My [make output](make_output.txt).

### Install

Copy the built tools and libraries to standard directories in your sysem:

```
make install
```

My [make install output](make_install_output.txt).


### Take ownership of the files

```
sudo chown YOUR_USERNAME -R /usr/local/lib/sm
sudo chown YOUR_USERNAME /usr/local/lib/libdevices.a
sudo chown YOUR_USERNAME /usr/local/lib/libplotsub.a
sudo chown YOUR_USERNAME /usr/local/lib/libutils.a
```

where `YOUR_USERNAME` needs to be replaced by your user name, which can be found by typing `whoami`.


### Test

In order to test that supermogo was installed properly, run

```
sm
```

It showed the prompt

> Can't find entry for xterm-256color in /usr/local/lib/sm/termcap
> Hello User, please give me a command
> :

#### Selecting device


```
device X11
```

You can select a different device instead of X11. The list of possible devices can be shown with `LIST DEVICE` command.

#### Show a graph

```
load demos square 20
```

You will see a graph shown in Fig. 1.

![A demo graph from supermongo](demo_graph.png)

Figure 1: A graph shown by `load demos square 20` in supermongo.


#### Jobs done

Congratulations, you successfully installed supermongo. To quit supermongo console, type

```
quit
```

## Useful links

* [Supermongo website](https://www.astro.princeton.edu/~rhl/sm/).


