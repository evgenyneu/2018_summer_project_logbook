# Installing DAOSPEC

Here are instructions on how to install DAOSPEC astronomical spectroscopy program on Ubuntu Linux.

1. [Install supermongo](installing_supermongo) plotting program.

1. [Install CFITSIO](installing_cfitsio) library for reading/writing FITS image files.

1. [Install IRAF](installing_iraf) astronomical data manipulation software.

1. Finally, [install DAOSPEC](installing_daospec).
