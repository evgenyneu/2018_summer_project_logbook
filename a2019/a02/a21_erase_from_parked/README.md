## 'Parked' glow extraction method

We changed the [glow reduction program](code/fortran/export_glow/) to use "PARKED" fibres for erasing other non-parked fibres, as shown in Figures 1 and 2.


![erasing_fibres_in_export_glow](erasing_fibres_in_export_glow_from_parked.png)

Figure 1: The method used in the program to erase the fibre pixels (yellow) with values from the parked pixels (green).


![Comparison of glow removal](glow_removal_from_parked.gif)

Figure 2: Three images containing frame 20 from 27 Oct p1 observation: raw image and extracted glow for old and new methods. The new method uses parked pixels to erase the fibre.




## Find y-positions of the fibres


```Python
from logbook.a2019.a02.a21_erase_from_parked.fibre_info import export_fibre_info
export_fibre_info()
```

The CSV files for tram positions: [fibres/](fibres):

