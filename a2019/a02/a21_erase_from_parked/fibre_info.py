from logbook.code.python.observations.tram import fibres_to_csv_ccd


def export_fibre_info():
    for ccd in range(1, 4):
        fibres_to_csv_ccd(ccd=ccd, day=27, take="p1", frame=20,
                          csv_path=f'logbook/a2019/a02/a21_erase_from_parked/fibres/27oct_p1_ccd{ccd}_fibres.csv')

        fibres_to_csv_ccd(ccd=ccd, day=29, take="p0", frame=24,
                          csv_path=f'logbook/a2019/a02/a21_erase_from_parked/fibres/29oct_p0_ccd{ccd}_fibres.csv')

        fibres_to_csv_ccd(ccd=ccd, day=29, take="p1_A", frame=19,
                          csv_path=f'logbook/a2019/a02/a21_erase_from_parked/fibres/29oct_p1_A_ccd{ccd}_fibres.csv')