## Implementing "3x11 median" glow removal method

We extracted glow using the new method (see Figures 1-3).

![3x5 vs 3x11 glow removal](3x5_vs_3x11_glow_removal.gif)

Figure 1: Comparison of spectra that was fully reduced using glow reduction methods: "3x5" and "3x11". The spectra are from frame 20 of 27 Oct p1 observation, CCD 1.


![Raw image and the extracted glow](glow_removed_source_box_interpolated.gif)

Figure 2: The glow extracted form the row image with fibre pixel replaced by the values of the pixels below and above them using the 3x11 interpolation method.


![Raw image and the extracted glow](glow_removed_source_box_interpolated_zoomed.gif)

Figure 3: The zoomed-in view of the glow extracted form the raw image using 3x11 interpolation method.


## Analyzing spectra during reduction


### 1. Raw data: 27oct10020.fits

Let's reduce an file 27oct10020.fits with 2DfDr using raw spectrum from fibre 381 (y=3924) shown in Fig. 5.

![Raw spectra](reducing/1_raw.png)

Figure 5: Raw spectrum from the 27oct10020.fits file.

**Raw signal**: 378 ± 7.4.


### 2. Subtracting glow: 27oct10020.fits ⇾ 27oct10020.fits

In this step [we subtract](a2019/a02/a05_fortran_python_benchmark/removing_glow.md) the glow (Fig. 6) from the raw spectrum (Fig 5).

![Raw glow](reducing/1_raw_glow.png)

Figure 6: Spectrum of the glow extracted from fram 20 of Oct 27 p1 observation.

**Bias signal**: 331.7 ± 0.7.



The resulting spectrum from the 27oct10020.fits file is shown in Fig 7.

![Glow subtracted](reducing/2_glow_subtracted.png)

Figure 7: Glow-subtracted spectrum.

**Glow-free 27oct10020.fits signal**: 48 ± 6.



## 3. Extracting Gaussian profile: 27oct10020im.fits ⇾ 27oct10020ex.fits

Extracting the Gaussian profile of the fibre shown in Fig. 8.

![Cross section of the fibre](reducing/3_ex_line_profile.png)

Figure 8: Intensity of the cross-section of the fibre 381 from 27oct10020im.fits file. X-axis is the y-coordinate in the FITS image, and Y-axis is the intensity.


The result of extraction is shown in Fig. 9.

![Extracted intensity form the Fibre](reducing/3_ex_gaussian_profile_extracted.png)

Figure 9: Gaussian profile intensity extracted from fibre 381 into the 27oct10020ex.fits file.

**27oct10020ex.fits signal**: 242 ± 18.


## 4. Dividing by the flat field 27oct10020ex.fits ⇾ 27oct10020red.fits

Dividing by flat field won't change the signal, since flat field is near 1 with small uncertainty.



## 5. Subtracting the sky: 27oct10020ex.fits ⇾ 27oct10020red.fits

The final operation is subtraction of the combined sky shown in Fig. 10 from extracted intensity shown in Fig. 9 (divided by flat).

![The combined sky](reducing/6_sky.gif)

Figure 10: The combined sky extracted from 27oct10020.fits.

**Combined sky signal**: -1.0 ± 3.


### The reduced spectrum: 27oct10020red.fits

The final fully reduced spectrum is shown in Fig. 11.

![The reduced spectrum](reducing/7_red_sky_subtracted.png)

Figure 11: The fully reduced spectrum of a single frame, from 27oct10020red.fits file. The signal to noise is measured in range: 4857.5 - 4860.0 Å.

**27oct10020red.fits signal**: 222 ± 18.


