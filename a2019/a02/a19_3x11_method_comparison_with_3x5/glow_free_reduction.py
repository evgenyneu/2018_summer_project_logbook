from logbook.code.python.jobs.reduce_data import start_reduction


def glow_free_reduction(name):
    """
    Reduce the data.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    """

    # Observations that will be combined
    observations = [
            {"day": 27, "take": "p1_glow_free"},
            {"day": 29, "take": "p0_glow_free"},
            {"day": 29, "take": "p1_A_glow_free"}
        ]

    description = "Subtracted glow from CCDs 1 and 3. Subtracted bias for CCDs 2. No throughput Calibration. Sky Subtracted. Doppler shifted. Normalized."

    start_reduction(observations=observations, name=name, day=30, take="glow_free", description=description)
