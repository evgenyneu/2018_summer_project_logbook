# Glow-free reduction of a pet star

We wanted to reduce the entire dataset.

## 1. Glow removal

First, we ran the following script to remove the glow from three observation sessions: 27 Oct p1, 29 Oct p0 and 29 Oct p1_A.
Note that glow was removed only from CCDs 1 and 3. CCD 2 was left as it is, since we assumed it was not affected by glow.


```
from logbook.a2019.a02.a18_3x11_method_mass_glow_removal.mass_glow_removal import mass_glow_removal

mass_glow_removal()
```

## 2. Reducing with 2DfDr

Next, we used 2DfDr to fully reduce the data, with following settings:

* Check "General > Subtract Bias Frame" for CCD 2 observations only (for CCD 1 and CCD 3 the bias has already been subtracted with glow removal).

* Uncheck "Sky > Throughput calibrate".

* Check "Plots > Generate plots of combined sky".

## 3. Extract for star 'AGBSYAZ017762'

Finally, we extract the spectrum for our pet star, stack the reductions, doppler correct and normalize the spectrum:

```
from logbook.a2019.a02.a19_3x11_method_comparison_with_3x5.glow_free_reduction import glow_free_reduction

glow_free_reduction(name="AGBSYAZ017762")
```

### Fixing normalization

Automatic normalization did not work well for CCD3, it choose continuum higher than was need for wavelength above 6700 A.

* cd to `data/observe/30Oct18/reduce/288/glow_free/stars/doppler_corrected/AGBSYAZ017762`

* Run pyraf

```
continuum ccd3.fits ccd3_normalized.fits order=3 markrej=no grow=0 high_reject=0 low_reject=1.2 function=spline3 niterat=10 override=yes
```

* Then pressed `s` and `s` again to exclude select the entire range of wavelength except the last part with the peak.

* Pressed `f` to refit.

* copy `ccd3_normalized.fits` to `data/observe/30Oct18/reduce/288/glow_free/stars/normalized/AGBSYAZ017762/ccd3.fits`

* copy `ccd3_normalized.fits` to `data/observe/reduced/AGBSYAZ017762/2019-02-19_11-18_ccd3.fits`


## TODO

* Extract glow from CCD2 and compare it with one without glow extraction.

* Fix the lineclean glitch.

* CCD 2.


## Find y position of fibre

```
from astropy.io import fits

hdul = fits.open('/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_glow_free/ccd1/27oct10022tlm.fits')

print(hdul[0].data[300, 1000])
``

