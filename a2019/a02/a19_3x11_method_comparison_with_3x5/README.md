# Comparing signal-to-noise of "3x5" and "3x11" methods

Here we measure the signal-to-noise of the five stacked fully reduced frames of 381 fibre (27oct1_combined.fits file) using two glow extraction methods - "3x5" and "3x11".

Raw data: [3_by_11_signal_to_noise.xlsx](3_by_11_signal_to_noise.xlsx)


## Plot signal-to-noise relative to each other

We can see form Fig. 2 signal-to-noise of the "3x11" method is mostly higher than that of the "3x5" method.

![Signal-to-noise of two methods relative to each other](n1_vs_n2.png)

Figure 2: Signal-to-noise of two methods of glow removal ("3x5" and "3x11") relative to each other. The error bars are two small to see at this scale.

Plotting code: [plot_n1_vs_n2.py](plot_n1_vs_n2.py).

## Our statistic

We will look at the difference of signal to noise values "3x11" minus "3x5".


## Confidence interval

First, we verify the assumption that our values are normally distributed. Shapiro-Wilk normality test suggests the values are normally distributed with p-value of 0.955. Moreover, the quantile-quantile plot on Fig. 5 shows strong linear relation, further supporting the normality hypothesis.

![Quantile-quantile plot](qq_plot.png)

Figure 5: Normal quantile-quantile plot of the difference of signal-to-noise of two methods ("3x11" minus "3x5").



Next, we calculate confidence interval of the mean for 95% confidence level:

* 95% confidence interval: (0.69, 3.95).

Conclusion: There is 0.95 probability that our confidence interval contains true population mean.

Code: [stat_inference.py](stat_inference.py)



## Box plot of difference of signal-to-noise

![Box plot difference of signal-to-noise](difference_of_signal_to_noise.png)

Figure 6: Difference of signal-to-noise of two methods of glow removal ("3x11" minus "3x5").





## Statistical test

We want to test if the mean signal-to-noise of "3x11" method ($`\mu_a`$) is higher than that of the "3x5" method ($`\mu_b`$) using paired sample t-test. Our data are the difference of the paired measurements of the signal-to-noise of the two methods stored in "Difference of signal-to-noise n4 - n3, d" column from [3_by_11_signal_to_noise.xlsx](3_by_11_signal_to_noise.xlsx) spreadsheet.

### Hypotheses

Null hypothesis $`H_0: \mu_a = \mu_b`$.

Alternative hypothesis $`H_A: \mu_a > \mu_b`$.

Significance level: 95%.

### Verifying assumptions

* Dependent variable is continuous: yes.

* The observations are independent of one another: yes, since we made independent measurements of the noise levels in different parts of the spectrum.

* The dependent variable does not contain any outliers: true, we see no outliers on see Fig. 6.

* The dependent variable is approximately normally distributed: yes, see Fig 5.



### Results and conclusion

The p-value of the test is 0.00521, which allows us to reject the null hypothesis $`H_0`$. In other words, if we assume that the two methods have the same mean signal-to-noise, then there is 0.00521 probability that we would observe our data purely by chance. Since this probability is smaller than 0.05, we conclude that our data show statistically significant evidence that mean signal-to-noise of the "3x11" method is higher than that of the "3x5" method.

Thus, we conclude the "3x11" method is preferable to the "3x5" method.

Code: [stat_inference.py](stat_inference.py)

## Glow-free reduction of a pet star

See details [here](glow_free_reduction.md).


