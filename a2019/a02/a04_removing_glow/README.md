## Reducing one frame


1. Extract raw glow

```
python export_glow.py -data_path='27oct10020.fits' -tram_path='27oct10022tlm.fits' -output_path='27oct10020_raw_glow.fits'
```


2. Clean with line-clean

```
lineclean 27oct10020_raw_glow.fits 27oct10020_raw_glow_linecleaned.fits sample = "*" interactive=no low_reject = 5.0 high_reject=5.0 function=spline1 order=100 grow=1 naverage = -1
```

3. Subtract raw glow from raw files: "glow-free raw"


```
python subtract.py -first_path='27oct10020.fits' -second_path='27oct10020_raw_glow_linecleaned.fits' -output_path='27oct10020_glow_free.fits'
```

4. Export fits to GIF

```
export 27oct10020.fits 27oct10020 format="gif"
export 27oct10020_glow_free.fits 27oct10020_glow_free format="gif"
```



![Raw image with and without glow](raw_with_and_without_glow.gif)

Figure 1: The raw image with and without glow from 27 Oct p1 observation, frame 20.


## TODO

* Fix the zero cropping.

* Reduce the glow free frame #20.

* Try use the glow as Dark frame in 2DfDr. Check source code to see why it fails.

* Use glow frame as BIAS. Change the object name to Bias.


