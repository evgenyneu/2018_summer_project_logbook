## Remove linecleaning from glow reduction script

The linecleaning of glow produces unwanted spikes at the right edge of the images (at overscan region). 


### 1. Remove glow from CCD1 and CCD3 only

```
from logbook.a2019.a02.a22_reduce_pet_star.mass_glow_removal import mass_glow_removal

mass_glow_removal(take_output_suffux="noglow_ccd1_3", exclude_ccds=[2])
```

#### 2. Reduce with 2DfDr

**Settings for CCD 1 and 3:**

* Do NOT check "Subtract bias frame".

* Keep "Thoughput callibrate".

* Uncheck "Subtract Sky".



**Settings for CCD 2:**

* Check "Subtract bias frame".

* Keep "Thoughput callibrate".

* Keep "Subtract Sky".


Through put callibraction was not done for CCD 1 and 2. From the log:

> No sky lines were found therefore not correcting for throughput.

For CCD 3:

> WARNING:  12 unexpected bad throughputs! Report written in Bad_Throughputs.txt. Probably due to bad pixels near sky. Try turning off CR rejection.

> 3  of  128  sky lines used for throughput calibration.


#### Throughput calibration analysis 

The throughput map for CCD 3 is shown on Fig 1.

![Throughput map CCD 3](throughput_27_oct_p1_noglow_ccd3.gif)

Figure 1: The throughput map of CCD 3 for 27 Oct p1 observation with glow removed.


Listing 1: Throughput calibration code from from file `fib_thput.F95` in 2DfDr.

```Fortran
! Divide Spectra for each fibre by the corresponding throughput estimate
DO I=1,NFIB
  SCALE = 0.0 ! Rob Sharp insists that if Throughput is bad 
              !that signals should be set to zero
  IF(THPUT_VEC(I) .NE. VAL__BADR) THEN
     SCALE = 1.0/THPUT_VEC(I)
  ENDIF
  WHERE (SPEC_ARR(:,I) .NE. VAL__BADR) SPEC_ARR(:,I) = SPEC_ARR(:,I)*SCALE
  WHERE (SVAR_ARR(:,I) .NE. VAL__BADR) SVAR_ARR(:,I) = SVAR_ARR(:,I)*SCALE**2
ENDDO
```


The code doing the throughput calibration is shown in Listing 1. We can see from the code that the signal is divided by the fibre throughput scale. On Fig. 1 we see that scales of some individual fibres may vary from 0.6 to 1.4. This will make the signal from the same fibre vary by the factor of larger than two between different frames of the same session. 


On Fig 2. we compare the reduced signal for fibre 5 (star RGBSYAZ021904) between reductions with and without throughput calibration. We can see that throughput calibration erased the signal from the star completely. This happened because fibre 5 was marked as 'bad throughput' and its signal was set to 0.0 (see Rab Sharp commend in Listing 1.). 

### Throughput calibration conclusion

Our throughput calibration only used three out of 128 sky lines. This could be the reason of high variation of throughput between frames (Fig. 1). This variation may introduce factor of two variation in signal between different frames, which does not seem to be sensible. Moreover, throughput calibration erased the signal of some stars because some fibres were marked to have "bad throughput". Besides, throughput calibration only worked for CCD 3. Considering these issues, we suggest to run reduction without throughput calibration.

![Throughput map CCD 3](ccd3_fib_5_throughput_on_and_off.png)

Figure 2: The reduced frame 16 of CCD 3 form 27 Oct p1 observation with throughput calibration on (top) and off (bottom).


## Remove glow from CCD1 and CCD3 only

### 1. Remove glow

```
from logbook.a2019.a02.a22_reduce_pet_star.mass_glow_removal import mass_glow_removal

mass_glow_removal(take_output_suffux="noglow_ccd1_3", exclude_ccds=[2])
```


### 2. Reduce with 2DfDr

**Settings for CCD 1 and 3:**

* Do NOT check "Subtract bias frame".

* Uncheck "Thoughput callibrate".

* Uncheck "Subtract Sky".



**Settings for CCD 2:**

* Check "Subtract bias frame".

* Uncheck "Thoughput callibrate".

* Keep "Subtract Sky".


### 3. Reduce star 'AGBSYAZ017762'

Finally, we extract the spectrum for our pet star, stack the reductions, doppler correct and normalize the spectrum:

```
from logbook.a2019.a02.a22_reduce_pet_star.single_star_reduction import reduce_single_star

description = "Subtracted glow from CCDs 1 and 3 only. Bias subtracted only for CCD 2. No throughput Calibration. Sky Subtracted only in CCD 2. Doppler shifted. Normalized."

reduce_single_star(name="AGBSYAZ017762", take="noglow_ccd1_3", description=description)
```


## Remove glow from all CCDs

### 1. Remove glow

```
from logbook.a2019.a02.a22_reduce_pet_star.mass_glow_removal import mass_glow_removal

mass_glow_removal(take_output_suffux="noglow_all_ccds", exclude_ccds=[])
```

### 2. Reduce with 2DfDr

**Settings for all CCDs:**

* Do NOT check "Subtract bias frame".

* Uncheck "Thoughput callibrate".

* Uncheck "Subtract Sky".


### 3. Reduce star 'AGBSYAZ017762'

Finally, we extract the spectrum for our pet star, stack the reductions, doppler correct and normalize the spectrum:

```
from logbook.a2019.a02.a22_reduce_pet_star.single_star_reduction import reduce_single_star

description = "Subtracted glow from all CCDs. No bias subtraction. No throughput calibration. No sky subtraction. Doppler shifted. Normalized."

reduce_single_star(name="AGBSYAZ017762", take="noglow_all_ccds", description=description)
```

The reduction finished in error:

```
Normalizing the spectrum:
Killing IRAF task `continuum'
Traceback (innermost last):
  File "<console>", line 1, in <module>
  File "./logbook/a2019/a02/a22_reduce_pet_star/single_star_reduction.py", line 21, in reduce_single_star
    start_reduction(observations=observations, name=name, day=30, take=take, description=description)
  File "./logbook/code/python/jobs/reduce_data.py", line 279, in start_reduction
    interactive=interactive_normalization)
  File "./logbook/code/python/observations/normalize_spectrum.py", line 78, in normalize_all_ccds
    function=function, niterat=niterat, interactive=interactive)
  File "./logbook/code/python/observations/normalize_spectrum.py", line 46, in normalize
    interactive=interactive)
stsci.tools.irafglobals.IrafError: Error running IRAF task continuum
IRAF task terminated abnormally
ERROR (1, "With range specification for `lines or bands'")
```

Fixed the correction by making paths smaller, since IRAF's commands fail with long paths (renamed directory `opservations` to `observe`).


## Mean skies

### Mean skies for 27 Oct 2018 p1 observation

The box plots of the mean sky levels are shown on Fig. 2.

```
from logbook.a2019.a02.a22_reduce_pet_star.sky_level_all import show_sky_levels

ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd1/27oct1_combined.fits"

ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd2/27oct2_combined.fits"

ccd3 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd3/27oct3_combined.fits"

title = "Mean sky levels for 27 Oct 2018 p1\nobservation with glow subtraction"

show_sky_levels(fits_path_ccds=[ccd1, ccd2, ccd3], title=title)
```

![Average sky across CCDs](mean_sky_27_oct_p1_glow_subtracted.png)

Figure 2: Mean sky levels for 27 Oct 2018 p1 observation with glow subtraction.



### Mean skies for Hermes 2014 observation

The box plots of the mean sky levels for Hermes 2014 observation are shown on Fig. 3. Comparing with Fig. 2 we can see that sky means for CCD 1 and 2 are similar for two observation. The Hermes 2014 observation seems to have larger spread of sky means in CCD 3 than the 2018 observation.


```
from logbook.a2019.a02.a22_reduce_pet_star.sky_level_all import show_sky_levels

ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd1/22aug1_combined.fits"

ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd2/22aug2_combined.fits"

ccd3 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd3/22aug3_combined.fits"

title = "Mean sky levels for 22 Aug 2014 observation"

show_sky_levels(fits_path_ccds=[ccd1, ccd2, ccd3], title=title)
```


![Average sky across CCDs](mean_sky_hermes_2014.png.png)

Figure 3: Mean sky levels for 22 Aug 2014 observation.


### Comparing 2014 and 2018 observations

The comparison of mean sky levels between two observations is shown on Fig 4.

![Average sky across CCDs](mean_skies_2014_2018_3.png)

Figure 4: Comparing mean sky levels for 22 Aug 2014 and 27 Oct 2018 observations. Some outliers are outside the range of the plot.


Plotting code:


```
from logbook.a2019.a02.a22_reduce_pet_star.sky_level_all import sky_level_compare_observations

ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd1/27oct1_combined.fits"

ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd2/27oct2_combined.fits"

ccd3 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd3/27oct3_combined.fits"

observation1 = [ccd1, ccd2, ccd3]

ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd1/22aug1_combined.fits"

ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd2/22aug2_combined.fits"

ccd3 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd3/22aug3_combined.fits"

observation2 = [ccd1, ccd2, ccd3]

subtitles = ["27 Oct 2018 p1", "22 Aug 2014"]

sky_level_compare_observations(paths1=observation1, paths2=observation2, title="Mean sky levels", subtitles=subtitles)
```


### Comparing with sky subtracted

On Fig. 5 we compare the mean sky levels for 27 Oct 2018 p1 observation with and without sky subtraction.

![Average sky across CCDs](mean_sky_subtracted_and_not_2.png)

Figure 5: Comparing mean sky levels for 27 Oct 2018 p1 observation without (left) and with (right) sky subtraction. Some outliers are outside the range of the plot.


Plotting code:


```
from logbook.a2019.a02.a22_reduce_pet_star.sky_level_all import sky_level_compare_observations

ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd1/27oct1_combined.fits"

ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd2/27oct2_combined.fits"

ccd3 = "/Users/evgenii\/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd3/27oct3_combined.fits"

observation1 = [ccd1, ccd2, ccd3]

ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds_subtract_sky/ccd1/27oct1_combined.fits"

ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds_subtract_sky/ccd2/27oct2_combined.fits"

ccd3 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds_subtract_sky/ccd3/27oct3_combined.fits"

observation2 = [ccd1, ccd2, ccd3]

subtitles = ["Sky not subtracted", "Sky subtracted"]

sky_level_compare_observations(paths1=observation1, paths2=observation2, title="Mean sky levels for 27 Oct 2018 p1 observation", subtitles=subtitles)
```


<!-- #### Extract star AGBSYAZ017762, stack, doppler correct, normalize


```
from logbook.a2019.a02.a21_analyzing_skies.glow_free_reduction2 import glow_free_reduction

description = "Subtracted glow from CCDs 1 and 3. Subtracted bias for CCDs 2. No throughput Calibration. Sky Subtracted. Doppler shifted. Normalized."

glow_free_reduction(name="AGBSYAZ017762", take="glow_free", description=description)
```



### Remove glow from all CCDs

We repeat the reduction, but this time we extract glow from all CCDs:

```
from logbook.a2019.a02.a21_analyzing_skies.mass_glow_removal import mass_glow_removal_all_ccds

mass_glow_removal_all_ccds()
```

Then we reduce the pet star:

```
from logbook.a2019.a02.a21_analyzing_skies.glow_free_reduction2 import glow_free_reduction

description = "Subtracted glow from *all CCDs*. No bias subtraction. No throughput Calibration. Sky Subtracted. Doppler shifted. Normalized."

glow_free_reduction(name="AGBSYAZ017762", take="all_glow_free", description=description)
```
 -->

