from logbook.code.python.jobs.reduce_data import start_reduction


def reduce_single_star(name, take, description):
    """
    Reduce the data.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    """

    # Observations that will be combined
    observations = [
            {"day": 27, "take": f"p1_{take}"},
            {"day": 29, "take": f"p0_{take}"},
            {"day": 29, "take": f"p1_A_{take}"}
        ]

    start_reduction(observations=observations, name=name, day=30, take=take, description=description)
