from logbook.code.python.jobs.remove_glow_job import remove_glow


def mass_glow_removal(take_output_suffux, exclude_ccds):
    remove_glow(day=27, take="p1", science_frames=[16, 17, 18, 19, 20], flat_arc_frames=[21, 22],
                take_output_suffux=take_output_suffux, exclude_ccds=exclude_ccds)

    remove_glow(day=29, take="p0", science_frames=[24, 25, 26, 27, 28], flat_arc_frames=[29, 30],
                take_output_suffux=take_output_suffux, exclude_ccds=exclude_ccds)

    remove_glow(day=29, take="p1_A", science_frames=[19, 20, 21, 22, 23], flat_arc_frames=[17, 18],
                take_output_suffux=take_output_suffux, exclude_ccds=exclude_ccds)

    print("Done •ᴥ•")