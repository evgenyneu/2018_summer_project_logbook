from .sky_level_all import show_sky_levels
import pytest


@pytest.mark.skip(reason="Skipping since it shows a plot")
def test_show_sky_levels():
    ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd1/27oct1_combined.fits"

    ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd2/27oct2_combined.fits"

    ccd3 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd3/27oct3_combined.fits"

    show_sky_levels(fits_path_ccds=[ccd1, ccd2, ccd3],
                    title="Mean sky levels for 27 Oct 2018 p1\nobservation with glow subtraction")