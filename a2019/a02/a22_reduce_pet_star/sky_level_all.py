from logbook.code.python.observations.mean_fibre import calculate_mean
import matplotlib.pyplot as plt


def show_sky_levels(fits_path_ccds, title):
    ccds = []

    for index, fits_path in enumerate(fits_path_ccds):
        df = calculate_mean(fits_path=fits_path, object_starts_with="SKY")
        ccds.append(df["Mean"].values)

    labels = [f"CCD {ccd}" for ccd in range(1, len(fits_path_ccds) + 1)]
    plt.boxplot(ccds, labels=labels)
    plt.title(title)
    plt.show()