import pandas as pd
import matplotlib.pyplot as plt

ccd1 = [12, 17, -17, 27, 24, -3, 9]
ccd2 = [7, 16, 20, 0, 15, 8, 19]
ccd3 = [35, 0, 65, 60, 20, 22, 20]
# Load data
df = pd.DataFrame({
        "ccd_1_sky_avg": ccd1,
        "ccd_2_sky_avg": ccd2,
        "ccd_3_sky_avg": ccd3
    })

# Check for outliers
df.boxplot(["ccd_1_sky_avg", "ccd_2_sky_avg", "ccd_3_sky_avg"])
plt.show()
