from logbook.code.python.observations.mean_fibre import calculate_mean
import matplotlib.pyplot as plt


def sky_level_compare_observations(paths1, paths2, title, subtitles):

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(9, 4), sharey=True)

    labels = [f"CCD {ccd}" for ccd in range(1, len(paths1) + 1)]

    colors = ['dodgerblue', 'lightgreen', 'orangered']

    for plot_id, paths in enumerate([paths1, paths2]):
        ccd_means = []

        for fits_path in paths:
            df = calculate_mean(fits_path=fits_path, object_starts_with="SKY")
            ccd_means.append(df["Mean"].values)

        bplot = axes[plot_id].boxplot(ccd_means, patch_artist=True, labels=labels, widths=0.7)

        for patch, color in zip(bplot['boxes'], colors):
            patch.set_facecolor(color)

        axes[plot_id].set_title(subtitles[plot_id])

    fig.suptitle(title, size='xx-large', y=0.99)
    fig.subplots_adjust(wspace=0)
    plt.show()