from .sky_level_compare_observations import sky_level_compare_observations
import pytest


@pytest.mark.skip(reason="Skipping since it shows a plot")
def test_show_sky_levels():
    ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd1/27oct1_combined.fits"

    ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd2/27oct2_combined.fits"

    ccd3 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/27Oct18/reduce/288/p1_noglow_all_ccds/ccd3/27oct3_combined.fits"

    observation1 = [ccd1, ccd2, ccd3]

    ccd1 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd1/22aug1_combined.fits"

    ccd2 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd2/22aug2_combined.fits"

    ccd3 = "/Users/evgenii/Documents/Physics/SummerProject/data/observe/2014_hermesdata/ccd3/22aug3_combined.fits"

    observation2 = [ccd1, ccd2, ccd3]

    subtitles = ["27 Oct 2018 p1", "22 Aug 2014"]

    sky_level_compare_observations(paths1=observation1, paths2=observation2, title="Mean sky levels", subtitles=subtitles)