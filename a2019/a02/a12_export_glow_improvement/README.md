# "3x3 random" method of glow extraction

Use a new method of extracting glow described in Fig 1a.


![Raw spectra](erasing_fibres_in_export_glow.png)

Figure 1: The "3x3 random" method used in the program to erase the fibre pixels (yellow) with values from the glow pixels (blue).




## Analyzing spectra during reduction


### 1. Raw data: 27oct10020.fits

Let's reduce an file 27oct10020.fits with 2DfDr using raw spectrum from fibre 381 (y=3924) shown in Fig. 1.

![Raw spectra](reducing/1_raw.png)

Figure 1: Raw spectrum from the 27oct10020.fits file.

**Raw signal**: 378 ± 7.4.



### 2. Subtracting glow: 27oct10020.fits ⇾ 27oct10020.fits

In this step [we subtract](a2019/a02/a05_fortran_python_benchmark/removing_glow.md) the glow (Fig. 2) from the raw spectrum (Fig 1).

![Raw glow](reducing/1_raw_glow.png)

Figure 2: Spectrum of the glow extracted from fram 20 of Oct 27 p1 observation.

**Bias signal**: 329 ± 4.3.



The resulting spectrum from the 27oct10020.fits file is shown in Fig 3.

![Bias subtracted](reducing/2_im_glow_subtracted.png)

Figure 3: Glow-subtracted spectrum.

**Glow-free 27oct10020.fits signal**: 47.7 ± 8.3.


## 3. Extracting Gaussian profile: 27oct10020im.fits ⇾ 27oct10020ex.fits

Extracting the Gaussian profile of the fibre shown in Fig. 4.

![Cross section of the fibre](reducing/3_ex_line_profile.png)

Figure 4: Intensity of the cross-section of the fibre 381 from 27oct10020im.fits file. X-axis is the y-coordinate in the FITS image, and Y-axis is the intensity.


The result of extraction is shown in Fig. 5.

![Extracted intensity form the Fibre](reducing/3_ex_gaussian_profile_extracted.png)

Figure 5: Gaussian profile intensity extracted from fibre 381 into the 27oct10020ex.fits file.

**27oct10020ex.fits signal**: 241 ± 29.

The error 29 here is lower than 40 from the previous ['single pixel' method](/a2019/a02/a08_understanding_reduction/understanding_glow_removal.md) of glow removal.



## 4. Dividing by the flat field 27oct10020ex.fits ⇾ 27oct10020red.fits

Dividing by flat field won't change the signal, since flat field is near 1 with small uncertainty.




## 5. Subtracting the sky: 27oct10020ex.fits ⇾ 27oct10020red.fits

The final operation is subtraction of the combined sky shown in Fig. 7 from extracted intensity shown in Fig. 5 (divided by flat).

![The combined sky](reducing/6_sky.gif)

Figure 7: The combined sky extracted from 27oct10020.fits.

**Combined sky signal**: -4.0 ± 8.


### The reduced spectrum: 27oct10020red.fits

The final fully reduced spectrum is shown in Fig. 8.

![The reduced spectrum](reducing/7_red_sky_subtracted.png)

Figure 8: The fully reduced spectrum of a single frame, from 27oct10020red.fits file. The signal to noise is measured in range: 4857.5 - 4860.0 Å.

**27oct10020red.fits signal**: 223 ± 27.

The noise 27 is lower than 43 from the previous ['single pixel' method](/a2019/a02/a08_understanding_reduction/understanding_glow_removal.md) of glow removal. However, this could be due to random fluctuations of noise. We need to compare multiple noise measurements between the two methods to see if the new method gives better result.




