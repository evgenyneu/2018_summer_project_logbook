# Comparison of two glow extraction methods

Here we measure the signal-to-noise of the same frame reduced using two glow extraction methods - single pixel and random 3x3 box - as shown in Fig 1. The spectrum from "one pixel" appears to have more noise.

![One pixel vs 3x3 random entire CCD1](one_pixel_vs_3_by_3_random/fibre381_entire_spectrum_ccd1.png)

Figure 1: Comparison of spectra that was fully reduced using glow reduction methods: "one pixel" (top) and "3x3 random" (bottom). The spectra are from frame 20 of 27 Oct p1 observation, CCD 1.


To use the same range on the y axis in splot we used:

* Pressed `:`.

* Entered `/ywindow 0 400`.

* Redrawn the window by pressing `r`.

## Measuring average signal, noise RMS and signal-to-noise

We measure average signal, noise RMS and signal-to-noise for the two spectra from Fig 1.

Raw data: [one_vs_3_by_3_random_signal_to_noise.xlsx](one_pixel_vs_3_by_3_random/one_vs_3_by_3_random_signal_to_noise.xlsx)





## Plot signal-to-noise relative to each other

We can see form Fig. 2 signla-to-noise of the "3x3 random" method is higher than that of the "one pixel" method.

![Signal-to-noise of two methods relative to each other](one_pixel_vs_3_by_3_random/n1_vs_n2.png)

Figure 2: Signal-to-noise of two methods of glow removal ("3x3 random" and "one pixel") relative to each other. The error bars are two small to see at this scale.

Plotting code: [plot_n1_vs_n2.py](one_pixel_vs_3_by_3_random/plot_n1_vs_n2.py).



## Plot difference of signal-to-noise  n2-1 vs signal-to-noise n1

![Difference of signal-to-noise (n2-n1) vs n1](one_pixel_vs_3_by_3_random/plot_n2_minus_n1_vs_n1.png)

Figure 3: Difference of signal-to-noise of two methods of glow removal "3x3 random" minus "one pixel" (y-axis) vs signal-to-noise  of "one pixel" method (x-axis).

Plotting code: [plot_n2_minus_n1_vs_n1.py](one_pixel_vs_3_by_3_random/plot_n2_minus_n1_vs_n1.py).

## Box plot of difference of signal-to-noise

![Box plot difference of signal-to-noise](one_pixel_vs_3_by_3_random/difference_of_signal_to_noise.png)

Figure 4: Difference of signal-to-noise of two methods of glow removal "3x3 random" minus "one pixel".

Plotting code: [box_plot_difference_signal_noise.py](one_pixel_vs_3_by_3_random/box_plot_difference_signal_noise.py).


## Descriptive statistics

* Mean difference of signal-to-noise: 2.9 ± 0.8.

Code: [descriptive_stats.py](one_pixel_vs_3_by_3_random/descriptive_stats.py)


## Statistical test

We want to test if the mean signal-to-noise of "3x3 random" method ($`\mu_a`$) is higher by two than that of the "one pixel" method ($`\mu_b`$) using paired sample t-test. Our data are the difference of the paired measurements of the signal-to-noise of the two methods stored in "Difference of signal-to-noise n2 - n1, d" column from [one_vs_3_by_3_random_signal_to_noise.xlsx](one_pixel_vs_3_by_3_random/one_vs_3_by_3_random_signal_to_noise.xlsx) spreadsheet.

### Hypotheses

Null hypothesis $`H_0: \mu_a = \mu_b + 2`$.

Alternative hypothesis $`H_A: \mu_a > \mu_b + 2`$.

Significance level: 95%.

### Verifying assumptions

* Dependent variable is continuous: yes.

* The observations are independent of one another: yes, since we made independent measurements of the noise levels in different parts of the spectrum.

* The dependent variable does not contain any outliers: yes, see Fig. 4.

* The dependent variable is approximately normally distributed: yes. Shapiro-Wilk normality test suggests the values are normally distributed with p-value of 0.654. Moreover, the quantile-quantile plot on Fig. 5 shows strong linear relation, further supporting the normality hypothesis.

![Quantile-quantile plot](one_pixel_vs_3_by_3_random/qq_plot.png)

Figure 5: Normal quantile-quantile plot of signal-to-noise of two methods of glow removal "3x3 random" minus "one pixel".


### Results and conclusion

The p-value of the test is 0.00250, which allows us to reject the null hypothesis $`H_0`$. In other words, if we assume that the difference of the mean signal-to-noise of the two methods is 2,then there is 0.00250 probability that we would observe our data purely by chance. Since this probability is smaller than 0.05, we conclude that our data show statistically significant evidence that mean signal-to-noise of the "3x3 random" method is higher by two than that of the "one pixel" method. Therefore, the "3x3 random" method is preferable to the "one pixel" method.

Code: [stat_inference.py](one_pixel_vs_3_by_3_random/stat_inference.py)





## TODO

* After I erase the fibre, calculate RMS for two rows (i.e. inside a fibre and outside). The RMS values should be the same. Do this in the glow extraction code and show warnings if RMS are different.

* Try algorithm with 3*5 median and interpolation. Try with fibre height 10.
