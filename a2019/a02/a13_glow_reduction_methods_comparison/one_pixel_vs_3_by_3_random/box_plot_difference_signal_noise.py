import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('one_vs_3_by_3_random_signal_to_noise.csv')
column = 'Difference of signal-to-noise n2 - n1, d'
df.boxplot(column)
plt.show()
