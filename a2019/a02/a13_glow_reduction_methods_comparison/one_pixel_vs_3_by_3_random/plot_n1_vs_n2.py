import pandas as pd
import matplotlib.pyplot as plt

xy_max = 12
df = pd.read_csv('one_vs_3_by_3_random_signal_to_noise.csv')
df_line = pd.DataFrame({'x': [0, xy_max], 'y': [0, xy_max]})
ax = None
ax = df_line.plot(ax=ax, x='x', y='y', color='g', legend=False)

xcolumn = 'One pixel signal-to-noise n1'
ycolumn = '3x3 random signal-to-noise n2'
xerr_column = 'Uncertainty u(n1)'
yerr_column = 'Uncertainty u(n2)'

ax = df.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, color='Red', s=10)

ax.errorbar(df[xcolumn], df[ycolumn], xerr=df[xerr_column], yerr=df[yerr_column],
            fmt='none', ecolor='Red', capsize=3, elinewidth=0.5, capthick=0.5)

plt.show()
