import pandas as pd
import matplotlib.pyplot as plt

xy_max = 12
df = pd.read_csv('one_vs_3_by_3_random_signal_to_noise.csv')
xcolumn = 'One pixel signal-to-noise n1'
ycolumn = 'Difference of signal-to-noise n2 - n1, d'
xerr_column = 'Uncertainty u(n1)'
yerr_column = 'Uncertainty of difference of signal-to-noise u(d)'

ax = df.plot.scatter(x=xcolumn, y=ycolumn, color='Red', s=10)

ax.errorbar(df[xcolumn], df[ycolumn], xerr=df[xerr_column], yerr=df[yerr_column],
            fmt='none', ecolor='Red', capsize=3, elinewidth=0.5, capthick=0.5)

plt.show()
