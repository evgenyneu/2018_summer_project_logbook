import pandas as pd

df = pd.read_csv('one_vs_3_by_3_random_signal_to_noise.csv')
column = 'Difference of signal-to-noise n2 - n1, d'


# Mean difference
mean = df[column].mean()
stdev = df[column].std()
print(f"Mean difference of signal-to-noise): {mean:0.01f} +/- {stdev:0.01f}")
