#
# Fortran make file
# Based on https://stackoverflow.com/a/35260489/297131
#

# --------------------------
# Fortran compiler
# --------------------------

FC = gfortran

#
# Debug (gfortran)
# -----------------
#
# This warns about undefined symbols, stops at the first error, turns on all debugging checks
# (bounds checks, array temporaries, ...) and turns on backtrace printing when something fails
# at runtime (typically accessing an array out of bounds).
#
# You can use -Werror to turn warnings into errors (so that the compilation stops
# when undefined symbol is used). With gfortran 4.4 and older,
# replace fcheck=all with -fbounds-check -fcheck-array-temporaries.
#
# Source: http://www.fortran90.org/src/faq.html
#

# FCFLAGS = -Wall -Wextra -fPIC -fmax-errors=1 -g -fcheck=all -fbacktrace

#
# Production (gfortran)
# -----------------
#
# This turns off all debugging options (like bounds checks) and turns on optimizing options
# (fast math and platform dependent code generation).
# One can also use -Ofast instead of -O3 -ffast-math.
#
# It still warns about undefined symbols, turns warnings into errors
# (so that the compilation stops when undefined symbol is used) and stops at the first error.
#
# Source: http://www.fortran90.org/src/faq.html
#

FCFLAGS = -Wall -Wextra -fPIC -Werror -fmax-errors=1 -O3 -march=native -ffast-math -funroll-loops


# --------------------------
# ifort compiler
# --------------------------

# FC = ifort

#
# Debug (ifort)
# -----------------
#
# Source: http://www.fortran90.org/src/faq.html
#

# FCFLAGS = -warn all -check all

#
# Production (ifort)
# -----------------
#
# Source: http://www.fortran90.org/src/faq.html
#

# FCFLAGS = -warn all -fast

# External libraries we are using
LIBRARIES = -lcfitsio -lcurl

# Find all source files, create a list of corresponding object files

PROGRAM = export_glow

# Find all source files, create a list of corresponding object files
SRCS=$(wildcard *.f90)
OBJS=$(patsubst %.f90,%.o,$(SRCS))

# Make the list of modules (all objects excluding the executable)
MODS=$(patsubst $(PROGRAM).f90,,$(SRCS))
MOD_OBJS=$(patsubst %.f90,%.o,$(MODS))

PRG_OBJ = $(PROGRAM).o

# Clean the suffixes
.SUFFIXES:

# Set the suffixes we are interested in
.SUFFIXES: .f90 .o

# make without parameters will make first target found.
default : $(PROGRAM)

# Compiler steps for all objects
$(OBJS) : %.o : %.f90
	$(FC) -c $(FCFLAGS) -o $@ $<

# Linker
$(PROGRAM) : $(OBJS)
	$(FC) $(FLFLAGS) $(LIBRARIES) -o $@ $^

debug:
	@echo "SRCS = $(SRCS)"
	@echo "OBJS = $(OBJS)"
	@echo "MODS = $(MODS)"
	@echo "MOD_OBJS = $(MOD_OBJS)"
	@echo "PROGRAM = $(PROGRAM)"
	@echo "PRG_OBJ = $(PRG_OBJ)"

clean:
	rm -rf $(OBJS) $(PROGRAM) $(patsubst %.o,%.mod,$(MOD_OBJS))

.PHONY: debug default clean

# Dependencies
# -------------------

# Main program depends on all modules
$(PRG_OBJ) : $(MOD_OBJS)

# Blocks and allocations depends on shared
# mod_blocks.o mod_allocations.o : mod_shared.o