program export_glow
use types, only: sp, i2
implicit none

call export_glow

contains

subroutine export_glow
! --------
! Creates a .fits image by keeping pixels between fibres and erasing the fibres.
!
! Command line arguments:
!  $ ./export_glow data.fits tram.fits output.fits
! where data.fits and tram.fits are the FITS files containing image data
! and tram.
! output.fits is the name of the output file that output.fits
! --------

integer, parameter:: max_fibres=400 ! Used for definining array dimension
integer, parameter:: max_x=5000 ! Used for definining array dimension
integer status, unit_tram, unit_data, readwrite, blocksize, nfound, naxes_tram(2), total_fibres
integer unit_output
integer hdutype, naxes_data(2), xmax, colnum, frow, felem, nelems, irow
character filename_tram*1024, filename_data*1024, record*80, length*80, comment*80, nullstr*1
character name*20, filename_output*1024
logical exact, anynull
integer n_nonparked ! Number of nonparked fibres
integer nonparked(max_fibres) ! Nonparked fibre indexes
integer x, i_fibre, fibre_index, next_fibre_index, group
integer nullval
real(sp) fibre_y(max_x, max_fibres), tram_y
integer, parameter:: fibre_thickness=15 ! Width of fibres in pixels
real(sp) tram_y_start, next_tram_y_start ! The y-coordinate of the start of the fibre
real(sp) tram_y_end ! The y-coordinate of the end of the fibre
integer(i2) image_data(max_x, max_x)
logical file_exists

! Check the command line arguments were provided
if(command_argument_count().ne.3)then
  print *, 'ERROR: three command line arguments required.'
  print *, 'Example:'
  print *, '$ export_glow data.fits tram.fits output.fits'
  stop
endif

! Read the arguments
call get_command_argument(1, filename_data)
call get_command_argument(2, filename_tram)
call get_command_argument(3, filename_output)

! Check if the output file already exists
inquire(file=filename_output, exist=file_exists)
if (file_exists) then
    print *,'ERROR: output file already exists: '//trim(filename_output)
  return
endif

! The STATUS parameter must always be initialized.
status=0

! Open the tram FITS file
! -----------------

! Get an unused Logical Unit Number to use to open the FITS file.
call ftgiou(unit_tram,status)

! Open the tram FITS file
readwrite=0
call ftopen(unit_tram,filename_tram,readwrite,blocksize,status)

! Open data file
! -----------------

! Get an unused Logical Unit Number to use to open the FITS file.
call ftgiou(unit_data,status)

! Open the FITS file, with read-only access.
readwrite=0
call ftopen(unit_data,filename_data,readwrite,blocksize,status)

! Determine the size of the tram image.
! ----------------

call ftgknj(unit_tram, 'NAXIS', 1, 2, naxes_tram, nfound, status)

! Check that it found both NAXIS1 and NAXIS2 keywords.
if (nfound .ne. 2)then
  print *,'READIMAGE failed to read the NAXISn keywords for tram.'
  return
end if

total_fibres = naxes_tram(2)

! Determine the size of the data image.
! ----------------

call ftgknj(unit_data, 'NAXIS', 1, 2, naxes_data, nfound, status)

! Check that it found both NAXIS1 and NAXIS2 keywords.
if (nfound .ne. 2)then
  print *,'READIMAGE failed to read the NAXISn keywords for data.'
  return
end if

! Find the width of the image
xmax = min(naxes_tram(1), naxes_data(1))

! Load the image data
group=1
nullval=0
call ftg2di(unit_data, group, nullval, max_x, naxes_data(1), naxes_data(2), image_data, anynull, status)

! Find the table containing information about fibres
! ----------------

! Move to the next extension
call ftmrhd(unit_data,1,hdutype,status)

call ftgsky(unit_data,'EXTNAME',1,80, record, length, comment, status)

if (record /= 'STRUCT.MORE.FIBRES' .and. record /= 'FIBRES') then
    print *, 'Can not find the fibres table.'
    return
endif

! Find the non-parked fibres
! ---------------------------

! FTGCNO gets the column number of the `NAME' column
exact=.false.
call ftgcno(unit_data, exact, 'NAME', colnum, status)

frow=1
felem=1
nelems=1
nullstr=' '

! Collect the indexes of nonparked fibres
n_nonparked = 0
do irow=1, total_fibres
    call ftgcvs(unit_data, colnum, irow, felem, nelems, nullstr, name, anynull, status)

    if (name /= 'PARKED') then
        n_nonparked = n_nonparked + 1
        nonparked(n_nonparked) = irow
    endif
end do

! Load the y coordinates of fibres
! -------------

group=1
nullval=0
call ftg2de(unit_tram, group, nullval, max_x, naxes_tram(1), naxes_tram(2), fibre_y, anynull, status)

tram_y_start = -1.0_sp

! Loop over the x values
do x=1, xmax
    ! Loop over fibres
    fibre_loop: do i_fibre=1, n_nonparked
        fibre_index = nonparked(i_fibre)

        ! Calculate the start and end y-coordinate of the fibre
        ! --------------------

        tram_y = fibre_y(x, fibre_index)

        if (abs(tram_y_start + 1.0_sp) <= epsilon(tram_y_start)) then
            tram_y_start = floor(tram_y - fibre_thickness / 2.0_sp)
        endif

        tram_y_end = ceiling(tram_y + fibre_thickness / 2.0_sp)

        ! Check if the current tram fibre overlaps with the next
        if (i_fibre < n_nonparked) then
            next_fibre_index = nonparked(i_fibre + 1)
            next_tram_y_start = floor( fibre_y(x, next_fibre_index) - fibre_thickness / 2.0_sp)

            if (tram_y_end >= next_tram_y_start) then
                ! The fibre is very close to the next one.
                ! Skip until the next fibre is further away, since we want to erase fibres
                ! with brightness values from regions far away from fibres.
                cycle fibre_loop
            endif
        endif

        ! Erase the fibre pixels
        image_data(x, int(tram_y_start): int(tram_y_end)-1) = image_data(x, int(tram_y_start))

        ! Use next fibre as starting point for the region we want to erase
        tram_y_start = -1.0_sp
    end do fibre_loop
end do

! Create the output fits file

! Get an unused Logical Unit Number to use to create the FITS file.
call ftgiou(unit_output, status)

! Create the output file
blocksize=1
call ftinit(unit_output, filename_output, blocksize, status)

! Copy the entire data file to the output file
call ftcpfl(unit_data, unit_output, 1, 1, 1, status)

! Move back to the primary array
call ftmahd(unit_output, 1, hdutype, status)

! Save the data array into the output fits file
group=1
nullval=0
call ftp2di(unit_output, group, max_x, naxes_data(1), naxes_data(2), image_data, status)

if (status .gt. 0) then
    call printerror(status)
    return
end if

! Close the fits files and free the unit numbers
call ftclos(unit_tram, status)
call ftfiou(unit_tram, status)

call ftclos(unit_data, status)
call ftfiou(unit_data, status)

call ftclos(unit_output, status)
call ftfiou(unit_output, status)

print *, 'Glow extracted successfully to '//trim(filename_output)

end subroutine

subroutine printerror(status)
! This subroutine prints out the descriptive text corresponding to the
! error status value and prints out the contents of the internal
! error message stack generated by FITSIO whenever an error occurs.

integer status
character errtext*30,errmessage*80

! Check if status is OK (no error); if so, simply return
if (status .le. 0) return

! Show error message
call ftgerr(status,errtext)
print *,'FITSIO Error Status =',status,': ',errtext

call ftgmsg(errmessage)
do while (errmessage .ne. ' ')
    print *,errmessage
    call ftgmsg(errmessage)
end do

end subroutine

end program