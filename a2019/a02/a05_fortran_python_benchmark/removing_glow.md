Yesterday, we removed the glow from frame 20, but the portion below 0 was cut out. We want to fix the glow subtraction script and prevent this cropping.

## Reducing one frame


1. Extract raw glow

```
./export_glow 27oct10020.fits 27oct10022tlm.fits 27oct10020_raw_glow.fits
```



2. Clean with line-clean

```
lineclean 27oct10020_raw_glow.fits 27oct10020_raw_glow_linecleaned.fits sample = "*" interactive=no low_reject = 5.0 high_reject=5.0 function=spline1 order=100 grow=1 naverage = -1
```

3. Subtract raw glow from raw files: "glow-free raw"


```
from logbook.code.python.observations.subtract import subtract

subtract(first_path='Scratchpad/27oct10020.fits', second_path='Scratchpad/27oct10020_raw_glow_linecleaned.fits', output_path='Scratchpad/27oct10020_glow_free.fits')
```

4. Replace ccd1/27oct10020.fits with 27oct10020_glow_free.fits.

5. Remove all other frames from ccd1, remove BIASCombined.fits. Unreduce all.

6. In 2DfDr do not check "Subtract BIAS frame". Check "Subtract Sky" and reduce. Uncheck "Throughput calibration".

## Comparing the original with glow-free spectrum

Comparing original and glow-free fully reduced single frame spectrum on Figures 1a, 1b, 1c. The original spectrum signal-to-noise is significantly higher than the glow-free spectrum.

Original: [27oct10020red.fits_original.zip](removing_glow/27oct10020red.fits_original.zip)

Glow-free: [27oct10020red.fits_glow_free.zip](removing_glow/27oct10020red.fits_glow_free.zip)


#### Fibre 116

![Original vs glow free spectrum](removing_glow/fibre116_original_vs_glow_removed.png)

Figure 1a: Original and glow-free fully reduced spectrum (Frame 20, Oct 27 p1 ccd1 observation, fibre 116).


#### Fibre 347

![Original vs glow free spectrum fibre 398](removing_glow/fibre347_original_vs_glow_removed.png)

Figure 1b: Original and glow-free fully reduced spectrum (Frame 20, Oct 27 p1 ccd1 observation, fibre 347).

#### Fibre 398

![Original vs glow free spectrum fibre 398](removing_glow/fibre398_original_vs_glow_removed.png)

Figure 1c: Original and glow-free fully reduced spectrum (Frame 20, Oct 27 p1 ccd1 observation, fibre 398).


## Raw glow

The spectra of the raw glow are shown in Figures 2a, 2b and 2c.

Raw glow FITS file: [27oct10020_raw_glow.fits.zip](removing_glow/raw_glow/27oct10020_raw_glow.fits.zip)

#### y=1001

![The extracted raw glow](removing_glow/raw_glow/27oct10020_raw_glow_1001.png)

Figure 2a: Glow extracted from raw image at y=1001 (Frame 20, Oct 27 p1 ccd1 observation).


#### y=2500

![Original vs glow free spectrum fibre 398](removing_glow/raw_glow/27oct10020_raw_glow_2500.png)

Figure 2b: Glow extracted from raw image at y=2500 (Frame 20, Oct 27 p1 ccd1 observation)

#### y=4000

![Original vs glow free spectrum fibre 398](removing_glow/raw_glow/27oct10020_raw_glow_4000.png)

Figure 2c: Glow extracted from raw image at y=4000 (Frame 20, Oct 27 p1 ccd1 observation)


## Using glow frame as dark frame

* Copy 27oct10020_raw_glow.fits to dark/27oct10200.fits.

* Change the class of dark/27oct10200.fits to "DARK":

```
from logbook.code.python.observations.fits_class import set_class

set_class(path="Scratchpad/dark/27oct10200.fits", obj_class="DARK")
```

* Reduce dark/27oct10200.fits with 2DfDr to 27oct10200red.fits.

* Place 27oct10200red.fits to CCD1 directory as DARKCombined.fits

* Reduce everything with "Subtract BIAS", "Subtract Dark" and "Subtract Sky"

The signal to noise of the reduced image is around 3, which is lower than ~10 from the original method.

![Frame 20 with dark subtracted](removing_glow/frame20_dark_subtracted.png)

Figure 3: Fully reduced spectrum with dark subtracted (Frame 20, Oct 27 p1 ccd1 observation).


## Reducing with BIAS subtracted dark

* Open dark directory containing 27oct10200.fits.

* Place BIASCombined.gits here.

* Reduce with 2DfDr with "Subtract Bias Frame". Uncheck "Subtract Sky" and "Throughput calibrate".

* Place to CCD1 as DARKCombined.fits and reduce:

  * Subtract BIAS and DARK frames.

  * Subtract Sky.

  * No throughput calibration.


Result: Signal-to-noise is around 3 (Fig. 4).

![Frame 20 with dark subtracted, dark reduced with BIASCombined](removing_glow/frame20_dark_subtracted_dark_reduced_with_bias_combined.png)

Figure 4: Fully reduced spectrum with dark subtracted (Frame 20, Oct 27 p1 ccd1 observation), dark reduced with BIASCombined.



## Using glow frame as BIAS


* Copy 27oct10020_raw_glow.fits to biases/27oct10300.fits.


* Change the class of biases/27oct10300.fits to "BIAS":

```
from logbook.code.python.observations.fits_class import set_class

set_class(path="Scratchpad/biases/27oct10300.fits", obj_class="BIAS")
```

* Reduce biases/27oct10300.fits with 2DfDr to 27oct10300red.fits.

* Place 27oct10200red.fits to CCD1 directory as BIASCombined.fits

* Reduce CCD1 with "Subtract BIAS", "Subtract SKY", NO throughput calibration.


Result: Signal-to-noise is about 3 (Fig 5.)

![Frame 20 glow used as BIAS](removing_glow/frame20_glow_used_as_bias.png)

Figure 5: Fully reduced spectrum with glow frame used as BIAS(Frame 20, Oct 27 p1 ccd1 observation).


