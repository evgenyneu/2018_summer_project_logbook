# Comparing execution time of Python and Fortran versions of a program

## Abstract

We compare execution time of similar codes written in Fortran and Python. We find that the Fortran ifort version of the program is 12.8 ± 0.2 times faster than Python version.

## Introduction

The time it takes to execute a program may vary depending on both the programming language used and the code. It can be difficult to compare performance of different languages since it is not always possible to write identical code in two languages. However, it can still be useful to get a rough estimate of performance difference between two languages by implementing many different algorithms and comparing their execution times. The codes available on [Computer Language Benchmarks Game web site](https://benchmarksgame-team.pages.debian.net/benchmarksgame) indicate that implementations of certain algorithm using Fortran can be 40 (and up to 100) times faster compared to their Python versions (Fig. 1).

![Performance comparison of Fortran and Python codes](benchamrks_game_execution_time_ratio.jpg)

Figure 1: Performance comparison of Fortran and Python codes [1].


The program execution times for Python and Fortran are shown in Fig. 2.

![Program execution time comparison of Fortran and Python codes](benchamrks_game_execution_time.jpg)

Figure 2: Program execution time comparison of Fortran and Python codes [1].


In this work we implement versions of the same program in Python and Fortran and then compare execution times between the languages.


## Method

Our program extracts background glow from an astronomical .fits file. The codes read a 32 MB file containing an image from a CDD of a telescope. The image size is about 4000 by 4000 pixels. Next, the codes manipulate individual pixels from that image and write the result to an output image file of the same size.

The Fortran and Python version of the code are compared in Fig 3. The codes are not identical due to the differences of the languages, but we tried to make the two codes as similar as we could. 


[![Execution time of program written in Python and Fortran](python_fortran_code_compare_thumbnail.jpg)](python_fortran_code_compare.png)

Figure 3: Line-by-line comparison of Python and Fortran version of the program. Click to view the full code.

The two codes accomplish the same task by producing identical output .fits files, shown in Fig 4. No special performance optimization was done in either of the codes apart from using vectorized array assignment with NumPy in Python code.

![Output image produced by the version of the program written in Python and Fortran](fortran_python_output.gif)

Figure 4: Comparison of two output image files produced by the code written in Python and Fortran. 

We ran the program five times for each of three compilers: Python 3.6.8, ifort 19.0.1.144 and GFortran 6.3.0. We measured its execution time with `time` command. Then we calculated the average time. The uncertainty `SE` was calculated using the formula for the standard error of the mean:

```math
SE = \frac{s}{\sqrt{n}},
```
where `s` is the sample standard deviation and `n=5` is the sample size.

For Fortran we used the following compiler flags:

* *GFortran*: -Wall -Wextra -fPIC -Werror -fmax-errors=1 -O3 -march=native -ffast-math -funroll-loops

* *ifort*: -warn all -fast


## Results

We measured the execution time Python version of the program to be 11.4 ± 0.1 s (Fig. 5). We found that execution times for ifort and GFortran were 0.88 ± 0.01 s and 0.89 ± 0.02 respectively, and that the times agree within uncertainties. Our data show that the Fortran ifort version of the program is 12.8 ± 0.2 times faster than Python's version. This result is within the range of performance ratios available on Computer Language Benchmarks Game web site [1].

![Execution time of program written in Python and Fortran](fortran_python_execution_time.jpg)

Figure 5: Comparison of performance of the glow extraction program written in Fortran and Python.


## References

1. [Manderbolt](https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/mandelbrot.html), [n-body](https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/nbody.html), [pidigits](https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/pidigits.html), [spectral-norm](https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/spectralnorm.html), [binary trees](https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/binarytrees.html), [fannkuch-redux](https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/fannkuchredux.html) and [k-nucleotide](https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/knucleotide.html) benchmarks from [The Computer Language Benchmarks Game web site](https://benchmarksgame-team.pages.debian.net/benchmarksgame).



## Code and data

* Fortran code: [export_glow_benchmark](export_glow_benchmark).

* Python code: [export_glow_benchmark.py](export_glow_benchmark.py).

* Measurements: [fortran_python_benchmark.xlsx](fortran_python_benchmark.xlsx).

* Benchmarks game data: [benchmarks_game.xlsx](benchmarks_game.xlsx).


