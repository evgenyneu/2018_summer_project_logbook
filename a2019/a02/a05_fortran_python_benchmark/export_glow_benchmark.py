from astropy.io import fits
import math
import numpy as np
import os
import sys



def export_glow(arguments):
    """
    Creates a .fits image by keeping pixels between fibres and erasing the fibres.

    Parameters
    ----------
    arguments : list of str
        A list containing command line arguments
    """

    fibre_thickness = 15  # Width of fibres in pixels



















    # Check the command line arguments were provided
    if len(arguments) != 3:
        print('ERROR: three command line arguments required.')
        print('Example:')
        print('$ python export_glow_benchmark.py data.fits tram.fits output.fits')
        return


    # Read the arguments
    filename_data = arguments[0]
    filename_tram = arguments[1]
    filename_output = arguments[2]

    # Check if the output file already exists
    if os.path.exists(filename_output):
        print(f"ERROR: output file already exists: '{filename_output}'.")
        return






    # Open the tram FITS file
    # -----------------

    hdul_tram = fits.open(filename_tram)






    # Open the data FITS file
    # -----------------

    hdul_data = fits.open(filename_data)






    # Determine the size of the tram image.
    # ----------------

    naxes_tram = [hdul_tram[0].header.get('NAXIS1'), hdul_tram[0].header.get('NAXIS2')]

    # Check that it found both NAXIS1 and NAXIS2 keywords.
    if naxes_tram[0] is None or naxes_tram[1] is None:
        print('Failed to read the NAXIS keywords for tram.')
        return


    total_fibres = naxes_tram[1]

    # Determine the size of the data image.
    # ----------------

    naxes_data = [hdul_data[0].header.get('NAXIS1'), hdul_tram[0].header.get('NAXIS2')]

    # Check that it found both NAXIS1 and NAXIS2 keywords.
    if naxes_data[0] is None or naxes_data[1] is None:
        print('Failed to read the NAXIS keywords for data.')
        return


    # Find the width of the image
    xmax = min(naxes_tram[0], naxes_data[0])

    # Load the image data
    image_data = hdul_data["PRIMARY"].data



    # Find the table containing information about fibres
    # ----------------

    record = hdul_data[1].header['EXTNAME']




    if record != 'STRUCT.MORE.FIBRES' and record != 'FIBRES':
        print('Can not find the fibres table.')



    # Find the non-parked fibres
    # ---------------------------
    max_x = 5000  # Used for definining array dimension
    nonparked = np.empty(max_x, dtype=int)








    # Collect the indexes of nonparked fibres
    n_nonparked = 0

    for irow in range(total_fibres):
        name = hdul_data[1].data['NAME'][irow]
        if name != 'PARKED':
            nonparked[n_nonparked] = irow
            n_nonparked += 1



    # Load the y coordinates of fibres
    # -------------

    fibre_y = hdul_tram["PRIMARY"].data



    tram_y_start = None

    # Loop over the x values
    for x in range(xmax):
        # Loop over fibres
        for i_fibre in range(n_nonparked):
            fibre_index = nonparked[i_fibre]

            # Calculate the start and end y-coordinate of the fibre
            # --------------------

            tram_y = fibre_y[fibre_index, x]

            if tram_y_start is None:
                tram_y_start = math.floor(tram_y - fibre_thickness / 2.0)


            tram_y_end = math.ceil(tram_y + fibre_thickness / 2.0)

            # Check if the current tram fibre overlaps with the next
            if i_fibre < n_nonparked - 1:
                next_fibre_index = nonparked[i_fibre + 1]
                next_tram_y_start = math.floor(fibre_y[next_fibre_index, x] - fibre_thickness / 2.0)

                if (tram_y_end >= next_tram_y_start):
                    # The fibre is very close to the next one.
                    # Skip until the next fibre is further away, since we want to erase fibres
                    # with brightness values from regions far away from fibres.
                    continue



            # Erase the fibre pixels
            image_data[tram_y_start: tram_y_end-1, x] = image_data[tram_y_start-1, x]

            # Use next fibre as starting point for the region we want to erase
            tram_y_start = None


















    # Save the data array into the output fits file
    hdul_data.writeto(filename_output)








    # Close the fits files
    hdul_tram.close()
    hdul_data.close()







    print(f'Glow extracted successfully to {filename_output}')


if __name__ == '__main__':
    export_glow(sys.argv[1:])









































