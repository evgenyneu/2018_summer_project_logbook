# Reducing two observations

* Plotted the [stacked skies](a2019/a02/a25_analyzing_glow_removal_form_parked/no_glow_all_ccds_subtract_sky) (Fig. 3) for three observations.

* [Reduced](combined_two_observations) two observaionts.

# ToDo

* Check sky box plots for fully reduced data.

* Compare SNR in alls CCDs: 3 stacked with 2 stacked observations.

* Show boxplots for CCD 1 and 3 for normal vs glow_gree reduction (https://gitlab.com/evgenyneu/2018_summer_project_logbook/tree/master/a2019/a02/a25_analyzing_glow_removal_form_parked/compare_ccd2_normal_vs_glow_free_skies)