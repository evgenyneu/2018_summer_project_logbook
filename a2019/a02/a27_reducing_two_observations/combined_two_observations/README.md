# Reduce data with two observations from Oct 29

We want to reduce data using two observation from Oct 29: "p0" and "p1_A":

```
from logbook.code.python.jobs.reduce_data import reduce_single_star

description = "Subtracted glow from all CCDs. No bias subtraction. No throughput calibration. Sky subtraction. Stacked two 29 Oct observations: 'p0' and 'p1_A'. Doppler shifted. Normalized."

take_prefixes = ["p0", "p1_A"]

reduce_single_star(name="AGBSYAZ017762", days=[29, 29], take_prefixes=take_prefixes, take_input="noglow_sub_sky", take_combined="noglow_29oct", description=description)
```

### Sky levels

The sky levels are shown in Figures 1 and 2.

![Mean skies](skies_observations.png)

Figure 1: Mean sky levels of 2018 observation, glow-free, sky subtracted.


The sky levels for stacked observation is shown on Fig. 3.

![Mean skies stacked](skies_stacked.png)

Figure 2: Mean sky levels of stacked 2018 observation, glow-free, sky subtracted.