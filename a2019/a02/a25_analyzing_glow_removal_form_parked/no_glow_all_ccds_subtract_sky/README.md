# Reducing all CCDs with glow removal and sky subtraction

## 1. Remove glow

```
from logbook.a2019.a02.a22_reduce_pet_star.mass_glow_removal import mass_glow_removal

mass_glow_removal(take_output_suffux="noglow_sub_sky", exclude_ccds=[])
```


## 2. Reduce with 2DfDr

**Settings for all CCDs:**

* Do NOT check "Subtract bias frame".

* Uncheck "Thoughput callibrate".

* Keep "Subtract Sky".


## 3. Reduce star 'AGBSYAZ017762'

Finally, we extract the spectrum for our pet star, stack the reductions, doppler correct and normalize the spectrum:

```
from logbook.code.python.jobs.reduce_data import reduce_single_star

description = "Subtracted glow from all CCDs. No bias subtraction. No throughput calibration. Sky subtraction. Doppler shifted. Normalized."
take_prefixes = ["p1", "p0", "p1_A"]

reduce_single_star(name="AGBSYAZ017762", days=[27, 29, 29], take_prefixes=take_prefixes, take_input="noglow_sub_sky", take_combined="noglow_sub_sky", description=description)
```

![Mean skies](sky_levels_glow_free_sky_subtracted.png)

Figure 2: Mean sky levels of 2018 observation, glow-free, sky subtracted.


The sky levels for stacked observation is shown on Fig. 3.

![Mean skies stacked](sky_levels_glow_free_sky_subtracted_stacked.png)

Figure 3: Mean sky levels of stacked 2018 observation, glow-free, sky subtracted.


Code:

```
from logbook.code.python.plot.compare_sky_levels import compare_sky_levels
path = "data/observe/30Oct18/reduce/288/noglow_sub_sky"

ccd1 = f"{path}/ccd1/30oct1_combined.fits"
ccd2 = f"{path}/ccd2/30oct2_combined.fits"
ccd3 = f"{path}/ccd3/30oct3_combined.fits"
observation1 = [ccd1, ccd2, ccd3]
subtitles = []

compare_sky_levels(paths=[observation1], subtitles=subtitles, title="Mean sky levels stacked, noglow_sub_sky")

```
