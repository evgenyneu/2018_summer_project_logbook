import pandas as pd
import matplotlib.pyplot as plt

xy_max = 60
df = pd.read_csv('sub_sky_signal_to_noise.csv')
df_line = pd.DataFrame({'x': [0, xy_max], 'y': [0, xy_max]})
ax = None
ax = df_line.plot(ax=ax, x='x', y='y', color='g', legend=False)

xcolumn = 'Near fibre signal-to-noise n4'
ycolumn = 'Parked subtracted sky signal-to-noise n5'
xerr_column = 'Uncertainty u(n4)'
yerr_column = 'Uncertainty u(n5)'

print(f"Near fibre mean SNR: {df[xcolumn].mean():.2f} ± {df[xcolumn].sem():.2f}")
print(f"Parked sub sky mean SNR: {df[ycolumn].mean():.2f} ± {df[ycolumn].sem():.2f}")

ax = df.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, color='Red', s=30)

ax.errorbar(df[xcolumn], df[ycolumn], xerr=df[xerr_column], yerr=df[yerr_column],
            fmt='none', ecolor='Red', capsize=3, elinewidth=0.5, capthick=0.5)

plt.show()
