# Comparing signal-to-noise of "near fibre" and "parked subtracted sky" methods

Here we measure the signal-to-noise of the five stacked fully reduced frames of 381 fibre (27oct1_combined.fits file) using two glow extraction methods - "near fibre" and "parked subtracted sky", where "near fibre" is the [previous](a2019/a02/a18_3x11_method_mass_glow_removal) 3x11 method of glow removing which used the pixels immediately below and above a 15 pixel-wide fibre.

Raw data: [sub_sky_signal_to_noise.xlsx](sub_sky_signal_to_noise.xlsx)


## Plot signal-to-noise relative to each other

The mean signal to noise values and their uncertainties (standard errors of the mean) are

**Near fibre mean SNR**: 38 ± 2

**parked subtracted sky mean SNR**: 38 ± 2.


From Fig. 2 it is hard to say whether the mean signal-to-noise of the two methods are significantly different.

![Signal-to-noise of two methods relative to each other](one_vs_other.png)

Figure 2: Signal-to-noise of two methods of glow removal ("Near fibre" and "Parked subtracted sky") relative to each other. The error bars are two small to see at this scale.

Plotting code: [plot_one_vs_other.py](plot_one_vs_other.py).



## Our statistic

We will look at the difference of signal to noise values of the two methods: "Parked subtracted sky" minus "Near fibre".


## Confidence interval

First, we verify the assumption that our values are normally distributed. Shapiro-Wilk normality test suggests the values are normally distributed with p-value of 0.948. Moreover, the quantile-quantile plot on Fig. 5 shows strong linear relation, further supporting the normality hypothesis.

![Quantile-quantile plot](qq_plot.png)

Figure 5: Normal quantile-quantile plot of the difference of signal-to-noise of two methods ("Parked subtracted sky" minus "Near fibre").



Next, we calculate confidence interval of the mean for 95% confidence level:

* 95% confidence interval: (-1.38, 1.15).

Conclusion: There is 0.95 probability that our confidence interval contains true population mean. Our data do not show significant evidence that the mean signal-to-noise values of the two methods are different.

Code: [stat_inference.py](stat_inference.py)


## Box plot of difference of signal-to-noise

The box plot on Fig. 6 shows a symmetric distribution around zero.

![Box plot difference of signal-to-noise](difference_of_signal_to_noise.png)

Figure 6: Difference of signal-to-noise of two methods of glow removal.




## Statistical test

We want to test if the mean signal-to-noise of "Parked subtracted sky" method ($`\mu_a`$) is different than that of the "Near Fibre" method ($`\mu_b`$) using paired sample t-test.


### Hypotheses

Null hypothesis $`H_0: \mu_a = \mu_b`$.

Alternative hypothesis $`H_A: \mu_a \neq \mu_b`$.

Significance level: 95%.


### Verifying assumptions

* Dependent variable is continuous: yes.

* The observations are independent of one another: yes, since we made independent measurements of the noise levels in different parts of the spectrum.

* The dependent variable does not contain any outliers: true, we see no outliers on see Fig. 6.

* The dependent variable is approximately normally distributed: yes, see Fig 5.



### Results and conclusion

The p-value of the test is 0.83930, therefore, we can not reject the null hypothesis $`H_0`$. In other words, if we assume that the two methods have the same mean signal-to-noise, then there is 0.83930 probability that we would observe our data purely by chance. Since this probability is larger than 0.05, we conclude that our data do not show statistically significant evidence that mean signal-to-noise values of the "Parked subtracted sky" and "Near fibre" methods are different.

Code: [stat_inference.py](stat_inference.py)
