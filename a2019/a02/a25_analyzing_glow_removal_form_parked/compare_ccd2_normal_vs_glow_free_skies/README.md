# Compare CCD 2 normal vs glow-free reduction

We want to compare the normal reduction with glow-free reduction for CCD 2 to see the effect of the glow-free reduction. We can see that glow-free reduction of CCD 2 resulted in less scatter of the of the mean sky levels.

![Mean skies](normal_glow_free_ccd2_2.png)

Figure 1: Normal vs glow free mean sky levels for CCD 2.



Code:

```
from logbook.code.python.plot.compare_sky_levels import compare_sky_levels
path27 = "data/observe/27Oct18/reduce/288"
path29 = "data/observe/29Oct18/reduce/288"

ccd1 = f"{path27}/p1_noglow_sub_sky/ccd2/27oct2_combined.fits"
ccd2 = f"{path29}/p0_noglow_sub_sky/ccd2/29oct2_combined.fits"
ccd3 = f"{path29}/p1_A_noglow_sub_sky/ccd2/29oct2_combined.fits"
observation1 = [ccd1, ccd2, ccd3]

ccd1 = f"{path27}/p1_noglow13_sub_sky/ccd2/27oct2_combined.fits"
ccd2 = f"{path29}/p0_noglow13_sub_sky/ccd2/29oct2_combined.fits"
ccd3 = f"{path29}/p1_A_noglow13_sub_sky/ccd2/29oct2_combined.fits"
observation2 = [ccd1, ccd2, ccd3]

subtitles = ["Glow-free", "Normal"]

labels = ['27 Oct p1', '29 Oct p0', '29 Oct p1 A']

compare_sky_levels(paths=[observation1, observation2], subtitles=subtitles,
                   title="Mean sky levels for CCD 2", labels=labels, colors=None)

```

