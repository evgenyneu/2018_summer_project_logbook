# Comparing signal-to-noise in CCD 2 between normal and glow-free reduction

Here we measure the signal-to-noise of the fully reduced and stacked spetrum of 'AGBSYAZ017762' star for normal and glow-free reductions.

Raw data: [compare_AGBSYAZ017762_normal_vs_glow_free.xlsx](compare_AGBSYAZ017762_normal_vs_glow_free.xlsx)



## Plot signal-to-noise relative to each other

The mean signal to noise values and their uncertainties (standard errors of the mean) are

**Glow-free SNR**: 75 ± 4.

**Normal SNR**: 80 ± 6.


From Fig. 2 we can see that there are more higher signal-to-noise measurements for "Normal" reduction. However, there is significant scatter present as well.

![Signal-to-noise of two methods relative to each other](one_vs_other.png)

Figure 2: Signal-to-noise of two methods of glow removal ("Noemal" and "Glow-free" relative to each other. The error bars are two small to see at this scale.

Plotting code: [plot_one_vs_other.py](plot_one_vs_other.py).


## Our random variable

For our variable we will use the difference of signal to noise values of the two methods: "Normal" minus "Glow-free".



## Box plot of difference of signal-to-noise

The box plot of the variable is shown on Fig. 6. It can be seen that the most of the measurements are above zero, with two outliers. We exclude the outliers from the dataset.

![Box plot difference of signal-to-noise](difference_of_signal_to_noise.png)

Figure 6: Difference of signal-to-noise of two methods of glow removal.



## Confidence interval

First, we verify the assumption that our values are normally distributed. Shapiro-Wilk normality test suggests the values are normally distributed with p-value of 0.943. Moreover, the quantile-quantile plot on Fig. 5 shows strong linear relation, further supporting the normality hypothesis.

![Quantile-quantile plot](qq_plot.png)

Figure 5: Normal quantile-quantile plot of the difference of signal-to-noise of two methods ("Normal" minus "Glwo-free").



Next, we calculate confidence interval of the mean for 95% confidence level:

* 95% confidence interval: (-1.01, 10.15).

Conclusion: There is 0.95 probability that our confidence interval contains true population mean. Our data do not show significant evidence that the mean signal-to-noise values of the two methods are different.

Code: [stat_inference.py](stat_inference.py)



## Statistical test

We want to test if the mean signal-to-noise of "Normal" method ($`\mu_a`$) is higher than that of the "Glow-free" method ($`\mu_b`$) using paired sample t-test.


### Hypotheses

Null hypothesis $`H_0: \mu_a = \mu_b`$.

Alternative hypothesis $`H_A: \mu_a > \mu_b`$.

Significance level: 95%.


### Verifying assumptions

* Dependent variable is continuous: yes.

* The observations are independent of one another: yes, since we made independent measurements of the noise levels in different parts of the spectrum.

* The dependent variable does not contain any outliers: true, after we excluded the outliers shown on Fig. 6.

* The dependent variable is approximately normally distributed: yes, Fig. 5 and Shapiro-Wilk test provide evidence for normality.



### Results and conclusion

The p-value of the test is 0.04694. Since the value is close to 0.05 we can not reject the null hypothesis $`H_0`$. The results are inconclusive and we would suggest to make additional measurements of signal-to-noise for different stars.

Code: [stat_inference.py](stat_inference.py)
