import pandas as pd
import matplotlib.pyplot as plt

xy_max = 120
df = pd.read_csv('compare_AGBSYAZ017762_normal_vs_glow_free.csv')
df_line = pd.DataFrame({'x': [0, xy_max], 'y': [0, xy_max]})
ax = None
ax = df_line.plot(ax=ax, x='x', y='y', color='g', legend=False)

xcolumn = 'Glow-free signal-to-noise n1'
ycolumn = 'Normal signal-to-noise n2'
xerr_column = 'Uncertainty u(n2)'
yerr_column = 'Uncertainty u(n1)'

print(f"Glow-free SNR: {df[xcolumn].mean():.2f} ± {df[xcolumn].sem():.2f}")
print(f"Normal SNR: {df[ycolumn].mean():.2f} ± {df[ycolumn].sem():.2f}")

ax = df.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, color='Red', s=30)

ax.errorbar(df[xcolumn], df[ycolumn], xerr=df[xerr_column], yerr=df[yerr_column],
            fmt='none', ecolor='Red', capsize=3, elinewidth=0.5, capthick=0.5)

plt.show()
