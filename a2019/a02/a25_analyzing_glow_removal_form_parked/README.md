# Analyzing 'parked' glow removal method

Here we use the ["parked" glow removal method](a2019/a02/a21_erase_from_parked) to reduce a single frame.

## 1. Raw data: 27oct10020.fits

Let's reduce 27oct10020.fits file with 2DfDr using raw spectrum from fibre 381 (y=3924) shown in Fig. 5.

![Raw spectra](reducing_sub_sky/1_raw.png)

Figure 5: Raw spectrum from the 27oct10020.fits file.

**Raw signal**: 378 ± 7.4.



## 2. Subtracting glow: 27oct10020.fits ⇾ 27oct10020.fits

In this step [we subtract](a2019/a02/a05_fortran_python_benchmark/removing_glow.md) the glow (Fig. 6) from the raw spectrum (Fig 5).

![Raw glow](reducing_sub_sky/1_raw_glow.png)

Figure 6: Spectrum of the glow extracted from fram 20 of Oct 27 p1 observation.

**Bias signal**: 332.9 ± 0.7.



The resulting spectrum from the 27oct10020.fits file is shown in Fig 7.

![Glow subtracted](reducing_sub_sky/2_glow_subtracted.png)

Figure 7: Glow-subtracted spectrum.

**Glow-free 27oct10020.fits signal**: 48 ± 9.



## 3. Extracting Gaussian profile: 27oct10020im.fits ⇾ 27oct10020ex.fits

Extracting the Gaussian profile of the fibre shown in Fig. 8.

![Cross section of the fibre](reducing_sub_sky/3_ex_line_profile.png)

Figure 8: Intensity of the cross-section of the fibre 381 from 27oct10020im.fits file. X-axis is the y-coordinate in the FITS image, and Y-axis is the intensity.


The result of extraction is shown in Fig. 9.

![Extracted intensity form the Fibre](reducing_sub_sky/3_ex_gaussian_profile_extracted.png)

Figure 9: Gaussian profile intensity extracted from fibre 381 into the 27oct10020ex.fits file.

**27oct10020ex.fits signal**: 227 ± 20.



## 4. Dividing by the flat field 27oct10020ex.fits ⇾ 27oct10020red.fits

Dividing by flat field won't change the signal, since flat field is near 1 with small uncertainty.




## 5. Subtracting the sky: 27oct10020ex.fits ⇾ 27oct10020red.fits

The final operation is subtraction of the combined sky shown in Fig. 10 from extracted intensity shown in Fig. 9 (divided by flat).

![The combined sky](reducing_sub_sky/6_sky.gif)

Figure 10: The combined sky extracted from 27oct10020.fits.

**Combined sky signal**: -1.0 ± 3.



## 6. The reduced spectrum: 27oct10020red.fits

The final fully reduced spectrum is shown in Fig. 11.

![The reduced spectrum](reducing_sub_sky/7_red_sky_subtracted.png)

Figure 11: The fully reduced spectrum of a single frame, from 27oct10020red.fits file. The signal to noise is measured in range: 4857.5 - 4860.0 Å.

**27oct10020red.fits signal**: 217 ± 20.


## Compare reduction methods

* ["Near fibre" vs "Parked subtracted sky"](compare_sub_sky).

* ["Subtracted sky" vs "Not subtracted sky" methods](compare_with_and_without).


## Reduce with sky subtraction

* [Glow-free all CCDs with sky subtraction](no_glow_all_ccds_subtract_sky).

* [Glow-free CCDs 1 and 3 with sky subtraction](no_glow_all_ccds1_3_subtract_sky).

## Analyze CCD 2

* [Compare normal and glow-free reduction mean skies](compare_ccd2_normal_vs_glow_free_skies).

* [Compare normal and glow-free reduction](compare_ccd2_normal_vs_glow_free_snr).




