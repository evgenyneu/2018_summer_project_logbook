# Comparing signal-to-noise of "Subtracted sky" and "Not subtracted sky" methods

Here we measure the signal-to-noise of the five stacked fully reduced frames of 381 fibre (27oct1_combined.fits file) using two glow extraction methods with and without sky subraction. Both methods use the same glow extraction method from the parked fibres.

Raw data: [with_and_without_sky.xlsx](with_and_without_sky.xlsx)



## Plot signal-to-noise relative to each other

The mean signal to noise values and their uncertainties (standard errors of the mean) are

**Near fibre mean SNR**: 38 ± 2

**parked subtracted sky mean SNR**: 38 ± 2.


From Fig. 2 it is hard to say whether the mean signal-to-noise of the two methods are significantly different.

![Signal-to-noise of two methods relative to each other](one_vs_other.png)

Figure 2: Signal-to-noise of two methods of glow removal ("Not subtracted sky" and "Subtracted sky" relative to each other. The error bars are two small to see at this scale.

Plotting code: [plot_one_vs_other.py](plot_one_vs_other.py).


## Our random variable

For our variable we will use the difference of signal to noise values of the two methods: "Not subtracted sky" minus "Subtracted sky".



## Box plot of difference of signal-to-noise

The box plot of the variable is shown on Fig. 6. It can be seen that the most of the measurements are above zero, with a single outlier at -1. We exclude this outlier from the dataset.

![Box plot difference of signal-to-noise](difference_of_signal_to_noise.png)

Figure 6: Difference of signal-to-noise of two methods of glow removal.



## Confidence interval

First, we verify the assumption that our values are normally distributed. Shapiro-Wilk normality test suggests the values are normally distributed with p-value of 0.769. Moreover, the quantile-quantile plot on Fig. 5 shows strong linear relation, further supporting the normality hypothesis.

![Quantile-quantile plot](qq_plot.png)

Figure 5: Normal quantile-quantile plot of the difference of signal-to-noise of two methods ("Not subtracted sky" minus "Subtracted sky").



Next, we calculate confidence interval of the mean for 95% confidence level:

* 95% confidence interval: (0.11, 0.89).

Conclusion: There is 0.95 probability that our confidence interval contains true population mean. Our data show show significant evidence that the mean signal-to-noise value of the "Not subtracted sky" is higher than that of the "Subtracted sky" method.

Code: [stat_inference.py](stat_inference.py)




## Statistical test

We want to test if the mean signal-to-noise of "Not subtracted sky method ($`\mu_a`$) is higher than that of the "Subtracted sky" method ($`\mu_b`$) using paired sample t-test.


### Hypotheses

Null hypothesis $`H_0: \mu_a = \mu_b`$.

Alternative hypothesis $`H_A: \mu_a > \mu_b`$.

Significance level: 95%.


### Verifying assumptions

* Dependent variable is continuous: yes.

* The observations are independent of one another: yes, since we made independent measurements of the noise levels in different parts of the spectrum.

* The dependent variable does not contain any outliers: true, after we excluded the outlier shown on Fig. 6.

* The dependent variable is approximately normally distributed: yes, Fig. 5 and Shapiro-Wilk test provide evidence for normality.



### Results and conclusion

The p-value of the test is 0.00869, therefore, we can reject the null hypothesis $`H_0`$. In other words, if we assume that the two methods have the same mean signal-to-noise, then there is 0.00869 probability that we would observe our data purely by chance. Since this probability is smaller than 0.05, we conclude that our data show statistically significant evidence (at 95% significance level) that mean signal-to-noise value of the "Not subtracted sky" is higher than that of the "Subtracted sky" method. However, the higher value 0.89 of the confidence interval is only 2% of the mean signal-to-noise of 38. Therefore, although present, the difference in signal-to-noise of the two methods is not important for the purpose of measurement of equivalent widths of absorption lines.

Code: [stat_inference.py](stat_inference.py)


