import pandas as pd
import matplotlib.pyplot as plt

xy_max = 60
df = pd.read_csv('with_and_without_sky.csv')
df_line = pd.DataFrame({'x': [0, xy_max], 'y': [0, xy_max]})
ax = None
ax = df_line.plot(ax=ax, x='x', y='y', color='g', legend=False)

xcolumn = 'Subtracted sky signal-to-noise n5'
ycolumn = 'Not subtracted sky signal-to-noise n6'
xerr_column = 'Uncertainty u(n5)'
yerr_column = 'Uncertainty u(n6)'

print(f"Subtracted sky mean SNR: {df[xcolumn].mean():.2f} ± {df[xcolumn].sem():.2f}")
print(f"Not subtracted sky mean SNR: {df[ycolumn].mean():.2f} ± {df[ycolumn].sem():.2f}")

ax = df.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, color='Red', s=30)

ax.errorbar(df[xcolumn], df[ycolumn], xerr=df[xerr_column], yerr=df[yerr_column],
            fmt='none', ecolor='Red', capsize=3, elinewidth=0.5, capthick=0.5)

plt.show()
