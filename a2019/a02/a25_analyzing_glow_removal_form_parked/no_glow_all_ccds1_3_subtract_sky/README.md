# Reducing with glow removal for CCDs 1 and 3 only and sky subtraction for all CCDs

## 1. Remove glow

```
from logbook.a2019.a02.a22_reduce_pet_star.mass_glow_removal import mass_glow_removal

mass_glow_removal(take_output_suffux="noglow13_sub_sky", exclude_ccds=[2])
```


## 2. Reduce with 2DfDr

**Settings for CCDs 1 and 3:**

* Do NOT check "Subtract bias frame".

* Uncheck "Thoughput callibrate".

* Keep "Subtract Sky".


**Settings for CCD 2:**

* Check "Subtract bias frame".

* Uncheck "Thoughput callibrate".

* Keep "Subtract Sky".


## 3. Reduce star 'AGBSYAZ017762'

Finally, we extract the spectrum for our pet star, stack the reductions, doppler correct and normalize the spectrum:

```
from logbook.code.python.jobs.reduce_data import reduce_single_star

description = "Subtracted glow from CCDs 1 and 3 only. No bias subtraction for CCDs 1 and 3. Bias reduced for CCD 2. No throughput calibration. Sky subtraction. Doppler shifted. Normalized."

take_prefixes = ["p1", "p0", "p1_A"]

reduce_single_star(name="AGBSYAZ017762", days=[27, 29, 29], take_prefixes=take_prefixes, take_input="noglow13_sub_sky", take_combined="noglow13_sub_sky", description=description)
```

![Mean skies](sky_levels_glow_free_ccds1_3_sky_subtracted.png)

Figure 2: Mean sky levels of 2018 observation, glow-free for CCDs 1 and 3 only, sky subtracted.
