# Understanding data reduction in 2DfDr

We want to understand exactly how the data is reduced in 2DfDr and how it affects the noise.

## 1. Raw data: 27oct10020.fits

Let's reduce an file 27oct10020.fits with 2DfDr using raw spectrum from fibre 381 (y=3924) shown in Fig. 1.

![Raw spectra](reducing/1_raw.png)

Figure 1: Raw spectrum from the 27oct10020.fits file.

**Raw signal**: 378 ± 7.4.



## 2. Subtracting bias: 27oct10020.fits ⇾ 27oct10020im.fits

In this step 2DfDr subtracts the combined bias (Fig. 2) from the raw spectrum (Fig 1).

![Combined bias](reducing/2_bias.png)

Figure 2: Spectrum of the reduced bias from Oct 27 p1 observation.

**Bias signal**: 278 ± 0.5.

The resulting spectrum from the 27oct10020im.fits file is shown in Fig 3.

![Bias subtracted](reducing/2_im_bias_subtracted.png)

Figure 3: Bias-subtracted spectrum from the 27oct10020im.fits file.


**27oct10020im.fits signal**: 105 ± 6.5.

#### Math check

```math
[raw] - [bias] = (378 ± 7.4) - (105 ± 0.5) = 100 ± 7.4.
```

This is close to what we get from 2DfDr. The uncertainties were added in quadrature:

```math
u(a - b) = \sqrt{u(a)^2 + u(b)^2}
```


## 3. Extracting Gaussian profile: 27oct10020im.fits ⇾ 27oct10020ex.fits

Next, 2DfDr extracts intensity of the fiber by adding the intensities of the pixels weighted by Gaussian function. For example, in Fig. 4 we plotted a vertical slice through the 27oct10020im.fits file and zoomed at our fibre of interest. 2DfDr sums the area under the curve and multiplies it by the value of the Gaussian at each value of x in `UMFIM_GSSEXTR` subroutine.

The code uses parameter `MWIDTH=4.14 pixels` is the measure of the width of the fibre, its value is stored in the header of the extracted tram lines file `27oct10022tlm.fits`. The value of sigma used for the Guassian function is

```math
\sigma = [MWIDTH] / 2.355 = 1.76 \ pixels.
```

Code iterates in the range of ±3*σ from the center of the fibre, which is ±5.3 pixels.

![Cross section of the fibre](reducing/4_ex_line_profile.png)

Figure 4: Intensity of the cross-section of the fibre 381 from 27oct10020im.fits file. X-axis is the y-coordinate in the FITS image, and Y-axis is the intensity.


### Extraction result: 27oct10020ex.fits

The result of extraction is shown in Fig. 5.

![Extracted intensity form the Fibre](reducing/3_ex_gaussian_profile_extracted.png)

Figure 5: Gaussian profile intensity extracted from fibre 381 into the 27oct10020ex.fits file.

**27oct10020ex.fits signal**: 608 ± 18.

Summing area under the Gaussian curve of height 100 and σ=1.76 gives value of 500. This is different from 608, probably, because 2DfDr uses weighted summation. Gaussian extraction makes the resulting signal 5.7 time larger. The uncertainty can be checked as

```math
(6.5) \sqrt{5.7} = 16,
```

which is not far from 18 observed in 27oct10020ex.fits.



## 4. Dividing by the flat field 27oct10020ex.fits ⇾ 27oct10020red.fits


Next, 2DfDr divides the extracted signal from Fig. 5 by the flat field signal form 27oct10022red.fits fils, shown in Fig. 6.


![Flat field](reducing/5_red_flat.png)

Figure 6: Spectrum of the flat field from 27oct10022red.fits.

**Flat field signal**: 1.003 ± 0.006.

The division by the flat field will not significantly change the signal or the error:

```math
\frac{[ex]}{[flat]} = \frac{608 ± 18}{1.003 ± 0.006} = 608/1.003 ± \sqrt{ \Big(\frac{18}{1.003} \Big)^2 +  \Big(\frac{608}{1.003^2} 0.006 \Big)^2} = 608 ± 18.
```


## 5. Subtracting the sky: 27oct10020ex.fits ⇾ 27oct10020red.fits

The final operation is subtraction of the combined sky shown in Fig. 7 from extracted intensity shown in Fig. 5 (divided by flat). The combined sky is made by calculating weighted medium from all the sky fibers.

![The combined sky](reducing/6_sky.gif)

Figure 7: The combined sky extracted from 27oct10020.fits.

**Combined sky signal**: 290 ± 6.


### The reduced spectrum: 27oct10020red.fits

The final fully reduced spectrum is shown in Fig. 8.

![The reduced spectrum](reducing/7_red_sky_subtracted.png)

Figure 8: The fully reduced spectrum of a single frame, from 27oct10020red.fits file.

**27oct10020red.fits signal**: 326 ± 18.


#### Math check

```math
[ex]/[flat] - [sky] = (608 ± 18) - (290 ± 6) = 318 ± 19.
```

Our calculation agrees with the signal and error produced by 2DfDr in 27oct10020red.fits file.





### Appendix: viewing a vertical slice in IRAF

Here is how to view a vertical slice of a spectrum from fits file in IRAF. First, we spap, the x and y-axes:

```
from astropy.io import fits
import numpy as np
hdul = fits.open("27oct10020im.fits")

data = hdul[0].data
data_swapped = np.swapaxes(data,0,1)
hdul[0].data = data_swapped
hdul.writeto("27oct10020im_swapped.fits")
hdul.close()
```

And then, we view the swapped image:

```
splot 27oct10020im_swapped.fits[0]
```
