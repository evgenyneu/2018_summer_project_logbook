from matplotlib import pyplot as plt
import numpy as np


def rms_from_y(y, ymax):
    y_right = y[int(len(y) / 2):]
    y_right = y_right - ymax
    return np.sqrt(np.mean(y_right**2))


def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


npoints = 1000
xmin = 4800
xmax = 4804
ymax = 100
x_values = np.linspace(xmin, xmax, npoints)
y_values = np.linspace(ymax, ymax, npoints)

# The absorption lines: (x position, sigma, amplitude)
lines = [
            (xmin+0.39, .12, ymax*0.5),
            (xmin+1.0, .15, ymax*0.8),
            (xmin+1.40, .1, ymax*0.2)
         ]

for mu, sig, amplitude in lines:
    y_values = y_values - amplitude*gaussian(x_values, mu, sig)


# Add Gaussian white noise
mean = 0
sigma = 0.1  # aka 'rms' of the noise
noise1 = np.random.normal(mean, sigma, npoints) * ymax
y_with_noise1 = y_values + noise1


# Calculate RMS of the signal
rms1 = rms_from_y(y_with_noise1, ymax)
print(f"Signal 1: rms={rms1}")

# Plot
# ------

plt.figure(num=1, figsize=(8, 7))

# Plot signal without
fig = plt.subplot(2, 1, 1)
plt.plot(x_values, y_values)
plt.title('Signal without noise')
plt.ylabel('Normalized intensity')
plt.xlabel('Wavelength [Å]')

# Plot signal with noise
plt.subplot(2, 1, 2)
plt.plot(x_values, y_with_noise1)
plt.title('Signal with noise')
plt.ylabel('Normalized intensity')
plt.xlabel('Wavelength [Å]')

plt.tight_layout()

plt.show()


# Add a second signal with its own noise
# ------

noise2 = np.random.normal(mean, sigma, npoints) * ymax
y_with_noise2 = y_values + noise2

# Calculate RMS of the signal
rms2 = rms_from_y(y_with_noise2, ymax)
print(f"Signal 2: rms={rms2}")


# Add two signals together
# ------
y_combined = y_with_noise1 + y_with_noise2

# Plot signal without
plt.plot(x_values, y_combined)
plt.title('Combined signal')
plt.ylabel('Normalized intensity')
plt.xlabel('Wavelength [Å]')
plt.tight_layout()
plt.show()
