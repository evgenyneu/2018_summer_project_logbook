# "3x5 median interpolated" method of glow extraction

We change the method of glow extraction ([see code](code/fortran/export_glow/)), explained in Fig. 1.

![Glow extraction method](erasing_fibres_in_export_glow_3x5_median.png)

Figure 1: The method used in the program to erase the fibre pixels (yellow) with values from the glow pixels (blue).

The result of the glow extraction is shown in Figures 2 and 3 for raw frame #20 from 27 Oct p1 observation.


![Raw image and the extracted glow](glow_removed_3x5_interpolated.gif)

Figure 2: The glow extracted form the row image with fibre pixel replaced by the values of the pixels below and above them using the 3x5 median interpolation method.


![Raw image and the extracted glow](glow_removed_3x5_interpolated_zoomed.gif)

Figure 3: The zoomed-in view of the glow extracted form the raw image using 3x5 median interpolation method.

### Python vs Fortran performance comparison

We reduced the same image using median interpolation method with [Fortran](code/fortran/export_glow/) and [Python](code/python/observations/export_glow.py) programs. We used single-core variant for Python code, since Fortran code is single-core:

Python command:

```
time python3 export_glow.py -data_path=27oct10020.fits -tram_path=27oct10022tlm.fits -output_path=glow.fits -interpolate=yes
```

The glow extraction times are:

* Fortran: 1.4 s.

* Python: 65 s.

**Result**: the glow extraction using our Fortran program was about 46 times faster than using our Python program.


## Reducing one frame

Next, we [reduce](a2019/a02/a05_fortran_python_benchmark/removing_glow.md) frame 20. The results are shown on Figures 3 and 4.


![Raw and glow removed images](raw_and_glow_free2.gif)

Figure 3: The raw and glow-free version of an image (Frame 20 from Oct 27 p1 observation). The glow was removed using "3x5 median interpolation" method.

![Raw and glow removed images zoomed](raw_and_glow_free_zoomed.gif)

Figure 4: The zoomed-in view of the raw and glow-free version of an image (Frame 20 from Oct 27 p1 observation). The glow was removed using "3x5 median interpolation" method.


## Analyzing spectra during reduction


### 1. Raw data: 27oct10020.fits

Let's reduce an file 27oct10020.fits with 2DfDr using raw spectrum from fibre 381 (y=3924) shown in Fig. 5.

![Raw spectra](reducing/1_raw.png)

Figure 5: Raw spectrum from the 27oct10020.fits file.

**Raw signal**: 378 ± 7.4.



### 2. Subtracting glow: 27oct10020.fits ⇾ 27oct10020.fits

In this step [we subtract](a2019/a02/a05_fortran_python_benchmark/removing_glow.md) the glow (Fig. 6) from the raw spectrum (Fig 5).

![Raw glow](reducing/1_raw_glow.png)

Figure 6: Spectrum of the glow extracted from fram 20 of Oct 27 p1 observation.

**Bias signal**: 331 ± 1.



The resulting spectrum from the 27oct10020.fits file is shown in Fig 7.

![Bias subtracted](reducing/2_glow_subtracted.png)

Figure 7: Glow-subtracted spectrum.

**Glow-free 27oct10020.fits signal**: 48 ± 6.



## 3. Extracting Gaussian profile: 27oct10020im.fits ⇾ 27oct10020ex.fits

Extracting the Gaussian profile of the fibre shown in Fig. 8.

![Cross section of the fibre](reducing/3_ex_line_profile.png)

Figure 8: Intensity of the cross-section of the fibre 381 from 27oct10020im.fits file. X-axis is the y-coordinate in the FITS image, and Y-axis is the intensity.


The result of extraction is shown in Fig. 9.

![Extracted intensity form the Fibre](reducing/3_ex_gaussian_profile_extracted.png)

Figure 9: Gaussian profile intensity extracted from fibre 381 into the 27oct10020ex.fits file.

**27oct10020ex.fits signal**: 242 ± 22.

The error 29 here is lower than 40 from the previous ['single pixel' method](/a2019/a02/a08_understanding_reduction/understanding_glow_removal.md) of glow removal.



## 4. Dividing by the flat field 27oct10020ex.fits ⇾ 27oct10020red.fits

Dividing by flat field won't change the signal, since flat field is near 1 with small uncertainty.



## 5. Subtracting the sky: 27oct10020ex.fits ⇾ 27oct10020red.fits

The final operation is subtraction of the combined sky shown in Fig. 10 from extracted intensity shown in Fig. 9 (divided by flat).

![The combined sky](reducing/6_sky.gif)

Figure 10: The combined sky extracted from 27oct10020.fits.

**Combined sky signal**: -1.0 ± 3.



### The reduced spectrum: 27oct10020red.fits

The final fully reduced spectrum is shown in Fig. 11.

![The reduced spectrum](reducing/7_red_sky_subtracted.png)

Figure 11: The fully reduced spectrum of a single frame, from 27oct10020red.fits file. The signal to noise is measured in range: 4857.5 - 4860.0 Å.

**27oct10020red.fits signal**: 222 ± 19.

The noise of 19 for the "3x5 median interpolated" method is lower than 27 from the previous ['3x3 random' method](a2019/a02/a12_export_glow_improvement) of glow removal. However, this could be due to random fluctuations of noise. We need to compare multiple noise measurements between the two methods to see if the new method gives better result.


