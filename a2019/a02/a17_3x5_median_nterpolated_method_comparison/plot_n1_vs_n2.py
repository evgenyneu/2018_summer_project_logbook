import pandas as pd
import matplotlib.pyplot as plt

xy_max = 12
df = pd.read_csv('3_by_3_random_vs_3_by_5_median_interpolated_signal_to_noise.csv')
df_line = pd.DataFrame({'x': [0, xy_max], 'y': [0, xy_max]})
ax = None
ax = df_line.plot(ax=ax, x='x', y='y', color='g', legend=False)

xcolumn = '3x3 random signal-to-noise n2'
ycolumn = '3x5 median signal-to-noise n3'
xerr_column = 'Uncertainty u(n2)'
yerr_column = 'Uncertainty u(n3)'

print(f"3x3 mean SNR: {df[xcolumn].mean():.2f} ± {df[xcolumn].sem():.2f}")
print(f"3x5 mean SNR: {df[ycolumn].mean():.2f} ± {df[ycolumn].sem():.2f}")

ax = df.plot.scatter(ax=ax, x=xcolumn, y=ycolumn, color='Red', s=10)

ax.errorbar(df[xcolumn], df[ycolumn], xerr=df[xerr_column], yerr=df[yerr_column],
            fmt='none', ecolor='Red', capsize=3, elinewidth=0.5, capthick=0.5)

plt.show()
