from scipy import stats
import pandas as pd
import pylab
import matplotlib.pyplot as plt
import math
import scipy


# Load data
df = pd.read_csv('3_by_3_random_vs_3_by_5_median_interpolated_signal_to_noise.csv')
difference_column = 'Difference of signal-to-noise n3 - n2, d'
data = df[difference_column]
data_difference = data.tolist()

# Descriptive statistics
sample_size = data.count()
mean = data.mean()
standard_error = scipy.stats.sem(data)
print(f"Mean: {mean:0.2f}")
print(f"Standard error of the mean: {standard_error:0.2f}")

# Confidence interval
confidence = 0.95
h = standard_error * scipy.stats.t.ppf((1 + confidence) / 2., sample_size - 1)
print(f"{confidence * 100:0.0f}% confidence interval: ({(mean - h):0.2f}, {(mean + h):0.2f})")

# Check for outliers
df.boxplot(difference_column)
plt.show()

# Check if distribution is normal using Shapiro-Wilk normality test
w, p_value = stats.shapiro(data_difference)
print("Shapiro-Wilk normality test")
print(f"W = {w:0.3f}, p-value = {p_value:0.3f}")

# Check the normal Quantile-Quantile plot
stats.probplot(data_difference, dist="norm", plot=pylab)
pylab.title("Normal Quantile-Quantile Plot")
pylab.show()

# Perform the paired sample t-test
difference_of_means = 1
sigma = df[difference_column].std() / math.sqrt(len(df[difference_column]))
degrees_of_freedom = len(df[difference_column]) - 1
t_statistic = (df[difference_column].mean() - difference_of_means) / sigma
p_value = stats.t.sf(t_statistic, degrees_of_freedom)
print(f"Paired sample t-test: t-statistic={t_statistic:0.3f}, p-value={p_value:0.5f}")
