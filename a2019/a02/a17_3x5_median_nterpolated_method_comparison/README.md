# Comparing signal-to-noise of "3x3 random" and "3x5 median" methods


Here we measure the signal-to-noise of the same frame reduced using two glow extraction methods - "3x3 random" and "3x5 median interpolated" - as shown in Fig 1. The spectrum from "3x3 random" appears to have more noise.

![3x3 random vs 3x5 median interpolated entire CCD1](fibre381_entire_spectrum_ccd1.png)

Figure 1: Comparison of spectra that was fully reduced using glow reduction methods: "3x3 random" (top) and "3x5 median interpolated" (bottom). The spectra are from frame 20 of 27 Oct p1 observation, CCD 1.


## Measuring average signal, noise RMS and signal-to-noise

We measure average signal, noise RMS and signal-to-noise for the two spectra from Fig 1.

Raw data: [3_by_3_random_vs_3_by_5_median_interpolated_signal_to_noise.xlsx](3_by_3_random_vs_3_by_5_median_interpolated_signal_to_noise.xlsx)


## Plot signal-to-noise relative to each other

We can see form Fig. 2 signal-to-noise of the "3x5 median interpolated" method is higher than that of the "3x3 random" method.

![Signal-to-noise of two methods relative to each other](n1_vs_n2.png)

Figure 2: Signal-to-noise of two methods of glow removal ("3x3 random" and "3x5 median interpolated") relative to each other. The error bars are two small to see at this scale.

Plotting code: [plot_n1_vs_n2.py](plot_n1_vs_n2.py).

## Mean and its error

For our statistic we use the difference of the signal-to-noise values ("3x5 median interpolated" minus "3x3 random"):

* Mean: 2.33.

* Standard error of the mean: 0.40.


## Confidence interval

First, we verify the assumption that our values are normally distributed. Shapiro-Wilk normality test suggests the values are normally distributed with p-value of 0.828. Moreover, the quantile-quantile plot on Fig. 5 shows strong linear relation, further supporting the normality hypothesis.

![Quantile-quantile plot](qq_plot.png)

Figure 5: Normal quantile-quantile plot of signal-to-noise of two methods of glow removal ("3x5 median interpolated" minus "3x3 random").



Next, we calculate confidence interval of the mean for 95% confidence level:

* 95% confidence interval: (1.42, 3.24).

Conclusion: There is 95% that our confidence interval contains true population mean.


Code: [stat_inference.py](stat_inference.py)


## Box plot of difference of signal-to-noise

![Box plot difference of signal-to-noise](difference_of_signal_to_noise.png)

Figure 6: Difference of signal-to-noise of two methods of glow removal ("3x5 median interpolated" minus "3x3 random").



## Statistical test

We want to test if the mean signal-to-noise of "3x5 median interpolated" method ($`\mu_a`$) is higher by 1.0 than that of the "3x3 random" method ($`\mu_b`$) using paired sample t-test. Our data are the difference of the paired measurements of the signal-to-noise of the two methods stored in "Difference of signal-to-noise n3 - n2, d" column from [3_by_3_random_vs_3_by_5_median_interpolated_signal_to_noise.xlsx](3_by_3_random_vs_3_by_5_median_interpolated_signal_to_noise.xlsx) spreadsheet.

### Hypotheses

Null hypothesis $`H_0: \mu_a = \mu_b + 1.0`$.

Alternative hypothesis $`H_A: \mu_a > \mu_b + 1.0`$.

Significance level: 95%.

### Verifying assumptions

* Dependent variable is continuous: yes.

* The observations are independent of one another: yes, since we made independent measurements of the noise levels in different parts of the spectrum.

* The dependent variable does not contain any outliers: true, we see no outliers on Fig. 6.

* The dependent variable is approximately normally distributed: yes, see Fig 5.



### Results and conclusion

The p-value of the test is 0.00464, which allows us to reject the null hypothesis $`H_0`$. In other words, if we assume that the difference of the mean signal-to-noise of the two methods is 1.0, then there is 0.00464 probability that we would observe our data purely by chance. Since this probability is smaller than 0.05, we conclude that our data show statistically significant evidence that mean signal-to-noise of the "3x5 median interpolated" method is higher by 1.0 than that of the "3x3 random" method. Therefore, the "3x5 median interpolated" method is preferable to the "3x3 random" method.

Code: [stat_inference.py](stat_inference.py)




