Contains **shared** data, i.e. used from more than one logbook entry.
Avoid changing the files in this directory, since it can break existing code.