* The mass loss was turned off in [parameters](data/monstar_plots/m0.80z001y245/):

```
IMLOSS  IMLOSS_RGB  FMLOSS_RGB  TMLOSS_RGB  FHLEFT_RGB   [IMLOSS=> 
15=Reimers, 8=Vass & Wood]
   -1        99        3.0        1.0e7        0.0
         IMLOSS_AGB  FMLOSS_AGB  TMLOSS_AGB  FHLEFT_AGB
             99        3.0        1.0e7        0.0
```

* Enabled mass loss on RGB by setting `IMLOSS_RGB` to `15`.