# Parameters of NGC 288 from literature


## Metallicity

* [Fe/H]=-1.19±0.12 [Hsyu (2014), arXiv:1406.5220](https://arxiv.org/abs/1406.5220).

* [Fe/H]=-1.20±0.14 [Caputo (1992), 1983A&A...128..190C](http://adsabs.harvard.edu/abs/1992ApJ...401..260C).

* [Fe/H]=-1.14 [Marin-Franch (2008), arXiv:0812.4541](https://arxiv.org/abs/0812.4541).

* [Fe/H] = -1.39±0.01 [Shetrone & Keane (2000), AJ, 119:840-850](http://iopscience.iop.org/article/10.1086/301232/fulltext/) for giant stars.

* [Fe/H] = -1.24 [Harris, William E, 2003 version of A Catalog of Parameters for Globular Clusters in the Milky Way](http://www.naic.edu/~pulsar/catalogs/mwgc.txt).

* [Fe/H] = 1.24±0.04 [Chen, Alfred Bing-Chih; Tsay, Wean-Shun (2000), 2000AJ....120.2569C](http://adsabs.harvard.edu/abs/2000AJ....120.2569C).

## Age

* 10.62 Gyr [Forbes & Bridges (2010) arXiv:1001.4289](https://arxiv.org/pdf/1001.4289.pdf).

* 14-19 Gyr [Penny (1984) 1984MNRAS.208..559P](http://adsabs.harvard.edu/full/1984MNRAS.208..559P).