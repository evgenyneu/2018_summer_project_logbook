# Create a starting model with different mass/metallicity/He

* Open `initial_model_parameters` file and set `New initial model?` to `1`.

* Set the required mass: change `Mass (Msun)` setting.

* Set the required metallicity, two ways:

  1. Set `Custom composition?` to 3 and change `[Fe/H]`.

  1. Set `Custom composition?` to 1 and change `Z`.

* Set the required "He".

* Run the evolution `./evoln &`, it will create new model in `last.xxx`. Evolution should stop automatically after the first model.

* Rename the model, for example `cp last.xxx m0.80z008y24.inmod`.

* Set "New initial model?: 0" in `initial_model_parameters`.

* Open `infiles.list`, replace the `.inmod` line with the new model (with "m0.80z008y24.inmod" for our example)

* Evolve (`./evoln`) again and stop after the output:

```
>>> Z on surface of *Starting Model* = YOU_NEW_Z <<<
```

where `YOU_NEW_Z` is the new metallicity.

* Replace the last line Z with `YOU_NEW_Z` in `infiles.list`.

* We've made the staring model.