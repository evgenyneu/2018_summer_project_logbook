## Testing MONSTAR code with GFortran

* Fixed a bug that prevented the code from compiling with gfortran.

* Removed bounds check from makefile (removed -fbounds-check flag). The code crashed in at least two place because the array indexes were out of bounds. For instance, line 220 of `rmshhb.f`

```Fortran
if(jm.gt.12.and.doremesh.ne.-1) then
   if(abs(W(3,jm+10)-W(3,jm-10)).gt.0.05d0) then
      IF(DX(JM).gE.40000.d0*DXMIN)  goto 104  
   endif
   if(abs(W(3,jm+4)-W(3,jm-3)).gt.0.02d0) then
      IF(DX(JM).gE.10.d0*DXMIN)  goto 104  
   endif
   if(abs(W(3,jm+2)-W(3,jm-1)).gt.0.005d0) then
      IF(DX(JM).gE.10.d0*DXMIN)  goto 104  
   endif
endif
```

Here we access array with index out of bounds: `W(3,jm+10)`, `W(3,jm+4)`, `W(3,jm+2)`. This code is not present in John Lattazio's version of MONSTAR. Also, his version works with ` -fbounds-check` flag present.

Should we fix the code? It can lead to instability and different results at each run, since with pickup random uninitialized memory location when accessing an array out of bounds.

* Evolved the same model m0.80z0004y24 using ifort and gfortran, in order to check if they are the same. The HR diagrams look identical, except for the different vertical lines closer to the tip of the AGB (Fig 1). See [plots and parameters data](data/monstar_plots/m0.80z0004y24/).

![ifort vs gfortran evolution](hr_compare_ifort_gfortran2.png)

Figure 1: HR diagram of m0.80z0004y24 model produced by MONSTAR compiled with gfortran and ifort.

* The gfortran evolution runs multiple times slower than ifort.




