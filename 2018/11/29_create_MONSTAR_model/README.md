##  Testing evolution model

I wanted to test the evolution code for a m0.80z0004y24a4 model and compare it with Fig. 3 of Simon's nature paper ([ arXiv:1305.7090](https://arxiv.org/abs/1305.7090)). In order to do this, I need to convert the HR diagram from Tff vs L (or g) to y vs v-y coordinates. Simon's paper says that transformations were done using the [arXiv:astro-ph/0311458](https://arxiv.org/abs/astro-ph/0311458v1) paper. However, the data and code referenced in that paper are no longer accessible. 

* Emailed the authors of [arXiv:astro-ph/0311458](https://arxiv.org/abs/astro-ph/0311458v1) and asked for access to the data/code to perform the transformations.


## Running MONSTAR models for NGC 288

Here we want to evolve models and compare them with observations. First, we need to find starting parameters for the model:

* Find metallicity of [Fe/H]=-1.19 from [arXiv:1406.5220](https://arxiv.org/abs/1406.5220).

* Use arbitrary helium fraction Y=0.245.

* Use arbitrary mass 0.8 M_solar.


### Generating new starting model

* Generating a new starting model by selecting "New initial model?: 1", "He:0.245" and "[Fe/H]: -1.19", "Mass (Msun): 0.80" in `initial_model_parameters`.

* Evolve.

* Saving the starting model (`cp last.xxx m0.80z001y245.inmod`).

* Set "New initial model?: 0" in `initial_model_parameters`.

* Replace the model with m0.80z001y245.inmod in `infiles.list` and evolve.

* Evolve and stop after the output:

```
>>> Z on surface of *Starting Model* = 0.0010323970 <<<
```

* MONSTAR calculated z=0.0010323970. Replace the last line Z with 0.0010323970 in `infiles.list`.

* We've made the staring model.


## Evolving the `m0.80z001y245` model to AGB

* Evolve, with [parameters](data/monstar_plots/m0.80z001y245/).
