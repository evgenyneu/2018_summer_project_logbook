## Changing mass to achieve 13 Gyr for Y=0.26

Previously we increased He fraction from 0.245 to 0.26. This, however, decreased the turnoff age from 13 Gyr to 11.7 Gyr. Here we want to decrease the mass from M=0.7915 M_sun in order to increase the age back to 13 Gyr while keeping Y=0.26.

* We create new starting models with parameters: M=[varying], Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

* Determine the ages of the currently evolving model by running [find_age_current_model.i](find_age_current_model.i).


We found the following ages for different model masses:

* 12.4 Gyr for M = 0.78

* Did not converge for M = 0.76

* 13.1 Gyr for M = 0.77

* 12.77 Gyr for M = 0.775

* 12.89 Gyr for M = 0.773

* 12.91 Gyr for M = 0.7725

* 12.95 Gyr for M = 0.772

* 13.002 Gyr for M = 0.7713 (Close enough)



## Comparing two models


On Fig, 1 we compare two 13 Gyr models:

1. Y=0.26, M=0.7713 (white).
1. Y=0.245, M=0.7915 (red).


![NGC 288 models with two masses](NGC_288_two_masses.png)

Figure 1: Hot horizontal-branch stars from NGC 288 (green dots) from spectroscopic data in [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2). The lines are the MONSTAR model for a z=0.001 star that has main sequence turnoff age of 13 Gyr. The white model is Y=0.26, M=0.7713 M_sun [parameters](data/monstar_plots/m0.7713z001y26/). The red model is Y=0.245, M=0.7915 M_sun [parameters](data/monstar_plots/m0.7915z001y245/).

Plotting code: [compare_masses.i](compare_masses.i)

We can see from Fig. 1 that horizontal branch of the Y=0.26 model is significantly bluer than that of the Y=0.245 model. The horizontal branch of Y=0.26 is also closer to the observed data, although, it still is not as blue as some of the observed stars.




