// Compares the HR diagram of the currently evolving model with M=0.8 M_sun

#include "code/yorick/include.i"

func smpPlot(void)
{
  ev2 = smpOpenPlotFromMonstarArchive("m0.7915z001y245/ev.xxx.zip")
  plg, log10(ev2.L), ev2.LogTe
  ev = smpOpenPlotFromEvXXX(dir="../monstar_models/y24/")
  plg, log10(ev.L), ev.LogTe, color="red", type="dash"
}