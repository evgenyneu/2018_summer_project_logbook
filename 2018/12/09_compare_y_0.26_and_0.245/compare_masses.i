#include "code/yorick/include.i"

func smpPlot(void)
{
  // Model 1
  ev = smpOpenPlotFromMonstarArchive("m0.7713z001y26/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="white"

  // Model 2
  ev = smpOpenPlotFromMonstarArchive("m0.7915z001y245/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="red"

  // Plot the observed data
  filepath = "data/NGC288/arXiv:1403.7397v2/HB_spectroscopic.csv";
  data = text_cells(filepath, ",")
  data = tonum(data(2:5,*)(*,2:))
  temperature = data(1,*)
  temperature_uncertainty = data(2,*)
  log_g = data(3,*)
  log_g_uncertainty = data(4,*)

  pleb, log_g, temperature,
    dx=temperature_uncertainty, dy=log_g_uncertainty, color=[0,120,0]

  plmk,log_g, temperature,marker=4,msize=0.2,width=11,color="green"

  // Captions and axes
  pltitle, "Hot HB stars (arXiv:1403.7397v2, spectroscopy) \nfrom NGC 288 and two z=0.001 MONSTAR models"
  xytitles,"T [K]","log g [cgs]"
  smpAddPlotMargin, 0.1
  revx
  revy

  // Legend
  plt, "Y=0.26, M=0.7713 M_sun", 0.22, 0.81, color="white";
  plt, "Y=0.245, M=0.7915 M_sun", 0.22, 0.78, color="red";
}