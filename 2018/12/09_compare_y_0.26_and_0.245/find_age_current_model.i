#include "code/yorick/read_plot.i"

func smpPlot(void)
{
  ev = smpOpenPlotFromEvXXX(dir="../monstar_models/y24/")
  plg, ev.LogTe, ev.age
  age = ev.age(ev.LogTe(mxx))
  write, "Age: ", age, " yr"
}