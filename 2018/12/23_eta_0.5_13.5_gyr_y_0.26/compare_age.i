#include "code/yorick/include.i"

func smpPlot(void)
{
  // Model
  ev = smpOpenPlotFromMonstarArchive("m0.764z001y26Reta0.5Age13.5/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="white"

  // Model
  ev = smpOpenPlotFromMonstarArchive("m0.7713z001y26a4Reta0.5/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="blue"

  // Observed data
  filepath = "data/NGC288/arXiv:1403.7397v2/HB_spectroscopic_all_hb_stars.csv";
  data = text_cells(filepath, ",")
  data = tonum(data(2:5,*)(*,2:))
  temperature = data(1,*)
  temperature_uncertainty = data(2,*)
  log_g = data(3,*)
  log_g_uncertainty = data(4,*)

  pleb, log_g, temperature,
    dx=temperature_uncertainty, dy=log_g_uncertainty, color=[0,120,0]

  plmk,log_g, temperature,marker=4,msize=0.2,width=11,color="green"

  // Observed data
  filepath = "data/NGC288/arXiv:1406.5220/RGB_stars___T_eff_K___Log_g_cgs.csv";
  data = text_cells(filepath, ",")
  data = tonum(data(2:3,*)(*,2:))
  log_g = data(2,*)
  temperature = data(1,*)
  plmk,log_g, temperature,marker=3,msize=0.3,width=11,color="red"

  // Captions and axes
  pltitle, "Two MONSTAR models\nwith higher mass loss eta=0.5,\nhigher helium content Y=0.26\nand different ages"
  xytitles,"T [K]","log g [cgs]"
  smpAddPlotMargin, 0.05
  revx
  revy

  // Legend
  plt, "13 Gyr", 0.22, 0.81, color="blue";
  plt, "13.5 Gyr", 0.22, 0.78, color="white";
}