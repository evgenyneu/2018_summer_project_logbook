## Creating a 13.5 Gyr model

Here we want to create an older model with y=0.26 and mass loss parameter eta=0.5. Our goal is to see if the orizontal branch matches the extreme hot horizontal branch stars.

* We generate a new starting model with parameters: M=0.764, Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

* The turnoff age is 13.4959 Gyr.


## Comparing 13 and 13.5 Gyr models with high mass loss `eta=0.5` and high helium `Y=0.26`

![NGC 288 models with different ages](compare_ages.png)

Figure 1: The lines are two [Fe/H]=-1.19 MONSTAR models created with eta=0.5 Reimers mass loss rate, high helium content (Y=0.26) and different ages. The white model has age 13.5 Gyr ([parameters](data/monstar_plots/m0.764z001y26Reta0.5Age13.5/)). The blue model has age 13 Gyr ([parameters](data/monstar_plots/m0.7713z001y26a4Reta0.5/)). The red triangles are observed RGB stars from NGC 288 ([arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)). The green dots are observed horizontal branch stars ([arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)).

Plotting code: [compare_age.i](compare_age.i)

We can see that the older model (white) is hotter and presents a better fit for the extremely hot horizontal branch stars. However, a better fit is needed for the hottest horizontal branch stars. We could try decreasing metallicity.



## Creating a 13.5 Gyr model with lower metallicity [Fe/H] = -1.3

* We generate a new starting model with parameters: M=[varying], Y=0.26, [Fe/H]=-1.3 with [method](2018/new_starting_model.md).

We vary the mass in order to get the age of 13.5 Gyr:

* Could not converge for M=0.764:

```
^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

*Density too high in LowT: jm,lgr,lgt=1055  1.000  3.673
 Returned from Model: KTINT=1 DT=   6.305955455045490E-003
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =   6.305955455045490E-003  6.305955455045490E-003
 ## tintn before =  1.783593545649187E-004
 ##Boosting timestep by 50, tintn=   8.917967728245935E-003
 >>                          DT =   8.917967728245935E-003
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing
```

* Try different values for TINTN*: 0.0001, 0.01, 0.1, 1, 10, 100, 10000. Still can't converge.

* 11.0647 Gyr for M=0.78.

* 12.1867 Gyr for M=0.78.

* 12.7915 Gyr for M=0.77.

* 13.0151 Gyr for M=0.767.

* 13.0407 Gyr for M=0.766.

* Could not converge for M=0.765:

```
 ------------------------------------------------
Convg probs: setting imix=100 for 10 models
 ------------------------------------------------

 ## tintn before =  5.824372807874241E-004
 ##Boosting timestep by 50, tintn=   2.912186403937120E-002
 >>                          DT =   2.912186403937120E-002
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing

 ------------------------------------------
 >>> Abund: IMIXEN = 100 --> NOT Mixing <<<
 ------------------------------------------

 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

*Density too high in LowT: jm,lgr,lgt=1053  1.000  3.675
 Stopping in modout after return from PHYS1

```

See parameters of the model with convergence failures [here](low_metal_params_can_not_converge).


## Fighting convergence problems `FHEPL = 0.9`

John suggested to use `FHEPL = 0.9`. Still having convergence problems ([parameters](convergence_problem_2_FHEPL_0.9)).