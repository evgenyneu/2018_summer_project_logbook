## Finding 13 Gyr model for [Fe/H]=-1.1


* We  generate a new starting model with parameters: M=[varying], Y=0.26, [Fe/H]=-1.1 with [method](2018/new_starting_model.md).

We get the following turnoff ages for masses:

* 13.1578 Gyr for M = 0.7713.

* 12.6843 Gyr for M = 0.78.

* 13.0603 Gyr for M = 0.7739.

* 12.9398 Gyr for M = 0.7753.

* 13.0076 Gyr for M = 0.7745.

* 12.9672 Gyr for M = 0.7746.

* 1.30096 Gyr for M = 0.77453. (Hmm, why is it not decreasing? Using M=0.7745)



## Comparing [Fe/H]=-1.1 and [Fe/H]=-1.19 models

![NGC 288 models with two metallicities](ngc288_compare_metallicity.png)

Figure 1: The RGB stars from NGC 288 (red triangles, [arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)) and hot horizontal branch stars (green dots, [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)). The lines are the MONSTAR model for a Y=0.26 star that has main sequence turnoff age of 13 Gyr. The white model has [Fe/H]=-1.1 ([parameters](data/monstar_plots/m0.7745z00124y26/)). The red model has[Fe/H]=-1.19 ([parameters](data/monstar_plots/m0.7713z001y26/)).

Plotting code: [compare_metallicity.i](compare_metallicity.i)

We can see that the higher metallicity model (white) produces a slightly redder horizontal branch.



## Finding 13 Gyr model for [Fe/H]=-2

The [Fe/H]=-1.3 model produced a bluer horizontal branch, but it was still not blue enough to fit the observed stars. We will try to match those stars by creating a low metallicity model [Fe/H]=-2.

* We  generate a new starting model with parameters: M=[varying], Y=0.26, [Fe/H]=-2 with [method](2018/new_starting_model.md).

We get the following turnoff ages for masses:

* Could not converge with M=0.7713:

```
------------------------------------------------
Convg probs: setting imix=100 for 10 models
 ------------------------------------------------
 
 ## tintn before =  3.490829792705054E-004
 ##Boosting timestep by 50, tintn=   1.745414896352527E-002
 >>                          DT =   1.745414896352527E-002
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing
 
 ------------------------------------------
 >>> Abund: IMIXEN = 100 --> NOT Mixing <<<
 ------------------------------------------
 
 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM
 
*Density too high in LowT: jm,lgr,lgt=1042  1.000  3.685
 Stopping in modout after return from PHYS1

```

* 10.8182 Gyr for M=0.8.

* 1.13714 Gyr for M=0.79.

* Could not converge with M=0.78.

Use M=0.79 1.13714 Gyr model.

## Evolving the [Fe/H]=-2.00 model

Evolution stoped at model NMOD=15209 due to convergence problems:

```
^^^Triple Alpha Running: Rate =    4671573950.53809     
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM
 
cen_heb                                     Mass=0.67746627, mdot= 6.394E-11
 T=1.2813305E+10 DT= 3.45E+02 NMOD=  15209 NM= 2345 ITS=  24 LTE= 3.871
       L(R,NU,H,HE,C,G) =  11.768  9.286 11.091 12.282  0.000 -12.161       
       XHE4(C)= 0.1957 MccKCVTN= 0.148 MccVEL= 0.195 T,P,DW,WR,H,He,Rad= 85 99 99 99 99 10 99

    TMAX,RHO,MC-MT,XHE,XC= 1.437E+08 2.388E+04 4.95E-01 0.195655 0.324430
    MC,MC-(EC,SCH,SCL,HE)= 0.4947716 -1.83E-01 3.46E-01 4.95E-01 3.46E-01
       Surface C= 2.3405E-05 N= 7.3276E-06 O= 5.7514E-05 C/O= 5.4259E-01

 
 Using instant mixing
 
 ^^^Triple Alpha Running: Rate =    4560366225.57242     
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM
 
 **** JM=1 in rmshhb! J=           1
 W<0 in Rad Zone at J=   7  I= 3 W=  2.0E-01
 Returned from Model: KTINT=1 DT=    344.597368355858     
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    344.597368355858        344.597368355858     
 Using instant mixing
 
 ^^^Triple Alpha Running: Rate =    4560366225.57242     
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM
 ```