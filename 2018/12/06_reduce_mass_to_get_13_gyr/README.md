## Changing model mass to M = 0.75 M_sun

* Here we want to adjust the mass of the model in order to get 13 Gyr age at the main-sequence turnoff (before we had 12.4 Gyr).

* Generating a new starting model with parameters: M=0.75, Y=0.245, [Fe/H]=-1.19 with [method](2018/new_starting_model.md) and [parameters](m0.75z001y245).

* Evolving. Evolution stopped with error:

```
*Density too high in LowT: jm,lgr,lgt=1052  1.001  3.652
 Returned from Model: KTINT=1 DT=    69190.5208593329     
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    69190.5208593329        69190.5208593329     
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing
 
 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM
 
*Density too high in LowT: jm,lgr,lgt=1047  1.000  3.658
 Stopping in modout after return from PHYS1
 ```

## Changing model mass to M = 0.78 M_sun

* Since 0.75 M_sun did not work, we try creating a 0.78 M_sun model.

* Generating M=0.78 model.

* The turnoff age is 13.6 Gyr, higher than 13 Gyr that we wanted ([method](2018/12/04_plot_spectroscopic_calculate_age)) or calculate the age at max LogTe:

```
ev.age(ev.LogTe(mxx))
```

We need a model with slightly higher mass.


## Trying different model masses

* M=0.79 model. Age = 13.065 Gyr. Need higher mass.

* M=0.791 model. Age = 13.0486 Gyr. Need higher mass.

* M=0.793 model. Age = 12.924 Gyr. Need lower mass. Overshot :)

* M=0.792 model. Age = 12.855 Gyr. **Huh? I expected the age to go up!**

* M=0.7915 model. Age = 12.994 Gyr. Close enough to 13 Gyr.


## The 13 Gyr model

* Evolving the model.

* While the model is evolving, checking the CMD with [compare_current_model.i](compare_current_model.i).





