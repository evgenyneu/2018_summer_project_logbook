## Try different mixing length factor for 13.5 Gyr model

Here we want to examine effects of changing the MXLOPS parameter. This is alpha, the "MiXing Length Over Pressure Scale height" parameter. We used `MXLOPS=1.6` previously.


* We generate a new starting two models with parameters: M=0.764, Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md). This is a 13.5 Gyr model.

* Use `MXLOPS=0.8` and `MXLOPS=2.4` for the two models.

* Use higher mass loss rate `eta=0.5`.

## `MXLOPS=0.8` model convergence problem

The `MXLOPS=0.8` stopped evolving within seconds due to convergence problems:

```
------------------------------------------
 >>> Abund: IMIXEN = 100 --> NOT Mixing <<<
 ------------------------------------------

 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

 -----------------------------------------------
  NITER>NITMAX, TINT(yr)=1.126E-08
 -----------------------------------------------
 Convergence numbers:
                G  L:    1459063.06818403
                G  T:    30952.6732986302
                G  R:   6.550987455421675E-007
                G  P:   4.589657556958598E-008
 -----------------------------------------------
                D  L:    10308.8495031694
                D  T:   4.402477802624136E-005
                D  R:   5.384234277131799E-007
                D  P:   4.589657556958598E-008
 -----------------------------------------------
 Returned from Model: KTINT=1 DT=   1.126163004464002E-008
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =   1.126163004464002E-008  1.126163004464002E-008

 ------------------------------------------------
Convg probs: setting imix=100 for 10 models
 ------------------------------------------------

 ## tintn before =  5.218746349505165E-006
 ##Boosting timestep by 50, tintn=   2.609373174752582E-004
 >>                          DT =   2.609373174752582E-004
 ***Temporarily setting PCT =   1.000000000000000E-002

 -------------------------------------------------
 *** Stopping: tint < dtmin (yr)***
 >>> tint(y)= DT =   5.630815022320009E-009  dtmin(yr)=   1.000000000000000E-008
  tintn=   0.177695008148366
 -------------------------------------------------
```

## Comparing models with mixing length parameter MXLOPS 1.4 and 2.4

* Setting `MXLOPS=2.4` increased the turnoff age gtom 13.5 to 13.67 Gyr.


![NGC 288 models with different mixing length parameters](13.5_Gyr_mxlops_2.4_and_1.6.png)

Figure 1: The lines are two 13.5 Gyr MONSTAR models having different values of the alpha (MXLOPS) mixing length parameter. Both models have parameters M=0.764, [Fe/H]=-1.19, higher helium Y=0.26 and higher mass loss rate eta=0.5 (Reimers). The white model has alpha=2.4 ([parameters](data/monstar_plots/m0.764z001y26Reta0.5Age13.5Mxlops2.4/)). The blue model has the default alpha=1.6 ([parameters](data/monstar_plots/m0.764z001y26Reta0.5Age13.5/)). The red triangles are observed RGB stars from NGC 288 ([arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)). The green dots are observed horizontal branch stars ([arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)).

Plotting code: [compare_mixing_length_13.5gyr.i](compare_mixing_length_13.5gyr.i)

We can see from Fig. 1 that the model with increased alpha parameter (white) has a significantly redder horizontal branch. This model did not fit the observed horizontal stars well. Here we need the horizontal branch of our model to become hotter, not redder. Therefore, we could try decreasing alpha and to make a better fit. However, the 13.5 Gyr model with lower alpha did not evolve due to the convergence problems. We might try using this alpha with a 13 Gyr model instead.


## Try mixing length parameter `MXLOPS=1.0` for 13 Gyr model

* Use `MXLOPS=1.0`.

* Use higher Reimers mass loss rate `RETA=0.5D0` in Line 324 in `mloss.f` file and run `make`. Set `IMLOSS_RGB=15` in current.param.

* Make a 13 Gyr model with parameters: M=0.7713, Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

The model stopped almost immediately due to convergence problems:


```
 -----------------------------------------------
  NITER>NITMAX, TINT(yr)=2.929E-08
 -----------------------------------------------
 Convergence numbers:
                G  L:    15378.9068519826
                G  T:    42.1913363334612
                G  R:   2.786083545627900E-008
                G  P:   5.287608760767318E-007
 -----------------------------------------------
                D  L:    155.617779862649
                D  T:   5.858998029962936E-006
                D  R:   1.526957646584902E-006
                D  P:   5.287608760767318E-007
 -----------------------------------------------
 Returned from Model: KTINT=1 DT=   2.929472249292309E-008
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =   2.929472249292309E-008  2.929472249292309E-008

 ------------------------------------------------
Convg probs: setting imix=100 for 10 models
 ------------------------------------------------

 ## tintn before =  1.357545270655320E-005
 ##Boosting timestep by 50, tintn=   6.787726353276603E-004
 >>                          DT =   6.787726353276603E-004
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =   9.050301804368802E-006   22.2420217144168
```


## Try mixing length parameter `MXLOPS=1.2` for 13 Gyr model

* Repeat above method with `MXLOPS=1.2`.

Stopped with convergence problems:

```
*Density too high in LowT: jm,lgr,lgt=1039  1.000  3.650
 Returned from Model: KTINT=1 DT=    5297.63539788058
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    5297.63539788058        5297.63539788058
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing

 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

*Density too high in LowT: jm,lgr,lgt=1037  1.000  3.651
 Returned from Model: KTINT=1 DT=    234.124619630953
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    234.124619630953        234.124619630953
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    882.939232980097        10595.2707957612
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing
```


## Try mixing length parameter `MXLOPS=1.4` for 13 Gyr model

* Repeat above method with `MXLOPS=1.4`.

Convergence problems:

```
*Density too high in LowT: jm,lgr,lgt=1059  1.000  3.648
 Returned from Model: KTINT=1 DT=    261593.499220245
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    261593.499220245        261593.499220245
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing

 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM
```


## Try mixing length parameter `MXLOPS=1.5` for 13 Gyr model


* Repeat above method with `MXLOPS=1.5`.

Convergence problems:

```
------------------------------------------
 >>> Abund: IMIXEN = 100 --> NOT Mixing <<<
 ------------------------------------------

 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

*Density too high in LowT: jm,lgr,lgt=1060  1.000  3.671
 Returned from Model: KTINT=1 DT=   4.246304015962740E-002
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =   4.246304015962740E-002  4.246304015962740E-002
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing
```

* Repeated with `FHEPL=0.9` instead of HEPL=0.99`, could not converge.


## Try mixing length parameter `MXLOPS=1.55` for 13 Gyr model


* Repeat above method with `MXLOPS=1.5`.

* The age is 13 Gyr.