## Trying 13.5 Gyr age

Previously, we used a 13 Gyr model. Here we create a model with age of 13.5 Gyr to see if it makes the horizontal branch bluer.

* We generate a new starting model with parameters: M=[varying], Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

By varying masses, we made the model with the following turnoff ages:

* 13.0017 Gyr for M = 0.7713

* Failed to converge for M = 0.76:

```
 ------------------------------------------
 >>> Abund: IMIXEN = 100 --> NOT Mixing <<<
 ------------------------------------------

 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

*Density too high in LowT: jm,lgr,lgt=1057  1.000  3.668
 Returned from Model: KTINT=1 DT=   7.296793561596737E-002
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =   7.296793561596737E-002  7.296793561596737E-002
 ## tintn before =  7.296793561596736E-004
 ##Boosting timestep by 50, tintn=   3.648396780798368E-002
 >>                          DT =   3.648396780798368E-002
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing
```

* 13.085 Gyr for M = 0.77

* 13.2482 Gyr for M = 0.768

* 13.3607 Gyr for M = 0.766

* 13.4959 Gyr for M = 0.764 (Close enough)





