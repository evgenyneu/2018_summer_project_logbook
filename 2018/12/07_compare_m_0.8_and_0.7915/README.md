## Comparing 0.8000 and 0.7915 M_sun models

* We evolve 0.7915 M_sun model which has 13 Gyr age on main-sequence turnoff and compare it with the 0.8000 M_sun model.

![NGC 288 models with two masses](NGC_288_two_masses_3.png)

Figure 1: Hot horizontal-branch stars from NGC 288 (green dots) from spectroscopic data in [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2). The lines are the MONSTAR model for a z=0.001, Y=0.245 star with M=0.8000 M_sun (red, [parameters](data/monstar_plots/m0.80z001y245_RGB_mass_loss/)) and M=0.7915 M_sun (white, [parameters](data/monstar_plots/m0.7915z001y245/)). Both models use mass loss on RGB branch.

Plotting code: [compare_masses.i](compare_masses.i)

* The track of the 0.7915 M_sun model is almost identical to the one of 0.8000 M_sun. However, the horizontal branch of M=0.7915 M_sun model is noticeably bluer. This is good, since it is closer to the observations (green dots).


