#include "code/yorick/include.i"

func smpPlot(void)
{
  // Model 1
  ev = smpOpenPlotFromMonstarArchive("m0.7713z001y26/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="white"

  // Observed data
  filepath = "data/NGC288/arXiv:1403.7397v2/HB_spectroscopic_all_hb_stars.csv";
  data = text_cells(filepath, ",")
  data = tonum(data(2:5,*)(*,2:))
  temperature = data(1,*)
  temperature_uncertainty = data(2,*)
  log_g = data(3,*)
  log_g_uncertainty = data(4,*)

  pleb, log_g, temperature,
    dx=temperature_uncertainty, dy=log_g_uncertainty, color=[0,120,0]

  plmk,log_g, temperature,marker=4,msize=0.2,width=11,color="green"

  // Observed data
  filepath = "data/NGC288/arXiv:1406.5220/RGB_stars___T_eff_K___Log_g_cgs.csv";
  data = text_cells(filepath, ",")
  data = tonum(data(2:3,*)(*,2:))
  log_g = data(2,*)
  temperature = data(1,*)
  plmk,log_g, temperature,marker=3,msize=0.3,width=11,color="red"

  // Captions and axes
  pltitle, "NGC 288 hot HB stars (arXiv:1403.7397v2, spectroscopy),\nRGB stars (arXiv:1406.5220v1) and a 13 Gyr\n MONSTAR model (z=0.001, M=0.7713 M_sun_, Y=0.26)"
  xytitles,"T [K]","log g [cgs]"
  smpAddPlotMargin, 0.1
  revx
  revy
  smpErrorBarsFixedTopLeft, dx=100, dy=0.3, color="red", width=2
}