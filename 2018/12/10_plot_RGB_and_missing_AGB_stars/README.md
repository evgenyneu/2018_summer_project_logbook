## Plotting RGB and very hot horizontal branch stars

* We plot RGB stars and horizontal branch stars bluer than the u-jump (11500 K) that were missing previously.

![NGC 288 observations and model](ngc288_hb_and_rgb_stars_4.png)

Figure 1: The RGB stars from NGC 288 (red triangles, [arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)) and hot horizontal branch stars (green dots, [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)). The solid line a 13 Gyr MONSTAR model (z=0.001, M=0.7713 M_sun, Y=0.26, [parameters](data/monstar_plots/m0.7713z001y26/)).

Plotting code: [plot_rgb.i](plot_rgb.i)