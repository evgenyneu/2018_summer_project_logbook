## Comparing 13 and 13.5 Gyr models

![NGC 288 models with different ages](ngc288_compare_ages.png)

Figure 1: The lines are the MONSTAR models for helium fraction Y=0.26. The white model has main sequence turnoff age of 13.5 Gyr ([parameters](data/monstar_plots/m0.764z001y26/)). The blue model has age 13 Gyr ([parameters](data/monstar_plots/m0.7713z001y26/)). The red triangles are observed RGB stars from NGC 288 ([arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)). The green dots are observed horizontal branch stars ([arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)).

Plotting code: [compare_ages.i](compare_ages.i)

We can see that the old 13.5 Gyr model (white) has hotter horizontal branch than the 13 Gyr model (blue). However, the horizontal branch is not hot enough to fit the observations.
