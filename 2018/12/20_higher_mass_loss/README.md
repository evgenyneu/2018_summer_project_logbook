## Using higher mass loss

Here we want to try to with the blue horizontal branch stars by increasing the mass loss rate. We will use the 13 Gyr and helium fraction of Y=0.245.

* Set `0` for "Automatically select from initial model directory?" setting in `initial_model_parameters`.

* Use the `m0.80z0004y24a4.inmod` as a starting model in `infiles.list`.

* Set `16` for `IMLOSS_RGB` in `current.param`. This corressponds to `eta=0.6` in Reimers' mass loss equation.

* Make model with parameters: M=[varying], Y=0.245, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

Next, we try different masses to reach the age of 13 Gyr:

* Could not converge for M=0.764:

```
Returned from Model: KTINT=1 DT=    19.4862685542735
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    19.4862685542735        19.4862685542735
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    73.4873207186613        881.847848623936
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing

 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

*Density too high in LowT: jm,lgr,lgt=1045  1.000  3.662
 Stopping in modout after return from PHYS1
```


* 12.994 Gyr for M=0.7915, close enough.



## Evolving model with higher mass loss `eta=0.6`.

Having convergence problems after horizontal branch:

```
-----------------------------------------------
  NITER>NITMAX, TINT(yr)=1.544E-08
 -----------------------------------------------
 Convergence numbers:
                G  L:    18864565170210.9
                G  T:   0.282325827683933
                G  R:   2.053410703038701E-005
                G  P:   5.273760336599286E-005
 -----------------------------------------------
                D  L:    3969.28882265322
                D  T:    1.32352385333296
                D  R:   1.387482051989218E-005
                D  P:   5.273760336599286E-005
 -----------------------------------------------
 Returned from Model: KTINT=1 DT=   1.544121724018932E-008
 ** Having convergence probs, altering dt
 ** Problem tint, tintn =   1.544121724018932E-008  1.544121724018932E-008

 ------------------------------------------------
Convg probs: setting imix=100 for 10 models
 ------------------------------------------------

 ** Having convergence probs, altering dt
 ** Problem tint, tintn =    527343.750000000       5.406623606369037E+019

 ------------------------------------------------
Convg probs: setting imix=100 for 10 models
 ------------------------------------------------
```

## Comparing two 13 Gyr models with mass loss parameters `eta=0.4` and `eta=0.6`

![NGC 288 models with different mass loss](ngc288_different_mass_loss.png)

Figure 1: The lines are two 13 Gyr, Y=0.245, [Fe/H]=-1.19 MONSTAR models created with different Reimers mass loss rates. The white model uses eta=0.6 ([parameters](data/monstar_plots/m0.7915z001y245a4reta0.6/)). The blue model uses eta=0.4 ([parameters](data/monstar_plots/m0.7915z001y245/)). The red triangles are observed RGB stars from NGC 288 ([arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)). The green dots are observed horizontal branch stars ([arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)).

Plotting code: [compare_mass_loss_0.6.i](compare_mass_loss_0.6.i)

We can see that the track of the eta=0.6 model (white) becomes erratic after once it reaches the horizontal branch.


## Evolving model with higher mass loss `eta=0.5`.

Since the model `eta=0.6` did not behave well, we want to try a lower `eta=0.5` and see if it produces a sensible HR plot.

* Set `15` for `IMLOSS_RGB` in `current.param`.

* Change Line 324 in `mloss.f` file from `RETA=0.4D0` to `RETA=0.5D0` and run `make`.

```FORTRAN
C -(15)--- Reimers rate for Red Giant Branch -----
IF(IMLOSS.EQ.15)THEN
   RETA=0.5D0
   MDOTCGS=(RETA*1.34D-5*(LOLSUN**1.5)/((M/MSUN)*
&         ((10**L10TE)**2)))*6.3D25
   DM=MDOTCGS*TINT
ENDIF
```

The evolution stopped at the horizontal branch.

The last log records:

```
 **** JM=1 in rmshhb! J=           1
cen_heb                                     Mass=0.58445839, mdot= 8.605E-11
 T=1.5441855E+10 DT= 4.44E+03 NMOD=  18729 NM= 2371 ITS=  25 LTE= 3.873
       L(R,NU,H,HE,C,G) =  11.749  9.616 11.363 11.713  0.000 -11.261
       XHE4(C)= 0.0139 MccKCVTN= 0.103 MccVEL= 0.000 T,P,DW,WR,H,He,Rad= 17 99 99 74 99  8 99

    TMAX,RHO,MC-MT,XHE,XC= 1.608E+08 4.186E+04 4.87E-01 0.013945 0.351459
    MC,MC-(EC,SCH,SCL,HE)= 0.4867303 -9.77E-02 3.83E-01 4.87E-01 3.68E-01
       Surface C= 1.5220E-04 N= 5.0312E-05 O= 3.7843E-04 C/O= 5.3625E-01


 Using instant mixing

 ^^^Triple Alpha Running: Rate =    100679047.015087
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

 **** JM=1 in rmshhb! J=           1
 **** JM=1 in rmshhb! J=           1
```

The console output:

```
,j1,k1=          42          17
 highTop1(j,k) =  -0.886729081132706
 highTop1(j,k) =   -1.00118889409474
 highTop1(j,k) =  -0.887322863663852
 highTop1(j,k) =   -1.00139822210555
 T,rho =    398506722.068429        18481.5973986352
 difft,diffrho =    2.99401197604791        4.00000000000000
 highTop2(1),highTop2(2)= -0.978377687881576      -0.978663635547055

 STOP -- OPAC: non-positive value of T6=  3.985E+02 or R=  0.000E+00
```



## Comparing two 13 Gyr models with mass loss parameters `eta=0.4` and `eta=0.5`

![NGC 288 models with different mass loss eta=0.5](ngc288_different_mass_loss_eta_0.5.png)

Figure 2: The lines are two 13 Gyr, Y=0.245, [Fe/H]=-1.19 MONSTAR models created with different Reimers mass loss rates. The white model uses eta=0.5 ([parameters](data/monstar_plots/m0.7915z001y245a4reta0.5/)). The blue model uses eta=0.4 ([parameters](data/monstar_plots/m0.7915z001y245/)). The red triangles are observed RGB stars from NGC 288 ([arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)). The green dots are observed horizontal branch stars ([arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)).

Plotting code: [compare_mass_loss_0.5.i](compare_mass_loss_0.5.i)

We can see from Fig. 2 that the track of the eta=0.5 model (white) is significantly bluer than that of the eta=0.4 model (blue). The track has a better match for the observed hot horizontal branch stars, although it is a little bit less luminous. Perhaps, increasing the mass loss rate or age could make it more luminous.
