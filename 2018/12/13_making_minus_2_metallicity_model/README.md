## Making [Fe/H] = -1.9 model

Yesterday we tried making [Fe/H] = -2.0 but it did not converge. Today we try making a [Fe/H] = -1.9 Y=0.26 model with [method](2018/new_starting_model.md). 

The ages and masses were

* 11.3102 Gyr for M=0.79

* Failed to converge for M=0.78

## Making [Fe/H] = -1.8 model

* Failed to converge for M=0.78


## Making [Fe/H] = -1.7 model

* 11.3529 Gyr for M=0.79

* Failed to converge for M=0.77

* Failed to converge for M=0.78


## Making [Fe/H] = -1.6 model

* 11.3825 Gyr for M=0.79

* 11.9195 Gyr for M=0.78

* Failed to converge for M=0.77

* Failed to converge for M=0.775


## Making [Fe/H] = -1.5 model


* 11.413 Gyr for M=0.79

* 11.9837 Gyr for M=0.78

* Failed to converge for  M=0.77



Could not make a 13 Gyr model for -2.0 < [Fe/H] < -1.5.
