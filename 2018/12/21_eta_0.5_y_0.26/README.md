## Creating a model with high mass loss `eta=0.5` and high helium `y=0.26`

Yesterday we increased mass loss parameter eta from 0.4 to 0.5. This made the horizontal branch significantly bluer and produced a better fit to the blue horizontal branch stars.

Today we want to keep `eta=0.5` and increase the helium to y=0.26 and see if the horizontal branch can get bluer still to fit the extreme blue horizontal branch stars.

* Use `RETA=0.5D0` in Line 324 in `mloss.f` file and run `make`.

* Make a 13 Gyr model with parameters: M=0.7713, Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

## Comparing high mass loss `eta=0.5` models with different helium fractions `Y=0.245` and `Y=0.26`

![NGC 288 models with different helium content](different_helium_content.png)

Figure 1: The lines are two 13 Gyr, [Fe/H]=-1.19 MONSTAR models created with eta=0.5 Reimers mass loss rate and different helium content. The white model has Y=0.26 ([parameters](data/monstar_plots/m0.7713z001y26a4Reta0.5/)). The blue model has Y=0.245 ([parameters](data/monstar_plots/m0.7915z001y245a4reta0.5/)). The red triangles are observed RGB stars from NGC 288 ([arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)). The green dots are observed horizontal branch stars ([arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)).

Plotting code: [compare_helium.i](compare_helium.i)

We can see that the track of the model with higher helium (white) is hotter than the Y=0.245 model (blue). However, the model still does not get as hot as some of the extreme hot horizontal branch stars. Next, we could try increasing the age to 13.5 Gyr and see if the model can fit the extreme hot horizontal branch stars.
