#include "code/yorick/include.i"

func smpPlot(void)
{
  ev = smpOpenPlotFromMonstarArchive("m0.7915z001y24/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="green"

  ev = smpOpenPlotFromMonstarArchive("m0.7915z001y245/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="white"

  ev = smpOpenPlotFromMonstarArchive("m0.7915z001y26/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="red"

  ev = smpOpenPlotFromMonstarArchive("m0.7915z001y27/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="blue"

  ev = smpOpenPlotFromMonstarArchive("m0.7915z001y28/ev.xxx.zip")
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="yellow"

  // Captions and axes
  pltitle, "MONSTAR models (M=0.7915 M_sun_, z=0.001) \n with different He content"
  xytitles,"T [K]","log g [cgs]", [-0.02, 0]
  smpAddPlotMargin, 0.05
  revx
  revy

  // Legend
  plt, "Y=0.24", 0.22, 0.81, color="green";
  plt, "Y=0.245", 0.22, 0.78, color="white";
  plt, "Y=0.26", 0.22, 0.75, color="red";
  plt, "Y=0.27", 0.22, 0.72, color="blue";
  plt, "Y=0.28", 0.22, 0.69, color="yellow";
}