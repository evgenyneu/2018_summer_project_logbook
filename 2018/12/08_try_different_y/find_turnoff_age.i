#include "code/yorick/include.i"

func showAges(void)
{
  write, "Main sequence turnoff ages:"

  smpShowTurnoffGyr, "m0.7915z001y24/ev.xxx.zip"
  smpShowTurnoffGyr, "m0.7915z001y245/ev.xxx.zip"
  smpShowTurnoffGyr, "m0.7915z001y26/ev.xxx.zip"
  smpShowTurnoffGyr, "m0.7915z001y27/ev.xxx.zip"
  smpShowTurnoffGyr, "m0.7915z001y28/ev.xxx.zip"
}

func plotAges(void)
{
  heFraction = [0.24,0.245, 0.26, 0.27, 0.28]
  agesGyr = [13.3, 13.0, 11.7, 11.0, 10.3]
  plmk, agesGyr, heFraction, marker=2, msize=0.5, width=1, color="green"

  // Captions and axes
  pltitle, "Age of the main-sequence turnoff point \n for MONSTAR models (M=0.7915 M_sun_, z=0.001) \n with varying He fraction"
  smpAddPlotMargin, 0.05
  xytitles,"Y (He fraction)","Age [Gyr]", [-0.02,0]
}

// Prints the age of the horizontal branch turnoff, in yr
func smpShowTurnoffGyr(plotArchiveFilePath)
{
  age = smpCalculateTurnoffGyr(plotArchiveFilePath)
  write, plotArchiveFilePath, ": ", age, "yr"
}

// Calculates the age of the horizontal branch turnoff, in yr
func smpCalculateTurnoffGyr(plotArchiveFilePath)
{
  ev = smpOpenPlotFromMonstarArchive(plotArchiveFilePath)
  age = ev.age(ev.LogTe(1:10000)(mxx))
  return age
}