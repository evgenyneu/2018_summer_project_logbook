## Making HB bluer by increasing He contents

* We want to change He from 0.245 to 0.26 and see how it affects the horizontal branch.

* We  generate a new starting model with parameters: M=0.7915, Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

![NGC 288 models with different He content](NGC_288_compare_helium.png)

Figure 1: Hot horizontal-branch stars from NGC 288 (green dots) from spectroscopic data in [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2). The lines are the MONSTAR model for a M=0.7915 M_sun, z=0.001 star with different helium content: Y=0.245 (white, [parameters](data/monstar_plots/m0.7915z001y245/)) and Y=0.26 (red, [parameters](data/monstar_plots/m0.7915z001y26/)).

Plotting code: [compare_helium.i](compare_helium.i)

* We can see that increasing He resulted in a **slightly redder** horizontal branch.


## Trying models with different He fraction

Just for fun, we created models with other value for He content to check how it affects the horizontal branch.

![NGC 288 models with different He content](NGC_288_five_helium_fractions_2.png)

Figure 2: MONSTAR model for a M=0.7915 M_sun, z=0.001 star with different helium content: Y=0.24 (green, [parameters](data/monstar_plots/m0.7915z001y24/)), Y=0.245 (white, [parameters](data/monstar_plots/m0.7915z001y245/)), Y=0.26 (red, [parameters](data/monstar_plots/m0.7915z001y26/)), Y=0.27 (blue, [parameters](data/monstar_plots/m0.7915z001y27/)) and Y=0.28 (yellow, [parameters](data/monstar_plots/m0.7915z001y28/)).

![NGC 288 models with different He content](NGC_288_five_helium_fractions_zoomed_3.png)

Figure 3: Same plot as Fig. 2 but zoomed into the blue edge of the horizontal branch.

Plotting code: [compare_five_helium_fractions.i](compare_five_helium_fractions.i)

We can see from Fig. 3 that the bluest horizontal branch is from the Y=0.24 model and the reddest is from the Y=0.27. Higher He content corresponds to redder horizontal branch for all models except Y=0.28.


## The ages

We calculated the ages of the main sequence turnoff point for the models with varying helium content.

![Age vs He fraction](age_vs_he_fraction.png)

Figure 4: Age of the main sequence turnoff point for  MONSTAR models (M=0.7915 M_sun, z=0.001) with varying He fraction.

We can see that there is a strong inverse linear relationship between He fraction and the age. As we increased He, the age decreased, and the model produced a redder horizontal branch. Maybe next we can try again the Y=0.26 model, but this time we adjust its mass in order to keep it at 13 Gyr (same age as for the Y=0.245 model)?

Code: [find_turnoff_age.i](find_turnoff_age.i)


## The metallicities

May not be important. As we changed the He fraction, MONSTAR automatically changed the reported metallicities as well. 

* Y=0.24:  z = 0.0010392340.
* Y=0.245: z = 0.0010323970.
* Y=0.26:  z = 0.0010118858.
* Y=0.27:  z = 0.0009982116.
* Y=0.28:  z = 0.0009845375.

By "reported metallicities" we mean the output of MONSTAR like this:

```
>>> Z on surface of *Starting Model* = 0.0010392340 <<<
```


We  can see that metallicity decrease with the increasing He content. The change in metallicity is small: the metallicity of Y=0.28 model is 5% lower than that of Y=0.24.