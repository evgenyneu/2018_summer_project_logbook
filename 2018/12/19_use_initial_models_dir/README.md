## Creating an initial model using a different .inmod file

 Prevously, we had the 'Automatically select from initial model directory?' setting set to `0` in `initial_model_paramers`. Consequently, the initial model was created from an *.inmod file specified in the `infiles.list`. For example, when we  made an initial 13.5 Gyr model, we had `m0.80z0004y24a4.inmod` in `infiles.list`, and it was used to make the initial model. Here we want to make a 13.5 Gyr model by using an initial model from `initial_models` directory, and compare the results with our previous 13.5 Gyr model.

* Change 'Automatically select from initial model directory?' from `0` to `1` in `initial_model_paramers` file. This will make MONSTAR choose the best model from `inial_models` directory.

* Make model with parameters: M=0.764, Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

* The code uses `initial_models/m1.00z0001y2500.inmod01` model to create the initial model. It has significantly higher mass than we need. Therefore, we want to try the `m0.85z001y25.inmod` model that has lower mass M=0.85, by adding the following line to the start of `initial_models/file_list`, and increasing the first number that denotes the number of files:

```
0.85  0.0010   m0.85z001y25.inmod
```

* Make model with parameters: M=0.764, Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

* The turnoff age is slightly 13.5305 Gyr, this is higher than 13.5 that we needed. We can see that the model used to create an initial model does affect the age of the model.

In order to keep the age at 13.5 Gyr we need to try higher masses:

* 13.5305 Gyr for M=0.764.

* 13.0957 Gyr for M=0.77.

* 13.3699 Gyr for M=0.766.

* 13.4361 Gyr for M=0.7648.

* 13.4572 Gyr for M=0.7646.

* 13.4791 Gyr for M=0.7644.

* 13.5082 Gyr for M=0.7643, close enough.


## Comparing two models


![NGC 288 models with different initial models](ngc288_different_initial_models_2.png)

Figure 1: The lines are two 13.5 Gyr MONSTAR models created with different initial model files. The white model was created using `initial_models/m0.764z001y26.inmod` ([parameters](data/monstar_plots/m0.7643z001y26/)). The blue model was created from `m0.80z0004y24a4.inmod` ([parameters](data/monstar_plots/m0.764z001y26/)). The red triangles are observed RGB stars from NGC 288 ([arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)). The green dots are observed horizontal branch stars ([arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)).

Plotting code: [compare_models.i](compare_models.i)

We can see that the two models has nearly the same tracks. The white model has slightly different pre-main sequence track and a slightly more luminous tip of the giant branch. The horizontal branches of the two models seem to be identical. We conclude that the choice of the initial model did not have a noticeable impact.





## Using no alpha-enhancement

Here we want to make and evolve a 13.5 Gyr model without using alpha-enhancement. Previously, we created iniail models using setting [a/Fe]=0.4 in `initial_model_paramers`.

* Use `[a/Fe]=0.0`.

* Make model with parameters: M=0.7643, Y=0.26, [Fe/H]=-1.19 with [method](2018/new_starting_model.md).

We can see that it increased the turnoff age slightly to 13.527 Gyr from 13.5 Gyr. We want to keep the age at 13.5 Gyr by changing the masses:


* 13.527 Gyr for M=0.7643.

* 13.4542 Gyr for M=0.7646.

* 13.4668 Gyr for M=0.7644.

* 13.4729 Gyr for M=0.76435.

* 13.5250 Gyr for M=0.76433.

* 13.527 Gyr for M=0.7643.

* 13.2919 Gyr for M=0.7645.

* 13.2881 Gyr for M=0.7643.

* 13.2881 Gyr for M=0.7643.

* 13.2365 Gyr for M=0.765.

* Could not converge for M=0.76:

```
 ** Problem tint, tintn =    58.7551647492211        58.7551647492211
 ***Temporarily setting PCT =   1.000000000000000E-002
 Using instant mixing

 ^^^Triple Alpha Running: Rate =   0.000000000000000E+000
 JEQHe3, C, O =           0           0           0
 Need all = 0 to skip conv zone EQM

*Density too high in LowT: jm,lgr,lgt=1035  1.000  3.670
 Stopping in modout after return from PHYS1
```


* Could not converge for M=0.761.

* Change TINTN* from +1.0E+00 to +0.1E+00: could not converge.

* Set TINTN* to +0.01E+00: could not converge.

* Set TINTN* to +1.0E+02: could not converge.

* 13.2407 Gyr for M=0.764:

* Could not converge Gyr for M=0.763:

We could not make a 13.5 Gyr model with `[a/Fe]=0.0` due to convergence problems. Note that with `[a/Fe]=0.0` setting the reported metallicity was `0.0008652561` while `[a/Fe]=0.4` gave `0.0010118858`.








