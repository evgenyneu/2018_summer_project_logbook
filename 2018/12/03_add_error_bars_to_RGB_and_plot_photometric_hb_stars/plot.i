#include "code/yorick/include.i"

func smpPlot(void)
{
  // Read plot model
  f = smpOpenPlotFromMonstarArchive("m0.80z001y245/ev.xxx.zip")

  // Plot the model
  log_g = smpCalculateLogG(f)
  plg, log_g, 10^f.LogTe(i)

  // Plot the observed data
  filepath = "data/NGC288/arXiv:1406.5220/RGB_stars___T_eff_K___Log_g_cgs.csv";
  data = text_cells(filepath, ",")
  data = tonum(data(2:3,*)(*,2:))
  log_g = data(2,*)
  temperature = data(1,*)
  plmk,log_g, temperature,marker=4,msize=0.5,width=11,color="green"


  // Captions and axes
  pltitle, "NGC 288 RGB stars from Hsyu 2014 and MONSTAR model"
  xytitles,"T (K)","log(g) (cgs)";
  smpAddPlotMargin, 0.1
  revx
  revy

  smpErrorBarsFixedTopLeft, dx=100, dy=0.3, color="green", width=4
}


