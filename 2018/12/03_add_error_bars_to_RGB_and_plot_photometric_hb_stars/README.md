## Adding error bars

* Added error bars to the plot (Fig. 1). I was wrong, [arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1) did have uncertainties: T=±100K, log(g)=±0.3 cgs.

![NGC_288_RBG_monstar](NGC_288_arXiv_1406.5220v1_monstar_model_with_error_bars.png)

Figure 1: The AGB stars from NGC 288 (green dots) from [arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1). The solid line is the MONSTAR model for a M=0.80 M_sun, z=0.001, Y=0.245 star ([model parameters](data/monstar_plots/m0.80z001y245/)).

Plotting code: [plot.i](plot.i)


## Plotting hot HB stars using photometric data

* [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2) has photometric and spectroscopic values for T and log(g). First, we plot the spectroscopic values.

![NGC_288_hot_HB_monstar](NGC_288_hot_HB_photometric_arXiv_1403.7397v2_monstar_model.png)

Figure 2: Hot horizontal-branch stars from NGC 288 (green dots) from photometric data in [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2). The solid line is the MONSTAR model for a M=0.80 M_sun, z=0.001, Y=0.245 star ([model parameters](data/monstar_plots/m0.80z001y245/)).

Plotting code: [plot_hb_photometric.i](plot_hb_photometric.i)


