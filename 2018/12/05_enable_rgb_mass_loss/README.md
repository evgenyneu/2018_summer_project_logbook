## Enabling mass loss on RGB

* Previously, the mass loss was turned off in [parameters](data/monstar_plots/m0.80z001y245/):

```
IMLOSS  IMLOSS_RGB  FMLOSS_RGB  TMLOSS_RGB  FHLEFT_RGB   [IMLOSS=> 
15=Reimers, 8=Vass & Wood]
   -1        99        3.0        1.0e7        0.0
         IMLOSS_AGB  FMLOSS_AGB  TMLOSS_AGB  FHLEFT_AGB
             99        3.0        1.0e7        0.0
```

* We enabled mass loss on RGB by setting `IMLOSS_RGB` to `15`.



![NGC 288 hot HB monstar with mass loss](NGC_288_hot_HB_spectroscopic_arXiv_1403.7397v2_monstar_model_with_mass_loss.png)

Figure 1: Hot horizontal-branch stars from NGC 288 (green dots) from spectroscopic data in [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2). The lines are the MONSTAR model for a M=0.80 M_sun, z=0.001, Y=0.245 star with RGB mass loss (white, [parameters](data/monstar_plots/m0.80z001y245_RGB_mass_loss/)) and no mass loss (blue, [parameters](data/monstar_plots/m0.80z001y245/)).

Plotting code: [mass_loss_effect.i](mass_loss_effect.i)

* We can see that mass loss produces a longer and bluer horizontal branch (white line). The model is closer to observations but is still far from agreeing.

