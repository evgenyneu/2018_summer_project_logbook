![NGC_288_RBG_monstar](NGC_288_arXiv_1406.5220v1_monstar_model.png)

Figure 1: The RGB stars from NGC 288 (green dots) from [arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1). The solid line is the MONSTAR model for a 0.80 M_solar, z=0.001, Y=0.245 star ([model parameters](data/monstar_plots/m0.80z001y245/)).

Plotting code: [plot.i](plot.i)

The [arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1) does not have uncertainties for T, log(g) data. Therefore, this fit is probably meaningless.





