## Finding NGC 288 metallicity in literature

* [Fe/H]=-1.19±0.12 [Hsyu (2014), arXiv:1406.5220](https://arxiv.org/abs/1406.5220).

* [Fe/H]=-1.20±0.14 [Caputo (1992), 1983A&A...128..190C](http://adsabs.harvard.edu/abs/1992ApJ...401..260C).

* [Fe/H]=-1.14 [Marin-Franch (2008), arXiv:0812.4541](https://arxiv.org/abs/0812.4541).

* [Fe/H] = -1.39±0.01 [Shetrone & Keane (2000), AJ, 119:840-850](http://iopscience.iop.org/article/10.1086/301232/fulltext/) for giant stars.

* [Fe/H] = -1.24 [Harris, William E, 2003 version of A Catalog of Parameters for Globular Clusters in the Milky Way](http://www.naic.edu/~pulsar/catalogs/mwgc.txt).

* [Fe/H] = -1.24±0.04 [Chen, Alfred Bing-Chih; Tsay, Wean-Shun (2000), 2000AJ....120.2569C](http://adsabs.harvard.edu/abs/2000AJ....120.2569C).


## Finding 13 Gyr model for [Fe/H]=-1.3

Here we want to try different metallicities and see if we can fit the observations better. We also want to keep the turnoff age constant at 13 Gyr by adjusting mass.

* We  generate a new starting model with parameters: M=[varying], Y=0.26, [Fe/H]=-1.3 with [method](2018/new_starting_model.md).

We get the following turnoff ages for masses.

* 12.75 Gyr for M = 0.7713.

* 11.59 Gyr for M = 0.79.

* 12.9866 Gyr for M = 0.7677.

* 12.982 Gyr for M = 0.7676.

* 12.975 Gyr for M = 0.767.

* 13.052 Gyr for M = 0.766.

* 13.013 Gyr for M = 0.7665.

* 13.001 Gyr for M = 0.7667 (Close enough).

## Comparing [Fe/H]=-1.3 and [Fe/H]=-1.19 models

![NGC 288 models with two metallicities](ngc288_compare_metallicity.png)

Figure 1: The RGB stars from NGC 288 (red triangles, [arXiv:1406.5220v1](https://arxiv.org/abs/1406.5220v1)) and hot horizontal branch stars (green dots, [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2)). The lines are the MONSTAR model for a Y=0.26 star that has main sequence turnoff age of 13 Gyr. The white model has [Fe/H]=-1.3 ([parameters](data/monstar_plots/m0.7667z000786y26/)). The red model has[Fe/H]=-1.19 [parameters](data/monstar_plots/m0.7713z001y26/).

Plotting code: [compare_metallicity.i](compare_metallicity.i)

We can see that the lower metallicity model (white) produces a slightly bluer horizontal branch.
