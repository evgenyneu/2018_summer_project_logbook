#include "code/yorick/read_plot.i"

func smpPlot(dir=)
{
  ev = smpOpenPlotFromEvXXX(dir=dir)
  plg, ev.LogTe, ev.age
  age = ev.age(ev.LogTe(mxx))
  write, "Age: ", age, " yr"
}