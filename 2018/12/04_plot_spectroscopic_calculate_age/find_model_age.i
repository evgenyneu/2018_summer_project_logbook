#include "code/yorick/include.i"

func smpPlot(void)
{
  // Plot the model
  ev = smpOpenPlotFromMonstarArchive("m0.80z001y245/ev.xxx.zip")
  plg, ev.LogTe, ev.age
  write, "Age of T_eff (turnoff point):", ev.age(ev.LogTe(mxx)), " a"

  // Captions and axes
  pltitle, "Finding age for a model m0.80z001y245"
  xytitles,"Age [a]","log T_eff", [-0.02,0]
}