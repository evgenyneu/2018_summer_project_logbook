## Plotting hot HB stars using spectroscopic data


![NGC_288_hot_HB_monstar](NGC_288_hot_HB_spectroscopic_arXiv_1403.7397v2_monstar_model.png)

Figure 1: Hot horizontal-branch stars from NGC 288 (green dots) from spectroscopic data in [arXiv:1403.7397v2](https://arxiv.org/abs/1403.7397v2). The solid line is the MONSTAR model for a M=0.80 M_sun, z=0.001, Y=0.245 star ([model parameters](data/monstar_plots/m0.80z001y245/)).

Plotting code: [plot_hb_spectroscopic.i](plot_hb_spectroscopic.i)

* The model does not fit the observations. Need to make other models with different M, z and Y parameters and try again.

## Age of NGC 288 in literature

* [arXiv:1001.4289](https://arxiv.org/pdf/1001.4289.pdf): 10.62 Gyr.

* [AJ Penny 1984](http://adsabs.harvard.edu/full/1984MNRAS.208..559P): 14-19 Gyr.


## Determining the age of the [m0.80z001y245](data/monstar_plots/m0.80z001y245/) model

* Plotted T_eff vs age, the age at the maximium T_eff is about 12.4 Gyr.

Code: [find_model_age.i](find_model_age.i)

![Finding model age](age_of_model.png)

Figure 2: Finding age of the turnoff point for the MONSTAR model for a M=0.80 M_sun, z=0.001, Y=0.245 star ([model parameters](data/monstar_plots/m0.80z001y245/)).

* The turnoff age 12.4 Gyr of our model is larger than estimated age of NGC 288 of 10.62 Gyr given [arXiv:1001.4289](https://arxiv.org/pdf/1001.4289.pdf).

* We can decrease the turnoff age by increasing the mass of the model star.

