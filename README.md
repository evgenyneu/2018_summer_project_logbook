# 2018 Summer project logbook

This is a logbook for Evgenii's 2018/2019 summer project, supervised by Simon Campbell. The logbook files are located in the years/months/days directories.

## Setup

* In order to make the plots, setup the Yorick code from https://gitlab.com/evgenyneu/monstar_plots repository.

* Link the observations directory containing .fits files from the telescope to `data/observe`. For example:

```
cd data
ln -s ../../data/observe ./
```

### Installing Conda, IRAF and Pyraf

Follow [this manual](a2019/a01/a11_learning_pyraf/install_iraf_pyraf_conda.md) to install IRAF and Pyraf.

### Initialize IRAF

From parent directory of logbook, run

```
mkiraf
```

and select `xgterm`.

### 2DfDr

[Install 2DfDr](https://www.aao.gov.au/science/software/2dfdr) in order to run unit tests.

### Python

Here is the setup needed to run Python code:

1. Install conda with IRAF, Pyraf and Python 3 using these [instructions](a2019/a01/a11_learning_pyraf/install_iraf_pyraf_conda.md).

1. Install the following Python packages:

Run `source activate iraf` and install the Python packages:

```
pip install pandas
pip install uncertainties
pip install astropy
pip install pytest
pip install pytest-random-order
pip install freezegun
pip install tqdm
```

## Plotting

Example of plotting using [2018/12/02_plot_RGB_stars/plot.i](2018/12/02_plot_RGB_stars/plot.i) file:


* Run yorick concole **from the root directory** of this repository:

```
rlwrap -c yorick
```

* Include the plot file:

```
#include "2018/12/02_plot_RGB_stars/plot.i"
```

* Run the plotting function from `plot.i`:

```
smpPlot
```


## Running Python code

Here is how to run Python scripts located in [code/python](code/python) directory.

* `cd` to the parent of the logbook directory.

* Rename logbook directory `mv 2018_summer_project_logbook logbook`.


There are two way of running the scripts:


#### 1. From unix terminal:

```
python -m logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocity_histogram
```

where `logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocity_histogram` is the path to the Python script with `/` replaced with `.` and the `.py` extension removed.


#### 2. From pyraf Terminal

Run `pyraf` terminal and run the `plot` function from `plot_velocity_histogram.py` file:


```
from logbook.a2019.a01.a12_find_velocities_of_stars.plot_velocity_histogram import plot
plot()
```

## Compiling `export_glow`

In order to run full data reduction or unit tests, one needs to compile the [export_glow utility](code/fortran/export_glow/).


## Running Python tests

To run the Python unit test located in `*_test.py` file:

* `cd` to the logbook directory.

* Activate iraf environment: `source activate iraf`

* Run all tests:

```
pytest
```

* Run all fast tests:

```
pytest -k-slow
```

