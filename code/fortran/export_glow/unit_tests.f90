module unit_tests
use median_mod_test, only: median_test_all
use qsort_mod_test, only: qsort_test_all
use source_box_mod_test, only: source_box_test_all
implicit none
private
public test_all

contains

subroutine test_all
! Runs all unit tests

    print *, ""
    print *, "Running unit tests..."
    print *, ""
    call median_test_all
    call qsort_test_all
    call source_box_test_all

end subroutine

end module