# Installing CFITSIO

The following instruction shows how to install CFITSIO library that reads/writes FITS image files on Ubuntu Linux and Mac.

### Download

Download the source code:

```
wget http://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio_latest.tar.gz
```

### Extract the archive

```
tar xvzf cfitsio_latest.tar.gz
```

### Configure

```
cd cfitsio
./configure --prefix=/usr/local

```

My [configure output](configure_output.txt).

### Compile

```
make
```

My [make output](make_output.txt).


### Install

Finally, run the following command to copy libraries to system location:

```
sudo make install
```

My [make install output](make_install_output.txt).


### Test

To verify your installation, compile and run the test program:

```
make testprog
./testprog > testprog.lis
```

Finally, compare the output of the program with what was expected:

```
diff testprog.lis testprog.out
cmp testprog.fit testprog.std
```

If everything went well, you will see no messages. Congrats, jobs done!



## Useful links

* [CFITSIO website](https://heasarc.gsfc.nasa.gov/docs/software/fitsio/fitsio.html).