module command_line_parameters_mod
implicit none
private
public command_line_parameters, print_help

contains

subroutine command_line_parameters(filename_data, filename_tram, filename_output, silent, test, &
    do_work, rewrite)
    ! Returns the command line command line parameters
    !
    ! Arguments
    ! ----------
    !
    ! filename_data     path to a FITS file containing the image'
    ! filename_tram     path to a FITS file containing tram lines (a *tlm.fits file created by 2DfDr)'
    ! filename_output   path to the output FITS file that will be created'
    ! silent:           If true, do not show non-error output.
    ! test:             If true, run unit tests and exit.
    ! do_work:          If true, export glow.
    ! rewrite:          rewrite the output file if it exists.
    !

    character, intent(out) :: filename_tram*1024, filename_data*1024, filename_output*1024
    logical, intent(out) :: silent, test, do_work, rewrite
    character(len=1024) :: arg
    integer :: i, start_option

    filename_tram = ""
    filename_data = ""
    filename_output = ""
    silent = .false.
    test = .false.
    do_work = .false.
    rewrite = .false.
    start_option = 1

    ! Check the command line arguments were provided
    if (command_argument_count() >= 3) then
        call get_command_argument(1, filename_data)
        call get_command_argument(2, filename_tram)
        call get_command_argument(3, filename_output)
        do_work = .true.
        start_option = 4
    endif

    if (command_argument_count() == 0) then
        test = .true.
        call print_help()
        return
    end if

    do i = start_option, command_argument_count()
        call get_command_argument(i, arg)

        select case (arg)
        case ("-t", "--test")
          test = .true.
          do_work = .false.
          return
        case ("-s", "--silent")
          silent = .true.
          return
        case ("-r", "--rewrite")
          rewrite = .true.
          return
        case ('-h', '--help')
          call print_help()
          do_work = .false.
          stop
        case default
          print '(a,a,/)', 'Unrecognized command-line option: ', trim(arg)
          call print_help()
          stop
        end select
    end do

end subroutine

subroutine print_help()
    print '(a)', 'Usage: export_glow IMAGE TRAMLINE OUTPUT [OPTIONS]'
    print '(a)', ''
    print '(a)', 'Options:'
    print '(a)', ''
    print '(a)', '  IMAGE             path to a FITS file containing the image'
    print '(a)', '  TRAMLINE          path to a FITS file containing tram lines'
    print '(a)','                      (a *tlm.fits file created by 2DfDr)'
    print '(a)', '  OUTPUT            path to the output FITS file that will be created'
    print '(a)', '  -t, --test        run unit tests and exit'
    print '(a)', '  -s, --silent      do not show non-error output'
    print '(a)', '  -r, --rewrite     rewrites the output file if it exists'
    print '(a)', '  -h, --help        print help'
end subroutine print_help

end module