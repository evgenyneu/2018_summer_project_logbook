module qsort_mod_test
use qsort_mod, only: qsort
use test_asserts, only: assert_equal, assert_true
implicit none
private
public qsort_test_all

contains

subroutine qsort_int_test()
  integer :: a(5)= [4, 12, 9, 2, 1]
  call qsort(a)
  call assert_equal("qsort_int2_test", a, [1, 2, 4, 9, 12])
end

subroutine qsort_test_all
    call qsort_int_test
end

end module