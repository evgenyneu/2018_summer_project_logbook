module source_box_mod_test
use source_box_mod, only: source_box_below, source_box_above
use test_asserts, only: assert_true, assert_equal
implicit none
private
public source_box_test_all

contains

subroutine test_source_box_below_full_size()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_below(image_data=image_data, x=5, y=5, xmax=10, box=result)

    expect_result = [(i, i=11,40)]
        call assert_equal("test_source_box_below_full_size", result, expect_result)
end

subroutine test_source_box_below_no_box()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_below(image_data=image_data, x=1, y=1, xmax=10, box=result)

    call assert_equal("test_source_box_below_no_box", size(result), 0)
end

subroutine test_source_box_below_small_bottom_left()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_below(image_data=image_data, x=2, y=2, xmax=10, box=result)

    expect_result = [1, 2, 3, 4, 5, 6, 7]
    call assert_equal("test_source_box_below_small_bottom_left", result, expect_result)
end

subroutine test_source_box_below_small_top_left()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_below(image_data=image_data, x=2, y=10, xmax=10, box=result)

    expect_result = [61, 62, 63, 64, 65, 66, 67, 71, 72, 73, 74, 75, 76, 77, 81, 82, 83, 84, 85, 86, 87]
    call assert_equal("test_source_box_below_small_top_left", result, expect_result)
end

subroutine test_source_box_below_small_bottom_right()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_below(image_data=image_data, x=9, y=2, xmax=10, box=result)

    expect_result = [4, 5, 6, 7, 8, 9, 10]
    call assert_equal("test_source_box_below_small_bottom_right", result, expect_result)
end

subroutine test_source_box_below_small_top_right()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_below(image_data=image_data, x=9, y=10, xmax=10, box=result)

    expect_result = [64, 65, 66, 67, 68, 69, 70,  74, 75, 76, 77, 78, 79, 80, 84, 85, 86, 87, 88, 89, 90]
    call assert_equal("test_source_box_below_small_top_right", result, expect_result)
end

subroutine test_source_box_above_full_size()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_above(image_data=image_data, x=5, y=5, xmax=10, box=result)

    expect_result = [(i, i=51,80)]
    call assert_equal("test_source_box_above_full_size", result, expect_result)
end

subroutine test_source_box_above_no_box()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_above(image_data=image_data, x=1, y=10, xmax=10, box=result)

    call assert_equal("test_source_box_above_no_box", size(result), 0)
end

subroutine test_source_box_above_small_bottom_left()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_above(image_data=image_data, x=2, y=2, xmax=10, box=result)

    expect_result = [21, 22, 23, 24, 25, 26, 27, 31, 32, 33, 34, 35, 36, 37, 41, 42, 43, 44, 45, 46, 47]
    call assert_equal("test_source_box_above_small_bottom_left", result, expect_result)
end

subroutine test_source_box_above_small_top_left()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_above(image_data=image_data, x=2, y=9, xmax=10, box=result)

    expect_result = [91, 92, 93, 94, 95, 96, 97]
    call assert_equal("test_source_box_above_small_top_left", result, expect_result)
end

subroutine test_source_box_above_small_top_right()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_above(image_data=image_data, x=9, y=9, xmax=10, box=result)

    expect_result = [94, 95, 96, 97, 98, 99, 100]
    call assert_equal("test_source_box_above_small_top_right", result, expect_result)
end

subroutine test_source_box_above_small_bottom_right()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)
    integer, ALLOCATABLE :: expect_result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_above(image_data=image_data, x=9, y=1, xmax=10, box=result)

    expect_result = [14, 15, 16, 17, 18, 19, 20, 24, 25, 26, 27, 28, 29, 30, 34, 35, 36, 37, 38, 39, 40]
    call assert_equal("test_source_box_above_small_bottom_right", result, expect_result)
end


subroutine test_source_box_above_outside_bounds()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_above(image_data=image_data, x=16, y=16, xmax=10, box=result)
    call assert_equal("test_source_box_outside_bounds", size(result), 0)

    call source_box_above(image_data=image_data, x=-6, y=-6, xmax=12, box=result)
    call assert_equal("test_source_box_outside_bounds", size(result), 0)
end

subroutine test_source_box_below_outside_bounds()
    integer :: image_data(10,10)
    integer :: i
    integer, ALLOCATABLE :: result(:)

    image_data = reshape([(i, i=1,100)], [10, 10])

    call source_box_below(image_data=image_data, x=16, y=16, xmax=-4, box=result)
    call assert_equal("test_source_box_outside_bounds", size(result), 0)

    call source_box_below(image_data=image_data, x=-6, y=-6, xmax=14, box=result)
    call assert_equal("test_source_box_outside_bounds", size(result), 0)
end


subroutine source_box_test_all
    call test_source_box_below_full_size
    call test_source_box_below_no_box
    call test_source_box_below_small_bottom_left
    call test_source_box_below_small_top_left
    call test_source_box_below_small_bottom_right
    call test_source_box_below_small_top_right

    call test_source_box_above_full_size
    call test_source_box_above_no_box
    call test_source_box_above_small_bottom_left
    call test_source_box_above_small_top_left
    call test_source_box_above_small_top_right
    call test_source_box_above_small_bottom_right

    call test_source_box_above_outside_bounds
    call test_source_box_below_outside_bounds
end

end module