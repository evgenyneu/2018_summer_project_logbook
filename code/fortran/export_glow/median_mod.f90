module median_mod
use types, only: dp
use qsort_mod, only: qsort
implicit none
private
public median

interface median
    module procedure median_int
end interface

contains

real(dp) function median_int(a)
! Returns the median value of the `a` array

  integer, intent(in) :: a(:)
  integer :: l
  integer :: ac(size(a,1))

  ac = a
  call qsort(ac)

  l = size(a,1)
  if ( mod(l, 2) == 0 ) then
    median_int = real( ac(l/2+1) + ac(l/2), dp) / 2.0_dp
  else
    median_int = real(ac(l/2+1), dp)
  end if

end function

end module