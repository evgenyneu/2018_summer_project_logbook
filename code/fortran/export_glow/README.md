# Glow extraction utility

A utility that creates a .fits image by erasing the fibres with pixes taken from the parked fibres (Fig. 1). This allows to make an image which is similar to a sky frame that contains only the background glow of the CCD and does not include the signal form the fibres.

![Raw image and the extracted glow](graphics/glow_removed_source_box_interpolated.gif)

Figure 1: The glow extracted form the row image with fibre pixel replaced by the values of the parked pixels below and above the fibre.


## Usage

```
./export_glow data.fits tram.fits output.fits
```

where data.fits and tram.fits are the FITS files containing image data and tram. The third argument, output.fits, is the name of the output file that will be created.

Run export glow utility without parameters to see available options.

## Installation

Before compiling export_glow utility, we need to install CFITSIO library, which is used for reading and writing the FITS files.


### Installing CFITSIO

See [installing_cfitsio](installing_cfitsio) instruction.


## Compiling export_glow


Install [gfortran](https://gcc.gnu.org/wiki/GFortran) compiler. On Ubuntu:

```
sudo apt-get install gfortran.
```

## Install libcurl (Ubuntu only)

```
sudo apt-get remove libcurl3-dev
```


### Compiling

Finally, to build the export_glow executable, `cd` to export_glow directory and run

```
make
```

## How the program works

The program erases the fibre pixels by interpolating between two meadian values found in the "source pixel" boxes located along the parked fibres, as shown on Fig. 2.


![erasing_fibres_in_export_glow](graphics/erasing_fibres_in_export_glow_from_parked.png)

Figure 2: The method used in the program to erase the fibre pixels (yellow) with values from the parked fibres (green).

The result of the glow extraction is shown on Fig 3.

![Raw image and the extracted glow](graphics/glow_removed_source_box_interpolated_zoomed.gif)

Figure 3: The zoomed-in view of the glow extracted form the raw image.