! Returns the source pixels from the image data.
module source_box_mod
implicit none

private
public source_box_below, source_box_above


contains

subroutine source_box_above(image_data, x, y, xmax, box)
    ! Returns an array of the source pixels located above the position x and y
    integer, intent(in) :: image_data(:, :)
    integer, intent(in) :: x, y
    integer, intent(in) :: xmax
    integer, allocatable, intent(out) :: box(:)
    integer :: source_box_x_start, source_box_x_end, source_box_y_start, source_box_y_end
    integer, parameter :: source_box_width=11
    integer, parameter :: source_box_height=3
    integer :: ymax
    integer :: box_size

    ymax = size(image_data, 2)

    source_box_x_start = x - int(real(source_box_width) / 2)
    if (source_box_x_start < 1) source_box_x_start = 1
    source_box_x_end = x + int(real(source_box_width) / 2)
    if (source_box_x_end > xmax) source_box_x_end = xmax

    source_box_y_start = y + 1

    if (source_box_y_start < 1) source_box_y_start = 1

    if (source_box_y_start > ymax) then
        allocate(box(1))
        box = box(1:0) ! Empty array
        return
    endif

    source_box_y_end = source_box_y_start + source_box_height - 1
    if (source_box_y_end > ymax) source_box_y_end = ymax

    box_size = (source_box_x_end - source_box_x_start + 1) * &
                (source_box_y_end - source_box_y_start + 1)

    allocate(box(box_size))

    box = pack(image_data(source_box_x_start:source_box_x_end, source_box_y_start:source_box_y_end), mask=.true.)
end

subroutine source_box_below(image_data, x, y, xmax, box)
    ! Returns an array of the source pixels located below the position x and y
    integer, intent(in) :: image_data(:, :)
    integer, intent(in) :: x, y
    integer, intent(in) :: xmax
    integer, allocatable, intent(out) :: box(:)
    integer :: source_box_x_start, source_box_x_end, source_box_y_start, source_box_y_end
    integer, parameter :: source_box_width=11
    integer, parameter :: source_box_height=3
    integer :: ymax
    integer :: box_size

    ymax = size(image_data, 2)

    source_box_x_start = x - int(real(source_box_width) / 2)
    if (source_box_x_start < 1) source_box_x_start = 1
    source_box_x_end = x + int(real(source_box_width) / 2)
    if (source_box_x_end > xmax) source_box_x_end = xmax

    source_box_y_end = y - 1

    if (source_box_y_end < 1) then
        allocate(box(1))
        box = box(1:0) ! Empty array
        return
    endif

    if (source_box_y_end > ymax) source_box_y_end = ymax

    source_box_y_start = source_box_y_end - source_box_height + 1
    if (source_box_y_start < 1) source_box_y_start = 1

    box_size = (source_box_x_end - source_box_x_start + 1) * &
                (source_box_y_end - source_box_y_start + 1)

    allocate(box(box_size))

    box = pack(image_data(source_box_x_start:source_box_x_end, source_box_y_start:source_box_y_end), mask=.true.)
end

end module