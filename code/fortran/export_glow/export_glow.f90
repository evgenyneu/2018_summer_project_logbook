program export
use export_glow_mod, only: export_glow
use unit_tests, only: test_all
use command_line_parameters_mod, only: command_line_parameters, print_help
implicit none

character :: filename_tram*1024, filename_data*1024, filename_output*1024
logical :: silent, test, do_work, rewrite

call command_line_parameters(filename_data=filename_data, &
                             filename_tram=filename_tram, &
                             filename_output=filename_output, &
                             silent=silent, test=test, do_work=do_work, rewrite=rewrite)

if (test) call test_all

if (do_work) then
    call export_glow(filename_data=filename_data, filename_tram=filename_tram, &
                     filename_output=filename_output, silent=silent, rewrite=rewrite)
endif

end program