module export_glow_mod
use types, only: sp, dp
use source_box_mod, only: source_box_below, source_box_above
use median_mod, only: median
implicit none
private
public export_glow

contains

subroutine export_glow(filename_data, filename_tram, filename_output, silent, rewrite)
! --------
! Creates a .fits image by keeping pixels between fibres and erasing the fibres.
!
! Command line arguments:
!  $ ./export_glow data.fits tram.fits output.fits
! where data.fits and tram.fits are the FITS files containing image data
! and tram.
! output.fits is the name of the output file.
! Returns the command line command line parameters
!
!
!
! Arguments
! ----------
! filename_data     path to a FITS file containing the image'
! filename_tram     path to a FITS file containing tram lines (a *tlm.fits file created by 2DfDr)'
! filename_output   path to the output FITS file that will be created'
! silent:           if true, do not show non-error output.
! rewrite:          rewrite the output file if it exists.
!


character, intent(in) :: filename_tram*1024, filename_data*1024, filename_output*1024
logical, intent(in) :: silent, rewrite
character :: record*80, length*80, comment*80, nullstr*1
integer, parameter :: max_fibres=400 ! Used for definining array dimension
integer, parameter :: max_x=5000 ! Used for definining array dimension
integer :: status, unit_tram, unit_data, readwrite, blocksize, nfound, naxes_tram(2), total_fibres
integer :: unit_output
integer :: hdutype, naxes_data(2), xmax, colnum, frow, felem, nelems, irow
character :: name*20, cmd_delete*1200, fibre_names(max_fibres)*20
logical :: exact, anynull
integer :: x, fibre_index, group
integer :: nullval
real(sp) :: fibre_y(max_x, max_fibres)

! The y-coordinates of the start/end of the region that will be erased
integer :: y_erase_start, y_erase_end

integer :: i
integer :: previous_parked_y ! The y-coordinate of the prvious PARKED fibre
integer :: image_data(max_x, max_x)
logical :: file_exists
integer, ALLOCATABLE :: source_box_start(:), source_box_end(:)
real(dp) :: start_median, end_median
integer :: hdunum, ihdu
logical :: found_fibres

! The fistance from the center of parked fibre.
! This is used to defined the edges of the region to erase
integer, parameter :: parked_fibre_margin = 2

! Check if the output file already exists
inquire(file=filename_output, exist=file_exists)
if (file_exists) then
    if (rewrite) then
        cmd_delete = 'rm '//filename_output
        call system(trim(cmd_delete))
    else
        print *,'ERROR: output file already exists: '//trim(filename_output)
        return
    end if
endif

! The STATUS parameter must always be initialized.
status=0

! Open the tram FITS file
! -----------------

! Get an unused Logical Unit Number to use to open the FITS file.
call ftgiou(unit_tram,status)

! Open the tram FITS file
readwrite=0
call ftopen(unit_tram,filename_tram,readwrite,blocksize,status)

! Open data file
! -----------------

! Get an unused Logical Unit Number to use to open the FITS file.
call ftgiou(unit_data,status)

! Open the FITS file, with read-only access.
readwrite=0
call ftopen(unit_data,filename_data,readwrite,blocksize,status)

! Determine the size of the tram image.
! ----------------

call ftgknj(unit_tram, 'NAXIS', 1, 2, naxes_tram, nfound, status)

! Check that it found both NAXIS1 and NAXIS2 keywords.
if (nfound .ne. 2)then
  print *,'READIMAGE failed to read the NAXISn keywords for tram.'
  return
end if

total_fibres = naxes_tram(2)

! Determine the size of the data image.
! ----------------

call ftgknj(unit_data, 'NAXIS', 1, 2, naxes_data, nfound, status)

! Check that it found both NAXIS1 and NAXIS2 keywords.
if (nfound .ne. 2)then
  print *,'READIMAGE failed to read the NAXISn keywords for data.'
  return
end if

! Find the width of the image
xmax = min(naxes_tram(1), naxes_data(1))

! Load the image data
group=1
nullval=0
call ftg2dj(unit_data, group, nullval, max_x, naxes_data(1), naxes_data(2), image_data, anynull, status)

! Find the table containing information about fibres
! ----------------

! Get the total number of extensions in the file
call ftthdu(unit_data, hdunum, status)

found_fibres = .false.

hdu_loop: do ihdu=1, hdunum
    ! Move to the next extension
    call ftmrhd(unit_data,1,hdutype,status)

    call ftgsky(unit_data,'EXTNAME',1,80, record, length, comment, status)

    if (record == 'STRUCT.MORE.FIBRES' .or. record == 'FIBRES') then
        found_fibres = .true.
        exit hdu_loop
    endif
end do hdu_loop

if (.not. found_fibres) then
    print *, 'Can not find fibres table.'
    return
end if


! Find the non-parked fibres
! ---------------------------

! FTGCNO gets the column number of the `NAME' column
exact=.false.
call ftgcno(unit_data, exact, 'NAME', colnum, status)

frow=1
felem=1
nelems=1
nullstr=' '

! Collect the indexes of fibres (excluding parked)
do irow=1, total_fibres
    call ftgcvs(unit_data, colnum, irow, felem, nelems, nullstr, name, anynull, status)
    fibre_names(irow) = name
end do

! Load the y coordinates of fibres
! -------------

group=1
nullval=0
call ftg2de(unit_tram, group, nullval, max_x, naxes_tram(1), naxes_tram(2), fibre_y, anynull, status)

y_erase_start = -1
previous_parked_y = -1

! Loop over the x values
do x=1, xmax
    ! Loop over fibres
    fibre_loop: do fibre_index=1, total_fibres
        if (y_erase_start == -1) then ! we have not found the start of the region to erase yet
            if (fibre_names(fibre_index) == 'PARKED') then
                previous_parked_y = nint(fibre_y(x, fibre_index))
            else
                ! Use the center of the parked fibed as the start of the region that will be erased
                y_erase_start = previous_parked_y + parked_fibre_margin

                if (previous_parked_y == -1) then
                    print *, "ERROR: the bottom fibre is not PARKED for fibre: ", fibre_index
                    stop 1
                end if
            end if

            cycle fibre_loop
        else
            if (fibre_names(fibre_index) /= 'PARKED') then
                ! The next fibre is not parked
                ! Skip until the next fibre is parked, since we want to erase fibres
                ! with brightness values from parked fibres.
                cycle fibre_loop
            end if
        end if

        ! Store this parked fibre y to be used as the start of the erased region for the NEXT
        ! non-parked fibre
        previous_parked_y = nint(fibre_y(x, fibre_index))

        ! Use the center of the parked fibed as the end of the region that will be erased
        y_erase_end = nint(fibre_y(x, fibre_index)) - parked_fibre_margin

        ! Get the median brightness values from the source boxes below and above the fibre
        ! ----------------

        call source_box_below(image_data=image_data, x=x, y=y_erase_start, xmax=xmax, &
                              box=source_box_start)

        start_median = median(source_box_start)

        call source_box_above(image_data=image_data, x=x, y=y_erase_end, xmax=xmax, &
                              box=source_box_end)

        end_median = median(source_box_end)

        ! Erase the fibre pixels by interpolating linearly between start_median and end_median
        ! values across the fibre
        forall(i = y_erase_start: y_erase_end)
            image_data(x, i) = nint(start_median + &
                (real(i, dp) - y_erase_start) / (y_erase_end - y_erase_start) * (end_median - start_median))
        end forall

        ! Use next fibre as starting point for the region we want to erase
        y_erase_start = -1
    end do fibre_loop
end do

! Create the output fits file

! Get an unused Logical Unit Number to use to create the FITS file.
call ftgiou(unit_output, status)

! Create the output file
blocksize=1
call ftinit(unit_output, filename_output, blocksize, status)

! Copy the entire data file to the output file
call ftcpfl(unit_data, unit_output, 1, 1, 1, status)

! Move back to the primary array
call ftmahd(unit_output, 1, hdutype, status)

! Save the data array into the output fits file
group=1
nullval=0
call ftp2dj(unit_output, group, max_x, naxes_data(1), naxes_data(2), image_data, status)

if (status .gt. 0) then
    call printerror(status)
    return
end if

! Close the fits files and free the unit numbers
call ftclos(unit_tram, status)
call ftfiou(unit_tram, status)

call ftclos(unit_data, status)
call ftfiou(unit_data, status)

call ftclos(unit_output, status)
call ftfiou(unit_output, status)

if (.not. silent) then
    print *, 'Glow extracted successfully to '//trim(filename_output)
end if

end subroutine

subroutine printerror(status)
! ------------------------
! This subroutine prints out the descriptive text corresponding to the
! error status value and prints out the contents of the internal
! error message stack generated by FITSIO whenever an error occurs.
! ------------------------

integer status
character errtext*30,errmessage*80

! Check if status is OK (no error); if so, simply return
if (status .le. 0) return

! Show error message
call ftgerr(status,errtext)
print *,'FITSIO Error Status =',status,': ',errtext

call ftgmsg(errmessage)
do while (errmessage .ne. ' ')
    print *,errmessage
    call ftgmsg(errmessage)
end do

end subroutine

end module