module median_mod_test
use median_mod, only: median
use types, only: dp
use test_asserts, only: assert_equal
implicit none
private
public median_test_all

contains

subroutine median_int_test_odd()
  integer :: a(5)= [4, 12, 9, 2, 1]
  call assert_equal("median_int_test_odd", median(a), 4.0_dp)
end

subroutine median_int_test_even
  integer :: a(6)= [23, 1, 6, 3, 34, 0]
  call assert_equal("median_int_test_even", median(a), 4.5_dp)
end

subroutine median_test_all
    call median_int_test_odd
    call median_int_test_even
end

end module