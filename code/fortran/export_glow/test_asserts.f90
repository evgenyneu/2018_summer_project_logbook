! Assertions to be used in unit tests

module test_asserts
use types, only: dp
implicit none
private
public assert_equal, assert_true

interface assert_equal
    module procedure assert_equal_int, assert_equal_real_dp, assert_equal_array_int
end interface

contains

subroutine assert_true(name, condition)
    character(*), intent(in) :: name
    logical, intent(in) :: condition

    if (condition .neqv. .true.) then
        print '(a, " ERROR: expected condition to be true")', name
    end if
end

subroutine assert_equal_int(name, value, expectation)
    character(*), intent(in) :: name
    integer, intent(in) :: value, expectation

    if (value /= expectation) then
        print '(a, " ERROR: ", i0, " != ", i0)', name, value, expectation
    end if
end

subroutine assert_equal_real_dp(name, value, expectation)
    character(*), intent(in) :: name
    real(dp), intent(in) :: value, expectation

    if (abs(value - expectation) > epsilon(value)) then
        print '(a, " ERROR: ", G0, " != ", G0)', name, value, expectation
    end if
end

subroutine assert_equal_array_int(name, value, expectation)
    character(*), intent(in) :: name
    integer, intent(in) :: value(:), expectation(:)
    integer size_value, size_expectation, i
    size_value = size(value)
    size_expectation = size(expectation)

    if (size_value /= size_expectation) then
        print '(a, " ERROR: array sizes do not match ", i0, " != ", i0)', name, size_value, size_expectation
        return
    end if

    do i=1, size_value
        if (value(i) /= expectation(i)) then
            print '(a, " ERROR: arrays are not equal: ")', name
            print *, "   array 1: ", value
            print *, "   array 2: ", expectation
            exit
        end if
    enddo
end

end module