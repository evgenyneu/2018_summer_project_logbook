Contains **shared** code, i.e. used from more than one logbook entry.
Avoid changing the code in this directory, since it can break existing code.