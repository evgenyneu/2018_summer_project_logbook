# from .fits_file import fits_copy
from .fits_file import fits_copy, fits_copy_file
from .paths import ccd_day_take_dir
from ..shared import fixture_paths, files
from ..shared import cleanup
import os


def test_fits_copy():
    cleanup.remove_test_fits()

    # Prepare the input .fits files
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_data = f"{pathdir}/29oct10016.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    result = fits_copy(ccd=1, day=29, frame=16, take="p0", take_output="p0_output")

    expected_output = ccd_day_take_dir(ccd=1, day=29, take="p0_output")
    assert result == f"{expected_output}/29oct10016.fits"
    assert os.path.exists(result) is True

    cleanup.remove_test_fits()


def test_fits_copy_file():
    cleanup.remove_test_fits()

    # Prepare the input .fits files
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_data = f"{pathdir}/fish.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    result = fits_copy_file(ccd=1, day=29, take="p0", filename="fish.fits", take_output="p0_output")

    expected_output = ccd_day_take_dir(ccd=1, day=29, take="p0_output")
    assert result == f"{expected_output}/fish.fits"
    assert os.path.exists(result) is True

    cleanup.remove_test_fits()
