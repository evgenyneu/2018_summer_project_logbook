from .fibres import science_stars, star_name_by_fibre, fibre_by_star_name
from ..shared import fixtures, cleanup


def test_science_stars():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)
    df = science_stars(ccd=1, day=29, take="p0")
    assert len(df) == 90
    assert df.loc[4, 'Name'] == 'RGBSYAZ021904'
    cleanup.remove_test_fits()


def test_star_name_by_fibre():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)
    result = star_name_by_fibre(fibre_number=113, ccd=1, day=29, take="p0")
    assert result == 'AGBSYAZ017762'
    cleanup.remove_test_fits()


def test_star_name_by_fibre_does_not_exist():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)
    result = star_name_by_fibre(fibre_number=23423423, ccd=1, day=29, take="p0")
    assert result is None
    cleanup.remove_test_fits()


def test_fibre_by_star_name():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)
    result = fibre_by_star_name(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    assert result == 113
    cleanup.remove_test_fits()


def test_fibre_by_star_name_does_not_exist():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)
    result = fibre_by_star_name(name='sheep', ccd=1, day=29, take="p0")
    assert result is None
    cleanup.remove_test_fits()
