from astropy.io import fits


def set_header_value(path, extension, key, value):
    '''
    Update the header value in the FITS file.

    Parameters
    ----------
    path : str
        Path to the fits file.
    extension: str
        Name of the FITS extension
    key: str
        Name of the header key.
    value: str
        The value of the header item.
    '''
    hdul = fits.open(path, mode='update')
    header = hdul[extension].header
    header[key] = value
    hdul.flush()
    hdul.close()