# Remove background glow (light between fibres) form a fits file.
# Note that there is a Fortran alternative which does exactly the same task
# and is 10+ times faster here: /code/fortran/export_glow.

from astropy.io import fits
import math
import numpy as np
from tqdm import tqdm
import os
import sys
import multiprocessing
from multiprocessing import Pool


def calculate_bightness(data, x, tram_y_start, tram_y_end, interpolate):
    """
    Calculates the brightness value that will be used to erase a fibre at the given x-coordinate.

    Parameters
    ----------
    x : int
        The x-coordinate in the image.
    tram_y_start: int
        The y-coordinate of the start of the fibre.
    tram_y_end: int
        The y-coordinate of the start of the fibre.
    interpolate: Boolean
        If True, the fibres are filled with brightness values interpolated from pixels outside
        the fibres, which results in smoother glow image. If False, the each fibre is filled
        with single brightness value form a pixellocated just below the fibre.

    Returns
    -------
    int or numpy.ndarray
        The brightness value to be used to erase the fibre. If interpolate is True, then we
        return an array of brightness values.
    """

    ymax = data.shape[0]

    if interpolate:
        # Calculate the median brightness of pixels in a square region
        box_size = 3

        if box_size > 1 and tram_y_start - box_size >= 0 and x - box_size >= 0:
            pixels = data[tram_y_start - box_size: tram_y_start, x - box_size: x]
            pixels = pixels[~np.isnan(pixels)]  # Remove NaN values
            brightness_start = np.median(pixels)
        else:
            brightness_start = float(data[tram_y_start, x])

        if box_size > 1 and tram_y_end + box_size < ymax and x - box_size >= 0:
            pixels = data[tram_y_end: tram_y_end + box_size, x - box_size: x]
            pixels = pixels[~np.isnan(pixels)]  # Remove NaN values
            brightness_end = np.median(pixels)
        else:
            brightness_end = float(data[tram_y_end, x])

        # Interpolate brightness across the fibre
        return np.interp(list(range(tram_y_start, tram_y_end)),
                         [tram_y_start, tram_y_end], [brightness_start, brightness_end])
    else:
        return float(data[tram_y_start, x])


def do_the_work(datum):
    """
    The function called by each CPU worker multiple times in parallel to erase the fibres
    over the range of x coordinates passed in datum['range_of_x'].

    Parameters
    ----------
    datum : dict
        Contains various input data for this worker.

    Returns
    -------
    numpy.ndarray
        The part of the CCD this worker was dealing with. The parts from mutiple CCD will
        later be concatenated to contruct the entire CCD.
    """

    fibre_indices = datum['fibre_indices']
    fibre_thickness = datum['fibre_thickness']
    tramline_data = datum['tramline_data']
    interpolate = datum['interpolate']
    data = datum['data']
    range_of_x = datum['range_of_x']
    show_progress = datum['show_progress']

    # The start y coordinate of the tram line
    tram_y_start = None

    myrange = range_of_x

    # Display the progress bar for the first worker
    if range_of_x[0] == 0 and show_progress:
        myrange = tqdm(range_of_x)

    # Loop over the x values
    for x in myrange:
        # Loop over fibres
        for i, fibre_index in enumerate(fibre_indices):
            # Calculate the start and end y-coordinate of the fibre
            tram_y = tramline_data[fibre_index, x]

            if tram_y_start is None:
                tram_y_start = math.floor(tram_y - fibre_thickness/2)

            tram_y_end = math.ceil(tram_y + fibre_thickness/2)

            # Check if the current tram fibre overlaps with the next
            if i < len(fibre_indices) - 1:
                next_fibre_index = fibre_indices[i + 1]
                next_tram_y_start = math.floor(tramline_data[next_fibre_index, x] - fibre_thickness/2)

                if (tram_y_end >= next_tram_y_start):
                    # The fibre is very close to the next one.
                    # Skip until the next fibre is further away, since we want to erase fibres
                    # with brightness values from regions far away from fibres.
                    continue

            brightness = calculate_bightness(data=data, x=x, tram_y_start=tram_y_start,
                                             tram_y_end=tram_y_end, interpolate=interpolate)

            # Erase the fibre pixels
            data[tram_y_start: tram_y_end, x] = brightness

            tram_y_start = None

    # Return the subset of the array this worker was dealing with
    return data[:, range_of_x[0]: range_of_x[-1]+1]


def export_glow(data_path, tram_path, output_path, fibre_thickness=15, box_size=3, interpolate=False, show_progress=True):
    """
    Creates a .fits image by keeping pixels between fibres and erasing the fibres.

    Example
    ----------

    python export_glow.py -data_path='data.fits' -tram_path='/Users/evgenii/Downloads/tram.fits' -output_path='output.fits' -interpolate=no

    Parameters
    ----------
    data_path : str
        Path to the .fits file containing the spectrum.
    tram_path : str
        The .fits file containing the tram lines (*tlm.fits file).
    output_path : str
        The path to the output .fits file containing the extracted glow.
    fibre_thickness: int
        Width of fibres in pixels.

    box_size: int
        The size of a box in pixels that is used to calculate the mean brightness for erasing
        the light form fibres. The higher number will have more uniformed fill.

    interpolate: Boolean
        If True, the fibres are filled with brightness values interpolated from pixels outside
        the fibres, which results in smoother glow image. If False, the each fibre is filled
        with single brightness value form a pixellocated just below the fibre.

    show_progress: Boolean
        If true, show the progress bar.
    """

    if os.path.exists(output_path):
        print(f"ERROR: the output file already exists: '{output_path}'.")
        return

    # Read the tram lines
    with fits.open(tram_path) as hdul:
        tramline_data = hdul["PRIMARY"].data

    # Read the data fits file
    hdul = fits.open(data_path)
    data = hdul["PRIMARY"].data

    # Get the name of the data with fibres
    data_names = [i.name for i in hdul]
    fibres_data_name = "STRUCT.MORE.FIBRES"

    if fibres_data_name not in data_names:
        fibres_data_name = "FIBRES"

    fibreinfo = hdul[fibres_data_name]

    total_fibres = tramline_data.shape[0]
    xmax = min(data.shape[1], tramline_data.shape[1])

    # Store non-parked fibre indexes
    fibre_indices = []

    for fibre_index in range(total_fibres):
        fibre_name = fibreinfo.data["NAME"][fibre_index]

        if fibre_name == "PARKED":
            continue

        fibre_indices.append(fibre_index)

    # Divide work evenly between CPU cores,
    # each CPU will process a range of x values in the image
    cpus = multiprocessing.cpu_count()
    xranges = np.array_split(range(xmax), cpus)

    # Prepare data to be passed to each worker
    process_data = []
    for range_of_x in xranges:
        datum = {
            'range_of_x': range_of_x,
            'fibre_indices': fibre_indices,
            'fibre_thickness': fibre_thickness,
            'tramline_data': tramline_data,
            'interpolate': interpolate,
            'data': data,
            'show_progress': show_progress
        }

        process_data.append(datum)

    # Do the work in parallel and collect `results`
    with Pool(cpus) as pool:
        results = pool.map(do_the_work, process_data)

    if show_progress:
        print("Saving the FITS file...")

    # Join results from multiple workers into a single array
    joined_data = np.concatenate(results, axis=1)

    # Save extracted glow data to a .fits file
    data[:, 0: xmax] = joined_data
    hdul.writeto(output_path)
    hdul.close()

    if show_progress:
        print("Done (ᵔᴥᵔ)")


if __name__ == '__main__':
    arguments = dict(map(lambda x: x.lstrip('-').split('='), sys.argv[1:]))

    interpolate = arguments.get('interpolate')

    if isinstance(interpolate, str) and interpolate.lower() == 'yes':
        interpolate = True
    else:
        interpolate = False

    export_glow(data_path=arguments.get('data_path'),
                tram_path=arguments.get('tram_path'),
                output_path=arguments.get('output_path'),
                interpolate=interpolate)
