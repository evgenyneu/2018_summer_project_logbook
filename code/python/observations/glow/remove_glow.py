from .export_glow_fortran import export_glow_fortran
from ..paths import add_suffix_to_file_name, fits_path_one_frame, tramline_single_path, ccd_day_take_dir, fits_file_name
from ..subtract import subtract
from ..lineclean import lineclean
import os


def remove_glow(data_path, tram_path, output_path):
    """
    Removes the glow from a a fits file

    Parameters
    ----------
    data_path : str
        Path to the .fits file containing the spectrum.
    tram_path : str
        The .fits file containing the tram lines (*tlm.fits file).
    output_path : str
        The path to the output .fits file containing the glow-free file.
    """

    if data_path == output_path:
        raise ValueError('Error removing flow, the input and output paths are the same.')

    # Extract the glow
    # ---------

    glow_path = add_suffix_to_file_name(path=output_path, suffix="_glow")
    export_glow_fortran(data_path=data_path, tram_path=tram_path, output_path=glow_path)

    # Subtract the glow
    # ---------

    subtract(first_path=data_path, second_path=glow_path, output_path=output_path)


def remove_glow_one_frame(ccd, day, take, frame, take_output, silent):
    """
    Removes the glow from a single frame.

    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    frame : int
        The frame of the input file to remove the glow from.
        For example, for 27oct10016.fits file, the frame is 16.
    take_output : str
        The output directory where the glow-free files will be placed.
    silent: bool
        If true, do not show any non-error messages.

    Returns
    -------
    str
        Path to the glow-free .fits file.
    """

    data_path = fits_path_one_frame(ccd=ccd, day=day, take=take, frame=frame)
    tram_path = tramline_single_path(ccd=ccd, day=day, take=take)
    output_dir = ccd_day_take_dir(ccd=ccd, day=day, take=take_output)
    output_filename = fits_file_name(ccd=ccd, day=day, frame=frame)
    output_path = os.path.join(output_dir, output_filename)

    if not silent:
        print(f"Removing glow: {output_filename}")

    remove_glow(data_path=data_path, tram_path=tram_path, output_path=output_path)

    return output_path


def remove_glow_many_frames(ccd, day, take, frames, take_output, silent):
    """
    Removes the glow from a single frame.

    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    frames : int
        The frame numbers for input file to remove the glow from.
    take_output : str
        The output directory where the glow-free files will be placed.
    silent: bool
        If true, do not show any non-error messages.

    Returns
    -------
    list of str
        Paths to the glow-free .fits file.
    """

    output_paths = []

    for frame in frames:
        path = remove_glow_one_frame(ccd=ccd, day=day, take=take, frame=frame,
                                     take_output=take_output, silent=silent)

        output_paths.append(path)

    return output_paths