from .export_glow_fortran import export_glow_fortran
from ..paths import ccd_day_take_dir
from ...shared import fixture_paths, files
from ...shared import cleanup
import os


def test_remove_glow():
    # Prepare the input .fits files

    # Copy the tramline file
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_tramlime = f"{pathdir}/tramline.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, path_tramlime)

    # Copy the raw image
    path_data = f"{pathdir}/rawdata.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    output_path = f"{pathdir}/glow_free.fits"

    if os.path.exists(output_path):
        os.remove(output_path)

    export_glow_fortran(data_path=path_data, tram_path=path_tramlime, output_path=output_path)

    assert os.path.exists(output_path) is True

    cleanup.remove_test_fits()