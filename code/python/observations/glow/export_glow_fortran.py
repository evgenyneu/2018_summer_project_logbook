import subprocess
import os
from ..constants import EXPORT_GLOW_PATH


def export_glow_fortran(data_path, tram_path, output_path):
    """
    Exports the background glow from the .fits image using the fortran utility.

    Parameters
    ----------
    data_path : str
        Path to the .fits file containing the spectrum.
    tram_path : str
        The .fits file containing the tram lines (*tlm.fits file).
    output_path : str
        The path to the output .fits file containing the extracted glow.
    """

    if os.path.exists(output_path):
        raise FileExistsError(f"ERROR: the output file already exists: '{output_path}'.")

    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if type(data_path) != str or not os.path.exists(data_path):
        raise FileNotFoundError(f"ERROR: the data file does not exist: '{data_path}'.")

    if type(tram_path) != str or not os.path.exists(tram_path):
        raise FileNotFoundError(f"ERROR: the tram file does not exist: '{tram_path}'.")

    subprocess.run([EXPORT_GLOW_PATH, data_path, tram_path, output_path, "--silent"])