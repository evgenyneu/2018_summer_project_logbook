from .remove_glow import remove_glow, remove_glow_one_frame, remove_glow_many_frames
from ..paths import ccd_day_take_dir
from ...shared import fixture_paths, files
from ...shared import cleanup
import os
import sys
import pytest


@pytest.mark.slow
def test_remove_glow_one():
    cleanup.remove_test_fits()

    # Copy the tramline file
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_tramlime = f"{pathdir}/tramline.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, path_tramlime)

    # Copy the raw image
    path_data = f"{pathdir}/rawdata.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    output_path = f"{pathdir}/glow_free.fits"

    remove_glow(data_path=path_data, tram_path=path_tramlime, output_path=output_path)

    assert os.path.exists(output_path) is True

    cleanup.remove_test_fits()


@pytest.mark.slow
def test_remove_glow_one_frame():
    cleanup.remove_test_fits()

    # Prepare the input .fits files

    # Copy the tramline file
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_tramlime = f"{pathdir}/29oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, path_tramlime)

    # Copy the raw image
    path_data = f"{pathdir}/29oct10016.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    result = remove_glow_one_frame(ccd=1, day=29, take="p0", frame=16, take_output="p1_glow_free",
                                   silent=True)

    expected_output = ccd_day_take_dir(ccd=1, day=29, take="p1_glow_free")
    assert result == f"{expected_output}/29oct10016.fits"
    assert os.path.exists(result) is True

    cleanup.remove_test_fits()


@pytest.mark.slow
def test_remove_glow_many_frames():
    cleanup.remove_test_fits()

    # Prepare the input .fits files

    # Copy the tramline file
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_tramlime = f"{pathdir}/29oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, path_tramlime)

    # Copy the raw images
    path_data = f"{pathdir}/29oct10016.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/29oct10017.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    result = remove_glow_many_frames(ccd=1, day=29, take="p0", frames=[16, 17],
                                     take_output="p1_glow_free", silent=True)

    expected_output = ccd_day_take_dir(ccd=1, day=29, take="p1_glow_free")

    assert len(result) == 2

    path1 = result[0]
    assert path1 == f"{expected_output}/29oct10016.fits"
    assert os.path.exists(path1) is True

    path2 = result[1]
    assert path2 == f"{expected_output}/29oct10017.fits"
    assert os.path.exists(path2) is True

    cleanup.remove_test_fits()