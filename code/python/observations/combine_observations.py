from .paths import fits_path_one_ccd, single_obsevation_path_for_combining, combined_observations_fits_from_2dfdr
import os
from shutil import copyfile
from .constants import CCDs


def copy_ccd(ccd, day, take, day_combined, take_combined):
    """
    Copies the combined .fits file into the directory for combination

    Parameters
    ----------
    ccd : int
        The CCD identifier, i.e. 1, 2, 3.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    day_combined: int
        The day of combining.
    take_combined: int
        The directory containing combined observation files.

    Returns
    -------
    str
        Path to the copied .fits file.
    """

    input_path = fits_path_one_ccd(ccd=ccd, day=day, take=take)

    output_path = single_obsevation_path_for_combining(ccd=ccd, day=day, take=take,
                                                       day_combined=day_combined,
                                                       take_combined=take_combined)

    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if os.path.exists(output_path):
        os.remove(output_path)

    copyfile(input_path, output_path)

    return output_path


def copy_all_ccds(day, take, day_combined, take_combined):
    """
    Copies the combined .fits files for all CCDs into the directory for combination.

    Parameters
    ----------
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    day_combined: int
        The day of combining.
    take_combined: int
        The directory containing combined observation files.

    Returns
    -------
    list of str
        List of paths to the output .fits files.
    """
    paths = []

    for ccd_number in CCDs:
        path = copy_ccd(ccd=ccd_number, day=day, take=take,
                        day_combined=day_combined, take_combined=take_combined)

        paths.append(path)

    return paths


def copy_to_canonical_path(ccd, day, take):
    """
    Make a copy of the combined observetions .fits file to file name that follows naming convention of 2DfDr for combined files.

    Parameters
    ----------
    ccd : int
        The CCD identifier, i.e. 1, 2, 3.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to the output .fits file.
    """

    input_path = combined_observations_fits_from_2dfdr(ccd=ccd, day=day, take=take)
    output_path = fits_path_one_ccd(ccd=ccd, day=day, take=take)

    if os.path.exists(output_path):
        os.remove(output_path)

    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    copyfile(input_path, output_path)

    return output_path


def copy_to_canonical_path_all_ccds(day, take):
    """
    Make a copy of the combined observetions .fits file to file name that follows naming convention of 2DfDr for combined files.

    Parameters
    ----------
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.


    Returns
    -------
    list of str
        List of paths to the output .fits files.
    """
    paths = []

    for ccd_number in CCDs:
        path = copy_to_canonical_path(ccd=ccd_number, day=day, take=take)
        paths.append(path)

    return paths
