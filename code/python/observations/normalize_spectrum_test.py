from .normalize_spectrum import normalize, normalize_all_ccds
from .paths import star_normalized_path, star_doppler_corrected_path
from ..shared import fixture_paths, files
import os
from ..shared import cleanup
from .constants import CCDs


def test_normalize():
    # Prepare the input .fits file for the star
    output_path_unprocessed = star_doppler_corrected_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    input_path = fixture_paths.individual_fits_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, output_path_unprocessed)

    output_path = star_normalized_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")

    if os.path.exists(output_path):
        os.remove(output_path)

    result = normalize(name='AGBSYAZ017762', ccd=1, day=29, take="p0", interactive="no")

    assert os.path.exists(output_path) is True
    assert result == output_path
    cleanup.remove_test_fits()


def test_normalize_all_ccds():
    # Prepare the input .fits file for the star
    for ccd_number in CCDs:
        output_path_unprocessed = star_doppler_corrected_path(name='AGBSYAZ017762', ccd=ccd_number, day=29, take="p0")
        input_path = fixture_paths.individual_fits_fixture_path(ccd=ccd_number)
        files.copy_file_rewrite(input_path, output_path_unprocessed)

    output_path1 = star_normalized_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")

    if os.path.exists(output_path1):
        os.remove(output_path1)

    output_path2 = star_normalized_path(name='AGBSYAZ017762', ccd=2, day=29, take="p0")

    if os.path.exists(output_path2):
        os.remove(output_path2)

    output_path3 = star_normalized_path(name='AGBSYAZ017762', ccd=3, day=29, take="p0")

    if os.path.exists(output_path3):
        os.remove(output_path3)

    result = normalize_all_ccds(name='AGBSYAZ017762', day=29, take="p0", interactive="no")

    assert os.path.exists(output_path1) is True
    assert output_path1 in result

    assert os.path.exists(output_path2) is True
    assert output_path2 in result

    assert os.path.exists(output_path3) is True
    assert output_path3 in result

    cleanup.remove_test_fits()
