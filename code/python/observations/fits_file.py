# Functions for moving fits files around
from .paths import fits_path_one_frame, ccd_day_take_dir, fits_file_name, ccd_day_take_dir
from ..shared.files import copy_file
import os


def fits_copy(ccd, day, take, frame, take_output):
    """
    Copies the fits file to a different directory.

    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    frame : int
        The frame of the input file to copy.
        For example, for 27oct10016.fits file, the frame is 16.
    take_output : str
        The output directory where the file will be copied

    Returns
    -------
    str
        Path to the glow-free .fits file.
    """

    data_path = fits_path_one_frame(ccd=ccd, day=day, take=take, frame=frame)
    output_dir = ccd_day_take_dir(ccd=ccd, day=day, take=take_output)
    output_filename = fits_file_name(ccd=ccd, day=day, frame=frame)
    output_path = os.path.join(output_dir, output_filename)

    copy_file(input_path=data_path, output_path=output_path)

    return output_path


def fits_copy_file(ccd, day, take, filename, take_output):
    """
    Copies the fits file to a different directory.

    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    filename : str
        The name of the file to copy.

    take_output : str
        The output directory where the file will be copied

    Returns
    -------
    str
        Path to the glow-free .fits file.
    """

    input_dir = ccd_day_take_dir(ccd=ccd, day=day, take=take)
    data_path = os.path.join(input_dir, filename)
    output_dir = ccd_day_take_dir(ccd=ccd, day=day, take=take_output)
    output_path = os.path.join(output_dir, filename)

    copy_file(input_path=data_path, output_path=output_path)

    return output_path