from subprocess import check_output
from io import StringIO
import pandas as pd
from .paths import fits_path_one_ccd


def all_fibres(fits_path):

    """
    Returns
    -------
    pandas.DataFrame
        All fibres from the .fits file.
    """
    data = check_output(["2dfinfo", f"{fits_path} fibres"])
    data = data.decode()
    dataIO = StringIO(data)
    df = pd.pandas.read_fwf(dataIO)
    columns = ["Fib#", "Name", "Ty", "Piv", "RA(rads)", "DEC(rads)", "X", "Y", "Mag"]
    df = df[columns] #  Keep only known columns
    df = df.dropna(how='any') # drop rows with empty values
    df['Fib#'] = df['Fib#'].astype(int) # convert fibres to integers
    df.rename(columns={'Fib#': 'Fib', 'RA(rads)': 'RA', 'DEC(rads)': 'Dec'}, inplace=True)
    df.set_index('Fib', inplace=True, verify_integrity=True) # Use fibre numbers as indexes

    return df


def science_stars(ccd, day, take):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    pandas.DataFrame
        All science stars (i.e. not guide or sky stars). The index is the fibre number 'Fib'.
    """
    path = fits_path_one_ccd(ccd=ccd, day=day, take=take)
    df = all_fibres(path)
    return df[df.Ty == 'P']


def star_name_by_fibre(fibre_number, ccd, day, take):
    """
    Parameters
    ----------
    fibre_number: int
        The fibre number, i.e. 113.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Star name, i.e. 'AGBSYAZ017762'.
    """

    path = fits_path_one_ccd(ccd=ccd, day=day, take=take)
    df = all_fibres(path)

    if fibre_number not in df.index: return None
    return df.loc[fibre_number]['Name']


def fibre_by_star_name(name, ccd, day, take):
    """
    Parameters
    ----------
    name: str
        Star name, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    int
        The fibre number, i.e. 113.
    """

    path = fits_path_one_ccd(ccd=ccd, day=day, take=take)
    df = all_fibres(path)

    filtered_by_name = df.loc[df['Name'] == name]
    fibres = filtered_by_name.index.values

    if len(fibres) == 0: return None
    return fibres[0]

