from .paths import ccd_day_take_dir
from ..shared import fixture_paths, files
from ..shared import cleanup
from .fits_header import set_header_value
from astropy.io import fits


def test_set_header_type():
    # Prepare the input .fits files
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_data = f"{pathdir}/data.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    set_header_value(path=path_data, extension="PRIMARY", key="NDFCLASS", value="DARK")

    # Check the header was updated

    hdul = fits.open(path_data)
    header = hdul["PRIMARY"].header
    assert header["NDFCLASS"] == "DARK"
    hdul.close()
    cleanup.remove_test_fits()