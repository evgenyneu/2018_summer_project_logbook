from astropy.io import fits


def set_class(path, obj_class):
    '''
    Update the class of the object in the FITS file.

    Parameters
    ----------
    path : str
        Path to the fits file.
    obj_class: str
        Object class shown in 2DfDr (MFOBJECT, DARK etc.)
    '''
    hdul = fits.open(path, mode='update')
    hdul['STRUCT.MORE.NDF_CLASS'].data['NAME'][0] = obj_class
    hdul.flush()
    hdul.close()
