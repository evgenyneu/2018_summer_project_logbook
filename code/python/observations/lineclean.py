from pyraf import iraf
from iraf import imfit
import os


def lineclean(input_path, output_path):
    """
    Cleans the glow form cosmic rays.

    Parameters
    ----------
    input_path : str
        Path to the intput .fits file.

    output_path : str
        Path to the output .fits file that will be created.
    """

    if os.path.exists(output_path):
        print(f"ERROR: the output file already exists: '{output_path}'.")
        return

    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    imfit.lineclean(input=input_path, output=output_path,
                    sample="*", interactive="no",
                    low_reject=5.0, high_reject=5.0, function="spline1",
                    order=100, grow=1, naverage=-1)