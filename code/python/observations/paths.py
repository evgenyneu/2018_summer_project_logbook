import os
from ..is_testing import IS_TESTING
from .constants import CCDs
import datetime
import glob
import pathlib

if IS_TESTING:
    OBSERVATIONS_DIR = "code/python/test_data"
else:
    OBSERVATIONS_DIR = "data/observe"


if IS_TESTING:
    REDUCED_DIR = "code/python/test_data/reduced_spectra"
else:
    REDUCED_DIR = "logbook/data/reduced_spectra"


def add_suffix_to_file_name(path, suffix):
    """
    Adds the suffix to the file name. For example, if we add suffix "possum",
    the path mydir/myfile.fits becomes mydir/myfile_possum.fits

    Parameters
    ----------
    path: str
        Path to a file.
    suffix : str
        A string to be added to the file name before the extension.

    Returns
    -------
    str
        The new path with suffix added to the file name.
    """

    dir_path = os.path.dirname(path)
    basename = pathlib.Path(path).stem
    extension = pathlib.Path(path).suffix
    new_filename = f"{basename}{suffix}{extension}"

    return os.path.join(dir_path, new_filename)


def day_take_dir(day, take):
    """
    Returns
    -------
    str
        Path to the directory corresponding to the day and take.
    """
    return f"{OBSERVATIONS_DIR}/{day}Oct18/reduce/288/{take}"


def ccd_day_take_dir(ccd, day, take):
    """
    Returns
    -------
    str
        Path to the directory corresponding to the ccd, day and take.
    """
    return f"{day_take_dir(day=day, take=take)}/ccd{ccd}"


def all_stars_dir(day, take):
    """
    Returns
    -------
    str
        Path to the directory containing individual stars.
    """
    return f"{day_take_dir(day=day, take=take)}/stars"


def fits_file_name(ccd, day, frame):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    frame : int
        The frame of the input file to remove the glow from.
        For example, for 27oct10016.fits file, the frame is 16.

    Returns
    -------
    str
        Name of the fits file.
    """
    return f"{day}oct{ccd}{frame:04d}.fits"


def fits_path_one_frame(ccd, day, take, frame):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    frame : int
        The frame of the input file to remove the glow from.
        For example, for 27oct10016.fits file, the frame is 16.

    Returns
    -------
    str
        Path to a .fits file.
    """
    name = fits_file_name(ccd=ccd, day=day, frame=frame)
    return f"{ccd_day_take_dir(ccd=ccd, day=day, take=take)}/{name}"


def fits_path_one_ccd(ccd, day, take):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to a .fits file with combined fibres.
    """
    return f"{ccd_day_take_dir(ccd=ccd, day=day, take=take)}/{day}oct{ccd}_combined.fits"


def star_unprocessed_path(name, ccd, day, take):
    """
    Parameters
    ----------
    name; str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to the individual star's unprocessed .fits file that was extracted from the combined .fits.
    """

    stars_dir = all_stars_dir(day=day, take=take)
    return f"{stars_dir}/unprocessed/{name}/ccd{ccd}.fits"


def star_doppler_corrected_path(name, ccd, day, take):
    """
    Parameters
    ----------
    name; str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to the individual star's .fits file which was doppler corrected.
    """

    stars_dir = all_stars_dir(day=day, take=take)
    return f"{stars_dir}/doppler_corrected/{name}/ccd{ccd}.fits"


def star_normalized_path(name, ccd, day, take):
    """
    Parameters
    ----------
    name: str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to the star's normalized .fits file.
    """

    stars_dir = all_stars_dir(day=day, take=take)
    return f"{stars_dir}/normalized/{name}/ccd{ccd}.fits"


def star_normalized_path_present(name, day, take):
    """
    Parameters
    ----------
    name: str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    Boolean
        True of normalized .fits files are present for ALL CCDs. False if a .fits file is missing for at least one CCD.
    """
    for ccd_number in CCDs:
        path = star_normalized_path(name=name, ccd=ccd_number, day=day, take=take)

        if not os.path.exists(path):
            return False

    return True


def fits_sun_path():
    """
    Returns
    -------
    str
        Path to the Sun's fits file.
    """
    return f"{OBSERVATIONS_DIR}/good_sprectra/sun.fits"


def timestamp():
    """
    Returns
    -------
    str
        Return current date and time.
    """
    time = datetime.datetime.now()
    return f"{time.year}-{time.month:02d}-{time.day:02d}_{time.hour:02d}-{time.minute:02d}"


def reduced_fits_path(name, ccd):
    """
    Parameters
    ----------
    name: str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd: int
        The CCD number.

    Returns
    -------
    str
        Path to reduced .fits file.
    """

    return f"{REDUCED_DIR}/{name}/{timestamp()}_ccd{ccd}.fits"


def reduced_description_path(name):
    """
    Parameters
    ----------
    name: str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd: int
        The CCD number.

    Returns
    -------
    str
        Path to the text description file of the reduced data.
    """

    return f"{REDUCED_DIR}/{name}/{timestamp()}.txt"


def single_obsevation_path_for_combining(ccd, day, take, day_combined, take_combined):
    """
    Parameters
    ----------
    ccd: int
        The CCD number.
    day: int
        The day of the observation.
    take: str
        The directory containing observation files.
    day_combined: int
        The day of combining.
    take_combined: int
        The directory containing combined observation files.

    Returns
    -------
    str
        Path to .fits file of a single observations that will be used for combining multiple observations.
    """

    dir_combined = ccd_day_take_dir(ccd=ccd, day=day_combined, take=take_combined)
    return f"{dir_combined}/{day}oct{ccd}_{take}_combined.fits"


def combined_observations_fits_from_2dfdr(ccd, day, take):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.

    Returns
    -------
    str
        Returns the path to the file produced by 2DfDr after combining multiple observations.
    """
    return f"{ccd_day_take_dir(ccd=ccd, day=day, take=take)}/combined_frames.fits"


def combined_observations_fits_from_2dfdr_present(day, take):
    """
    Returns
    -------
    Boolean
        True of combined observation .fits files are present for ALL CCDs. False if a .fits file is missing for at least one CCD.
    """
    for ccd_number in CCDs:
        path = combined_observations_fits_from_2dfdr(ccd=ccd_number, day=day, take=take)

        if not os.path.exists(path):
            return False

    return True


# Velocities
# -----------------

def candidate_radial_velocities_path(name, ccd, day, take):
    """
    Parameters
    ----------
    name: str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd: int
        The CCD number.
    day: int
        The day of the observation.
    take: str
        The directory containing observation files.

    Returns
    -------
    str
        Path to .csv file containing all measured candidate radial velocities of the star.
    """
    stars_dir = all_stars_dir(day=day, take=take)
    return f"{stars_dir}/velocities/{name}/ccd{ccd}/candidates.csv"


def vetted_velocities_path(name, ccd, day, take):
    """
    Parameters
    ----------
    name: str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to .csv file containing vetted velocities of the star.
    """
    stars_dir = all_stars_dir(day=day, take=take)
    return f"{stars_dir}/velocities/{name}/ccd{ccd}/vetted.csv"


def vetted_velocities_relative_path(candidates_path):
    """
    Parameters
    ----------
    candidates_path: str
        Path to a .csv file containing velocity candidates.


    Returns
    -------
    str
        Path to .csv file vetted visually by the user. The file is located in the same directory as `candidates_path`.
    """
    output_dir = os.path.dirname(candidates_path)
    return os.path.join(output_dir, 'vetted.csv')


def mean_velocity_path(name, ccd, day, take):
    """
    Parameters
    ----------
    name: str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to .csv file containing mean radial velocity of the star.
    """
    stars_dir = all_stars_dir(day=day, take=take)
    return f"{stars_dir}/velocities/{name}/ccd{ccd}/mean_velocity.csv"


def mean_velocity_relative_path(vetted_path):
    """
    Parameters
    ----------
    vetted_path: str
        Path to a .csv file containing vetted velocities.


    Returns
    -------
    str
        Path to .csv file containing mean radial velocity. The file is located in the same directory as `vetted_path`.
    """
    output_dir = os.path.dirname(vetted_path)
    return os.path.join(output_dir, 'mean_velocity.csv')


# Tramline
# ---------

def tramline_paths(ccd, day, take):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    list of str
        Array of paths to the tramline .fits files.
    """

    path = ccd_day_take_dir(ccd=ccd, day=day, take=take)
    files = glob.glob(f"{path}/{day}oct{ccd}*tlm.fits")

    return files


def tramline_single_path(ccd, day, take):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to the tramline .fits file. If there are multipl tramline files in directory, returns the first one.
    """

    paths = tramline_paths(ccd=ccd, day=day, take=take)

    if len(paths) == 0:
        return None

    return paths[0]
