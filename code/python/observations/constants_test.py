from .constants import CCDs, HERMES_BANDS


def test_number_of_ccds():
    assert CCDs == [1, 2, 3]


def test_hermes_bands():
    assert HERMES_BANDS[1]['start'] == 4715
    assert HERMES_BANDS[1]['end'] == 4900

    assert HERMES_BANDS[2]['start'] == 5649
    assert HERMES_BANDS[2]['end'] == 5873

    assert HERMES_BANDS[3]['start'] == 6478
    assert HERMES_BANDS[3]['end'] == 6737