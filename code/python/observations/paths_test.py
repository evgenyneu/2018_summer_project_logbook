from .paths import fits_path_one_ccd, fits_sun_path, star_unprocessed_path, star_doppler_corrected_path, star_normalized_path, single_obsevation_path_for_combining, combined_observations_fits_from_2dfdr, candidate_radial_velocities_path, vetted_velocities_relative_path, mean_velocity_relative_path, OBSERVATIONS_DIR, REDUCED_DIR, mean_velocity_path, vetted_velocities_path, combined_observations_fits_from_2dfdr_present, day_take_dir, ccd_day_take_dir, star_normalized_path_present, reduced_fits_path, reduced_description_path, tramline_paths, tramline_single_path, add_suffix_to_file_name, fits_path_one_frame, fits_file_name
import os
from ..shared import files, fixture_paths, cleanup
from .constants import CCDs
from freezegun import freeze_time


def test_day_take_dir():
    result = day_take_dir(day=17, take="mouse")
    assert result == f"{OBSERVATIONS_DIR}/17Oct18/reduce/288/mouse"


def test_ccd_day_take_dir():
    result = ccd_day_take_dir(ccd=12, day=17, take="mouse")
    assert result == f"{OBSERVATIONS_DIR}/17Oct18/reduce/288/mouse/ccd12"


def test_fits_file_name():
    assert fits_file_name(ccd=2, day=27, frame=6) == "27oct20006.fits"


def test_fits_path_one_frame():
    assert fits_path_one_frame(ccd=1, day=29, take="p0", frame=22) == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/p0/ccd1/29oct10022.fits"


def test_fits_path_one_ccd():
    assert fits_path_one_ccd(ccd=1, day=29, take="p0") == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/p0/ccd1/29oct1_combined.fits"
    assert fits_path_one_ccd(ccd=2, day=29, take="p0") == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/p0/ccd2/29oct2_combined.fits"
    assert fits_path_one_ccd(ccd=1, day=29, take="p1_A") == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/p1_A/ccd1/29oct1_combined.fits"


def test_fits_sun_path():
    assert fits_sun_path() == f"{OBSERVATIONS_DIR}/good_sprectra/sun.fits"
    assert os.path.exists(f"{OBSERVATIONS_DIR}/good_sprectra/sun.fits") is True


def test_star_unprocessed_path():
    result = star_unprocessed_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    assert result == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/p0/stars/unprocessed/AGBSYAZ017762/ccd1.fits"

    result = star_unprocessed_path(name='AGBSYAZ017762', ccd=2, day=28, take="p1_A")
    assert result == f"{OBSERVATIONS_DIR}/28Oct18/reduce/288/p1_A/stars/unprocessed/AGBSYAZ017762/ccd2.fits"


def test_star_doppler_corrected_path():
    result = star_doppler_corrected_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    assert result == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/p0/stars/doppler_corrected/AGBSYAZ017762/ccd1.fits"

    result = star_doppler_corrected_path(name='AGBSYAZ017762', ccd=2, day=28, take="p1_A")
    assert result == f"{OBSERVATIONS_DIR}/28Oct18/reduce/288/p1_A/stars/doppler_corrected/AGBSYAZ017762/ccd2.fits"


def test_star_normalized_path():
    result = star_normalized_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    assert result == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/p0/stars/normalized/AGBSYAZ017762/ccd1.fits"

    result = star_normalized_path(name='AGBSYAZ017762', ccd=2, day=28, take="p1_A")
    assert result == f"{OBSERVATIONS_DIR}/28Oct18/reduce/288/p1_A/stars/normalized/AGBSYAZ017762/ccd2.fits"


def test_star_normalized_path_present_true():
    # Prepare the input .fits file for the star
    for ccd_number in CCDs:
        output_path = star_normalized_path(name='AGBSYAZ017762', ccd=ccd_number, day=30, take="combine")
        input_path = fixture_paths.individual_fits_fixture_path(ccd=ccd_number)
        files.copy_file_rewrite(input_path, output_path)

    result = star_normalized_path_present(name='AGBSYAZ017762', day=30, take="combine")

    assert result is True

    cleanup.remove_test_fits()


def test_combined_observations_fits_from_2dfdr_present_false():
    cleanup.remove_test_fits()

    # Prepare the input .fits file for the star
    output_path = star_normalized_path(name='AGBSYAZ017762', ccd=1, day=30, take="combine")
    input_path = fixture_paths.individual_fits_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, output_path)

    result = star_normalized_path_present(name='AGBSYAZ017762', day=30, take="combine")

    assert result is True

    cleanup.remove_test_fits()


@freeze_time("2031-01-14 14:21:34")
def test_reduced_fits_path():
    result = reduced_fits_path(name='AGBSYAZ017762', ccd=2)

    assert result == f"{REDUCED_DIR}/AGBSYAZ017762/2031-01-14_14-21_ccd2.fits"


@freeze_time("2022-11-03 12:32:01")
def test_reduced_description_path():
    result = reduced_description_path(name='AGBSYAZ017762')

    assert result == f"{REDUCED_DIR}/AGBSYAZ017762/2022-11-03_12-32.txt"


def test_add_suffix_to_file_name():
    result = add_suffix_to_file_name(path="dir1/dir2/myfile.fits", suffix="_possum")
    assert result == "dir1/dir2/myfile_possum.fits"


# Combined observations
# -----------------


def test_single_obsevation_path_for_combining():
    result = single_obsevation_path_for_combining(ccd=2, day=27, take="p1", day_combined=30, take_combined="mouse")
    assert result == f"{OBSERVATIONS_DIR}/30Oct18/reduce/288/mouse/ccd2/27oct2_p1_combined.fits"

    result = single_obsevation_path_for_combining(ccd=1, day=29, take="p0", day_combined=3, take_combined="horse")
    assert result == f"{OBSERVATIONS_DIR}/3Oct18/reduce/288/horse/ccd1/29oct1_p0_combined.fits"


def test_combined_observations_fits_from_2dfdr():
    result = combined_observations_fits_from_2dfdr(ccd=1, day=21, take="omlet")
    assert result == f"{OBSERVATIONS_DIR}/21Oct18/reduce/288/omlet/ccd1/combined_frames.fits"

    result = combined_observations_fits_from_2dfdr(ccd=2, day=30, take="pin")
    assert result == f"{OBSERVATIONS_DIR}/30Oct18/reduce/288/pin/ccd2/combined_frames.fits"


def test_combined_observations_fits_from_2dfdr_present_true():
    # Prepare the input .fits file for the star
    for ccd_number in CCDs:
        output_path = combined_observations_fits_from_2dfdr(ccd=ccd_number, day=30, take="combine")
        input_path = fixture_paths.individual_fits_fixture_path(ccd=ccd_number)
        files.copy_file_rewrite(input_path, output_path)

    result = combined_observations_fits_from_2dfdr_present(day=30, take="combine")

    assert result is True

    cleanup.remove_test_fits()


def test_combined_observations_fits_from_2dfdr_present_false():
    cleanup.remove_test_fits()

    # Prepare the input .fits file for the star
    output_path = combined_observations_fits_from_2dfdr(ccd=1, day=30, take="combine")
    input_path = fixture_paths.individual_fits_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, output_path)

    result = combined_observations_fits_from_2dfdr_present(day=30, take="combine")

    assert result is False

    cleanup.remove_test_fits()


# Velocities
# -----------------

def test_candidate_radial_velocities_path():
    result = candidate_radial_velocities_path(name='AGBSYAZ017762', ccd=1, day=13, take="monkey")
    assert result == f"{OBSERVATIONS_DIR}/13Oct18/reduce/288/monkey/stars/velocities/AGBSYAZ017762/ccd1/candidates.csv"

    result = candidate_radial_velocities_path(name='ANOTHERSTAR', ccd=2, day=19, take="cat")
    assert result == f"{OBSERVATIONS_DIR}/19Oct18/reduce/288/cat/stars/velocities/ANOTHERSTAR/ccd2/candidates.csv"


def test_vetted_velocities_path():
    result = vetted_velocities_path(name='JUST_A_STAR', ccd=1, day=29, take="volf")

    assert result == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/volf/stars/velocities/JUST_A_STAR/ccd1/vetted.csv"


def test_vetted_velocities_relative_path():
    path_to_file = os.path.realpath(__file__)
    dirname = os.path.dirname(path_to_file)
    file_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/candidates.csv"

    result = vetted_velocities_relative_path(file_path)

    assert result == f"{OBSERVATIONS_DIR}/fixtures/velocities/vetted.csv"


def test_mean_velocity_path():
    result = mean_velocity_path(name='JUST_A_STAR', ccd=1, day=29, take="volf")

    assert result == f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/volf/stars/velocities/JUST_A_STAR/ccd1/mean_velocity.csv"


def test_mean_velocity_relative_path():
    path_to_file = os.path.realpath(__file__)
    dirname = os.path.dirname(path_to_file)
    file_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/vetted_fixture.csv"

    result = mean_velocity_relative_path(file_path)

    assert result == f"{OBSERVATIONS_DIR}/fixtures/velocities/mean_velocity.csv"


# Tramline
# ------------

def test_tramline_paths():
    tramline_dir = ccd_day_take_dir(ccd=1, day=27, take="p1")
    output_path = f"{tramline_dir}/27oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, output_path)

    result = tramline_paths(ccd=1, day=27, take="p1")

    assert len(result) == 1
    assert result[0] == f"{OBSERVATIONS_DIR}/27Oct18/reduce/288/p1/ccd1/27oct10022tlm.fits"

    cleanup.remove_test_fits()


def test_tramline_path():
    tramline_dir = ccd_day_take_dir(ccd=1, day=27, take="p1")
    output_path = f"{tramline_dir}/27oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, output_path)

    result = tramline_single_path(ccd=1, day=27, take="p1")

    assert result == f"{OBSERVATIONS_DIR}/27Oct18/reduce/288/p1/ccd1/27oct10022tlm.fits"

    cleanup.remove_test_fits()


def test_tramline_path_no_found():
    cleanup.remove_test_fits()
    result = tramline_single_path(ccd=1, day=27, take="p1")

    assert result is None
