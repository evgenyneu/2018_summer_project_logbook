import os
from .extract_stars import extract_fibre, extract_star, extract_star_all_ccds
from .paths import star_unprocessed_path
from .paths import fits_path_one_ccd
from ..shared import fixtures, cleanup


def test_extract_ccd_fibre():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)
    input_path = fits_path_one_ccd(ccd=1, day=29, take="p0")
    output_path_dir = os.path.dirname(os.path.realpath(__file__))
    output_path = os.path.join(output_path_dir, "extracted_stars_234.fits")

    if os.path.exists(output_path):
        os.remove(output_path)

    extract_fibre(input_path=input_path, output_path=output_path, fibre_number=113)

    assert os.path.exists(output_path) is True
    os.remove(output_path)
    cleanup.remove_test_fits()


def test_extract_star():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)
    output_path = star_unprocessed_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")

    if os.path.exists(output_path):
        os.remove(output_path)

    result = extract_star(name='AGBSYAZ017762', ccd=1, day=29, take="p0")

    assert os.path.exists(output_path) is True
    assert result == output_path
    cleanup.remove_test_fits()


def test_extract_star_all_ccds():
    fixtures.copy_combined_fibres(day=29, take="p0")
    output_path_ccd1 = star_unprocessed_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")

    if os.path.exists(output_path_ccd1):
        os.remove(output_path_ccd1)

    output_path_ccd2 = star_unprocessed_path(name='AGBSYAZ017762', ccd=2, day=29, take="p0")

    if os.path.exists(output_path_ccd2):
        os.remove(output_path_ccd2)

    output_path_ccd3 = star_unprocessed_path(name='AGBSYAZ017762', ccd=3, day=29, take="p0")

    if os.path.exists(output_path_ccd3):
        os.remove(output_path_ccd3)

    result = extract_star_all_ccds(name='AGBSYAZ017762', day=29, take="p0")

    assert os.path.exists(output_path_ccd1) is True
    assert os.path.exists(output_path_ccd2) is True
    assert os.path.exists(output_path_ccd3) is True

    assert result[0] == output_path_ccd1
    assert result[1] == output_path_ccd2
    assert result[2] == output_path_ccd3

    cleanup.remove_test_fits()
