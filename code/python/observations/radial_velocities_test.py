from .radial_velocities import run_fxcor_ccd, star_velocities, mean_radial_velocity, doppler_correct, doppler_correct_all_ccds, calculate_radial_velocities, store_candidate_radial_velocities, calculate_radial_velocities_all_ccds, visual_velocity_check, mean_velocity_from_vetted, calculate_mean_velocities, mean_radial_velocities_known
import pytest
from . import radial_velocities
from pytest import approx
import os
from .paths import star_doppler_corrected_path, candidate_radial_velocities_path, vetted_velocities_relative_path, mean_velocity_relative_path, OBSERVATIONS_DIR, mean_velocity_path, star_unprocessed_path, vetted_velocities_path
import pandas as pd
from ..shared import user_input, files, fixture_paths, fixtures, cleanup
from unittest import mock
from unittest.mock import patch
from shutil import copyfile
from .constants import CCDs


@pytest.mark.slow
def test_run_fxcor_ccd():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)

    df = run_fxcor_ccd(ccd=1, wavelength_start=4800, range_width=20, max_fibres=2, day=29, take="p0")

    assert len(df) == 2
    assert df.loc[4, 'VREL'] == -28.9249
    assert df.loc[4, 'VERR'] == 0.073
    cleanup.remove_test_fits()


@pytest.mark.slow
def test_star_velocities():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)

    df = star_velocities(ccd=1, day=29, take="p0", wavelength_start=4800, range_width=20, max_fibres=2)

    assert len(df) == 2
    assert df.loc[11, 'Name'] == 'RGBSYAZ025174'
    assert df.loc[11, 'VREL'] == -28.6983
    assert df.loc[11, 'VERR'] == approx(0.208)
    assert df.loc[11, 'RA'] == 0.23108159
    assert df.loc[11, 'Dec'] == -0.4635177
    assert df.loc[11, 'Mag'] == 15.4
    cleanup.remove_test_fits()


@pytest.mark.slow
def test_star_velocities_include_stars():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)

    df = star_velocities(ccd=1, day=29, take="p0", wavelength_start=4800, range_width=20, max_fibres=5,
                         include_stars=['RGBSYAZ025174', 'RGBSYAZ027701'])

    assert len(df) == 2
    assert df.loc[11, 'Name'] == 'RGBSYAZ025174'
    assert df.loc[301, 'Name'] == 'RGBSYAZ027701'
    cleanup.remove_test_fits()


def test_mean_radial_velocity():
    # Prepare the csv file containing velocity
    output_path = mean_velocity_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    input_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/mean_velocity_fixture.csv"
    files.copy_file_rewrite(input_path, output_path)

    result = mean_radial_velocity(name='AGBSYAZ017762', ccd=1, day=29, take="p0")

    assert result.nominal_value == approx(-31.4665)
    assert result.std_dev == approx(0.6333778519441084)
    cleanup.remove_test_fits()


def test_mean_radial_velocity_not_found():
    result = mean_radial_velocity(name='nonsense', ccd=1, day=27, take="p1")

    assert result is None


def test_mean_radial_velocities_known_true():
    # Prepare the csv file containing velocity
    for ccd_number in CCDs:
        output_path = mean_velocity_path(name='AGBSYAZ017762', ccd=ccd_number, day=29, take="p0")
        input_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/mean_velocity_fixture.csv"
        files.copy_file_rewrite(input_path, output_path)

    result = mean_radial_velocities_known(name='AGBSYAZ017762', day=29, take="p0")

    assert result is True

    cleanup.remove_test_fits()


def test_mean_radial_velocities_known_false():
    cleanup.remove_test_fits()

    # Prepare the csv file containing velocity
    output_path = mean_velocity_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    input_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/mean_velocity_fixture.csv"
    files.copy_file_rewrite(input_path, output_path)

    result = mean_radial_velocities_known(name='AGBSYAZ017762', day=29, take="p0")

    assert result is False

    cleanup.remove_test_fits()


def test_dopper_correct():
    # Prepare the csv file containing velocity
    output_path_velocity = mean_velocity_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    input_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/mean_velocity_fixture.csv"
    files.copy_file_rewrite(input_path, output_path_velocity)

    # Prepare the input .fits file for the star
    output_path_unprocessed = star_unprocessed_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")
    input_path = fixture_paths.individual_fits_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, output_path_unprocessed)

    output_path = f"{OBSERVATIONS_DIR}/29Oct18/reduce/288/p0/stars/doppler_corrected/AGBSYAZ017762/ccd1.fits"

    if os.path.exists(output_path):
        os.remove(output_path)

    result = doppler_correct(name='AGBSYAZ017762', ccd=1, day=29, take="p0")

    assert os.path.exists(output_path) is True
    assert result == output_path
    cleanup.remove_test_fits()


def test_dopper_correct_star_does_not_exist():
    with pytest.raises(ValueError):
        doppler_correct(name='nonsense', ccd=1, day=29, take="p0")


def test_dopper_correct_all_ccds():
    for ccd_number in CCDs:
        # Prepare the csv file containing velocity
        output_path_velocity = mean_velocity_path(name='AGBSYAZ017762', ccd=ccd_number, day=29, take="p0")
        input_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/mean_velocity_fixture.csv"
        files.copy_file_rewrite(input_path, output_path_velocity)

        # Prepare the input .fits file for the star
        output_path = star_unprocessed_path(name='AGBSYAZ017762', ccd=ccd_number, day=29, take="p0")
        input_path = fixture_paths.individual_fits_fixture_path(ccd=ccd_number)
        files.copy_file_rewrite(input_path, output_path)

    output_path_ccd1 = star_doppler_corrected_path(name='AGBSYAZ017762', ccd=1, day=29, take="p0")

    if os.path.exists(output_path_ccd1):
        os.remove(output_path_ccd1)

    output_path_ccd2 = star_doppler_corrected_path(name='AGBSYAZ017762', ccd=2, day=29, take="p0")

    if os.path.exists(output_path_ccd2):
        os.remove(output_path_ccd2)

    output_path_ccd3 = star_doppler_corrected_path(name='AGBSYAZ017762', ccd=3, day=29, take="p0")

    if os.path.exists(output_path_ccd3):
        os.remove(output_path_ccd3)

    result = doppler_correct_all_ccds(name='AGBSYAZ017762', day=29, take="p0")

    assert os.path.exists(output_path_ccd1) is True
    assert os.path.exists(output_path_ccd2) is True
    assert os.path.exists(output_path_ccd3) is True

    assert result[0] == output_path_ccd1
    assert result[1] == output_path_ccd2
    assert result[2] == output_path_ccd3

    cleanup.remove_test_fits()


@pytest.mark.slow
def test_calculate_radial_velocities():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=1)

    df = calculate_radial_velocities(names=['AGBSYAZ017762'], ccd=1, day=29, take="p0",
                                     measurements=3)

    assert len(df) == 3
    df_star = df.loc[df['Name'] == 'AGBSYAZ017762']
    assert len(df_star) == 3
    assert df_star.iloc[0]['VREL'] == approx(-31.6799, rel=1e-3, abs=0)
    assert df_star.iloc[0]['VERR'] == 0.086
    assert df_star.iloc[0]['Ccd'] == 1
    assert df_star.iloc[0]['Day'] == 29
    assert df_star.iloc[0]['Take'] == 'p0'

    start = df_star.iloc[0]['Wavelength start [A]']
    assert start == 4720

    end = df_star.iloc[0]['Wavelength end [A]']
    assert end == 4755

    cleanup.remove_test_fits()


@pytest.mark.slow
def test_calculate_radial_velocities_all_ccds():
    fixtures.copy_combined_fibres(day=29, take="p0")

    df = calculate_radial_velocities_all_ccds(names=None, day=29, take="p0", measurements=3,
                                              max_fibres=2)

    assert len(df) == 18
    df_star = df.loc[df['Name'] == 'RGBSYAZ021904']
    assert len(df_star) == 9
    assert df_star.iloc[3]['VREL'] == -29.07
    assert df_star.iloc[3]['VERR'] == approx(0.137)
    assert df_star.iloc[3]['Ccd'] == 2
    assert df_star.iloc[3]['Day'] == 29
    assert df_star.iloc[3]['Take'] == 'p0'

    start = df_star.iloc[3]['Wavelength start [A]']
    assert start == 5654

    end = df_star.iloc[3]['Wavelength end [A]']
    assert end == 5689

    cleanup.remove_test_fits()


@pytest.mark.slow
def test_store_candidate_radial_velocities():
    fixtures.copy_combined_fibres(day=29, take="p0")

    output_path1 = candidate_radial_velocities_path(name='AGBSYAZ024409', ccd=2, day=29, take="p0")

    if os.path.exists(output_path1):
        os.remove(output_path1)

    output_path2 = candidate_radial_velocities_path(name='RGBSYAZ021904', ccd=2, day=29, take="p0")

    if os.path.exists(output_path2):
        os.remove(output_path2)

    output_path3 = candidate_radial_velocities_path(name='RGBSYAZ025174', ccd=2, day=29, take="p0")

    if os.path.exists(output_path3):
        os.remove(output_path3)

    output_path4 = candidate_radial_velocities_path(name='RGBSYAZ025174', ccd=3, day=29, take="p0")

    if os.path.exists(output_path4):
        os.remove(output_path4)

    result = store_candidate_radial_velocities(names=['AGBSYAZ024409', 'RGBSYAZ021904', 'RGBSYAZ025174'],
                                               day=29, take="p0", measurements=3)

    assert len(result) == 9

    assert os.path.exists(output_path1) is True
    assert output_path1 in result

    assert os.path.exists(output_path2) is True
    assert output_path2 in result

    assert os.path.exists(output_path3) is True
    assert output_path3 in result

    assert os.path.exists(output_path4) is True
    assert output_path4 in result

    # Check the CSV file
    # --------------------

    df = pd.read_csv(output_path1)

    assert len(df) == 3

    # Check index column
    # --------------------

    assert df.columns[0] == "Index"
    assert df["Index"][0] == 0
    assert df["Index"][1] == 1
    assert df["Index"][2] == 2

    # Check star data
    # --------------------

    df_star = df.loc[df['Name'] == 'AGBSYAZ024409']
    assert len(df_star) == 3
    assert df_star.iloc[0]['VREL'] == -28.8381
    assert df_star.iloc[0]['VERR'] == 0.24
    assert df_star.iloc[0]['Ccd'] == 2
    assert df_star.iloc[0]['Day'] == 29
    assert df_star.iloc[0]['Take'] == 'p0'

    start = df_star.iloc[0]['Wavelength start [A]']
    assert start == 5654

    end = df_star.iloc[0]['Wavelength end [A]']
    assert end == 5689

    cleanup.remove_test_fits()


def visual_velocity_check_input_integer_mock(message, min_value, max_value):
    if message == '5 Rate the quality of fit from 0 (worst) to 5 (best):':
        return 0

    if message == '6 Rate the quality of fit from 0 (worst) to 5 (best):':
        return 4

    if message == '1 Rate the quality of fit from 0 (worst) to 5 (best):':
        return 3

    if message == '0 Rate the quality of fit from 0 (worst) to 5 (best):':
        return 5

    if message == '3 Rate the quality of fit from 0 (worst) to 5 (best):':
        return 2


@patch('builtins.print')
@patch.object(radial_velocities, 'star_velocities_interactive')
@patch.object(user_input, 'input_integer')
def test_visual_velocity_check(input_integer_patch, star_velocities_patch, print_patch):
    input_integer_patch.side_effect = visual_velocity_check_input_integer_mock
    file_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/candidates.csv"
    output_path = vetted_velocities_relative_path(file_path)

    if os.path.exists(output_path):
        os.remove(output_path)

    result = visual_velocity_check(file_path, checks=5)

    assert os.path.exists(output_path) is True
    assert result == output_path

    # Check the CSV file
    # --------------------

    df = pd.read_csv(output_path)
    df = df.set_index('Index')

    assert len(df) == 5
    df_star = df.loc[df['Name'] == 'AGBSYAZ017762']
    assert len(df_star) == 5
    assert df_star.loc[5]['VREL'] == -30.9353
    assert df_star.loc[5]['VERR'] == 0.064
    assert df_star.loc[5]['Quality of fit'] == 0
    assert df_star.loc[6]['Quality of fit'] == 4
    assert df_star.loc[1]['Quality of fit'] == 3
    assert df_star.loc[0]['Quality of fit'] == 5
    assert df_star.loc[3]['Quality of fit'] == 2


def test_mean_velocity_from_vetted():
    file_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/vetted_fixture.csv"

    result = mean_velocity_from_vetted(file_path)

    output_path = mean_velocity_relative_path(file_path)
    assert result == output_path

    # Check the CSV file
    # --------------------

    df = pd.read_csv(output_path)

    assert len(df) == 1
    assert df.columns[0] == "Name"
    row = df.iloc[0]
    assert row['Name'] == 'AGBSYAZ017762'
    assert row['Ccd'] == 1
    assert row['Day'] == 30
    assert row['Take'] == 'combined'
    assert row['Mean radial relocity V_mean [km/s]'] == -31.4665
    assert row['Uncertainty u(V_mean) [km/s]'] == 0.6333778519441084

    # Cleanup
    os.remove(output_path)


def test_mean_velocity_from_vetted_bad_fits():
    file_path = f"{OBSERVATIONS_DIR}/fixtures/velocities/vetted_fixture_bad_fits.csv"

    # Create an output .csv file to verify that it will be removed
    output_path = mean_velocity_relative_path(file_path)
    file = open(output_path, "w")
    file.write("Nonsense")
    file.close()
    assert os.path.exists(output_path) is True

    result = mean_velocity_from_vetted(file_path)

    assert result is None
    assert os.path.exists(output_path) is False


@pytest.mark.slow
@patch('builtins.print')
@patch.object(radial_velocities, 'star_velocities_interactive')
@patch.object(user_input, 'input_integer')
def test_calculate_mean_velocities(input_integer_patch, star_velocities_patch, print_patch):
    fixtures.copy_combined_fibres(day=29, take="p0")
    input_integer_patch.side_effect = visual_velocity_check_input_integer_mock

    result = calculate_mean_velocities(names=['AGBSYAZ024409'], day=29, take="p0", measurements=3)

    assert len(result) == 3
    path1 = mean_velocity_path(name='AGBSYAZ024409', ccd=1, day=29, take="p0")
    assert path1 in result

    path2 = mean_velocity_path(name='AGBSYAZ024409', ccd=2, day=29, take="p0")
    assert path2 in result

    path3 = mean_velocity_path(name='AGBSYAZ024409', ccd=2, day=29, take="p0")
    assert path3 in result

    # Check the CSV file
    # --------------------

    df = pd.read_csv(path1)

    assert len(df) == 1
    assert df.columns[0] == "Name"
    row = df.iloc[0]
    assert row['Name'] == 'AGBSYAZ024409'
    assert row['Ccd'] == 1
    assert row['Day'] == 29
    assert row['Take'] == 'p0'
    assert row['Mean radial relocity V_mean [km/s]'] == approx(-30.00065, rel=1e-3, abs=0)
    assert row['Uncertainty u(V_mean) [km/s]'] == approx(2.22695, rel=1e-3, abs=0)

    cleanup.remove_test_fits()