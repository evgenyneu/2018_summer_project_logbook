from .fibres import all_fibres
from astropy.io import fits
import pandas as pd
import numpy as np
import numpy

# Columns that will be returns with calculate_mean function
FIBRES_MEAN_COLUMNS = ['Name', 'Mean']


def calculate_mean(fits_path, object_starts_with=""):
    """
    Parameters
    ----------
    fits_path : str
        The path to the FITS file containing image and information about fibres.
    object_starts_with : str
        If supplied, only the fibres with names that start with the given string will be returned.
        If empty (default), all fibres will be returned.
    take : str
        The directory containing observation files.

    Returns
    -------
    pandas.DataFrame
        Returns the table containing fibre numbes, names and average values.
    """
    df_fibres = all_fibres(fits_path)
    df_filtered = df_fibres[df_fibres['Name'].str.startswith(object_starts_with)].copy(deep=True)
    df_filtered['Mean'] = 0
    hdul = fits.open(fits_path)
    data = hdul[0].data

    for fibre_id, row in df_filtered.iterrows():
        values = data[fibre_id - 1, :]
        values = values[~np.isnan(values)]  # Remove NaN values
        df_filtered.loc[fibre_id, "Mean"] = np.average(values)

    return df_filtered[FIBRES_MEAN_COLUMNS]
