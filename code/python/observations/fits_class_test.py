from .paths import ccd_day_take_dir
from ..shared import fixture_paths, files
from ..shared import cleanup
from .fits_class import set_class
from astropy.io import fits


def test_set_class():
    # Prepare the input .fits files
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_data = f"{pathdir}/data.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    set_class(path=path_data, obj_class="DARK")

    hdul = fits.open(path_data)
    obj_class = hdul['STRUCT.MORE.NDF_CLASS'].data['NAME'][0]
    assert obj_class == "DARK"
    hdul.close()
    cleanup.remove_test_fits()
