from .lineclean import lineclean
from .paths import ccd_day_take_dir
from ..shared import fixture_paths, files
from ..shared import cleanup
import pytest
import os


@pytest.mark.slow
def test_lineclean():
    cleanup.remove_test_fits()

    # Copy the tramline file
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")

    # Copy the raw image
    path_data = f"{pathdir}/rawdata.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    output_path = f"{pathdir}/output.fits"

    lineclean(input_path=input_path, output_path=output_path)

    assert os.path.exists(output_path) is True

    cleanup.remove_test_fits()