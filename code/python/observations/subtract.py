from astropy.io import fits
import numpy as np
import os
import sys


def subtract(first_path, second_path, output_path):
    """
    Subtracts the data of second image from the first image
    and saves to .fits file at `output_path`.

    Parameters
    ----------
    first_path : str
        Path to the first fits file.
    second_path: str
        Path to the second fits file.
    output_path: str
        Path to the output fits file.
    """

    if os.path.exists(output_path):
        raise FileExistsError(f"ERROR: the output file already exists: '{output_path}'.")

    if first_path == second_path:
        raise ValueError(f"ERROR: the two paths are the same: {first_path} and {second_path}")

    # Read the first fits file
    hdul1 = fits.open(first_path)
    data1 = hdul1["PRIMARY"].data

    # Read the second fits file
    hdul2 = fits.open(second_path)
    data2 = hdul2["PRIMARY"].data

    if data1.shape != data2.shape:
        raise ValueError(f"ERROR: The data have different shape: {data1.shape} and {data2.shape}")

    data1 = data1.astype(np.float32)
    data2 = data2.astype(np.float32)
    data1 -= data2
    hdul1["PRIMARY"].data = data1

    hdul1.writeto(output_path)

    hdul1.close()
    hdul2.close()


if __name__ == '__main__':
    arguments = dict(map(lambda x: x.lstrip('-').split('='), sys.argv[1:]))

    subtract(first_path=arguments.get('first_path'),
             second_path=arguments.get('second_path'),
             output_path=arguments.get('output_path'))