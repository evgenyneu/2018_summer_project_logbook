from astropy.io import fits
from .fibres import all_fibres
from .paths import fits_path_one_frame, tramline_single_path
import os

# Columns that will be present in the CSV file
FIBRES_CSV_COLUMNS = ['Name', 'Mag', 'y']


def tram_positions(tram_path):
    """
    Parameters
    ----------
    tram_path: str
        Path to a *tlm.fits file containing positions of the tramlines.


    Returns
    -------
        Table containing positions of fibres.
    """

    with fits.open(tram_path) as hdul:
        return hdul[0].data


def fibres_with_positions(data_path, tram_path):
    """
    Parameters
    ----------
    data_path: str
        Path to the FITS file containing the image.

    tram_path: str
        Path to a *tlm.fits file containing positions of the tramlines.

    Returns
    -------
        Table containing information about fibres along with their y-positions ('y' column)
        in the image.
    """

    tram = tram_positions(tram_path)
    fibres = all_fibres(data_path)

    fibres['y'] = 0

    for index, row_fibre in fibres.iterrows():
        fibres.loc[index, 'y'] = tram[index-1, 2000]

    return fibres


def fibres_to_csv(data_path, tram_path, csv_path, columns=FIBRES_CSV_COLUMNS):
    """
    Exports information about fibres into a CSV file at `csv_path`.

    Parameters
    ----------
    data_path: str
        Path to the FITS file containing the image.

    tram_path: str
        Path to a *tlm.fits file containing positions of the tramlines.

    csv_path: str
        Path to a CSV file that will be created containing information about fibres

    columns: list of str
        The names of the columns to include in the CSV file.
    """

    if os.path.exists(csv_path):
        raise FileExistsError(f"ERROR: the CSV file already exists: '{csv_path}'.")

    output_dir = os.path.dirname(csv_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    df = fibres_with_positions(data_path=data_path, tram_path=tram_path)
    df[columns].to_csv(csv_path)


def fibres_to_csv_ccd(ccd, day, take, frame, csv_path, columns=FIBRES_CSV_COLUMNS):
    """
    Exports information about fibres into a CSV file at `csv_path`.

    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    frame : int
        The frame of the input file to remove the glow from.
        For example, for 27oct10016.fits file, the frame is 16.
    csv_path: str
        Path to a CSV file that will be created containing information about fibres

    columns: list of str
        The names of the columns to include in the CSV file.
    """

    data_path = fits_path_one_frame(ccd=ccd, day=day, take=take, frame=frame)
    tram_path = tramline_single_path(ccd=ccd, day=day, take=take)
    fibres_to_csv(data_path=data_path, tram_path=tram_path, csv_path=csv_path, columns=columns)