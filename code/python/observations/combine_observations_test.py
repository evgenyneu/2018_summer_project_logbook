from .combine_observations import copy_ccd, copy_all_ccds, copy_to_canonical_path, copy_to_canonical_path_all_ccds
import os
from .paths import OBSERVATIONS_DIR, combined_observations_fits_from_2dfdr
from ..shared import fixtures, cleanup, files, fixture_paths


def test_copy_combined():
    fixtures.copy_combined_fibres(day=29, take="p0", ccd=2)
    output_path = f"{OBSERVATIONS_DIR}/30Oct18/reduce/288/mouse/ccd2/29oct2_p0_combined.fits"

    if os.path.exists(output_path):
        os.remove(output_path)

    result = copy_ccd(ccd=2, day=29, take="p0",day_combined=30, take_combined="mouse")

    assert result == output_path
    assert os.path.exists(output_path) is True
    cleanup.remove_test_fits()


def test_copy_combined_all_ccds():
    fixtures.copy_combined_fibres(day=29, take="p0")
    output_path_ccd1 = f"{OBSERVATIONS_DIR}/30Oct18/reduce/288/chess/ccd1/29oct1_p0_combined.fits"
    output_path_ccd2 = f"{OBSERVATIONS_DIR}/30Oct18/reduce/288/chess/ccd2/29oct2_p0_combined.fits"
    output_path_ccd3 = f"{OBSERVATIONS_DIR}/30Oct18/reduce/288/chess/ccd3/29oct3_p0_combined.fits"

    if os.path.exists(output_path_ccd1):
        os.remove(output_path_ccd1)

    if os.path.exists(output_path_ccd2):
        os.remove(output_path_ccd2)

    if os.path.exists(output_path_ccd3):
        os.remove(output_path_ccd3)

    result = copy_all_ccds(day=29, take="p0", day_combined=30, take_combined="chess")

    assert result[0] == output_path_ccd1
    assert os.path.exists(output_path_ccd1) is True

    assert result[1] == output_path_ccd2
    assert os.path.exists(output_path_ccd2) is True

    assert result[2] == output_path_ccd3
    assert os.path.exists(output_path_ccd3) is True

    cleanup.remove_test_fits()


def test_copy_to_canonical_path():
    output_path_ccd1 = f"{OBSERVATIONS_DIR}/27Oct18/reduce/288/match/ccd1/27oct1_combined.fits"

    if os.path.exists(output_path_ccd1):
        os.remove(output_path_ccd1)

    # Make dummy input .fits file
    output_path = combined_observations_fits_from_2dfdr(ccd=1, day=27, take="match")
    input_path = fixture_paths.individual_fits_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, output_path)

    result = copy_to_canonical_path(ccd=1, day=27, take="match")

    assert result == output_path_ccd1
    assert os.path.exists(output_path_ccd1) is True

    cleanup.remove_test_fits()


def test_copy_to_canonical_path_all_ccd1():
    output_path_ccd1 = f"{OBSERVATIONS_DIR}/27Oct18/reduce/288/possum/ccd1/27oct1_combined.fits"

    if os.path.exists(output_path_ccd1):
        os.remove(output_path_ccd1)

    output_path_ccd2 = f"{OBSERVATIONS_DIR}/27Oct18/reduce/288/possum/ccd2/27oct2_combined.fits"

    if os.path.exists(output_path_ccd2):
        os.remove(output_path_ccd2)

    output_path_ccd3 = f"{OBSERVATIONS_DIR}/27Oct18/reduce/288/possum/ccd3/27oct3_combined.fits"

    if os.path.exists(output_path_ccd3):
        os.remove(output_path_ccd3)

    # Make dummy input .fits files
    # ------------

    input_path1 = combined_observations_fits_from_2dfdr(ccd=1, day=27, take="possum")
    input_path1_dir = os.path.dirname(input_path1)
    if not os.path.exists(input_path1_dir):
        os.makedirs(input_path1_dir)
    open(input_path1, 'a').close()

    input_path2 = combined_observations_fits_from_2dfdr(ccd=2, day=27, take="possum")
    input_path2_dir = os.path.dirname(input_path2)
    if not os.path.exists(input_path2_dir):
        os.makedirs(input_path2_dir)
    open(input_path2, 'a').close()

    input_path3 = combined_observations_fits_from_2dfdr(ccd=3, day=27, take="possum")
    input_path3_dir = os.path.dirname(input_path3)
    if not os.path.exists(input_path3_dir):
        os.makedirs(input_path3_dir)
    open(input_path3, 'a').close()

    result = copy_to_canonical_path_all_ccds(day=27, take="possum")

    assert result[0] == output_path_ccd1
    assert os.path.exists(output_path_ccd1) is True

    assert result[1] == output_path_ccd2
    assert os.path.exists(output_path_ccd2) is True

    assert result[2] == output_path_ccd3
    assert os.path.exists(output_path_ccd3) is True

    cleanup.remove_test_fits()