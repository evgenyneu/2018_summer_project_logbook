from .paths import star_doppler_corrected_path, star_normalized_path
import os
from pyraf import iraf
from iraf import onedspec
from .constants import CCDs
from ..shared.files import copy_file_rewrite
import tempfile


def normalize(name, ccd, day, take, order=3, grow=0, high_reject=0, low_reject=1.2,
              function="spline3", niterat=10, interactive="yes"):
    """
    Normalizes the star's spectrum.
    This function needs to be called after the star's spectrum has been corrected for the
    doppler shirt with `doppler_correct`.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to the normalized .fits file.
    """

    input_path = star_doppler_corrected_path(name=name, ccd=ccd, day=day, take=take)
    output_path = star_normalized_path(name=name, ccd=ccd, day=day, take=take)

    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if os.path.exists(output_path):
        os.remove(output_path)

    # IRAF's continuum function crashes if the output path is too long
    # As a workaround, we work in system's temporary directory in order to make paths shorter
    temp_dir = tempfile.gettempdir()
    input_path_temp = os.path.join(temp_dir, "in.fits")
    output_path_temp = os.path.join(temp_dir, "out.fits")
    copy_file_rewrite(input_path=input_path, output_path=input_path_temp)

    onedspec.continuum(input=input_path_temp, output=output_path_temp, order=order, mark="no",
                       grow=grow, high_reject=high_reject, low_reject=low_reject,
                       function=function, niterat=niterat, override="yes",
                       interactive=interactive)

    # Copy the output file from temporary directory back to our desired location
    copy_file_rewrite(input_path=output_path_temp, output_path=output_path)

    # Remove files from the temporary directory
    if os.path.exists(input_path_temp):
        os.remove(input_path_temp)

    if os.path.exists(output_path_temp):
        os.remove(output_path_temp)

    return output_path


def normalize_all_ccds(name, day, take, order=3, grow=0, high_reject=0, low_reject=1.2,
                       function="spline3", niterat=10, interactive="yes"):
    """
    Normalizes the star's spectrum for all CCDs.
    This function needs to be called after the star's spectrum has been corrected for the
    doppler shirt with `doppler_correct`.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    list of str
        List of paths to the output .fits files.
    """

    paths = []

    for ccd_number in CCDs:
        path = normalize(name=name, ccd=ccd_number, day=day, take=take, order=order,
                         grow=grow, high_reject=high_reject, low_reject=low_reject,
                         function=function, niterat=niterat, interactive=interactive)

        paths.append(path)

    return paths
