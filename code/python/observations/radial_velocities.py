from .fibres import science_stars
from .paths import fits_path_one_ccd, fits_sun_path, star_unprocessed_path, star_doppler_corrected_path, candidate_radial_velocities_path, vetted_velocities_relative_path, mean_velocity_relative_path, mean_velocity_path
from ..iraf.fxcor import parse_output
from pyraf import iraf
from iraf import rv
from iraf import onedspec
import os
import pandas as pd
from uncertainties import ufloat
from .constants import CCDs, HERMES_BANDS
import math
from ..shared import user_input

MEAN_RADIAL_VELOCITY_COLUMN_NAME = 'Mean radial relocity V_mean [km/s]'
UNCERTAINTY_OF_MEAN_RADIAL_VELOCITY_COLUMN_NAME = 'Uncertainty u(V_mean) [km/s]'


def chunks(list, chunk_size):
    """
    Break a list into n-sized chunks
    """
    for i in range(0, len(list), chunk_size):
        yield list[i:i + chunk_size]


def run_fxcor_ccd(ccd, day, take, wavelength_start, range_width, output_dir="", max_fibres=0, remove_output=True,
                  interactive=False, include_stars=None, show_log=None):
    """
    Calculates radial velocity estimates by running IRAF's fxcor returns its output.

    Parameters
    ----------
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    wavelength_start : int
        The start of the wavelength range, in angstroms.
    range_width: int
        The width of the range in angstroms.
    output_dir: str
        The path where the output file from fxcor is generater.
        Use the current directory if None (Default).
    max_fibres: int
        Max number of fibres to process. 0 for all fibres (default).
    remove_output: bool
        If True (default) the text output file is removed.
    interactive: bool
        Make fxcor run interactively. False by default.
    include_stars : list of str
        If supplied, only return records for the given stars.
    show_log: function
        A function that is called for each run of fxcor.
        It can be used to show progress to the user.
        The function is called with single str parameter.
        If None, the function is ignored (Default)

    Returns
    -------
    pandas.DataFrame
        Table containing the fxcor output.
    """

    fits_path = fits_path_one_ccd(ccd=ccd, day=day, take=take)
    stars = science_stars(ccd=ccd, day=day, take=take)
    if include_stars != None: stars = stars.loc[stars['Name'].isin(include_stars)]
    fibres = stars.index.values
    if max_fibres != 0: fibres = fibres[:max_fibres]
    fibres_per_run = 30

    # Break list of fibres into smaller chunks, since fxcor can not process too many of them (should be less than 50, roughly)
    fibres_chunked = list(chunks(fibres, fibres_per_run))

    wavelength_end = wavelength_start + range_width
    wavelength_range = f"{wavelength_start}-{wavelength_end}"
    output_path = os.path.join(output_dir, f"fxcor-ccd{ccd}-{wavelength_range}")
    output_path_with_extension = f"{output_path}.txt"
    if os.path.exists(output_path_with_extension): os.remove(output_path_with_extension)
    interactive_str = "no"

    if interactive:
        interactive_str = "yes"

    for fibres in fibres_chunked:
        fibres_joined = " ".join(str(n) for n in fibres)

        if show_log is not None:
            show_log(f"Wavelengths {wavelength_range} A")

        rv.fxcor(objects=f"{fits_path}[0]", templates=fits_sun_path(),
                 apertures=fibres_joined, autowrite="yes", osample=wavelength_range,
                 rsample=wavelength_range, rebin="largest", continuum="both", filter="no",
                 verbose="txtonly", interactive=interactive_str,
                 output=output_path)

    df = parse_output(output_path_with_extension)

    if remove_output:
        os.remove(output_path_with_extension)

    return df


def star_velocities(ccd, wavelength_start, range_width, day, take,
                    output_dir="", max_fibres=0, remove_output=True,
                    interactive=False, include_stars=None, show_log=None):
    """
    Calculates radial velocity estimates by running IRAF's fxcor returns its output.

    Returns
    -------
    pandas.DataFrame
        Table containing the the velocities of the stars that can be used for plotting.
        This is the output of fxcor that also has other information about stars, such as names,
        positions, magnitudes etc.
    """

    df_fxcor = run_fxcor_ccd(ccd=ccd, wavelength_start=wavelength_start, range_width=range_width,
                             output_dir=output_dir, max_fibres=max_fibres,
                             remove_output=remove_output, interactive=interactive,
                             day=day, take=take, include_stars=include_stars, show_log=show_log)

    stars = science_stars(ccd=ccd, day=day, take=take)

    df_merged = pd.concat([df_fxcor, stars], axis=1, join="inner")
    columns = ['Name','VREL', 'VERR', 'RA', 'Dec', 'Mag']
    return df_merged[columns]


def star_velocities_interactive(ccd, wavelength_start, range_width, day, take, output_dir="",
                                max_fibres=0, remove_output=True, include_stars=None):
    """
    Executes star_velocities method in interactive mode. This wrapper is needed in order
    for the unit test where we fake the interactivity.
    """

    return star_velocities(ccd=ccd, wavelength_start=wavelength_start, range_width=range_width, 
                           output_dir=output_dir, max_fibres=max_fibres,
                           remove_output=remove_output, interactive=True, day=day,
                           take=take, include_stars=include_stars)


def mean_radial_velocity(name, ccd, day, take):
    """
    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    uncertainties.ufloat
        Radial velocity of the star in km/s with respect to the observer (the telescope).
    """
    csv_path = mean_velocity_path(name=name, ccd=ccd, day=day, take=take)

    if not os.path.exists(csv_path):
        return None

    df = pd.read_csv(csv_path)
    df_star = df.loc[df['Name'] == name]

    if len(df_star) != 1:
        return None

    velocity_data = df_star.iloc[0][[MEAN_RADIAL_VELOCITY_COLUMN_NAME, UNCERTAINTY_OF_MEAN_RADIAL_VELOCITY_COLUMN_NAME]].values
    velocity = ufloat(velocity_data[0], velocity_data[1])

    return velocity


def mean_radial_velocities_known(name, day, take):
    """
    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    Boolean
        True if radial velocities for ALL CCDs are know. False, if at least one CCD is missing radial velocity value.
    """
    for ccd_number in CCDs:
        velocity = mean_radial_velocity(name=name, ccd=ccd_number, day=day, take=take)
        if velocity is None:
            return False

    return True


def doppler_correct(name, ccd, day, take):
    """
    Corrects the star's spectrum for the doppler shift due to star's radial velocity.
    This function needs to be called after the star has been extracted (with `extract_star`).

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to the doppler corrected .fits file.
    """

    input_path = star_unprocessed_path(name=name, ccd=ccd, day=day, take=take)
    output_path = star_doppler_corrected_path(name=name, ccd=ccd, day=day, take=take)

    velocity = mean_radial_velocity(name=name, ccd=ccd, day=day, take=take)

    if velocity is None:
        raise ValueError(f"Can not find velocity for star '{name}'")

    if os.path.exists(output_path):
        os.remove(output_path)

    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    onedspec.dopcor(input=input_path, output=output_path, redshift=velocity.nominal_value,
                    isvelocity="yes")

    return output_path


def doppler_correct_all_ccds(name, day, take):
    """
    Corrects the star's spectrum for the doppler shift due to star's radial velocity.
    This method needs to be called after the star has been extracted (with `extract_star`).

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    list of str
        List of paths to the doppler corrected .fits files.
    """

    paths = []

    for ccd_number in CCDs:
        paths.append(doppler_correct(name=name, ccd=ccd_number, day=day, take=take))

    return paths


def calculate_radial_velocities(names, ccd, day, take, measurements=10, range_width=35,
                                max_fibres=0, show_log=None):
    """
    Calculates multiple radial velocities of the star. We take several measurement of the velocity taken at different wavelengths, evenly distributes across the range of wavelengths for this CCD.

    Parameters
    ----------
    names : str
        Name of stars, i.e. ['AGBSYAZ017762']. Calculates all stars if None.
    ccd : int
        The CCD number.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    measurements: int
        The number of velocity measurements to make.
    range_width: int
        The width of the window of measurement in Angstroms.
    max_fibres: int
        Max number of fibers (stars) to process when `names` parameter is None. 0 for all fibers (default).
    show_log: function
        A function that is called for each run of fxcor.
        It can be used to show progress to the user.
        The function is called with single str parameter.
        If None, the function is ignored (Default)

    Returns
    -------
    pandas.DataFrame
        Table containing the the velocities of the stars.
    """

    # Get the star and end wavelength for the CCD.
    # We make the range narrower by a margin on both sides to avoid measuring at the very edge.
    wavelength_margin = 5
    ccd_start_wavelength = HERMES_BANDS[ccd]['start'] + wavelength_margin
    ccd_end_wavelength = HERMES_BANDS[ccd]['end'] - wavelength_margin - range_width
    wavelength_range = ccd_end_wavelength - ccd_start_wavelength

    # We want to take measurement evenly distributes across the range of wavelength.
    # So here we calculate the increment.
    if measurements == 1:
        wavelength_increment = 0
    else:
        wavelength_increment = math.floor(wavelength_range / (measurements - 1))

    df_combined = pd.DataFrame()

    for i in range(0, measurements):
        wavelength_start = ccd_start_wavelength + wavelength_increment * i
        wavelength_end = wavelength_start + range_width

        if wavelength_end > HERMES_BANDS[ccd]['end']:
            print(f"Ignoring the velocity measurement since its range {wavelength_start}-{wavelength_end} is outside the range of the CCD{ccd}.")
            continue

        df = star_velocities(ccd=ccd, day=day, take=take, wavelength_start=wavelength_start,
                             range_width=range_width, interactive=False, include_stars=names,
                             max_fibres=max_fibres, show_log=show_log)

        df['Ccd'] = ccd
        df['Day'] = day
        df['Take'] = take
        df['Wavelength start [A]'] = wavelength_start
        df['Wavelength end [A]'] = wavelength_end
        df_combined = pd.concat([df_combined, df])

    return df_combined


def calculate_radial_velocities_all_ccds(names, day, take, measurements=10, range_width=35,
                                         max_fibres=0, show_log=None):
    """
    Calculates multiple radial velocities of the star for all CCDs. We take several measurement of the velocity taken at different wavelengths, evenly distributes across the range of wavelength of the CCD.

    Parameters
    ----------
    names : str
        Name of stars, i.e. ['AGBSYAZ017762']. Calculates all stars if None.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    measurements: int
        The number of velocity measurements to make.
    range_width: int
        The width of the window of measurement in Angstroms.
    max_fibres: int
        Max number of fibers (stars) to process when `names` parameter is None. 0 for all fibers (default).
    show_log: function
        A function that is called for each run of fxcor.
        It can be used to show progress to the user.
        The function is called with single str parameter.
        If None, the function is ignored (Default)

    Returns
    -------
    pandas.DataFrame
        Table containing the the velocities of the stars.
    """

    df_combined = pd.DataFrame()

    for ccd_number in CCDs:
        df = calculate_radial_velocities(names=names, ccd=ccd_number, day=day, take=take,
                                         measurements=measurements, range_width=range_width,
                                         max_fibres=max_fibres, show_log=show_log)

        df_combined = pd.concat([df_combined, df])

    return df_combined


def store_candidate_radial_velocities(names, day, take, measurements=10, range_width=35, max_fibres=0,
                                      show_log=None):
    """
    Calculates radial velocities and save them in .csv files for each star.

    Parameters
    ----------
    names : str
        Name of stars, i.e. ['AGBSYAZ017762']. Calculates all stars if None.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    measurements: int
        The number of velocity measurements to make.
    range_width: int
        The width of the window of measurement in Angstroms.
    max_fibres: int
        Max number of fibers (stars) to process when `names` parameter is None. 0 for all fibers (default).
    show_log: function
        A function that is called for each run of fxcor.
        It can be used to show progress to the user.
        The function is called with single str parameter.
        If None, the function is ignored (Default)

    Returns
    -------
    list of str
        List of paths to the .csv files.
    """

    df = calculate_radial_velocities_all_ccds(names=names, day=day, take=take,
                                              measurements=measurements, range_width=range_width,
                                              max_fibres=max_fibres,
                                              show_log=show_log)

    dataset_names = df["Name"].unique()
    ccd_names = df["Ccd"].unique()
    output_paths = []

    for name in dataset_names:
        for ccd in ccd_names:
            output_csv_path = candidate_radial_velocities_path(name=name, ccd=ccd, day=day, take=take)

            if os.path.exists(output_csv_path):
                os.remove(output_csv_path)

            output_dir = os.path.dirname(output_csv_path)

            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            output_paths.append(output_csv_path)
            df_star = df.loc[(df['Name'] == name) & (df['Ccd'] == ccd)]

            # Create a new index column named "Index" with values 0, 1, 2, ...
            df_star = df_star.reset_index(drop=True)
            df_star.index.name = 'Index'

            df_star.to_csv(output_csv_path)

    return output_paths


def visual_velocity_check(candidates_path, checks=5):
    """
    Presents fxcor fits for top `checks` velocity calculates with lowest errors from the candidates CSV file. The use marks the quality of the fit interactively, and the best fits are stored to a csv file.

    Parameters
    ----------
    candidates_path : str
        Path to a .csv file containing velocities generated by `store_candidate_radial_velocities` functions.
    checks : int
        The number of fits of check.

    Returns
    -------
    str
        Path to a .csv file contening velocities vetted by the user.
    """

    df = pd.read_csv(candidates_path)
    df['Quality of fit'] = 0
    df = df.set_index("Index")

    # Keep `checks` rows with lowest velocity errors
    df = df.loc[df['VERR'].nsmallest(checks).index]

    print(f"Velocities: {df['VREL'].values}")
    print(f"Errors: {df['VERR'].values}")

    for index, row in df.iterrows():
        ccd = row['Ccd']
        day = row['Day']
        take = row['Take']
        wavelength_start = row['Wavelength start [A]']
        wavelength_end = row['Wavelength end [A]']
        range_width = wavelength_end - wavelength_start

        star_velocities_interactive(ccd=ccd, day=day, take=take, wavelength_start=wavelength_start,
                                    range_width=range_width, max_fibres=1,
                                    include_stars=[row['Name']])

        value = user_input.input_integer(f'{index} Rate the quality of fit from 0 (worst) to 5 (best):',
                                         min_value=0, max_value=5)

        df.at[index, 'Quality of fit'] = value

    # Save vetted data to .csv
    output_csv_path = vetted_velocities_relative_path(candidates_path)

    if os.path.exists(output_csv_path):
        os.remove(output_csv_path)

    df.to_csv(output_csv_path)

    return output_csv_path


def mean_velocity_from_vetted(vetted_path):
    """
    Calculate mean radial velocity and its uncertainty from the .csv given by `vetted_path`.

    In order to calculate mean velocity it picks top 3 velovity calculations having highest `Quality of fit` value. Rows with zero quality of fit are dicarded.

    If there one or zero velocity calculations with non-zero quality of fit, the mean velocity is not calculated and function returns None value.

    Parameters
    ----------
    vetted_path : str
        Path to a .csv file containing vetted radial velocities.

    Returns
    -------
    str
        Path to a .csv file containing mean radial velocity and its uncertainty.
        Returns None if there are one or zero velocity calculations with non-zero quality of fit
    """

    # Remove output CSV
    # -------------

    output_csv_path = mean_velocity_relative_path(vetted_path)

    if os.path.exists(output_csv_path):
        os.remove(output_csv_path)

    # Read vetted CSV
    # ------------

    df = pd.read_csv(vetted_path)

    # Exclude velocity calculations with zero quality of fit.
    df = df.loc[df['Quality of fit'] > 0]

    # Keep 3 rows with highest quqality of fit
    df = df.sort_values(by=['Quality of fit', 'VERR'], ascending=[False, True])
    df = df.head(3)

    # Not enough quality velocity calculations to estimate mean velocity
    if len(df) < 2:
        return None

    # Calculate mean velocity
    mean_velocity = df['VREL'].mean()

    # Calculate standard error of the mean velocity:
    #   Error = [Sample standard deviation] / SQRT(Sample size)
    uncertainty_of_velocity_mean = df['VREL'].sem()

    # Save to .csv
    # -----------

    df_mean = pd.DataFrame({
        'Name': [df['Name'].values[0]]
    })

    df_mean['RA'] = df['RA'].values[0]
    df_mean['Dec'] = df['Dec'].values[0]
    df_mean['Mag'] = df['Mag'].values[0]
    df_mean['Ccd'] = df['Ccd'].values[0]
    df_mean['Day'] = df['Day'].values[0]
    df_mean['Take'] = df['Take'].values[0]
    df_mean[MEAN_RADIAL_VELOCITY_COLUMN_NAME] = mean_velocity
    df_mean[UNCERTAINTY_OF_MEAN_RADIAL_VELOCITY_COLUMN_NAME] = uncertainty_of_velocity_mean

    # Use 'Name' as index to remove the default index column containing 0, 1, 2 ...
    df_mean = df_mean.set_index('Name')

    df_mean.to_csv(output_csv_path)

    return output_csv_path


def calculate_mean_velocities(names, day, take, measurements=10, range_width=35, max_fibres=0,
                              show_log=None):
    """
    Calculates mean radial velocities and saves them in .csv files for each star and ccd.

    Parameters
    ----------
    names : str
        Name of stars, i.e. ['AGBSYAZ017762']. Calculates all stars if None.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    measurements: int
        The number of velocity measurements to make.
    range_width: int
        The width of the window of measurement in Angstroms.
    max_fibres: int
        Max number of fibers (stars) to process when `names` parameter is None. 0 for all fibers (default).
    show_log: function
        A function that is called for each run of fxcor.
        It can be used to show progress to the user.
        The function is called with single str parameter.
        If None, the function is ignored (Default)

    Returns
    -------
    list of str
        List of paths to the .csv files with mean radial velocities.
    """

    candidate_paths = store_candidate_radial_velocities(names=names, day=day, take=take,
                                                        measurements=measurements,
                                                        range_width=range_width,
                                                        max_fibres=max_fibres,
                                                        show_log=show_log)
    output_paths = []

    for candidate_path in candidate_paths:
        vetted_path = visual_velocity_check(candidate_path)
        output_path = mean_velocity_from_vetted(vetted_path)
        output_paths.append(output_path)

    return output_paths
