from .mean_fibre import calculate_mean
from ..shared import fixture_paths
from pytest import approx


def test_show_sky_levels():
    input_path = fixture_paths.combined_fits_fixture_path(ccd=1)
    result = calculate_mean(fits_path=input_path, object_starts_with="SKY")

    assert result.loc[29, "Name"] == 'SKY009'
    assert result.loc[29, "Mean"] == approx(50.25067)

    assert result.loc[105, "Name"] == 'SKY016'
    assert result.loc[105, "Mean"] == approx(-6.126471)
