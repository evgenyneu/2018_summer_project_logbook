from ..is_testing import IS_TESTING
import os

# An array containing the channels present in the observation data
CCDs = [1, 2, 3]

# The wavelength in Angstrom of Hermes bands. The keys are CCD numbers
HERMES_BANDS = {
    1: {'start': 4715, 'end': 4900},
    2: {'start': 5649, 'end': 5873},
    3: {'start': 6478, 'end': 6737},
    4: {'start': 7585, 'end': 7887}
}

# Root directory of the project (the parent of the logbook)
ROOT_DIR = os.path.abspath(os.curdir)

if IS_TESTING:
    ROOT_DIR = os.path.dirname(ROOT_DIR)

# Path to the export glow utility
EXPORT_GLOW_PATH = f"{ROOT_DIR}/logbook/code/fortran/export_glow/export_glow"
