from .tram import tram_positions, fibres_with_positions, fibres_to_csv, fibres_to_csv_ccd
from ..shared import cleanup, fixture_paths, files
from .paths import ccd_day_take_dir
from pytest import approx
import pandas as pd


def test_fibre_positions():
    tramline_dir = ccd_day_take_dir(ccd=1, day=27, take="p1")
    output_path = f"{tramline_dir}/27oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, output_path)

    result = tram_positions(output_path)
    assert result[10, 2000] == approx(126.74247)

    cleanup.remove_test_fits()


def test_fibres_with_positions():
    tramline_dir = ccd_day_take_dir(ccd=1, day=27, take="p1")
    tram_path = f"{tramline_dir}/27oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, tram_path)

    # Copy the raw image
    path_data = f"{tramline_dir}/rawdata.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    df = fibres_with_positions(data_path=path_data, tram_path=tram_path)

    assert len(df) == 400
    assert df.loc[31, 'y'] == approx(319.3858642578125)

    cleanup.remove_test_fits()


def test_fibres_to_csv():
    cleanup.remove_test_fits()

    tramline_dir = ccd_day_take_dir(ccd=1, day=27, take="p1")
    tram_path = f"{tramline_dir}/27oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, tram_path)

    # Copy the raw image
    path_data = f"{tramline_dir}/rawdata.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    csv_path = f"{tramline_dir}/27oct_fibres.csv"

    fibres_to_csv(data_path=path_data, tram_path=tram_path, csv_path=csv_path)

    # Check the CSV file
    # --------------------

    df = pd.read_csv(csv_path)

    assert len(df) == 400

    # Check index column
    # --------------------

    assert df.columns[0] == "Fib"
    df.set_index('Fib', inplace=True, verify_integrity=True)
    assert df.loc[31, 'y'] == approx(319.3858642578125)

    cleanup.remove_test_fits()


def test_fibres_to_csv_ccd():
    cleanup.remove_test_fits()

    tramline_dir = ccd_day_take_dir(ccd=1, day=27, take="p1")
    tram_path = f"{tramline_dir}/27oct10022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, tram_path)

    # Copy the raw image
    path_data = f"{tramline_dir}/27oct10020.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    csv_path = f"{tramline_dir}/27oct_fibres.csv"

    fibres_to_csv_ccd(ccd=1, day=27, take="p1", frame=20, csv_path=csv_path)

    # Check the CSV file
    # --------------------

    df = pd.read_csv(csv_path)

    assert len(df) == 400

    # Check index column
    # --------------------

    assert df.columns[0] == "Fib"
    df.set_index('Fib', inplace=True, verify_integrity=True)
    assert df.loc[31, 'y'] == approx(319.3858642578125)

    cleanup.remove_test_fits()