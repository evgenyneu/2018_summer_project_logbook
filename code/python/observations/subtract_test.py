from .paths import ccd_day_take_dir
from ..shared import fixture_paths, files
from ..shared import cleanup
from .subtract import subtract
import os
import pytest
from astropy.io import fits


@pytest.mark.slow
def test_export_glow():
    # Prepare the input .fits files
    pathdir = ccd_day_take_dir(ccd=1, day=29, take="p0")
    path_data1 = f"{pathdir}/rawdata1.fits"
    path_data2 = f"{pathdir}/rawdata2.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data1)
    files.copy_file_rewrite(input_path, path_data2)

    output_path = f"{pathdir}/subtracted.fits"

    if os.path.exists(output_path):
        os.remove(output_path)

    subtract(first_path=path_data1, second_path=path_data2, output_path=output_path)

    # Verify the subtracted fits file
    hdul = fits.open(output_path)
    data = hdul["PRIMARY"].data

    assert data.shape[0] == 4112
    assert data.shape[1] == 4146
    assert data[128, 312] == 0
    assert os.path.exists(output_path) is True

    cleanup.remove_test_fits()