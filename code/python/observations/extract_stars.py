from pyraf import iraf
from iraf import onedspec
import os
from .paths import fits_path_one_ccd, star_unprocessed_path
from .fibres import fibre_by_star_name
from .constants import CCDs


def extract_fibre(input_path, output_path, fibre_number):
    """
    Extracts a single fibre from the combined .fits file.

    Parameters
    ----------
    input_path : str
        Path to the combined .fits file
    output_path : str
        Path to the .fits file of the individual fits file
    fibre_number: int
        The fibre number.
    """

    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if os.path.exists(output_path):
        os.remove(output_path)

    onedspec.scopy(input=f"{input_path}[0]", output=output_path, aperture=f"{fibre_number}")


def extract_star(name, ccd, day, take):
    """
    Extracts a single star for a given ccd into a .fits file.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD identifier, i.e. 1, 2, 3.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    str
        Path to the output .fits file.
    """

    input_path = fits_path_one_ccd(ccd=ccd, day=day, take=take)
    output_path = star_unprocessed_path(name=name, ccd=ccd, day=day, take=take)
    fibre_number = fibre_by_star_name(name=name, ccd=ccd, day=day, take=take)
    extract_fibre(input_path=input_path, output_path=output_path, fibre_number=fibre_number)
    return output_path


def extract_star_all_ccds(name, day, take):
    """
    Extracts a single star for a ccds into a .fits file.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.

    Returns
    -------
    list of str
        List of paths to the output .fits files.
    """

    paths = []

    for ccd_number in CCDs:
        paths.append(extract_star(name=name, ccd=ccd_number, day=day, take=take))

    return paths
