from .compare_boxplots import compare_boxplots
from ..shared import cleanup
from ..observations.paths import day_take_dir
import os


def test_compare_boxplots():
    cleanup.remove_test_fits()

    subtitles = ["27 Oct p1", "29 Oct p0", "29 Oct p1_A"]

    image_dir = day_take_dir(day=29, take="mouse")
    image_path = os.path.join(image_dir, 'subdir', "image.png")

    groups = []

    ccd1 = [1, 2, 5, 3, 16, 2]
    ccd2 = [3, 1, -1, 4, 11, 1]
    ccd3 = [4, 2, -2, 1, -10, 3]
    groups.append([ccd1, ccd2, ccd3])

    ccd1 = [3, 1, 1, 2, 2, 8]
    ccd2 = [2, 2, -3, 2, 4, 9]
    ccd3 = [3, 1, -1, 5, 4, 4]
    groups.append([ccd1, ccd2, ccd3])

    compare_boxplots(groups=groups, subtitles=subtitles,
                     title="Mean sky levels of 2018 observation", show_plot=False,
                     save_path=image_path)

    assert os.path.exists(image_path) is True

    cleanup.remove_test_fits()