from ..observations.mean_fibre import calculate_mean
from ..observations.constants import CCDs
from ..observations.paths import fits_path_one_ccd
from .compare_boxplots import compare_boxplots, BOXPLOT_COLORS


def compare_sky_levels(paths, subtitles, title, show_plot=True, labels=None,
                       colors=BOXPLOT_COLORS, save_path=None):
    """
    Shows a box plots comparing the sky levels for fiven fits files.

    Parameters
    ----------
    paths : list of list
        A list of lists containing paths to three FITS files from CCDs 1, 2 and 3 in that order.
    subtitles : list of str
        The list of subtitles for each of the plots.
    title: str
        The main title of the plot.
    show_plot: bool
        If True the plot will be shown. Used for unit testing where we do not show the plot.
    labels: list of str
        The x-axis labels for the box plots in each graph.
        If None, the labels are CCD 1, CCD 2 and CCD 3.
    colors: list of str
        A list of colors to use for the boxplots. The number of colors needs to be the same as
        the number of paths in each element of paths array.
        If None the boxes are colored with default color.
        Color names: https://matplotlib.org/examples/color/named_colors.html
    save_path: str
        A path to image file where plot will be save. If None the plot is not saved.
    """

    groups = []

    for inner_paths in paths:
        ccd_means = []

        for fits_path in inner_paths:
            df = calculate_mean(fits_path=fits_path, object_starts_with="SKY")
            ccd_means.append(df["Mean"].values)

        groups.append(ccd_means)

    compare_boxplots(groups=groups, subtitles=subtitles, title=title, show_plot=show_plot,
                     labels=labels, colors=colors, save_path=save_path)


def compare_sky_levels_days_takes(days, take_prefixes, take_suffix, title=None, subtitles=None,
                                  show_plot=True, save_path=None):
    """
    Shows a box plots comparing the sky levels for fiven fits files.

    Parameters
    ----------
    days : list of int
        An array of observation days.
    take_prefixes : list of str
        An array of prefixed of observation takes, i.e. ["p1", "p0", "p1_A"].
    take_suffix: str
        The suffix that will be added to the take prefixes to create the output directories,
        i.e. "noglow_sub_sky"
    subtitles : list of str
        The list of subtitles for each of the plots. If None, subtitle are created automatically.
    title: str
        The main title of the plot. If None, the title is constructed automatically.
    show_plot: bool
        If True the plot will be shown. Used for unit testing where we do not show the plot.
    save_path: str
        A path to image file where plot will be save. If None the plot is not saved.
    """

    many_observations = []
    auto_subtitles = []

    for index, day in enumerate(days):
        take_prefix = take_prefixes[index]
        take = f"{take_prefix}_{take_suffix}"
        onservations = []

        for index, ccd in enumerate(CCDs):
            path = fits_path_one_ccd(ccd=ccd, day=day, take=take)
            onservations.append(path)

        many_observations.append(onservations)
        subtitle = f"{day} Oct {take_prefix}"
        auto_subtitles.append(subtitle)

    if subtitles is None:
        subtitles = auto_subtitles

    if title is None:
        title = f"Mean sky levels, {take_suffix}"

    compare_sky_levels(paths=many_observations, subtitles=subtitles, title=title,
                       show_plot=show_plot, save_path=save_path)