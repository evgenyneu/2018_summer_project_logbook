from .compare_sky_levels import compare_sky_levels, compare_sky_levels_days_takes
from ..observations.constants import CCDs
from ..shared import files, fixture_paths, cleanup, user_input, fixtures
from ..observations.paths import fits_path_one_ccd, day_take_dir
import os


def test_compare_sky_levels():
    cleanup.remove_test_fits()

    paths = []

    for observation in range(0, 3):
        ccd_paths = []

        for ccd in CCDs:
            input_path = fixture_paths.combined_fits_fixture_path(ccd=ccd)
            output_path = fits_path_one_ccd(ccd=ccd, day=29, take="mouse")
            files.copy_file_rewrite(input_path, output_path)
            ccd_paths.append(input_path)

        paths.append(ccd_paths)

    subtitles = ["27 Oct p1", "29 Oct p0", "29 Oct p1_A"]

    image_dir = day_take_dir(day=29, take="mouse")
    image_path = os.path.join(image_dir, 'subdir', "image.png")

    compare_sky_levels(paths=paths, subtitles=subtitles,
                       title="Mean sky levels of 2018 observation", show_plot=False,
                       save_path=image_path)

    assert os.path.exists(image_path) is True

    cleanup.remove_test_fits()


def test_compare_sky_levels_single_observation():
    cleanup.remove_test_fits()

    paths = []

    for observation in range(0, 1):
        ccd_paths = []

        for ccd in CCDs:
            input_path = fixture_paths.combined_fits_fixture_path(ccd=ccd)
            output_path = fits_path_one_ccd(ccd=ccd, day=29, take="mouse")
            files.copy_file_rewrite(input_path, output_path)
            ccd_paths.append(input_path)

        paths.append(ccd_paths)

    subtitles = ["27 Oct p1", "29 Oct p0", "29 Oct p1_A"]

    compare_sky_levels(paths=paths, subtitles=subtitles,
                       title="Mean sky levels of 2018 observation", show_plot=False)

    cleanup.remove_test_fits()


def test_compare_sky_levels_days_takes():
    cleanup.remove_test_fits()

    take_prefixes = ["p1", "p0", "p1_A"]
    take_suffix = "noglow_sub_sky"
    days = [27, 29, 29]

    for index, day in enumerate(days):
        take_prefix = take_prefixes[index]
        take = f"{take_prefix}_{take_suffix}"

        for ccd in CCDs:
            input_path = fixture_paths.combined_fits_fixture_path(ccd=ccd)
            output_path = fits_path_one_ccd(ccd=ccd, day=day, take=take)
            files.copy_file_rewrite(input_path, output_path)

    image_dir = day_take_dir(day=29, take="p1_A_noglow_sub_sky")
    image_path = os.path.join(image_dir, "image.png")

    compare_sky_levels_days_takes(days=[27, 29, 29], take_prefixes=take_prefixes,
                                  take_suffix=take_suffix,
                                  title="Mean sky levels of 2018 observation, glow-free sky subtraced",
                                  show_plot=False, save_path=image_path)

    assert os.path.exists(image_path) is True

    cleanup.remove_test_fits()