import matplotlib.pyplot as plt
import numpy as np
import os

# The default colors used for box plots
BOXPLOT_COLORS = ['dodgerblue', 'lightgreen', 'orangered']
DEFAULT_COLOR = 'khaki'


def compare_boxplots(groups, subtitles, title, show_plot=True, labels=None,
                     colors=BOXPLOT_COLORS, save_path=None):
    """
    Shows multiple box plots on one graph, in groups.

    Parameters
    ----------
    groups : list of list
        Each element of the list contains a list of values.
    subtitles : list of str
        The list of subtitles for each of the plots.
    title: str
        The main title of the plot.
    show_plot: bool
        If True the plot will be shown. Used for unit testing where we do not show the plot.
    labels: list of str
        The x-axis labels for the box plots in each graph.
        If None, the labels are CCD 1, CCD 2 and CCD 3.
    colors: list of str
        A list of colors to use for the boxplots. The number of colors needs to be the same as
        the number of paths in each element of paths array.
        If None the boxes are colored with default color.
        Color names: https://matplotlib.org/examples/color/named_colors.html
    save_path: str
        A path to image file where plot will be save. If None the plot is not saved.
    """

    fig, axes = plt.subplots(nrows=1, ncols=len(groups), sharey=True)

    if labels is None:
        labels = [f"CCD {ccd}" for ccd in range(1, len(groups[0]) + 1)]

    if colors is None:
        colors = [DEFAULT_COLOR] * len(groups[0])

    for plot_id, group in enumerate(groups):
        if type(axes) != np.ndarray:
            axes = [axes]

        bplot = axes[plot_id].boxplot(group, patch_artist=True, labels=labels, widths=0.7)

        for patch, color in zip(bplot['boxes'], colors):
            patch.set_facecolor(color)

        if plot_id < len(subtitles):
            axes[plot_id].set_title(subtitles[plot_id])

    fig.suptitle(title, size='xx-large', y=0.99)
    fig.subplots_adjust(wspace=0)

    if show_plot:
        plt.show()

    if save_path is not None:
        if os.path.exists(save_path):
            os.remove(save_path)

        output_dir = os.path.dirname(save_path)

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        fig.savefig(save_path)