from .user_input import input_text
from unittest.mock import patch
from pathlib import Path

def input_text_mock_response(message):
    print(message)

    if message == "Are you human? [yes, no]:":
        return "huh?"

    if message == "Enter yes, no:":
        return "No"


@patch('builtins.input')
def test_input_text(input_patch):
    input_patch.side_effect = input_text_mock_response
    result = input_text("Are you human?", options=['yes', 'no'])
    assert result == "no"