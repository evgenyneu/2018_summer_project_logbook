from .fixture_paths import individual_fits_fixture_path, combined_fits_fixture_path, tramline_fixture_path, raw_data_fixture_path
from ..observations.paths import OBSERVATIONS_DIR


def test_individual_fits_fixture_path():
    result = individual_fits_fixture_path(ccd=1)
    assert result == f"{OBSERVATIONS_DIR}/fixtures/individual_fibre_ccd1.fits"

    result = individual_fits_fixture_path(ccd=2)
    assert result == f"{OBSERVATIONS_DIR}/fixtures/individual_fibre_ccd2.fits"


def test_combined_fits_fixture_path():
    result = combined_fits_fixture_path(ccd=1)
    assert result == f"{OBSERVATIONS_DIR}/fixtures/combined_fibres_ccd1.fits"

    result = combined_fits_fixture_path(ccd=2)
    assert result == f"{OBSERVATIONS_DIR}/fixtures/combined_fibres_ccd2.fits"


def test_tramline_fixture_path():
    result = tramline_fixture_path()
    assert result == f"{OBSERVATIONS_DIR}/fixtures/tramlime_tlm.fits"


def test_raw_data_fixture_path():
    result = raw_data_fixture_path(ccd=1)
    assert result == f"{OBSERVATIONS_DIR}/fixtures/raw_ccd1.fits"