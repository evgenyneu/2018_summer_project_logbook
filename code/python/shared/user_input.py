def input_text(message, options=None):
    """
    Asks a user to enter a text

    Parameters
    ----------
    message : string
        Message to be shown to the user.
    options : string
        The possible input texts that are accepted.
    """

    if options is not None:
        options = [option.lower() for option in options]
        options_humanized = ", ".join(options)
        message = f"{message} [{options_humanized}]:"

    while True:
        response = input(message)
        response = response.lower()

        if options is None:
            return response

        if response not in options:
            options = [option.lower() for option in options]
            options_humanized = ", ".join(options)
            message = f"Enter {options_humanized}:"
            continue

        return response


def input_integer(message, min_value=-2234234, max_value=2234234):
    """
    Asks a user to enter an integer.

    Parameters
    ----------
    message : string
        Message to be shown to the user.
    min_value : int
        The minimum value of the intiger to accept.
    max_value : int
        The maximum value of the intiger to accept.
    """
    rating = True

    while rating:
        result = input(message)

        try:
            value = int(result)

            if min_value != -2234234 and value < min_value:
                message = f"Can not be smaller than {min_value}"
                continue

            if max_value != 2234234 and value > max_value:
                message = f"Can not be greater than {max_value}"
                continue

            return value

        except (ValueError, TypeError):
            continue
