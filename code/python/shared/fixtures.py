from . import files, fixture_paths
from ..observations.paths import fits_path_one_ccd
from ..observations.constants import CCDs


def copy_combined_fibres(day, take, ccd=None):
    """
    Copy the .fits files of the combined fibres to data observations directories.

    Parameters
    ----------
    day: int
        The day of the observation.
    take: str
        The directory containing observation files.
    ccd: int
        The CCD number to copy. Copy files for all CCDs if None (Default)

    Returns
    -------
    list of str
        Array of paths to the output files.
    """

    paths = []

    for ccd_number in CCDs:
        if ccd is not None and ccd != ccd_number:
            continue

        output_path = fits_path_one_ccd(ccd=ccd_number, day=day, take=take)
        input_path = fixture_paths.combined_fits_fixture_path(ccd=ccd_number)
        files.copy_file_rewrite(input_path, output_path)
        paths.append(output_path)

    return paths