from .fixtures import copy_combined_fibres
from .cleanup import remove_test_fits
from ..observations.paths import fits_path_one_ccd
import os


def test_copy_combined_fibres():
    output_path = fits_path_one_ccd(ccd=1, day=29, take="p0")
    if os.path.exists(output_path):
        os.remove(output_path)

    result = copy_combined_fibres(day=29, take="p0")

    assert os.path.exists(output_path) is True
    assert output_path in result

    remove_test_fits()
