import shutil
import os


def remove_test_fits():
    """
    Deletes all temporary .fits files we use for testing.
    """

    temp_dirs = ["27Oct18", "29Oct18", "30Oct18", "reduced_spectra"]

    for temp_dir in temp_dirs:
        path_to_remove = f"code/python/test_data/{temp_dir}"

        if os.path.exists(path_to_remove):
            shutil.rmtree(path_to_remove)