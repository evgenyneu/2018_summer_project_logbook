from ..observations.paths import OBSERVATIONS_DIR


def individual_fits_fixture_path(ccd):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.
    Returns
    -------
    str
        Path to a .fits file of infividual fibre.
    """

    return f"{OBSERVATIONS_DIR}/fixtures/individual_fibre_ccd{ccd}.fits"


def combined_fits_fixture_path(ccd):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.

    Returns
    -------
    str
        Path to a .fits file comtaining multiple fibes
    """

    return f"{OBSERVATIONS_DIR}/fixtures/combined_fibres_ccd{ccd}.fits"


def tramline_fixture_path():
    """
    Returns
    -------
    str
        Path to a .fits file comtaining tramlines.
    """

    return f"{OBSERVATIONS_DIR}/fixtures/tramlime_tlm.fits"


def raw_data_fixture_path(ccd):
    """
    Parameters
    ----------
    ccd : int
        The CCD number.

    Returns
    -------
    str
        Path to a .fits file comtaining the raw data.
    """

    return f"{OBSERVATIONS_DIR}/fixtures/raw_ccd{ccd}.fits"