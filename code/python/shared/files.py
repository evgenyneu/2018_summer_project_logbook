from shutil import copyfile
import os


def copy_file_rewrite(input_path, output_path):
    """
    Copies the input file:
        - Creating its parent directory if it does not exist
        - Rewrites existing file if necessary.

    Parameters
    ----------
    input_path : str
        Path to the source file.
    output_path : str
        Path to the destination file.
    """
    if os.path.exists(output_path):
        os.remove(output_path)

    copy_file(input_path=input_path, output_path=output_path)


def copy_file(input_path, output_path):
    """
    Copies the input file:
        - Fails if the destination file already exists.
        - Creates its parent directory if it does not exist

    Parameters
    ----------
    input_path : str
        Path to the source file.
    output_path : str
        Path to the destination file.
    """

    if os.path.exists(output_path):
        raise FileExistsError(f"File already exists {output_path}")

    output_dir = os.path.dirname(output_path)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    copyfile(input_path, output_path)
