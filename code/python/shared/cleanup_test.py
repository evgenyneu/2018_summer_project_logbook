from .cleanup import remove_test_fits
from ..observations.paths import fits_path_one_ccd
from ..shared import files, fixture_paths
import os


def test_remove_test_fits():
    output_path = fits_path_one_ccd(ccd=1, day=29, take="p0")
    input_path = fixture_paths.individual_fits_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, output_path)

    assert os.path.exists(output_path) is True

    remove_test_fits()

    assert os.path.exists(output_path) is False
