from .reduce_data import start_reduction, copy_reduced, copy_reduced_all_ccds, create_description, reduce_single_star
from ..shared import files, fixture_paths, cleanup, user_input, fixtures
from ..observations.paths import combined_observations_fits_from_2dfdr, star_normalized_path, reduced_fits_path, reduced_description_path, day_take_dir
from ..observations.constants import CCDs
from unittest.mock import patch
from ..observations import radial_velocities
import pytest
import os
from freezegun import freeze_time
import getpass


@freeze_time("2022-11-03 12:32:01")
def test_copy_reduced():
    output_path = reduced_fits_path(name="AGBSYAZ017762", ccd=1)

    # Prepare the input .fits file for the star
    output_path_unprocessed = star_normalized_path(name="AGBSYAZ017762", ccd=1, day=30, take="skrewt")
    input_path = fixture_paths.individual_fits_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, output_path_unprocessed)

    result = copy_reduced(name="AGBSYAZ017762", ccd=1, day=30, take="skrewt")

    assert result == output_path
    assert os.path.exists(output_path)

    # Cleanup
    cleanup.remove_test_fits()


@freeze_time("2022-11-03 12:32:01")
def test_copy_reduced_all_ccds():
    for ccd_number in CCDs:
        output_path_unprocessed = star_normalized_path(name="AGBSYAZ017762", ccd=ccd_number, day=30, take="skrewt")
        input_path = fixture_paths.individual_fits_fixture_path(ccd=ccd_number)
        files.copy_file_rewrite(input_path, output_path_unprocessed)

    output_path1 = reduced_fits_path(name="AGBSYAZ017762", ccd=1)
    if os.path.exists(output_path1):
        os.remove(output_path1)

    output_path2 = reduced_fits_path(name="AGBSYAZ017762", ccd=2)
    if os.path.exists(output_path2):
        os.remove(output_path2)

    output_path3 = reduced_fits_path(name="AGBSYAZ017762", ccd=3)
    if os.path.exists(output_path3):
        os.remove(output_path3)

    result = copy_reduced_all_ccds(name="AGBSYAZ017762", day=30, take="skrewt")

    assert len(result) == 3

    assert output_path1 in result
    assert os.path.exists(output_path1)

    assert output_path2 in result
    assert os.path.exists(output_path2)

    assert output_path3 in result
    assert os.path.exists(output_path3)

    cleanup.remove_test_fits()


@freeze_time("2022-11-03 12:32:01")
def test_create_description():
    output_path = reduced_description_path(name="AGBSYAZ017762")
    if os.path.exists(output_path):
        os.remove(output_path)

    result = create_description(name="AGBSYAZ017762", description="thank you very much")

    assert result == output_path
    assert os.path.exists(output_path)

    with open(output_path, 'r') as myfile:
        text = myfile.read()
        user = getpass.getuser()
        assert text == f"AGBSYAZ017762\n{user}\nthank you very much\n"

    cleanup.remove_test_fits()


@pytest.mark.slow
@freeze_time("2022-11-03 12:32:01")
@patch('builtins.print')
@patch.object(radial_velocities, 'star_velocities_interactive')
@patch.object(user_input, 'input_integer', return_value=5)
@patch.object(user_input, 'input_text', return_value='done')
def test_reduce(input_text_patch, input_integer_patch, star_velocities_patch, patch_print):
    path_normalized = star_normalized_path(name='AGBSYAZ017762', ccd=1, day=30, take="flob")

    if os.path.exists(path_normalized):
        os.remove(path_normalized)

    output_path1 = reduced_fits_path(name="AGBSYAZ017762", ccd=1)
    if os.path.exists(output_path1):
        os.remove(output_path1)

    output_path2 = reduced_fits_path(name="AGBSYAZ017762", ccd=2)
    if os.path.exists(output_path2):
        os.remove(output_path2)

    output_path3 = reduced_fits_path(name="AGBSYAZ017762", ccd=3)
    if os.path.exists(output_path3):
        os.remove(output_path3)

    output_description = reduced_description_path(name="AGBSYAZ017762")
    if os.path.exists(output_description):
        os.remove(output_description)

    # Combine observations
    # ----------------------

    # Observations that will be combined
    observations = [
            {"day": 27, "take": "p1"},
            {"day": 29, "take": "p0"},
            {"day": 29, "take": "p1_A"}
          ]

    # Prepare the input files
    # ----------------------

    temporary_files = []

    for data in observations:
        day = data["day"]
        take = data["take"]
        output_paths = fixtures.copy_combined_fibres(day=day, take=take)
        temporary_files = temporary_files + output_paths

    for ccd in CCDs:
        input_path = fixture_paths.combined_fits_fixture_path(ccd=ccd)
        output_path = combined_observations_fits_from_2dfdr(ccd=ccd, day=30, take="flob")
        files.copy_file_rewrite(input_path, output_path)
        temporary_files.append(output_path)

    start_reduction(observations=observations, name='AGBSYAZ017762', velocity_measurements=2,
                    interactive_normalization="no", day=30, take="flob",
                    description="Blast-Ended Skrewts", show_plot=False)

    assert os.path.exists(path_normalized)
    assert os.path.exists(output_path1)
    assert os.path.exists(output_path2)
    assert os.path.exists(output_path3)
    assert os.path.exists(output_description)

    # Check the sky level images
    # ------------

    image_dir = day_take_dir(day=30, take="flob")
    image_path = os.path.join(image_dir, "sky_levels.png")
    assert os.path.exists(image_path)

    # Cleanup
    cleanup.remove_test_fits()


def text_input_mock(message, options):
    print(message)

    if message == 'Do you want to continue?':
        return "yes"

    return "done"


@pytest.mark.slow
@freeze_time("2022-11-03 12:32:01")
@patch('builtins.print')
@patch.object(radial_velocities, 'star_velocities_interactive')
@patch.object(user_input, 'input_integer', return_value=5)
@patch.object(user_input, 'input_text', return_value='done')
def test_reduce_single_start(input_text_patch, input_integer_patch, star_velocities_patch, patch_print):
    cleanup.remove_test_fits()

    input_text_patch.side_effect = text_input_mock

    path_normalized = star_normalized_path(name='AGBSYAZ017762', ccd=1, day=30, take="blast")

    if os.path.exists(path_normalized):
        os.remove(path_normalized)

    output_path1 = reduced_fits_path(name="AGBSYAZ017762", ccd=1)
    if os.path.exists(output_path1):
        os.remove(output_path1)

    output_path2 = reduced_fits_path(name="AGBSYAZ017762", ccd=2)
    if os.path.exists(output_path2):
        os.remove(output_path2)

    output_path3 = reduced_fits_path(name="AGBSYAZ017762", ccd=3)
    if os.path.exists(output_path3):
        os.remove(output_path3)

    output_description = reduced_description_path(name="AGBSYAZ017762")
    if os.path.exists(output_description):
        os.remove(output_description)

    # Combine observations
    # ----------------------

    # Observations that will be combined
    observations = [
            {"day": 27, "take": "p1_flob"},
            {"day": 29, "take": "p0_flob"},
            {"day": 29, "take": "p1_A_flob"}
          ]

    # Prepare the input files
    # ----------------------

    for data in observations:
        day = data["day"]
        take = data["take"]
        fixtures.copy_combined_fibres(day=day, take=take)

    for ccd in CCDs:
        input_path = fixture_paths.combined_fits_fixture_path(ccd=ccd)
        output_path = combined_observations_fits_from_2dfdr(ccd=ccd, day=30, take="blast")
        files.copy_file_rewrite(input_path, output_path)
        print(f"input_path={input_path} combined={output_path}")

    take_prefixes = ["p1", "p0", "p1_A"]

    reduce_single_star(name="AGBSYAZ017762", days=[27, 29, 29],
                       take_prefixes=take_prefixes,
                       take_input="flob",
                       take_combined="blast",
                       description="Blast-Ended Skrewts",
                       velocity_measurements=2,
                       interactive_normalization="no",
                       show_plot=False)

    assert os.path.exists(path_normalized)
    assert os.path.exists(output_path1)
    assert os.path.exists(output_path2)
    assert os.path.exists(output_path3)
    assert os.path.exists(output_description)

    # Check the sky level images
    # ------------

    image_dir = day_take_dir(day=30, take="blast")
    image_path = os.path.join(image_dir, "sky_levels_observations.png")
    assert os.path.exists(image_path)

    image_path = os.path.join(image_dir, "sky_levels.png")
    assert os.path.exists(image_path)

    # Cleanup
    cleanup.remove_test_fits()
