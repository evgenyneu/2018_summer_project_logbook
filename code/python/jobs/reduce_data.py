from ..observations.combine_observations import copy_all_ccds, copy_to_canonical_path_all_ccds
from ..shared import user_input
import os
from pathlib import Path
from ..observations.paths import combined_observations_fits_from_2dfdr, combined_observations_fits_from_2dfdr_present, star_normalized_path_present, star_normalized_path, reduced_fits_path, reduced_description_path, day_take_dir
from ..observations.extract_stars import extract_star_all_ccds
from ..observations.radial_velocities import calculate_mean_velocities, doppler_correct_all_ccds, mean_radial_velocities_known
from ..observations.constants import CCDs
from ..observations.normalize_spectrum import normalize_all_ccds
from ..plot.compare_sky_levels import compare_sky_levels_days_takes, compare_sky_levels
from shutil import copyfile
import getpass

# Defaul reduction day
REDUCTION_DAY = 30


def show_fxcor_log(message):
    """
    The function is called before calculating velocities with fxcor
    """
    print(message)


def copy_reduced(name, ccd, day, take):
    """
    Copy a reduced .fits file.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    ccd : int
        The CCD number.
    day : int
        The day of the reduction.
    take : str
        The directory containing reduced files.
    description: str
        A text description of the reduced data to be saved along with the file.

    Returns
    -------
    str
        Path to the reduced .fits file
    """
    input_path = star_normalized_path(name=name, ccd=ccd, day=day, take=take)
    output_path = reduced_fits_path(name=name, ccd=ccd)

    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if os.path.exists(output_path):
        os.remove(output_path)

    copyfile(input_path, output_path)

    return output_path


def copy_reduced_all_ccds(name, day, take):
    """
    Copy a reduced .fits file for all CCDs.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    day : int
        The day of the reduction.
    take : str
        The directory containing reduced files.
    description: str
        A text description of the reduced data to be saved along with the file.

    Returns
    -------
    str
        Paths to the reduced .fits files.
    """

    paths = []

    for ccd_number in CCDs:
        path = copy_reduced(name=name, ccd=ccd_number, day=day, take=take)
        paths.append(path)

    return paths


def create_description(name, description):
    """
    Create a description text file to describe the reduced .fits files.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    description: str
        A text describing the reduction.

    Returns
    -------
    str
        Paths to the description file.
    """
    output_path = reduced_description_path(name=name)
    output_dir = os.path.dirname(output_path)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if os.path.exists(output_path):
        os.remove(output_path)

    with open(output_path, "w") as text_file:
        print(name, file=text_file)
        print(getpass.getuser(), file=text_file)  # Current user
        print(description, file=text_file)

    return output_path


def extract_star(name, take, day=REDUCTION_DAY, velocity_measurements=10,
                 interactive_normalization="yes", description=None):

    """
    Extracts a star from stacked FITS file and do the remaning reduction, doppler correct amd normalize.
    This function is called as the last step in the full reduction.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    day : int
        The day of the reduction.
    take : str
        The directory containing reduced files.
    velocity_measurements: int
        The number of velocity measurements to make for each CCD.
    interactive_normalization: str
        If "yes", shows the normalization window.
    description: str
        A text describing the reduction.
    """

    # -----------------------
    # Extracting .fits files for the individual star
    # -----------------------

    print(f"\nExtracting the .fits files for star '{name}':")

    paths = extract_star_all_ccds(name=name, day=day, take=take)

    for item in paths:
        print(f"    {item}")

    print("Done")

    # -----------------------
    # Calculating radial velocities
    # -----------------------

    print(f"\nEstimating radial velocities:")

    calculate_valocities = True

    if mean_radial_velocities_known(name=name, day=day, take=take):
        message = "Radial velocities has been already calculated. Do you want calculate them again?"
        result = user_input.input_text(message, options=['yes', 'no'])

        if result == 'no':
            calculate_valocities = False
            print(f"Skipping calculation and using existing velocities.")

    if calculate_valocities:
        paths = calculate_mean_velocities([name], day=day, take=take,
                                          measurements=velocity_measurements,
                                          show_log=show_fxcor_log)

        for item in paths:
            print(f"    {item}")

        print("Done")

    # -----------------------
    # Correcting spectra for doppler shifts
    # -----------------------

    print(f"\nCorrecting spectra for doppler shifts:")

    paths = doppler_correct_all_ccds(name=name, day=day, take=take)

    for item in paths:
        print(f"    {item}")

    print("Done")

    # -----------------------
    # Normalizing the spectrum
    # -----------------------

    print(f"\nNormalizing the spectrum:")

    paths = normalize_all_ccds(name=name, day=day, take=take,
                               order=3, grow=0, high_reject=0,
                               low_reject=1.2, function="spline3", niterat=10,
                               interactive=interactive_normalization)

    for item in paths:
        print(f"    {item}")

    print("Done")

    # -----------------------
    # Copy reduced files for easier access
    # -----------------------

    print(f"\nCopying reduced files:")

    paths = copy_reduced_all_ccds(name, day, take)

    for item in paths:
        print(f"    {item}")

    if description != None:
        path = create_description(name=name, description=description)
        print(f"Description: {path}")

    print("\nData reduction complete (ᵔᴥᵔ)")


def start_reduction(observations, name, day, take, velocity_measurements=10,
                    interactive_normalization="yes", description=None, show_plot=True):
    """
    Performs all data reduction steps.

    Parameters
    ----------
    observations : list of dict
        A list of dictionaries containig keys "day" and "take" that include all the observations to process.
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.
    day : int
        The day of the reduction.
    take : str
        The directory containing reduced files.
    velocity_measurements: int
        The number of velocity measurements to make for each CCD.
    interactive_normalization: str
        If "yes", shows the normalization window.
    description: str
        A text describing the reduction.
    show_plot: bool
        If True plots will be shown. Used for unit testing where we do not show the plot.
    """

    # Check if the star has already been reduced

    if star_normalized_path_present(name=name, day=day, take=take):
        norm_path = star_normalized_path(name=name, ccd=1, day=day, take=take)
        norm_dir = os.path.dirname(norm_path)
        message = f"The star '{name}' has already been reduced in '{norm_dir}'. Do you want reduce it again?"
        result = user_input.input_text(message, options=['yes', 'no'])

        if result == 'no':
            print(f"Reduction cancelled.")
            return

    # -----------------------
    # Copy .fits files for combining
    # -----------------------

    print(f"\n\nStaring data reduction for star '{name}'.")
    print("Copying fits files for combining multiple observations: ")

    paths_for_combining = []

    for data in observations:
        paths = copy_all_ccds(day=data["day"], take=data["take"],
                              day_combined=day, take_combined=take)
        for item in paths:
            print(f"    {item}")
        paths_for_combining = paths_for_combining + paths

    print("Done")

    # -----------------------
    # Use drcontrol to combined the observations for each CCD
    # -----------------------

    path_for_combining = paths_for_combining[0]
    path_for_combining = os.path.dirname(path_for_combining)
    path_for_combining = Path(path_for_combining).parent

    combine_observations = True

    if combined_observations_fits_from_2dfdr_present(day=day, take=take):
        message = "Combined observations are already present. Do you want to combine them again?"
        result = user_input.input_text(message, options=['yes', 'no'])

        if result == 'yes':
            # Remove the previously combined .fits files
            for ccd_number in CCDs:
                combined_path = combined_observations_fits_from_2dfdr(ccd=ccd_number, day=day, take=take)

                if os.path.exists(combined_path):
                    os.remove(combined_path)
        else:
            combine_observations = False
            print(f"Using existing combined observations.")

    if combine_observations:
        message = f"\nCombine the .fits files from multiple observations with 2DfDr (from menu 'Commands > Combined Reduced Runs...') for each ccd directory in '{path_for_combining}'.\nType 'done' when finished"

        result = user_input.input_text(message, options=['done', 'quit'])

        if result == 'quit':
            return

    # -----------------------
    # Copy .fits files to locations suitable for processing
    # -----------------------

    print("\nCopying the combined to new locations for processing by IRAF: ")

    paths = copy_to_canonical_path_all_ccds(day=day, take=take)

    for item in paths:
        print(f"    {item}")

    print("Done")

    # -----------------------
    # Show the sky levels for combined observations
    # -----------------------

    # Make the plot of sky level
    image_dir = day_take_dir(day=day, take=take)
    image_path = os.path.join(image_dir, "sky_levels.png")

    compare_sky_levels(paths=[paths], subtitles=[], title=f"Mean sky levels stacked, {take}",
                       show_plot=show_plot, save_path=image_path)

    extract_star(name=name, day=day, take=take, velocity_measurements=velocity_measurements,
                 interactive_normalization=interactive_normalization, description=description)


def reduce_single_star(name, days, take_prefixes, take_input, take_combined,
                       description, velocity_measurements=10,
                       interactive_normalization="yes", show_plot=True):
    """
    Run the full reduction for a single star.

    Parameters
    ----------
    name : str
        Name of a star, i.e. 'AGBSYAZ017762'.

    days : list of int
        An array of observation days.
    take_prefixes : list of str
        An array of prefixed of observation takes, i.e. ["p1", "p0", "p1_A"].
    take_input str
        The suffix that will be added to the take prefixes to make the directories
        for the input files.
        i.e. "noglow_sub_sky"
    take_combined: str
        The directory containing combined observation files.
    velocity_measurements: int
        The number of velocity measurements to make for each CCD.
    interactive_normalization: str
        If "yes", shows the normalization window.
    show_plot: bool
        If True plots will be shown. Used for unit testing where we do not show the plot.
    """

    observations = []

    for index, day in enumerate(days):
        take_prefix = take_prefixes[index]
        observation = {"day": day, "take": f"{take_prefix}_{take_input}"}
        observations.append(observation)

    # Make the plot of sky level
    image_dir = day_take_dir(day=REDUCTION_DAY, take=take_combined)
    image_path = os.path.join(image_dir, "sky_levels_observations.png")

    compare_sky_levels_days_takes(days=days, take_prefixes=take_prefixes,
                                  take_suffix=take_input,
                                  show_plot=show_plot, save_path=image_path)

    result = user_input.input_text("Do you want to continue?", options=['yes', 'no'])

    if result == 'no':
        print(f"Reduction cancelled.")
        return

    start_reduction(observations=observations, name=name, day=REDUCTION_DAY, take=take_combined,
                    description=description,
                    velocity_measurements=velocity_measurements,
                    interactive_normalization=interactive_normalization,
                    show_plot=show_plot)
