from .remove_glow_job import remove_glow_job, remove_glow_one_observation_job
from ..observations.paths import ccd_day_take_dir
from ..shared import fixture_paths, files
from ..shared import cleanup
import pytest
import os


@pytest.mark.slow
def test_remove_glow_job():
    cleanup.remove_test_fits()

    # Prepare the input .fits files

    # Copy the tramline file
    pathdir = ccd_day_take_dir(ccd=2, day=27, take="p3")
    path_tramlime = f"{pathdir}/27oct20022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, path_tramlime)

    # Copy the raw image
    path_data = f"{pathdir}/27oct20016.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/27oct20017.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/27oct20021.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/27oct20022.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/fish.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/human.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    observation = {
        "ccd": 2,
        "day": 27,
        "take": "p3",
        "take_output": "glow_removed",
        "frames": [16, 17],
        "copy_frames": [21, 22],
        "copy_files": ["fish.fits", "human.fits"]
    }

    remove_glow_one_observation_job(observation=observation, silent=True)

    output_path = ccd_day_take_dir(ccd=2, day=27, take="glow_removed")

    expect_path = f"{output_path}/27oct20016.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20016_glow.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20017.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20017_glow.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20021.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20022.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/fish.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/human.fits"
    assert os.path.exists(expect_path) is True

    cleanup.remove_test_fits()


@pytest.mark.slow
def test_remove_glow_job():
    cleanup.remove_test_fits()

    # Prepare the input .fits files

    # Copy the tramline file
    pathdir = ccd_day_take_dir(ccd=2, day=27, take="p3")
    path_tramlime = f"{pathdir}/27oct20022tlm.fits"
    input_path = fixture_paths.tramline_fixture_path()
    files.copy_file_rewrite(input_path, path_tramlime)

    # Copy the raw image
    path_data = f"{pathdir}/27oct20016.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/27oct20017.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/27oct20021.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/27oct20022.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/fish.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    path_data = f"{pathdir}/human.fits"
    input_path = fixture_paths.raw_data_fixture_path(ccd=1)
    files.copy_file_rewrite(input_path, path_data)

    observation = {
        "ccd": 2,
        "day": 27,
        "take": "p3",
        "take_output": "glow_removed",
        "frames": [16, 17],
        "copy_frames": [21, 22],
        "copy_files": ["fish.fits", "human.fits"]
    }

    remove_glow_job(observations=[observation], silent=True)

    output_path = ccd_day_take_dir(ccd=2, day=27, take="glow_removed")

    expect_path = f"{output_path}/27oct20016.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20016_glow.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20017.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20017_glow.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20021.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/27oct20022.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/fish.fits"
    assert os.path.exists(expect_path) is True

    expect_path = f"{output_path}/human.fits"
    assert os.path.exists(expect_path) is True

    cleanup.remove_test_fits()