from ..observations.glow.remove_glow import remove_glow_many_frames
from ..observations.fits_file import fits_copy, fits_copy_file
from ..observations.constants import CCDs


def remove_glow_one_observation_job(observation, silent=False):
    """
    Removes glow from many frames in a single observation and
    saves the result in a different directory for further reduction.

    Parameters
    ----------
    observations : dict
        A dictionary of the following form:
            {
                "ccd": 2,
                "day": 27,
                "take": "p3",
                "take_output": "glow_removed",
                "frames": [16, 17],
                "copy_frames": [21, 22],
                "copy_files": ["one.fits", "two.fits"]
            }

        where
            frames: frames for which the glow will be removed

            copy_frames: frames that will be copied to output

            take_output: str
                The output directory where the glow-free and copied files will be placed.

            copy_files: files to copy to destination.

    silent: bool
        If true, do not show any non-error messages.
    """

    ccd = observation["ccd"]
    day = observation["day"]
    take = observation["take"]
    take_output = observation["take_output"]
    frames = observation["frames"]

    if not silent:
        print(f"-----------------")
        print(f"Observation {day} ccd{ccd} {take}")

    remove_glow_many_frames(ccd=ccd, day=day, take=take, frames=frames, take_output=take_output,
                            silent=silent)

    # Copy files
    # -----------

    copy_frames = observation["copy_frames"]

    for frame in copy_frames:
        if not silent:
            print(f"Copying frame {frame}")

        fits_copy(ccd=ccd, day=day, frame=frame, take=take, take_output=take_output)

    copy_files = observation["copy_files"]

    for file in copy_files:
        if not silent:
            print(f"Copying file {file}")

        fits_copy_file(ccd=ccd, day=day, take=take, filename=file, take_output=take_output)


def remove_glow_job(observations, silent=False):
    """
    Removes glow from many frames in the selected observations and
    saves the result in a different directory for further reduction.

    Parameters
    ----------
    observations : list of dict
        A list of dictionaries containg observations for glow removal.
        Each edictionary is of the following form:
            {
                "ccd": 2,
                "day": 27,
                "take": "p3",
                "take_output": "glow_removed",
                "frames": [16, 17],
                "copy_frames": [21, 22],
                "copy_files": ["one.fits", "two.fits"]
            }

        where
            frames:  frames for which the glow will be removed

            copy_frames:  frames that will be copied to output

            take_output: str
                The output directory where the glow-free and copied files will be placed.

            copy_files: files to copy to destination.

    silent: bool
        If true, do not show any non-error messages.
    """

    for observation in observations:
        remove_glow_one_observation_job(observation=observation, silent=silent)


def remove_glow(day, take, science_frames, flat_arc_frames, exclude_ccds=[], take_output_suffux="glow_free"):
    """
    Remove glow for all CCDs in the given observation.

    Parameters
    ----------
    day : int
        The day of the observation.
    take : str
        The directory containing observation files.
    science_frames : list of int
        The array of numbers corresponding to science frames.
    flat_arc_frames : list of int
        The array of numbers corresponding to flat and arc frames
    take_output_suffux : str
        A suffix that will be added to the take string to make the name of the output directory.
    exclude_ccds: list of int
        CCDs for which we do not subtract the glow but just copy the files to destination
        directory.
    """

    observations = []

    for ccd in CCDs:
        settings = {
            "ccd": ccd,
            "day": day,
            "take": take,
            "take_output": f"{take}_{take_output_suffux}",
            "frames": science_frames,
            "copy_frames": flat_arc_frames,
            "copy_files": []
        }

        if ccd in exclude_ccds:
            print(f"Keeping glow in CCD{ccd}")
            # Do not subtract glow, just copy the files
            settings["frames"] = []
            settings["copy_frames"] = science_frames + flat_arc_frames
            settings["copy_files"] = ["BIAScombined.fits"]

        observations.append(settings)

    remove_glow_job(observations=observations)
