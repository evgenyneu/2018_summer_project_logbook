import pandas as pd
import numpy as np

def parse_output(file_path):
    """
    Parameters
    ----------
    file_path : type
        Path to the fxcor output file.

    Returns
    -------
    pandas.DataFrame
        Table containing the fxcor output. The index is the fibre number 'Fib'.
    """
    header = ["N OBJECT", "IMAGE", "REF", "HJD", "AP", "CODES", "SHIFT", "HGHT", "FWHM", "TDR",
        "VOBS", "VREL", "VHELIO", "VERR"]

    df = pd.pandas.read_fwf(file_path, comment="#", header=None, names=header)
    df = df.replace(to_replace=["INDEF", "INDE"],value=np.nan) # Reaplce INDEF value with NaN
    df['AP'] = df['AP'].astype(int)
    df['VREL'] = pd.to_numeric(df['VREL'])
    df['VERR'] = pd.to_numeric(df['VERR'])
    df.rename(columns={'AP': 'Fib'}, inplace=True)
    df.set_index('Fib', inplace=True, verify_integrity=True) # Use fibre numbers as indexes
    return df