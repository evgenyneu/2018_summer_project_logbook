from .fxcor import parse_output
import os
from pytest import approx

def test_parse_output():
    path_to_file = os.path.realpath(__file__)
    dirname = os.path.dirname(path_to_file)
    file_path = os.path.join(dirname, "fxcor_output.txt")

    df = parse_output(file_path)

    assert df.loc[13, 'VREL'] == -28.0318
    assert df.loc[13, 'VERR'] == approx(0.116)
