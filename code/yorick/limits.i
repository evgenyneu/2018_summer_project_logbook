// Dealing with plot limits

// Sets the limits of the plot such that there is a margin
// between the plot and the frame.
// The amount of margin is specified by marginMultiplier,
// a value from 0 (no margin) to 1 (same margin as the range of values).
func smpAddPlotMargin(marginMultiplier)
{
  limits;
  lims = limits();
  xSpan = abs(lims(2) - lims(1))
  x2 = lims(2) + xSpan * marginMultiplier
  x1 = lims(1) - xSpan * marginMultiplier

  ySpan = abs(lims(3) - lims(4))
  y1=lims(3) - ySpan * marginMultiplier
  y2=lims(4) + ySpan * marginMultiplier
  limits, x1,x2,y1,y2
}