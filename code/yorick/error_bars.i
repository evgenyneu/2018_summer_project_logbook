// Show error bars

func smpErrorBarsFixedTopLeft(dx=, dy=, color=, width=, marginX=, marginY=)
{
  if (marginX == nil) marginX = 0.1
  if (marginY == nil) marginY = 0.1

  smpErrorBars, dx=dx, dy=dy, color=color, width=width, marginX=marginX, marginY=marginY
}

func smpErrorBarsFixedTopRight(dx=, dy=, color=, width=, marginX=, marginY=)
{
  if (marginX == nil) marginX = 0.9
  if (marginY == nil) marginY = 0.1

  smpErrorBars, dx=dx, dy=dy, color=color, width=width, marginX=marginX, marginY=marginY
}

/*
  marginX, marginY is a number between 0 and 1 specifying the margin between
  the error bars and the plot's border.  0 means no margin, 1 means the maximum margin. marginX and marginY are left and top margins.
*/
func smpErrorBars(dx=, dy=, color=, width=, marginX=, marginY=)
{
  lims = limits();
  write, lims(1), lims(2), lims(3), lims(4)
  xSpan = lims(2) - lims(1)
  ySpan = lims(4) - lims(3)

  errorBarX = lims(1) + marginX * xSpan
  errorBarY = lims(4) - marginY * ySpan

  pleb, [errorBarY], [errorBarX], dx=dx, dy=dy, color=color, width=width
}