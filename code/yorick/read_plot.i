// Reading the MONSTAR plot file and converting it to Yorick format

smpPlotDir = "data/monstar_plots/"

// Reads an compressed Monstar plot file `plotCompressedFilePath` ("ev.xxx.zip"),
// and returns a Yorick object for plotting
func smpOpenPlotFromMonstarArchive(plotCompressedFilePath)
{
  plotFilePath = smpCreatePlotFromMonstarArchive(plotCompressedFilePath)
  ev=openb(plotFilePath)
  return ev
}

// Reads an compressed Monstar plot file `plotCompressedFilePath` ("ev.xxx.zip"),
// creates a yorick plot file (`tmp/ev.ypl`) and returns the path to it.
func smpCreatePlotFromMonstarArchive(plotCompressedFilePath)
{
  xxxPlotFilePath = smpExtractPlotArchive(plotCompressedFilePath)
  plotFilePath = "tmp/ev.ypl"
  smpReadMonstarPlot, xxxPlotFilePath, plotFilePath
  return plotFilePath
}

// Extracts the Monstar plot file `plotCompressedFilePath` ("ev.xxx.zip")
// Returns the path to the plot file ev.xxx.
func smpExtractPlotArchive(plotCompressedFilePath)
{
  path = smpPlotDir + plotCompressedFilePath;
  system,"mkdir -p tmp"
  extractCommand = "unzip -o -q -d tmp \'" + path + "\'"
  system, extractCommand
  return "tmp/ev.xxx"
}

/*
  Reads and opens ev.xxx file for plotting.

  Parameters:

    dir: the directory where ev.xxx is located. Leave empty for current dir.
*/
func smpOpenPlotFromEvXXX(dir=)
{
  if (is_void(dir)) { dir = ""; }
  smpReadMonstarPlotEvXXX, dir=dir
  ev=openb(dir + "ev.ypl")
  return ev
}

// Reads monstar plot ev.xxx file and writes the
// plotting file to `ev.ypl`
func smpReadMonstarPlotEvXXX(dir=)
{
  if (is_void(dir)) { dir = ""; }
  smpReadMonstarPlot, dir + "ev.xxx", dir + "ev.ypl"
}

// Reads monstar plot XXX file from `xxxFilePath` and writes the
// plotting file to `yplFilePath`
func smpReadMonstarPlot(xxxFilePath, yplFilePath)
{
  fname=array(string,50);
  ch=1;
  line=0;
  nfiles=0;
  filesize=array(long,50);

  fname(1)=xxxFilePath
  nfiles=1
  primitive_function= i86_primitives;   /* see Yorick/std.i for choices */
  integer_type= long;   /* need int for 64 bit machines (Fortran weird) */
  integer_size= 4;      /* look up sizes in Yorick/include/prmtyp.i */
  double_size= 8;       /* 12 on Macs with some compiler switches */
  float_size= 4;
  endline_bytes= 4;
  startline_bytes= 4;

  linesize=500; //Assumes each line in ev file is 668 bytes! *********Change if ev file format changes******
  nlines =0;

  for(j=1;j<nfiles+1;++j)
  {
    f = open(fname(j),"rb");
    primitive_function, f;
    filesize(j) = sizeof(f);
    nlines+=filesize(j)/linesize;
    close,f;
  }

  ndata= 81;
  data=array(double,ndata);
  alldat=array(double,ndata,nlines); //nlines from file size, saves memory - but not if file is huge.
  nmods=array(long,nlines);          //Probably can do memory allocation better here...
  ages=array(double,nlines);

  imods = 0;
  cnt =0;

  for(j=1;j<nfiles+1;++j)
  {
    f = open(fname(j),"rb");

    primitive_function, f;

    mod= 1;
    thisage= 0.0;

    address = 16; //Initial padding for Fortran file.......! May change with architecture/compiler..
                  //Found value through trial and error.

    cnt =0;

    for(line=imods+1;line<nlines+100;++line) //Read data:
    {
      cnt = cnt+1;
      address+=startline_bytes; //Each Fortran line has padding bytes...

      _read,f,address,mod;
      nmods(line)=mod;

      address+= integer_size; //Advance address for reading next variable.

      _read,f,address,thisage;
      ages(line)=thisage;

      address+= double_size;

      _read,f,address,data; //Read large array all at once (OUTPUT() in evoln).
      alldat(,line)=data;

      address+= ndata*double_size; //Advance address due to large array just read.
      address+= endline_bytes;     //Padding at end of Fortran line.

      if(address+startline_bytes >= filesize(j)) //Check for end of file.
      {
        imods = line;
        close,f;
        goto bail;
      }
    } //End read lines loop.
  bail:
  } //End files loop.

  //---------------------------------------------------------------------------------
  //Now write out all the data to a Yorick binary file (give everything names first):
  //---------------------------------------------------------------------------------

  all=array(double,83,imods);

  for(var=81;var>=1;--var)
  {
    all(var+2,)=alldat(var,1:imods);
  }

  all(1,) = nmods(1:imods);
  all(2,) = ages(1:imods);

  model=all(1,); age=all(2,); mfracConvCoreKCVTN=all(3,); Tmax=all(4,); RhoTmax=all(5,); MHEXcore=all(6,); MTmax=all(7,); convenvInner=all(8,); Intshconvout=all(9,); Intshconvin=all(10,); MHeEXcore=all(11,); R=all(12,); Dcent=all(13,);Tcent=all(14,); Pcent=all(15,);L=all(16,);LH=all(17,); LHe=all(18,); LC=all(19,); LGr=all(20,); LN=all(21,); LogTe=all(22,); centH=all(23,); centHe4=all(24,); centC12=all(25,); He4Tmax=all(26,); C12Tmax=all(27,); MHshbot=all(28,); RHshbot=all(29,); TbotHsh=all(30,); DHshbot=all(31,);RHshmid=all(32,); THshmid=all(33,); DHshmid=all(34,); MHshtop=all(35,); RHshtop=all(36,); THshtop=all(37,); DHshtop=all(38,); MHeshbot=all(39,); RHeshbot=all(40,); THeshbot=all(41,); DHeshbot=all(42,); RHeshmid=all(43,); THeshmid=all(44,); DHeshmid=all(45,); MHeshtop=all(46,); RHeshtop=all(47,); THeshtop=all(48,); DHeshtop=all(49,); TIntshzbase=all(50,);DIntshzbase=all(51,);centIshconv=all(52,); TIntshzcent=all(53,);DIntshzcent=all(54,); TIntshztop=all(55,); DIntshztop=all(56,); EpsHmax=all(57,); TEpsHmax=all(58,); DEpsmaxH=all(59,); Tconvenvbase=all(60,); M=all(61,); Mdot=all(62,); Period=all(63,);Vexp=all(64,);dt=all(65,); surfH=all(66,);surfHe3=all(67,);surfHe4=all(68,);surfC12=all(69,);surfN14=all(70,);surfO16=all(71,);surfZ=all(72,);surfCO=all(73,);centO16=all(74,);mfracConvCoreVels=all(75,);MhomogEnv=all(76,);ThomogEnv=all(77,);DhomogEnv=all(78,);Lpp=all(79,);Lcno=all(80,);mLHmax=all(81,);mLHemax=all(82,);iplace=all(83,);

  model = double(model); //To keep everything real*8

  f = createb(yplFilePath);

  //Save everything in Yorick binary format:

  save, f, fname, model, age, mfracConvCoreKCVTN,Tmax,RhoTmax,MHEXcore,MTmax, convenvInner, Intshconvout,
    Intshconvin, MHeEXcore, R, Dcent, Tcent, Pcent, L, LH, LHe, LC, LGr, LN, LogTe, centH, centHe4, centC12,
    He4Tmax,C12Tmax, MHshbot, RHshbot, TbotHsh, DHshbot, RHshmid, THshmid, DHshmid, MHshtop, RHshtop, THshtop,
    DHshtop,MHeshbot, RHeshbot, THeshbot, DHeshbot, RHeshmid, THeshmid, DHeshmid,
    MHeshtop, RHeshtop, THeshtop, DHeshtop, TIntshzbase, DIntshzbase, centIshconv, TIntshzcent,
    DIntshzcent, TIntshztop, DIntshztop, EpsHmax, TEpsHmax, DEpsmaxH, Tconvenvbase, M, Mdot, Period,
    Vexp,dt, surfH,surfHe3,surfHe4,surfC12,surfN14,surfO16,surfZ,surfCO,centO16,mfracConvCoreVels,MhomogEnv,
    ThomogEnv,DhomogEnv,Lpp,Lcno,mLHmax,mLHemax,iplace;
  close, f;
}



