// Calculating values for plot axes from the plot data

#include "code/yorick/physics_constants.i"

func smpCalculateLogG(f)
{
  return log10(smpG * f.M * smpMSun / (f.R*smpRSun)^2)
}