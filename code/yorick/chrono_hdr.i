/* Plot markers on HR diagram separated by time steps given by `step` */
func smpChronoHrd(f, step)
{
  fma;
  n=numberof(f.L);/* no. of models */
  ct=1

  firstage = int(f.age(1)/step);
  ct = firstage;

  for(i=1;i<=n;++i)
  {
    test = int(f.age(i)/step);

    if(test!=ct){
      plmk,log10(f.L(i)), f.LogTe(i), marker=6,msize=0.5,width=11,color="green";
      ct=test;
    }
  }

  // Axes limits
  limits;
  lims = limits();
  x2 = lims(2)*1.1;
  x1 = lims(1)*0.90;
  y1=lims(3)-0.2;
  y2=lims(4)*1.1;
  if(x2>x1) limits, x2,x1,y1,y2;
  xytitles,"Teff","log_10_(L/L_sun_)";
}

/* Plot markers on HR diagram separated by time steps given by `step` */
func smpChronoHrdLogG(f, step)
{
  fma;
  n=numberof(f.L);/* no. of models */
  ct=1

  firstage = int(f.age(1)/step);
  ct = firstage;
  log_g = smpCalculateLogG(f)

  for(i=1;i<=n;++i)
  {
    test = int(f.age(i)/step);

    if(test!=ct){
      plmk,log_g(i), f.LogTe(i), marker=6,msize=0.5,width=11,color=color2;
      ct=test;
    }
  }

  // Axes limits
  limits;
  lims = limits();
  x2 = lims(2)*1.1;
  x1 = lims(1)*0.90;
  y1=lims(3)-0.2;
  y2=lims(4)*1.1;
  if(x2>x1) limits, x2,x1,y1,y2;
  xytitles,"Teff","log_10_(g)";
}