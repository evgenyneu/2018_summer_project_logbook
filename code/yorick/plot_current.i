/* Functions for plotting currently evolving model */

#include "code/yorick/read_plot.i"
#include "code/yorick/calc_axes_values.i"
#include "code/yorick/limits.i"

currentModelDirDefault = "../monstar_models/model/"

/*
  Plot the currently evolving model with a previos archived model:

  Example:

    compareCurrentModelWithArhchived, archiveXxx="m0.7713z001y26/ev.xxx.zip"

    // Specify current dir
    compareCurrentModelWithArhchived, archiveXxx="m0.7713z001y26/ev.xxx.zip", currentDir="../monstar_models/model2/"
*/
func compareCurrentModelWithArhchived(archiveXxx=, currentDir=)
{
  if (is_void(currentDir)) {
    currentDir = currentModelDirDefault
  }

  ev2 = smpOpenPlotFromMonstarArchive(archiveXxx)
  log_g = smpCalculateLogG(ev2)
  plg, log_g, 10^ev2.LogTe(i), color="white"


  ev = smpOpenPlotFromEvXXX(dir=currentDir)
  log_g = smpCalculateLogG(ev)
  plg, log_g, 10^ev.LogTe(i), color="red", type="dash"

  // Captions and axes
  xytitles,"T [K]","log g [cgs]"
  smpAddPlotMargin, 0.05
  revx
  revy
}


/*
  Calculate the main sequence turnoff age of the currently evolving model.

  Examples:

    calculateTurnoffAgeCurrent

    calculateTurnoffAgeCurrent, currentDir="../monstar_models/model2/"
*/
func calculateTurnoffAgeCurrent(currentDir=)
{
  if (is_void(currentDir)) {
    currentDir = currentModelDirDefault
  }

  ev = smpOpenPlotFromEvXXX(dir=currentDir)
  plg, ev.LogTe, ev.age
  modelMax = 10000
  totalModels = dimsof(ev.LogTe)(2)
  if (totalModels < modelMax) modelMax = totalModels

  // Look for max temperature within the first modelMax models
  age = ev.age(ev.LogTe(1:modelMax)(mxx))
  write, "Age: ", age, " yr"
}